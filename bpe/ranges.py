#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


class Ranges:
    @staticmethod
    def sub(range1, range2):
        ret0 = range1[0]
        ret1 = range1[1]
        # range2 is empty
        if range2[0] > range2[1]:
            return range1
        # same ranges
        if range1 == range2:
            # empty range
            return (1, -1)
        if ret0 > range2[0] and ret1 < range2[1]:
            print("Error: ranges.sub one range inside other range: "
                  "{0}, {1}".format(range1, range2))
            exit(1)
        if range2[0] > ret0 and range2[1] < ret1:
            print("Error: ranges.sub one range inside other range: "
                  "{0}, {1}".format(range1, range2))
            exit(1)
        if range2[0] == range2[1]:
            # .....
            #     .
            if ret1 == range2[0]:
                ret1 = range2[0] - 1
            # .....
            # .
            elif ret0 == range2[0]:
                ret0 = range2[0] + 1
            else:
                print("Error: unsupported ranges sub: "
                      "{0}, {1}".format(range1, range2))
                exit(1)
        # -----
        #   -----
        elif ret0 < range2[0] and range2[0] < ret1:
            ret1 = range2[0] - 1
        #   -----
        # -----
        elif ret1 > range2[1]:
            ret0 = range2[1] + 1
        else:
            print("Error: unsupported ranges sub: "
                  "{0}, {1}".format(range1, range2))
            exit(1)
        return (ret0, ret1)
