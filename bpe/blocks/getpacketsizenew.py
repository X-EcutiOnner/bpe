#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


blocks = [
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"  # 0  push packetId
            b"\xE8\xAB\xAB\xAB\xAB"  # 5  call CRagConnection::instanceR
            b"\x8B\xC8"              # 10 mov ecx, eax
            b"\xE8"                  # 12 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 12,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 6,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xBF\xAB\xAB\x00\x00"          # 0  mov edi, packetId
            b"\x8D\xAB\x00"                  # 5  lea ecx, [ecx+0]
            b"\xAB"                          # 8  push esi
            b"\x6A\x08"                      # 9  push 8
            b"\x8D\x85\xAB\xAB\xAB\xAB"      # 11 lea eax, [ebp+var_95C]
            b"\x50"                          # 17 push eax
            b"\xB9\xAB\xAB\xAB\xAB"          # 18 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"          # 23 call func1
            b"\xC7\xAB\xAB\xAB\x00\x00\x00"  # 28 mov [ebp+var_4], 9
            b"\xA1\xAB\xAB\xAB\xAB"          # 35 mov eax, cchWideChar
            b"\x8D\x8D\xAB\xAB\xAB\xAB"      # 40 lea  ecx, [ebp+var_95C]
            b"\x66\x89\xAB\xAB\xAB\xAB\xAB"  # 46 mov word ptr [ebp+A], di
            b"\x89\x85\xAB\xAB\xAB\xAB"      # 53 mov [ebp+A+2], eax
            b"\xE8\xAB\xAB\xAB\xAB"          # 59 call func2
            b"\x66\x89\xAB\xAB\xAB\xAB\xAB"  # 64 mov word ptr [ebp+A+6], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"      # 71 lea eax, [ebp+A]
            b"\x50"                          # 77 push eax
            b"\x0F\xAB\xAB\xAB\xAB\xAB\xAB"  # 78 movsx eax, word ptr [ebp+A]
            b"\x50"                          # 85 push eax
            b"\xE8\xAB\xAB\xAB\xAB"          # 86 call CRagConnection::instance
            b"\x8B\xC8"                      # 91 mov ecx, eax
            b"\xE8"                          # 93 call CRag...::GetPacketSize
        ),
        {
            "fixedOffset": 93,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 87,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xB9\xAB\xAB\x00\x00"  # 0  mov ecx, packetId
            b"\x66\xAB\xAB\xAB"      # 5  mov word ptr [ebp+buf], cx
            b"\x8B\xAB"              # 9  mov eax, [eax]
            b"\x89\xAB\xAB"          # 11 mov dword ptr [ebp+buf+2], eax
            b"\x8D\xAB\xAB"          # 14 lea eax, [ebp+buf]
            b"\x50"                  # 17 push eax
            b"\x51"                  # 18 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"  # 19 call CRagConnection::instanceR
            b"\x8B\xC8"              # 24 mov ecx, eax
            b"\xE8"                  # 26 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 20,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xB8\xAB\xAB\x00\x00"          # 0  mov eax, packetId
            b"\x66\xAB\xAB\xAB"              # 5  mov word ptr [ebp+buf], ax
            b"\x0F\xB6\xAB\xAB\xAB\xAB\xAB"  # 9  movzx eax, addr1
            b"\x89\x85\xAB\xAB\xAB\xAB"      # 16 mov [ebp+var_120], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"      # 22 lea eax, [ebp+var_120]
            b"\x50"                          # 28 push eax
            b"\x8D\xAB\xAB\xAB\x00\x00"      # 29 lea ecx, [ebx+0B0h]
            b"\xE8\xAB\xAB\xAB\xAB"          # 35 call func1
            b"\x8B\xAB"                      # 40 mov eax, [eax]
            b"\x89\xAB\xAB"                  # 42 mov dword ptr [ebp+buf+2], ea
            b"\x8D\xAB\xAB"                  # 45 lea eax, [ebp+buf]
            b"\x50"                          # 48 push eax
            b"\x0F\xAB\xAB\xAB"              # 49 movsx eax, word ptr [ebp+buf]
            b"\x50"                          # 53 push eax
            b"\xE8\xAB\xAB\xAB\xAB"          # 54 call CRagConnection::instance
            b"\x8B\xC8"                      # 59 mov ecx, eax
            b"\xE8"                          # 61 call CRagCon...::GetPacketSiz
        ),
        {
            "fixedOffset": 61,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 55,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"  # 0  push packetId
            b"\x66\x89\xAB\xAB"      # 5  mov word ptr [ebp+buf+2], cx
            b"\x66\x89\xAB\xAB"      # 9  mov word ptr [ebp+var_8], si
            b"\xE8\xAB\xAB\xAB\xAB"  # 13 call CRagConnection::instanceR
            b"\x8B\xC8"              # 18 mov ecx, eax
            b"\xE8"                  # 20 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 20,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 14,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"              # 0  push packetId
            b"\xC7\x85\xAB\xAB\xFF\xFF\xAB\x00\x00\x00"  # 5  mov [ebp+var1], 0
            b"\xC7\x85\xAB\xAB\xFF\xFF\xAB\x00\x00\x00"  # 15 mov [ebp+var2], 0
            b"\xC6\xAB\xAB\xAB\xFF\xFF\xAB"      # 25 mov byte ptr [ebp+var3],
            b"\xE8\xAB\xAB\xAB\xAB"              # 32 call ::instanceR
            b"\x8B\xC8"                          # 37 mov ecx, eax
            b"\xE8"                              # 39 call ::GetPacketSize
        ),
        {
            "fixedOffset": 39,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 33,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"          # 0  push packetId
            b"\x66\x89\xAB\xAB\xAB\xFF\xFF"  # 5  mov word ptr [ebp+Src], bx
            b"\xE8\xAB\xAB\xAB\xAB"          # 12 call CRagConnection::instance
            b"\x8B\xC8"                      # 17 mov ecx, eax
            b"\xE8"                          # 19 call CRagCon...::GetPacketSiz
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 13,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"          # 0  push packetId
            b"\x89\xAB\xAB"                  # 5  mov dword ptr [ebp+buf+2], ec
            b"\x0F\x95\xAB\xAB"              # 8  setnz [ebp+var_76]
            b"\xE8\xAB\xAB\xAB\xAB"          # 12 call CRagConnection::instance
            b"\x8B\xC8"                      # 17 mov ecx, eax
            b"\xE8"                          # 19 call CRagCon...::GetPacketSiz
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 13,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"  # 0  push packetId
            b"\xC6\x45\xAB\xAB"      # 5  mov byte ptr [ebp+0Eh], 5
            b"\xE8\xAB\xAB\xAB\xAB"  # 9  call CRagConnection::instanceR
            b"\x8B\xC8"              # 14 mov ecx, eax
            b"\xE8"                  # 16 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 16,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 10,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push packetId
            b"\x89\x8D\xAB\xAB\xAB\xFF"  # 5  mov [ebp+var_10E], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection::instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection::GetPacketSiz
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 12,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"        # 0  push packetId
            b"\x66\x0F\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  movq addr1, xmm0
            b"\xE8\xAB\xAB\xAB\xAB"        # 13 call CRagConnection::instanceR
            b"\x8B\xC8"                    # 18 mov ecx, eax
            b"\xE8"                        # 20 call CRagCon...::GetPacketSize
        ),
        {
            "fixedOffset": 20,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 14,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"  # 0  push packetId
            b"\x89\xAB\xAB"          # 5  mov [ebp+var_52], ebx
            b"\x66\x0F\xAB\xAB\xAB"  # 8  movq [ebp+var_36], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"  # 13 call CRagConnection::instanceR
            b"\x8B\xC8"              # 18 mov ecx, eax
            b"\xE8"                  # 20 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 20,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 14,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"  # 0  push packetId
            b"\x0F\xAB\xAB\xAB"      # 5  setz [ebp+var_7E]
            b"\xE8\xAB\xAB\xAB\xAB"  # 9  call CRagConnection::instanceR
            b"\x8B\xC8"              # 14 mov ecx, eax
            b"\xE8"                  # 16 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 16,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 10,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xB9\xAB\xAB\x00\x00"  # 0  mov ecx, 202h
            b"\xF3\x0F\xAB\x00"      # 5  movq xmm0, qword ptr [eax]
            b"\x66\x0F\xD6\xAB\xAB"  # 9  movq [ebp+var_1E], xmm0
            b"\xF3\x0F\x7E\xAB\xAB"  # 14 movq xmm0, qword ptr [eax+8]
            b"\x66\x0F\xD6\xAB\xAB"  # 19 movq [ebp+var_16], xmm0
            b"\xF3\x0F\x7E\xAB\xAB"  # 24 movq xmm0, qword ptr [eax+10h]
            b"\x8D\x45\xAB"          # 29 lea eax, [ebp+Src]
            b"\x50"                  # 32 push eax
            b"\x51"                  # 33 push ecx
            b"\x66\xAB\xAB\xAB"      # 34 mov [ebp+Src], cx
            b"\x66\x0F\xD6\xAB\xAB"  # 38 movq [ebp+var_E], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"  # 43 call CRagConnection::instanceR
            b"\x8B\xC8"              # 48 mov ecx, eax
            b"\xE8"                  # 50 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 50,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 44,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push packetId
            b"\xC7\x87\xAB\xAB\x00\x00\x00\x00\x00\x00"  # 5  mov [edi+A], 0
            b"\xB3\xAB"                  # 15 mov bl, 1
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection::instanceR
            b"\x8B\xC8"                  # 22 mov ecx, eax
            b"\xE8"                      # 24 call CRagConnection::GetPacketSiz
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 18,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xB8\xAB\xAB\x00\x00"          # 0  mov eax, packetId
            b"\x6A\x10"                      # 5  push 10h
            b"\x66\x89\xAB\xAB"              # 7  mov [ebp+Src], ax
            b"\xE8\xAB\xAB\xAB\xAB"          # 11 call func1
            b"\x50"                          # 16 push eax
            b"\x8D\xAB\xAB"                  # 17 lea  eax, [ebp+Dst]
            b"\x6A\x10"                      # 20 push 10h
            b"\x50"                          # 22 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"      # 23 call ds:memcpy_s
            b"\x66\x8B\x86\xAB\xAB\x00\x00"  # 29 mov ax, [esi+0C4h]
            b"\x66\x89\xAB\xAB"              # 36 mov word ptr [ebp+var14], ax
            b"\x83\xC4\xAB"                  # 40 add esp, 10h
            b"\x8D\x45\xAB"                  # 43 lea eax, [ebp+Src]
            b"\x50"                          # 46 push eax
            b"\x0F\xBF\xAB\xAB"              # 47 movsx eax, [ebp+Src]
            b"\x50"                          # 51 push eax
            b"\xE8\xAB\xAB\xAB\xAB"          # 52 call CRagConnection::instance
            b"\x8B\xC8"                      # 57 mov ecx, eax
            b"\xE8"                          # 59 call CRagCon...::GetPacketSiz
        ),
        {
            "fixedOffset": 59,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 53,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, packetId
            b"\x6A\x10"                  # 5  push 10h
            b"\x66\x89\xAB\xAB"          # 7  mov [ebp+Src], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call func1
            b"\x50"                      # 16 push eax
            b"\x8D\x45\xAB"              # 17 lea eax, [ebp+Dst]
            b"\x6A\x10"                  # 20 push 10h
            b"\x50"                      # 22 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 23 call ds:memcpy_s
            b"\x83\xC4\xAB"              # 29 add esp, 10h
            b"\x8D\x45\xAB"              # 32 lea eax, [ebp+Src]
            b"\x50"                      # 35 push eax
            b"\x0F\xBF\xAB\xAB"          # 36 movsx eax, [ebp+Src]
            b"\x50"                      # 40 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 41 call CRagConnection::instanceR
            b"\x8B\xC8"                  # 46 mov ecx, eax
            b"\xE8"                      # 48 call CRagConnection::GetPacketSiz
        ),
        {
            "fixedOffset": 48,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 42,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, packetId
            b"\x66\x89\xAB\xAB"          # 5  mov [ebp+Src], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 9  call func1
            b"\x50"                      # 14 push eax
            b"\x8D\x45\xAB"              # 15 lea eax, [ebp+Dst]
            b"\x6A\xAB"                  # 18 push 18h
            b"\x50"                      # 20 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 21 call ds:strcpy_s
            b"\x83\xC4\xAB"              # 27 add esp, 0Ch
            b"\x8D\x45\xAB"              # 30 lea eax, [ebp+Src]
            b"\x50"                      # 33 push eax
            b"\x0F\xBF\xAB\xAB"          # 34 movsx eax, [ebp+Src]
            b"\x50"                      # 38 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 39 call CRagConnection::instanceR
            b"\x8B\xC8"                  # 44 mov ecx, eax
            b"\xE8"                      # 46 call CRagConnection::GetPacketSiz
        ),
        {
            "fixedOffset": 46,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 40,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xBF\xAB\xAB\x00\x00"      # 0  mov edi, packetId
            b"\xEB\x08"                  # 5  jmp +8
            b"\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  unused
            b"\xA1\xAB\xAB\xAB\xAB"      # 15 mov eax, cchWideChar
            b"\x8D\x4E\xAB"              # 20 lea ecx, [esi+8]
            b"\x66\x89\xAB\xAB\xAB\xAB\xAB"  # 23 mov word ptr [ebp+var1], di
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 30 mov [ebp+var2], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 36 call func1
            b"\x66\x89\xAB\xAB\xAB\xAB\xAB"  # 41 mov word ptr [ebp+var3], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 48 lea eax, [ebp+var_142C]
            b"\x50"                      # 54 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 55 movsx eax, word ptr [ebp+var3
            b"\x50"                      # 62 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 63 call CRagConnection::instanceR
            b"\x8B\xC8"                  # 68 mov ecx, eax
            b"\xE8"                      # 70 call CRagConnection::GetPacketSiz
        ),
        {
            "fixedOffset": 70,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 64,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, packetId
            b"\xC7\x05\xAB\xAB\xAB\xAB\xFF\xFF\xFF\xFF"  # 5  mov g1, 0FFFFFFFF
            b"\xC7\x05\xAB\xAB\xAB\xAB\xFF\xFF\xFF\xFF"  # 15 mov g2, 0FFFFFFFF
            b"\xC7\x05\xAB\xAB\xAB\xAB\xFF\xFF\xFF\xFF"  # 25 mov g3, 0FFFFFFFF
            b"\x66\x89\xAB\xAB"          # 35 mov [ebp+Src], ax
            b"\x8B\x46\xAB"              # 39 mov eax, [esi+4]
            b"\x89\x45\xAB"              # 42 mov [ebp+var_1A], eax
            b"\x8B\xAB"                  # 45 mov eax, [esi]
            b"\x8D\x4E\xAB"              # 47 lea ecx, [esi+60h]
            b"\x89\x45\xAB"              # 50 mov [ebp+var_16], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call func1
            b"\x66\x89\xAB\xAB"          # 58 mov [ebp+var_12], ax
            b"\x8D\x45\xAB"              # 62 lea eax, [ebp+Src]
            b"\x50"                      # 65 push eax
            b"\x0F\xBF\x45\xAB"          # 66 movsx eax, [ebp+Src]
            b"\x50"                      # 70 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 71 call CRagConnection::instanceR
            b"\x8B\xC8"                  # 76 mov ecx, eax
            b"\xE8"                      # 78 call CRagConnection::GetPacketSiz
        ),
        {
            "fixedOffset": 78,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 72,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 907h
            b"\xFF\x75\xAB"              # 5  push [ebp+arg_0]
            b"\x66\x89\xAB\xAB"          # 8  mov [ebp+Src], ax
            b"\x66\x8B\xAB\xAB"          # 12 mov ax, [edx+4]
            b"\x66\x89\xAB\xAB"          # 16 mov [ebp+var_6], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call sub_5A4CB0
            b"\x83\xF8\xAB"              # 25 cmp eax, 3
            b"\x8D\x45\xAB"              # 28 lea eax, [ebp+Src]
            b"\x50"                      # 31 push eax
            b"\x0F\xBF\xAB\xAB"          # 32 movsx eax, [ebp+Src]
            b"\x50"                      # 36 push eax
            b"\x0F\x95\x45\xAB"          # 37 setnz [ebp+var_4]
            b"\xE8\xAB\xAB\xAB\xAB"      # 41 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 46 mov ecx, eax
            b"\xE8"                      # 48 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 48,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 42,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8DDh
            b"\x6A\xAB"                  # 5  push 18h
            b"\x50"                      # 7  push eax
            b"\x66\x89\xAB\xAB"          # 8  mov word ptr [ebp+buf], cx
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 12 call ds:strcpy_s
            b"\x8A\x45\xAB"              # 18 mov al, [ebp+arg_4]
            b"\x88\x45\xAB"              # 21 mov [ebp+var_1E], al
            b"\x83\xC4\xAB"              # 24 add esp, 0Ch
            b"\x8D\x45\xAB"              # 27 lea eax, [ebp+buf]
            b"\x50"                      # 30 push eax
            b"\x0F\xBF\x45\xAB"          # 31 movsx eax, word ptr [ebp+buf]
            b"\x50"                      # 35 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 36 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 41 mov ecx, eax
            b"\xE8"                      # 43 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 43,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 37,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8E0h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+Src], ax
            b"\x8B\x47\xAB"              # 9  mov eax, [edi+34h]
            b"\x83\xC0\xAB"              # 12 add eax, 4
            b"\x50"                      # 15 push eax
            b"\x8D\x45\xAB"              # 16 lea eax, [ebp+Dst]
            b"\x6A\xAB"                  # 19 push 18h
            b"\x50"                      # 21 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 22 call ds:strcpy_s
            b"\x8D\x47\xAB"              # 28 lea eax, [edi+1Ch]
            b"\x50"                      # 31 push eax
            b"\x8D\x45\xAB"              # 32 lea eax, [ebp+var_1D]
            b"\x6A\xAB"                  # 35 push 18h
            b"\x50"                      # 37 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 38 call ds:strcpy_s
            b"\x8B\x45\xAB"              # 44 mov eax, [ebp+arg_0]
            b"\x83\xC4\xAB"              # 47 add esp, 18h
            b"\x88\x45\xAB"              # 50 mov [ebp+var_36], al
            b"\x83\xF8\xAB"              # 53 cmp eax, 2
            b"\x75\xAB"                  # 56 jnz short loc_8015B1
            b"\xC6\x47\xAB\xAB"          # 58 mov byte ptr [edi+38h], 0
            b"\x8D\x45\xAB"              # 62 lea eax, [ebp+Src]
            b"\x50"                      # 65 push eax
            b"\x0F\xBF\x45\xAB"          # 66 movsx eax, [ebp+Src]
            b"\x50"                      # 70 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 71 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 76 mov ecx, eax
            b"\xE8"                      # 78 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 78,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 72,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xB8\xAB\xAB\xAB\xAB"      # 0  mov eax, 8D7h
            b"\x66\x89\x45\xAB"          # 5  mov word ptr [ebp+buf], ax
            b"\x66\x8B\x45\xAB"          # 9  mov ax, [ebp+arg_0]
            b"\x6A\xAB"                  # 13 push 17h
            b"\x51"                      # 15 push ecx
            b"\x66\x89\x45\xAB"          # 16 mov [ebp+var_1E], ax
            b"\x8D\x45\xAB"              # 20 lea eax, [ebp+Dst]
            b"\x6A\xAB"                  # 23 push 18h
            b"\x50"                      # 25 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 26 call ds:strncpy_s
            b"\x83\xC4\xAB"              # 32 add esp, 10h
            b"\x8D\x45\xAB"              # 35 lea eax, [ebp+buf]
            b"\x50"                      # 38 push eax
            b"\x0F\xBF\x45\xAB"          # 39 movsx eax, word ptr [ebp+buf]
            b"\x50"                      # 43 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 44 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 49 mov ecx, eax
            b"\xE8"                      # 51 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 51,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 45,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8DAh
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+Src], ax
            b"\x8B\x41\xAB"              # 9  mov eax, [ecx+34h]
            b"\x83\xC0\xAB"              # 12 add eax, 4
            b"\x50"                      # 15 push eax
            b"\x8D\x45\xAB"              # 16 lea eax, [ebp+Dst]
            b"\x6A\xAB"                  # 19 push 18h
            b"\x50"                      # 21 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 22 call ds:strcpy_s
            b"\x83\xC4\xAB"              # 28 add esp, 0Ch
            b"\x8D\x45\xAB"              # 31 lea eax, [ebp+Src]
            b"\x50"                      # 34 push eax
            b"\x0F\xBF\x45\xAB"          # 35 movsx eax, [ebp+Src]
            b"\x50"                      # 39 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 40 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 45 mov ecx, eax
            b"\xE8"                      # 47 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 47,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 41,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0D0h
            b"\xC7\x83\xAB\xAB\x00\x00\xAB\x00\x00\x00"  # 5  mov [ebx+A], 1
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 15 mov byte ptr [ebp+B], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 27 mov ecx, eax
            b"\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x50"                      # 0  push eax
            b"\x68\xAB\xAB\x00\x00"      # 1  push 368h
            b"\x89\x7D\xAB"              # 6  mov [ebp+var_6], edi
            b"\xE8\xAB\xAB\xAB\xAB"      # 9  call CRagConnection_instanceR
            b"\x8B\xC8"                  # 14 mov ecx, eax
            b"\xE8"                      # 16 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 16,
            "packetId": 2,
            "retOffset": 1,
        },
        {
            "instanceR": 10,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 360h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+Src], ax
            b"\xFF\xD6"                  # 9  call esi
            b"\x89\x45\xAB"              # 11 mov [ebp+var_6], eax
            b"\x8D\x45\xAB"              # 14 lea eax, [ebp+Src]
            b"\x50"                      # 17 push eax
            b"\x0F\xBF\x45\xAB"          # 18 movsx eax, [ebp+Src]
            b"\x50"                      # 22 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 28 mov ecx, eax
            b"\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 998h
            b"\x66\x89\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp-0DF9Eh], bx
            b"\x89\xB5\xAB\xAB\xAB\xAB"  # 12 mov [ebp-0DF9Ch], esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 23 mov ecx, eax
            b"\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 363h
            b"\x66\x89\x9D\xAB\xAB\xAB\xAB"  # 5  mov [ebp-0DE52h], bx
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 12 mov [ebp-0DE50h], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 24 mov ecx, eax
            b"\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0E4h
            b"\x89\x9D\xAB\xAB\xAB\xAB"  # 5  mov [ebp-0DF2Ah], ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 0A99h
            b"\x50"                      # 5  push eax
            b"\x51"                      # 6  push ecx
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 7  mov [ebp-0DDECh], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 436h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+Src], ax
            b"\xFF\xD3"                  # 9  call ebx
            b"\x89\x45\xAB"              # 11 mov [ebp+var_2E], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 14 mov eax, cchWideChar
            b"\x89\x45\xAB"              # 19 mov [ebp+var_3A], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 22 mov eax, dword_100355C
            b"\x89\x45\xAB"              # 27 mov [ebp+var_36], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 30 mov eax, dword_1002E1C
            b"\xB9\xAB\xAB\xAB\xAB"      # 35 mov ecx, offset g_session
            b"\x89\x45\xAB"              # 40 mov [ebp+var_32], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 43 call sub_A27B60
            b"\x88\x45\xAB"              # 48 mov [ebp+var_2A], al
            b"\x8D\x45\xAB"              # 51 lea eax, [ebp+Src]
            b"\x50"                      # 54 push eax
            b"\x0F\xBF\xAB\xAB"          # 55 movsx eax, [ebp+Src]
            b"\x50"                      # 59 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 60 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 65 mov ecx, eax
            b"\xE8"                      # 67 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 67,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 61,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x6A\xAB"                  # 0  push 65h
            b"\xC7\x05\xAB\xAB\xAB\xAB\xAB\x00\x00\x00"  # 2  mov A, 1
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2018-05-09
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 436h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var2], ax
            b"\xFF\xD7"                  # 12 call edi
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 14 mov dword ptr [ebp+var_2CC+2], ea
            b"\xA1\xAB\xAB\xAB\xAB"      # 20 mov eax, cchWideChar
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 25 mov dword ptr [ebp+var_2DC+6], ea
            b"\xA1\xAB\xAB\xAB\xAB"      # 31 mov eax, dword_100355C
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 36 mov dword ptr [ebp+var_2D4+2], ea
            b"\xA1\xAB\xAB\xAB\xAB"      # 42 mov eax, dword_1002E1C
            b"\xB9\xAB\xAB\xAB\xAB"      # 47 mov ecx, offset g_session
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 52 mov dword ptr [ebp+var_2D4+6], ea
            b"\xE8\xAB\xAB\xAB\xAB"      # 58 call sub_A27B60
            b"\x88\x85\xAB\xAB\xAB\xAB"  # 63 mov [ebp+var_2CC+6], al
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 69 lea eax, [ebp+var_2DC+4]
            b"\x50"                      # 75 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 76 movsx eax, word ptr [ebp+var1
            b"\x50"                      # 83 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 84 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 89 mov ecx, eax
            b"\xE8"                      # 91 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 91,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 85,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 233h
            b"\x89\x5D\xAB"              # 5  mov dword ptr [ebp+Src+2], ebx
            b"\x89\x75\xAB"              # 8  mov dword ptr [ebp+Src+6], esi
            b"\xC6\x45\xAB\x00"          # 11 mov [ebp+var_6], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 20 mov ecx, eax
            b"\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 233h
            b"\x89\x5D\xAB"              # 5  mov dword ptr [ebp+Src+6], ebx
            b"\xC6\x45\xAB\x00"          # 8  mov [ebp+var_6], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 232h
            b"\x89\x7D\xAB"              # 5  mov [ebp+var_E], edi
            b"\x88\x4D\xAB"              # 8  mov [ebp+var_8], cl
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 234h
            b"\x89\x75\xAB"              # 5  mov [ebp+var_12], esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 8  call CRagConnection_instanceR
            b"\x8B\xC8"                  # 13 mov ecx, eax
            b"\xE8"                      # 15 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 15,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 9,
        }
    ],
    # 2018-05-09
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 2D8h
            b"\xC7\x45\xAB\xAB\x00\x00\x00"  # 5  mov [ebp+var_E], 3
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 827h
            b"\x89\x45\xAB"              # 5  mov [ebp+var_6], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 8  call CRagConnection_instanceR
            b"\x8B\xC8"                  # 13 mov ecx, eax
            b"\xE8"                      # 15 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 15,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 9,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_8], 82Bh
            b"\x89\x55\xAB"              # 6  mov [ebp+var_10], edx
            b"\xC7\x45\xAB\xAB\xAB\x00\x00"  # 9  mov [ebp+var_C], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 16 call sub_4191D0
            b"\x8B\xD0"                  # 21 mov edx, eax
            b"\x8B\x02"                  # 23 mov eax, [edx]
            b"\x8B\x4A\xAB"              # 25 mov ecx, [edx+4]
            b"\x8B\x50\xAB"              # 28 mov edx, [eax+10h]
            b"\x89\x4D\xAB"              # 31 mov [ebp+var_1C], ecx
            b"\x8D\x45\xAB"              # 34 lea eax, [ebp+var_8]
            b"\x0F\xBF\xAB\xAB"          # 37 movsx ecx, [ebp+var_8]
            b"\x50"                      # 41 push eax
            b"\x51"                      # 42 push ecx
            b"\x89\x55\xAB"              # 43 mov [ebp+var_6], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 46 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 51 mov ecx, eax
            b"\xE8"                      # 53 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 53,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 47,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 28Ch
            b"\xF3\xA5"                  # 5  rep movsd
            b"\xE8\xAB\xAB\xAB\xAB"      # 7  call CRagConnection_instanceR
            b"\x8B\xC8"                  # 12 mov ecx, eax
            b"\xE8"                      # 14 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 14,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 8,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 802h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov [ebp+var_34], 802h
            b"\x89\x4D\xAB"              # 11 mov [ebp+var_2A], ecx
            b"\x89\x55\xAB"              # 14 mov [ebp+var_26], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 22 mov ecx, eax
            b"\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_18], 83Ch
            b"\x89\x4D\xAB"              # 6  mov [ebp+var_16], ecx
            b"\x89\x55\xAB"              # 9  mov [ebp+var_12], edx
            b"\x75\x05"                  # 12 jnz short loc_4BCDDB
            b"\xB8\xAB\xAB\xAB\xAB"      # 14 mov eax, offset byte_7397F4
            b"\x50"                      # 19 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call _atoi
            b"\x0F\xBF\xAB\xAB"          # 25 movsx ecx, [ebp+var_18]
            b"\x83\xC4\xAB"              # 29 add esp, 4
            b"\x66\x89\x45\xAB"          # 32 mov [ebp+var_E], ax
            b"\x8D\x45\xAB"              # 36 lea eax, [ebp+var_18]
            b"\x50"                      # 39 push eax
            b"\x51"                      # 40 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 41 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 46 mov ecx, eax
            b"\xE8"                      # 48 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 48,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 42,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 838h
            b"\x89\xBB\xAB\xAB\x00\x00"  # 5  mov [ebx+88h], edi
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 11 mov word ptr [ebp+arg_8+2], 838h
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 22 mov ecx, eax
            b"\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 2B6h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov [ebp+var_8], 2B6h
            b"\x89\x55\xAB"              # 11 mov [ebp+var_6], edx
            b"\x88\x45\xAB"              # 14 mov [ebp+var_2], al
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 22 mov ecx, eax
            b"\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x6A\xAB"                  # 0  push 7Dh
            b"\xE8\xAB\xAB\xAB\xAB"      # 2  call CRagConnection_instanceR
            b"\x8B\xC8"                  # 7  mov ecx, eax
            b"\xE8"                      # 9  call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 9,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 3,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_18], 89h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            b"\xFF\xD6"                  # 11 call esi
            b"\x0F\xBF\xAB\xAB"          # 13 movsx edx, [ebp+var_18]
            b"\x8D\x4D\xAB"              # 17 lea ecx, [ebp+var_18]
            b"\x89\x45\xAB"              # 20 mov [ebp+var_14+3], eax
            b"\x51"                      # 23 push ecx
            b"\x52"                      # 24 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 25 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 30 mov ecx, eax
            b"\xE8"                      # 32 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 32,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 26,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_14], 89h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            b"\xFF\xD3"                  # 11 call ebx
            b"\x0F\xBF\xAB\xAB"          # 13 movsx ecx, [ebp+var_14]
            b"\x89\x45\xAB"              # 17 mov [ebp+var_D], eax
            b"\x8D\x45\xAB"              # 20 lea eax, [ebp+var_14]
            b"\x50"                      # 23 push eax
            b"\x51"                      # 24 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 25 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 30 mov ecx, eax
            b"\xE8"                      # 32 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 32,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 26,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_C], 89h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            b"\xFF\xD7"                  # 11 call edi
            b"\x0F\xBF\xAB\xAB"          # 13 movsx ecx, [ebp+var_C]
            b"\x89\x45\xAB"              # 17 mov [ebp+var_5], eax
            b"\x8D\x45\xAB"              # 20 lea eax, [ebp+var_C]
            b"\x50"                      # 23 push eax
            b"\x51"                      # 24 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 25 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 30 mov ecx, eax
            b"\xE8"                      # 32 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 32,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 26,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_38], 8Ch
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            b"\x0F\xBF\xAB\xAB"          # 11 movsx ecx, [ebp+var_38]
            b"\x8B\x55\xAB"              # 15 mov edx, [ebp+arg_0]
            b"\x8D\x45\xAB"              # 18 lea eax, [ebp+var_38]
            b"\x50"                      # 21 push eax
            b"\x51"                      # 22 push ecx
            b"\x89\x55\xAB"              # 23 mov [ebp+var_2E], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 26 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 31 mov ecx, eax
            b"\xE8"                      # 33 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 33,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 27,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_40], 0A2h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            b"\x0F\xBF\xAB\xAB"          # 11 movsx edx, [ebp+var_40]
            b"\x8B\x45\xAB"              # 15 mov eax, [ebp+var_10]
            b"\x8D\x4D\xAB"              # 18 lea ecx, [ebp+var_40]
            b"\x51"                      # 21 push ecx
            b"\x52"                      # 22 push edx
            b"\x89\x45\xAB"              # 23 mov [ebp+var_36], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 26 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 31 mov ecx, eax
            b"\xE8"                      # 33 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 33,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 27,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_28], 0A2h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            b"\x0F\xBF\x55\xAB"          # 11 movsx edx, [ebp+var_28]
            b"\x8D\x4D\xAB"              # 15 lea ecx, [ebp+var_28]
            b"\x89\x7D\xAB"              # 18 mov [ebp+var_1E], edi
            b"\x51"                      # 21 push ecx
            b"\x52"                      # 22 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 28 mov ecx, eax
            b"\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 90h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov [ebp+var_10], 90h
            b"\x89\x45\xAB"              # 11 mov [ebp+var_E], eax
            b"\xC6\x45\xAB\xAB"          # 14 mov [ebp+var_A], 1
            b"\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 23 mov ecx, eax
            b"\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0D0h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+arg_0], 0D0h
            b"\xC6\x45\xAB\xAB"          # 11 mov byte ptr [ebp+arg_0+2], 1
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 20 mov ecx, eax
            b"\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 20Fh
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_1C], 20Fh
            b"\x89\x45\xAB"              # 11 mov [ebp+var_18+2], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 2C8h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+arg_0], 2C8h
            b"\x88\x4D\xAB"              # 11 mov byte ptr [ebp+arg_0+2], cl
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0BFh
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+arg_8], 0BFh
            b"\x88\x55\xAB"              # 11 mov byte ptr [ebp+arg_8+2], dl
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0B2h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+arg_8], 0B2h
            b"\x88\x45\xAB"              # 11 mov byte ptr [ebp+arg_8+2], al
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 2D6h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_14], 2D6h
            b"\x89\x55\xAB"              # 11 mov [ebp+var_14+2], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 7F5h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_14], 7F5h
            b"\x89\x4D\xAB"              # 11 mov [ebp+var_14+2], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0ABh
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+arg_4], 0ABh
            b"\x66\x89\x45\xAB"          # 11 mov word ptr [ebp+arg_4+2], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 20 mov ecx, eax
            b"\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0B8h
            b"\x88\x55\xAB"              # 5  mov byte ptr [ebp+var_10+2], dl
            b"\x89\x45\xAB"              # 8  mov [ebp+var_14+2], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0A9h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_14], 0A9h
            b"\x66\x89\x55\xAB"          # 11 mov word ptr [ebp+var_14+2], dx
            b"\x66\x89\x45\xAB"          # 15 mov word ptr [ebp+var_10], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 24 mov ecx, eax
            b"\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0A9h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_14], 0A9h
            b"\x66\x89\x7D\xAB"          # 11 mov word ptr [ebp+var_14+2], di
            b"\x66\x89\x75\xAB"          # 15 mov word ptr [ebp+var_10], si
            b"\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 24 mov ecx, eax
            b"\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_2C], 116h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            b"\x8D\x45\xAB"              # 11 lea eax, [ebp+var_28+4]
            b"\x6A\xAB"                  # 14 push 7
            b"\x50"                      # 16 push eax
            b"\x8B\xCB"                  # 17 mov ecx, ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 19 call sub_5AA240
            b"\x0F\xBF\x45\xAB"          # 24 movsx eax, [ebp+var_2C]
            b"\x66\x8B\x4D\xAB"          # 28 mov cx, word ptr [ebp+arg_8]
            b"\x8D\x55\xAB"              # 32 lea edx, [ebp+var_2C]
            b"\x52"                      # 35 push edx
            b"\x50"                      # 36 push eax
            b"\x66\x89\x75\xAB"          # 37 mov word ptr [ebp+var_28+2], si
            b"\x66\x89\x4D\xAB"          # 41 mov word ptr [ebp+var_28+0Bh], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 45 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 50 mov ecx, eax
            b"\xE8"                      # 52 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 52,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 46,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_18+4], 439h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_599B90
            b"\x0F\xBF\x55\xAB"          # 11 movsx edx, word ptr [ebp+var_18+4
            b"\x8D\x4D\xAB"              # 15 lea ecx, [ebp+var_18+4]
            b"\x89\x45\xAB"              # 18 mov [ebp-10h], eax
            b"\x51"                      # 21 push ecx
            b"\x52"                      # 22 push edx
            b"\x66\x89\xAB\xAB"          # 23 mov word ptr [ebp+var_18+6], di
            b"\xE8\xAB\xAB\xAB\xAB"      # 27 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 32 mov ecx, eax
            b"\xE8"                      # 34 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 34,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 28,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 90h
            b"\x88\x55\xAB"              # 5  mov [ebp-0Eh], dl
            b"\xE8\xAB\xAB\xAB\xAB"      # 8  call CRagConnection_instanceR
            b"\x8B\xC8"                  # 13 mov ecx, eax
            b"\xE8"                      # 15 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 15,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 9,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0E0h
            b"\xF3\xA4"                  # 5  rep movsb
            b"\xB9\xAB\xAB\x00\x00"      # 7  mov ecx, 6
            b"\x8D\xB5\xAB\xAB\xAB\xAB"  # 12 lea esi, [ebp+var_274]
            b"\x8D\x7D\xAB"              # 18 lea edi, [ebp+var_34+2]
            b"\xF3\xA5"                  # 21 rep movsd
            b"\x8B\x4D\xAB"              # 23 mov ecx, [ebp+arg_8]
            b"\x89\x4D\xAB"              # 26 mov [ebp+var_38+2], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 29 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 34 mov ecx, eax
            b"\xE8"                      # 36 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 36,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 30,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0E8h
            b"\xC7\x83\xAB\xAB\xAB\x00\xAB\xAB\x00\x00"  # 5  mov [ebx+0ECh], 1
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 15 mov word ptr [ebp+var_18+4], 0E8h
            b"\x66\x89\xAB\xAB"          # 21 mov word ptr [ebp+var_18+6], si
            b"\x89\x45\xAB"              # 25 mov [ebp-10h], eax
            b"\x89\x83\xAB\xAB\xAB\xAB"  # 28 mov [ebx+0FCh], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 34 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 39 mov ecx, eax
            b"\xE8"                      # 41 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 41,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 35,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 94h
            b"\x89\x55\xAB"              # 5  mov ptr [ebp+var_28+0Bh], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 8  call CRagConnection_instanceR
            b"\x8B\xC8"                  # 13 mov ecx, eax
            b"\xE8"                      # 15 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 15,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 9,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0F7h
            b"\x89\x4D\xAB"              # 5  mov dword ptr [ebp+var_28+9], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 8  call CRagConnection_instanceR
            b"\x8B\xC8"                  # 13 mov ecx, eax
            b"\xE8"                      # 15 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 15,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 9,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 126h
            b"\x66\x89\xAB\xAB"          # 5  mov word ptr [ebp+var_18+6], cx
            b"\x89\x55\xAB"              # 9  mov [ebp-10h], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 2C7h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_18+4], 2C7h
            b"\x88\x45\xAB"              # 11 mov [ebp-0Eh], al
            b"\x89\x4D\xAB"              # 14 mov dword ptr [ebp+var_18+6], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 22 mov ecx, eax
            b"\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 1E3h
            b"\x89\x4D\xAB"              # 5  mov dword ptr [ebp+var_28+2], ecx
            b"\x89\x55\xAB"              # 8  mov [ebp+var_28+0Ah], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 1F7h
            b"\x89\x55\xAB"              # 5  mov dword ptr [ebp+var1+2], edx
            b"\x89\x45\xAB"              # 8  mov dword ptr [ebp+var1+0Ah], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 103h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_4], 2Bh
            b"\x89\x45\xAB"              # 12 mov [ebp+var_38+2], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 20 mov ecx, eax
            b"\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 112h
            b"\xC7\x83\xAB\xAB\xAB\x00\xAB\xAB\x00\x00"  # 5  mov [ebx+0F8h], 1
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 15 mov word ptr [ebp+arg_8], 112h
            b"\x66\x89\x4D\xAB"          # 21 mov word ptr [ebp+arg_8+2], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 25 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 30 mov ecx, eax
            b"\xE8"                      # 32 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 32,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 26,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 438h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_18], 438h
            b"\x66\x89\xAB\xAB"          # 11 mov word ptr [ebp+var_18+2], si
            b"\x66\x89\xAB\xAB"          # 15 mov word ptr [ebp+var_18+4], di
            b"\x89\x45\xAB"              # 19 mov dword ptr [ebp+var_18+6], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 27 mov ecx, eax
            b"\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 113h
            b"\x66\x89\x7D\xAB"          # 5  mov word ptr [ebp+var_28+2], di
            b"\x66\x89\x45\xAB"          # 9  mov word ptr [ebp+var_28+6], ax
            b"\x66\x89\x4D\xAB"          # 13 mov word ptr [ebp+var_28+0Bh], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 22 mov ecx, eax
            b"\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 130h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_18+4], 130h
            b"\x89\x7D\xAB"              # 11 mov dword ptr [ebp+var_18+6], edi
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 143h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_18], 143h
            b"\x89\x75\xAB"              # 11 mov dword ptr [ebp+var_18+2], esi
            b"\x89\x55\xAB"              # 14 mov dword ptr [ebp+var_18+6], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 22 mov ecx, eax
            b"\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 16Eh
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_163], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 178h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+arg_8], 178h
            b"\x66\x89\x4D\xAB"          # 11 mov word ptr [ebp+arg_8+2], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 20 mov ecx, eax
            b"\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 17Ah
            b"\x89\x83\xAB\xAB\xAB\xAB"  # 5  mov [ebx+41Ch], eax
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 11 mov word ptr [ebp+arg_4], 17Ah
            b"\x66\x89\x45\xAB"          # 17 mov word ptr [ebp+arg_4+2], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 26 mov ecx, eax
            b"\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 18Ah
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+arg_4+2], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_18], 0A7h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            b"\x8B\x4D\xAB"              # 11 mov ecx, [ebp+arg_8]
            b"\x8B\x55\xAB"              # 14 mov edx, [ebp+arg_4]
            b"\x8D\x45\xAB"              # 17 lea eax, [ebp+var_18+6]
            b"\x50"                      # 20 push eax
            b"\x6A\xAB"                  # 21 push 0
            b"\x51"                      # 23 push ecx
            b"\x52"                      # 24 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 25 call sub_5C6820
            b"\x0F\xBF\xAB\xAB"          # 30 movsx ecx, word ptr [ebp+var_18]
            b"\x83\xC4\xAB"              # 34 add esp, 10h
            b"\x8D\x45\xAB"              # 37 lea eax, [ebp+var_18]
            b"\x50"                      # 40 push eax
            b"\x51"                      # 41 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 42 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 47 mov ecx, eax
            b"\xE8"                      # 49 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 49,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 43,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_18], 85h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            b"\x8D\x45\xAB"              # 11 lea eax, [ebp+var_18+6]
            b"\x6A\xAB"                  # 14 push 3
            b"\x50"                      # 16 push eax
            b"\x8B\xCB"                  # 17 mov ecx, ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 19 call sub_5AA240
            b"\x8A\x4D\xAB"              # 24 mov cl, byte ptr [ebp+arg_4]
            b"\x66\x8B\xAB\xAB"          # 27 mov dx, word ptr [ebp+arg_8]
            b"\x88\x4D\xAB"              # 31 mov [ebp-0Fh], cl
            b"\x8D\x45\xAB"              # 34 lea eax, [ebp+var_18]
            b"\x0F\xBF\x4D\xAB"          # 37 movsx ecx, word ptr [ebp+var_18]
            b"\x50"                      # 41 push eax
            b"\x51"                      # 42 push ecx
            b"\x66\x89\x55\xAB"          # 43 mov word ptr [ebp+var_18+4], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 47 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 52 mov ecx, eax
            b"\xE8"                      # 54 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 54,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 48,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 198h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_18+4], 198h
            b"\x66\x89\x45\xAB"          # 11 mov [ebp-10h], ax
            b"\x66\x89\x4D\xAB"          # 15 mov [ebp-0Eh], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 24 mov ecx, eax
            b"\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 1A8h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+arg_8], 1A8h
            b"\x66\x89\x55\xAB"          # 11 mov word ptr [ebp+arg_8+2], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 20 mov ecx, eax
            b"\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 208h
            b"\x89\x45\xAB"              # 5  mov dword ptr [ebp+var1+2], eax
            b"\x89\x4D\xAB"              # 8  mov dword ptr [ebp+var1+6], ecx
            b"\x89\x75\xAB"              # 11 mov dword ptr [ebp+var1+0Ah], esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 203h
            b"\x89\x45\xAB"              # 5  mov dword ptr [ebp+var_18+2], eax
            b"\x89\x4D\xAB"              # 8  mov dword ptr [ebp+var_18+6], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 22Dh
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_18+4], 22Dh
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 11 mov word ptr [ebp+var_18+6], 0
            b"\x88\x45\xAB"              # 17 mov [ebp-10h], al
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 25 mov ecx, eax
            b"\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_34], 231h
            b"\xF2\xAE"                  # 6  repne scasb
            b"\xF7\xD1"                  # 8  not ecx
            b"\x49"                      # 10 dec ecx
            b"\x83\xF9\xAB"              # 11 cmp ecx, 18h
            b"\x7D\x14"                  # 14 jge short loc_5C5715
            b"\x41"                      # 16 inc ecx
            b"\x8D\x7D\xAB"              # 17 lea edi, [ebp+var_34+2]
            b"\x8B\xD1"                  # 20 mov edx, ecx
            b"\xC1\xE9\xAB"              # 22 shr ecx, 2
            b"\xF3\xA5"                  # 25 rep movsd
            b"\x8B\xCA"                  # 27 mov ecx, edx
            b"\x83\xE1\xAB"              # 29 and ecx, 3
            b"\xF3\xA4"                  # 32 rep movsb
            b"\xEB\x0E"                  # 34 jmp short loc_5C5723
            b"\xB9\xAB\xAB\xAB\x00"      # 36 mov ecx, 6
            b"\x8D\x7D\xAB"              # 41 lea edi, [ebp+var_34+2]
            b"\xF3\xA5"                  # 44 rep movsd
            b"\xC6\x45\xAB\xAB"          # 46 mov [ebp+var_28+0Dh], 0
            b"\x0F\xBF\x4D\xAB"          # 50 movsx ecx, word ptr [ebp+var_34]
            b"\x8D\x45\xAB"              # 54 lea eax, [ebp+var_34]
            b"\x50"                      # 57 push eax
            b"\x51"                      # 58 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 59 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 64 mov ecx, eax
            b"\xE8"                      # 66 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 66,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 60,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 24Dh
            b"\x89\x55\xAB"              # 5  mov dword ptr [ebp+var_18+6], edx
            b"\x66\x89\x45\xAB"          # 8  mov [ebp-0Eh], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 2BAh
            b"\x88\x4D\xAB"              # 5  mov [ebp-0Eh], cl
            b"\xE8\xAB\xAB\xAB\xAB"      # 8  call CRagConnection_instanceR
            b"\x8B\xC8"                  # 13 mov ecx, eax
            b"\xE8"                      # 15 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 15,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 9,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 2D8h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_18], 2D8h
            b"\xC7\x45\xAB\xAB\xAB\x00\x00"  # 11 mov dword ptr [ebp+var1], 0
            b"\x89\x45\xAB"              # 18 mov dword ptr [ebp+var2], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 26 mov ecx, eax
            b"\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 247h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_18+4], 247h
            b"\x89\x45\xAB"              # 11 mov [ebp-10h], eax
            b"\xA3\xAB\xAB\xAB\xAB"      # 14 mov dword_83D0F0, eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 24 mov ecx, eax
            b"\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 7D7h
            b"\x88\x45\xAB"              # 5  mov [ebp-0Eh], al
            b"\x88\x4D\xAB"              # 8  mov [ebp-0Dh], cl
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 149h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov [ebp+var_C], 149h
            b"\x89\x45\xAB"              # 11 mov [ebp+var_A], eax
            b"\xC6\x45\xAB\xAB"          # 14 mov [ebp+var_6], 2
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 18 mov [ebp+var_5], 0Ah
            b"\xE8\xAB\xAB\xAB\xAB"      # 24 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 29 mov ecx, eax
            b"\xE8"                      # 31 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 31,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 25,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_4C], 436h
            b"\xFF\xD6"                  # 6  call esi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 8  mov ecx, dword_83B6A0
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 14 mov edx, dword_83AFD0
            b"\x89\x45\xAB"              # 20 mov [ebp+var_3E], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 23 mov eax, dword_83B69C
            b"\x89\x4D\xAB"              # 28 mov [ebp+var_46], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 31 mov ecx, offset dword_839C18
            b"\x89\x45\xAB"              # 36 mov [ebp+var_4A], eax
            b"\x89\x55\xAB"              # 39 mov [ebp+var_42], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 42 call sub_6E64A0
            b"\x0F\xBF\xAB\xAB"          # 47 movsx ecx, [ebp+var_4C]
            b"\x88\x45\xAB"              # 51 mov [ebp+var_3A], al
            b"\x8D\x45\xAB"              # 54 lea eax, [ebp+var_4C]
            b"\x50"                      # 57 push eax
            b"\x51"                      # 58 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 59 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 64 mov ecx, eax
            b"\xE8"                      # 66 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 66,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 60,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x6A\xAB"                  # 0  push 67h
            b"\x66\x89\x45\xAB"          # 2  mov [ebp+var_33], ax
            b"\x66\x89\x4D\xAB"          # 6  mov [ebp+var_31], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 10 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 15 mov ecx, eax
            b"\xE8"                      # 17 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 17,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 11,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_64], 1FBh
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call _strncpy
            b"\x0F\xBF\x55\xAB"          # 11 movsx edx, [ebp+var_64]
            b"\x83\xC4\xAB"              # 15 add esp, 0Ch
            b"\x8D\x4D\xAB"              # 18 lea ecx, [ebp+var_64]
            b"\xC6\x45\xAB\xAB"          # 21 mov [ebp+var_2D], 0
            b"\x51"                      # 25 push ecx
            b"\x52"                      # 26 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 27 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 32 mov ecx, eax
            b"\xE8"                      # 34 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 34,
            "packetId": (4, 2),
            "retOffset": 0,
            "goto": (27, 1)
        },
        {
            "instanceR": 28,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+cp], 436h
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 6  call ds:timeGetTime
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 12 mov ecx, dword_83AFD0
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 18 mov edx, dword_83B69C
            b"\x89\x45\xAB"              # 24 mov dword ptr [ebp+a+8], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 27 mov eax, dword_83B6A0
            b"\x89\x4D\xAB"              # 32 mov dword ptr [ebp+a+4], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 35 mov ecx, offset dword_839C18
            b"\x89\x55\xAB"              # 40 mov [ebp-1Eh], edx
            b"\x89\x45\xAB"              # 43 mov [ebp-1Ah], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 46 call sub_6E64A0
            b"\x88\x45\xAB"              # 51 mov byte ptr [ebp+var_10+2], al
            b"\x8D\x55\xAB"              # 54 lea edx, [ebp+cp]
            b"\x0F\xBF\x45\xAB"          # 57 movsx eax, [ebp+cp]
            b"\x52"                      # 61 push edx
            b"\x50"                      # 62 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 63 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 68 mov ecx, eax
            b"\xE8"                      # 70 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 70,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 64,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 841h
            b"\x88\x45\xAB"              # 5  mov byte ptr [ebp+var_2C+3], al
            b"\xE8\xAB\xAB\xAB\xAB"      # 8  call CRagConnection_instanceR
            b"\x8B\xC8"                  # 13 mov ecx, eax
            b"\xE8"                      # 15 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 15,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 9,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_30], 1DDh
            b"\x89\x55\xAB"              # 6  mov [ebp+var_2E], edx
            b"\xF3\xA5"                  # 9  rep movsd
            b"\x8D\x45\xAB"              # 11 lea eax, [ebp+var_12]
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 14 lea ecx, [ebp+var_94]
            b"\x50"                      # 20 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call sub_41AD80
            b"\xE8\xAB\xAB\xAB\xAB"      # 26 call sub_616BF0
            b"\x0F\xBF\x55\xAB"          # 31 movsx edx, [ebp+var_30]
            b"\x8D\x4D\xAB"              # 35 lea ecx, [ebp+var_30]
            b"\x88\x45\xAB"              # 38 mov [ebp+var_2], al
            b"\x51"                      # 41 push ecx
            b"\x52"                      # 42 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 43 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 48 mov ecx, eax
            b"\xE8"                      # 50 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 50,
            "packetId": (4, 2),
            "retOffset": 0,
            "goto": (43, 4)
        },
        {
            "instanceR": 44,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 232h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov [ebp+var_14], 232h
            b"\x89\x75\xAB"              # 11 mov [ebp+var_12], esi
            b"\x88\x55\xAB"              # 14 mov [ebp+var_D], dl
            b"\x88\x4D\xAB"              # 17 mov [ebp+var_C], cl
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 25 mov ecx, eax
            b"\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2010-08-10
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 233h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov [ebp+var_C], 233h
            b"\x89\x5D\xAB"              # 11 mov [ebp+var_A], ebx
            b"\x89\x45\xAB"              # 14 mov [ebp+var_6], eax
            b"\xC6\x45\xAB\xAB"          # 17 mov [ebp+var_2], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 26 mov ecx, eax
            b"\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A25h
            b"\x52"                      # 5  push edx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x45\xAB"          # 7  mov [ebp+var_8], ax
            b"\x89\x4D\xAB"              # 11 mov [ebp+var_6], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 974h
            b"\x51"                      # 5  push ecx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x45\xAB"          # 7  mov [ebp+var_4], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 0A13h
            b"\x66\x89\x4D\xAB"          # 5  mov [ebp+var_2C], cx
            b"\x8B\x8E\xAB\xAB\xAB\xAB"  # 9  mov ecx, [esi+9Ch]
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call sub_49D900
            b"\x50"                      # 20 push eax
            b"\x8D\x55\xAB"              # 21 lea edx, [ebp+var_2A]
            b"\x6A\xAB"                  # 24 push 18h
            b"\x52"                      # 26 push edx
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 27 call strcpy_s
            b"\x0F\xBF\x4D\xAB"          # 33 movsx ecx, [ebp+var_2C]
            b"\x83\xC4\xAB"              # 37 add esp, 0Ch
            b"\x8D\x45\xAB"              # 40 lea eax, [ebp+var_2C]
            b"\x50"                      # 43 push eax
            b"\x51"                      # 44 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 45 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 50 mov ecx, eax
            b"\xE8"                      # 52 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 52,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 46,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 9E9h
            b"\x50"                      # 5  push eax
            b"\x52"                      # 6  push edx
            b"\x66\x89\x55\xAB"          # 7  mov [ebp+var_44], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2015-06-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 9EEh
            b"\x88\x45\xAB"              # 5  mov byte ptr [ebp+var_1C+2], al
            b"\x89\x7D\xAB"              # 8  mov [ebp+var_1C+3], edi
            b"\x89\x4D\xAB"              # 11 mov [ebp+var_18+3], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 0A21h
            b"\x51"                      # 5  push ecx
            b"\x52"                      # 6  push edx
            b"\x66\x89\x55\xAB"          # 7  mov word ptr [ebp+arg_8], dx
            b"\x88\x45\xAB"              # 11 mov byte ptr [ebp+arg_8+2], al
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 9A7h
            b"\x51"                      # 5  push ecx
            b"\x52"                      # 6  push edx
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 7  mov [ebp+var_114], dx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 14 mov [ebp+var_112], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 25 mov ecx, eax
            b"\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 9A9h
            b"\x50"                      # 5  push eax
            b"\x51"                      # 6  push ecx
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 7  mov [ebp+var_114], cx
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 14 mov [ebp+var_112], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 25 mov ecx, eax
            b"\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 827h
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_12C], cx
            b"\x8B\x10"                  # 12 mov edx, [eax]
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 14 lea eax, [ebp+var_12C]
            b"\x50"                      # 20 push eax
            b"\x51"                      # 21 push ecx
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 22 mov [ebp+var_12A], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 28 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 33 mov ecx, eax
            b"\xE8"                      # 35 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 35,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 29,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 82Bh
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 5  lea eax, [ebp+var_124]
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 11 mov [ebp+var_12C], cx
            b"\x50"                      # 18 push eax
            b"\x8D\x8E\xAB\xAB\xAB\x00"  # 19 lea ecx, [esi+110h]
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 25 mov [ebp+var_124], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 31 call sub_495490
            b"\x8B\x08"                  # 36 mov ecx, [eax]
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 38 movsx eax, [ebp+var_12C]
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 45 lea edx, [ebp+var_12C]
            b"\x52"                      # 51 push edx
            b"\x50"                      # 52 push eax
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 53 mov [ebp+var_12A], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 59 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 64 mov ecx, eax
            b"\xE8"                      # 66 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 66,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 60,
        }
    ],
    # 2015-06-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 8D4h
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var1], dx
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 12 mov word ptr [ebp+var1+2], di
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 19 mov [ebp+var_126], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 26 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 31 mov ecx, eax
            b"\xE8"                      # 33 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 33,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 27,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 844h
            b"\x52"                      # 5  push edx
            b"\x51"                      # 6  push ecx
            b"\x66\x89\x4D\xAB"          # 7  mov word ptr [ebp+arg_8], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2015-06-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 998h
            b"\x66\x89\x55\xAB"          # 5  mov [ebp+var_32], dx
            b"\x89\x45\xAB"              # 9  mov [ebp+var_30], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 84Ah
            b"\x51"                      # 5  push ecx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 7  mov word ptr [ebp+var], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 9C3h
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var1], cx
            b"\x8D\x4F\xAB"              # 12 lea ecx, [edi+8]
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_1440+2], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call sub_4830E0
            b"\x0F\xBF\x8D\xAB\xAB\xAB\xAB"  # 26 movsx ecx, ptr [ebp+var1]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 33 mov ptr [ebp+var_143C+2], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 40 lea eax, [ebp+var_1440]
            b"\x50"                      # 46 push eax
            b"\x51"                      # 47 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 48 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 53 mov ecx, eax
            b"\xE8"                      # 55 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 55,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 49,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 28Ch
            b"\x89\x7D\xAB"              # 5  mov [ebp+var_52], edi
            b"\x66\x89\x55\xAB"          # 8  mov [ebp+var_58], dx
            b"\x89\x45\xAB"              # 12 mov [ebp+var_56], eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 15 mov ecx, 9
            b"\x8D\x75\xAB"              # 20 lea esi, [ebp+var_28]
            b"\x8D\x7D\xAB"              # 23 lea edi, [ebp+var_4E]
            b"\xF3\xA5"                  # 26 rep movsd
            b"\x8D\x4D\xAB"              # 28 lea ecx, [ebp+var_58]
            b"\x51"                      # 31 push ecx
            b"\x52"                      # 32 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 33 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 38 mov ecx, eax
            b"\xE8"                      # 40 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 40,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 34,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 9ABh
            b"\x50"                      # 5  push eax
            b"\x51"                      # 6  push ecx
            b"\x66\x89\x4D\xAB"          # 7  mov [ebp+var_C], cx
            b"\x89\x55\xAB"              # 11 mov [ebp+var_A], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 9E8h
            b"\x52"                      # 5  push edx
            b"\x51"                      # 6  push ecx
            b"\x66\x89\x4D\xAB"          # 7  mov [ebp+var_10], cx
            b"\xC6\x45\xAB\xAB"          # 11 mov [ebp+var_E], 0
            b"\x89\x75\xAB"              # 15 mov [ebp-0Dh], esi
            b"\x89\x75\xAB"              # 18 mov [ebp+var_A+1], esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 26 mov ecx, eax
            b"\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2015-06-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 8FCh
            b"\xA3\xAB\xAB\xAB\xAB"      # 5  mov dword_D47024, eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 10 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 15 mov ecx, eax
            b"\xE8"                      # 17 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 17,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 11,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 838h
            b"\x66\x89\x4D\xAB"          # 5  mov [ebp+var_1C], cx
            b"\x8D\x4E\xAB"              # 9  lea ecx, [esi+60h]
            b"\x89\x55\xAB"              # 12 mov [ebp+var_1A], edx
            b"\x89\x45\xAB"              # 15 mov [ebp+var_16], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 18 call sub_4830E0
            b"\x0F\xBF\x55\xAB"          # 23 movsx edx, [ebp+var_1C]
            b"\x8D\x4D\xAB"              # 27 lea ecx, [ebp+var_1C]
            b"\x51"                      # 30 push ecx
            b"\x52"                      # 31 push edx
            b"\x66\x89\x45\xAB"          # 32 mov [ebp+var_12], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 36 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 41 mov ecx, eax
            b"\xE8"                      # 43 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 43,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 37,
        }
    ],
    # 2015-06-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 364h
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_19E], ecx
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 11 mov [ebp+var_19A], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 22 mov ecx, eax
            b"\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 83Bh
            b"\x50"                      # 5  push eax
            b"\x52"                      # 6  push edx
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 7  mov word ptr [ebp+var1], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 2B6h
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_90+2], eax
            b"\x88\x8D\xAB\xAB\xAB\xAB"  # 11 mov [ebp-8Ah], cl
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 22 mov ecx, eax
            b"\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 907h
            b"\x66\x89\x55\xAB"          # 5  mov [ebp+var_8], dx
            b"\x8B\x55\xAB"              # 9  mov edx, [ebp+arg_4]
            b"\x66\x89\x45\xAB"          # 12 mov [ebp+var_6], ax
            b"\x8B\x45\xAB"              # 16 mov eax, [ebp+arg_0]
            b"\x52"                      # 19 push edx
            b"\x50"                      # 20 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call sub_4AD210
            b"\x83\xF8\xAB"              # 26 cmp eax, 3
            b"\x0F\xBF\x45\xAB"          # 29 movsx eax, [ebp+var_8]
            b"\x8D\x55\xAB"              # 33 lea edx, [ebp+var_8]
            b"\x52"                      # 36 push edx
            b"\x0F\x95\xAB"              # 37 setnz cl
            b"\x50"                      # 40 push eax
            b"\x88\x4D\xAB"              # 41 mov [ebp+var_4], cl
            b"\xE8\xAB\xAB\xAB\xAB"      # 44 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 49 mov ecx, eax
            b"\xE8"                      # 51 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 51,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 45,
        }
    ],
    # 2015-06-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 2B6h
            b"\x89\x4D\xAB"              # 5  mov [ebp+var_A], ecx
            b"\x88\x55\xAB"              # 8  mov [ebp+var_6], dl
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A35h
            b"\x52"                      # 5  push edx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 7  mov word ptr [ebp+var1], ax
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 14 mov word ptr [ebp+var1+2], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 26 mov ecx, eax
            b"\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 0A01h
            b"\x50"                      # 5  push eax
            b"\x51"                      # 6  push ecx
            b"\x66\x89\x4D\xAB"          # 7  mov word ptr [ebp+arg_0], cx
            b"\x88\x55\xAB"              # 11 mov byte ptr [ebp+arg_0+2], dl
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8D7h
            b"\x6A\xAB"                  # 5  push 18h
            b"\x50"                      # 7  push eax
            b"\x66\x89\x4D\xAB"          # 8  mov [ebp+var_20], cx
            b"\x66\x89\x55\xAB"          # 12 mov [ebp+var_1E], dx
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 16 call strncpy_s
            b"\x0F\xBF\x55\xAB"          # 22 movsx edx, [ebp+var_20]
            b"\x83\xC4\xAB"              # 26 add esp, 10h
            b"\x8D\x4D\xAB"              # 29 lea ecx, [ebp+var_20]
            b"\x51"                      # 32 push ecx
            b"\x52"                      # 33 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 34 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 39 mov ecx, eax
            b"\xE8"                      # 41 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 41,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 35,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 90Ah
            b"\x6A\xAB"                  # 5  push 18h
            b"\x52"                      # 7  push edx
            b"\x66\x89\x45\xAB"          # 8  mov [ebp+var_20], ax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 12 call strcpy_s
            b"\x0F\xBF\x4D\xAB"          # 18 movsx ecx, [ebp+var_20]
            b"\x83\xC4\xAB"              # 22 add esp, 0Ch
            b"\x8D\x45\xAB"              # 25 lea eax, [ebp+var_20]
            b"\x50"                      # 28 push eax
            b"\x51"                      # 29 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 30 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 35 mov ecx, eax
            b"\xE8"                      # 37 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 37,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 31,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8DDh
            b"\x6A\xAB"                  # 5  push 18h
            b"\x52"                      # 7  push edx
            b"\x66\x89\x4D\xAB"          # 8  mov [ebp+var_20], cx
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 12 call strcpy_s
            b"\x0F\xBF\x55\xAB"          # 18 movsx edx, [ebp+var_20]
            b"\x8A\x45\xAB"              # 22 mov al, [ebp+arg_4]
            b"\x83\xC4\xAB"              # 25 add esp, 0Ch
            b"\x8D\x4D\xAB"              # 28 lea ecx, [ebp+var_20]
            b"\x51"                      # 31 push ecx
            b"\x52"                      # 32 push edx
            b"\x88\x45\xAB"              # 33 mov [ebp+var_1E], al
            b"\xE8\xAB\xAB\xAB\xAB"      # 36 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 41 mov ecx, eax
            b"\xE8"                      # 43 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 43,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 37,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8E0h
            b"\x6A\xAB"                  # 5  push 18h
            b"\x52"                      # 7  push edx
            b"\x66\x89\x45\xAB"          # 8  mov [ebp+var_38], ax
            b"\xFF\xD7"                  # 12 call edi
            b"\x8D\x46\xAB"              # 14 lea eax, [esi+1Ch]
            b"\x50"                      # 17 push eax
            b"\x8D\x4D\xAB"              # 18 lea ecx, [ebp+var_1D]
            b"\x6A\xAB"                  # 21 push 18h
            b"\x51"                      # 23 push ecx
            b"\xFF\xD7"                  # 24 call edi
            b"\x8B\x45\xAB"              # 26 mov eax, [ebp+arg_0]
            b"\x83\xC4\xAB"              # 29 add esp, 18h
            b"\x88\x45\xAB"              # 32 mov [ebp+var_36], al
            b"\x83\xF8\xAB"              # 35 cmp eax, 2
            b"\x75\x04"                  # 38 jnz short loc_6E4FC0
            b"\xC6\x46\xAB\xAB"          # 40 mov byte ptr [esi+38h], 0
            b"\x0F\xBF\xAB\xAB"          # 44 movsx eax, [ebp+var_38]
            b"\x8D\x55\xAB"              # 48 lea edx, [ebp+var_38]
            b"\x52"                      # 51 push edx
            b"\x50"                      # 52 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 58 mov ecx, eax
            b"\xE8"                      # 60 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 60,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 54,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 2F1h
            b"\x52"                      # 5  push edx
            b"\x51"                      # 6  push ecx
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 7  mov [ebp+var_D8], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 90h
            b"\x52"                      # 5  push edx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x45\xAB"          # 7  mov [ebp+var_C], ax
            b"\x89\x4D\xAB"              # 11 mov [ebp+var_A], ecx
            b"\xC6\x45\xAB\xAB"          # 14 mov [ebp+var_6], 1
            b"\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 23 mov ecx, eax
            b"\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 35Fh
            b"\x89\x87\xAB\xAB\xAB\xAB"  # 5  mov [edi+318h], eax
            b"\x66\x89\x55\xAB"          # 11 mov [ebp+var_8], dx
            b"\xFF\xD6"                  # 15 call esi
            b"\x0F\xBF\x4D\xAB"          # 17 movsx ecx, [ebp+var_8]
            b"\x89\x45\xAB"              # 21 mov [ebp+var_6], eax
            b"\x8D\x45\xAB"              # 24 lea eax, [ebp+var_8]
            b"\x50"                      # 27 push eax
            b"\x51"                      # 28 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 29 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 34 mov ecx, eax
            b"\xE8"                      # 36 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 36,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 30,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 0D0h
            b"\x50"                      # 5  push eax
            b"\x52"                      # 6  push edx
            b"\xC7\x86\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  mov [esi+X], 1
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 17 mov word ptr [ebp+var1], dx
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 24 mov byte ptr [ebp+var1+2], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 31 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 36 mov ecx, eax
            b"\xE8"                      # 38 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 38,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 32,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 368h
            b"\x51"                      # 5  push ecx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x45\xAB"          # 7  mov [ebp+var_10], ax
            b"\x89\x5D\xAB"              # 11 mov [ebp+var_E], ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 149h
            b"\x89\x55\xAB"              # 5  mov [ebp+var_E], edx
            b"\xC6\x45\xF6\xAB"          # 8  mov [ebp+var_A], 2
            b"\x66\x89\x45\xAB"          # 12 mov [ebp+var_9], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 16 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 21 mov ecx, eax
            b"\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 17,
        }
    ],
    # 2015-06-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 998h
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var1+2], cx
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 12 mov [ebp+var_1068], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 23 mov ecx, eax
            b"\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2015-06-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 362h
            b"\xC7\x83\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebx+384h], 1
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 15 mov word ptr [ebp+var1+2], ax
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 22 mov [ebp+var_1064], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 29 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 34 mov ecx, eax
            b"\xE8"                      # 36 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 36,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 30,
        }
    ],
    # 2015-06-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 439h
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_1068], eax
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 11 mov word ptr [ebp+var1+2], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 23 mov ecx, eax
            b"\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 0E8h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov ptr [ebp+var1+2], ax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 12 mov eax, [ebp+var_14E0]
            b"\x51"                      # 18 push ecx
            b"\x52"                      # 19 push edx
            b"\xC7\x83\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 20 mov [ebx+108h], 1
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 30 mov [ebp+var_106C], dx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 37 mov ptr [ebp+var_106C+4], eax
            b"\x89\x83\xAB\xAB\xAB\xAB"  # 43 mov [ebx+118h], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 49 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 54 mov ecx, eax
            b"\xE8"                      # 56 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 56,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 50,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 802h
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var1], dx
            b"\x8D\x50\xAB"              # 12 lea edx, [eax+1]
            b"\x8A\x08"                  # 15 mov cl, [eax]
            b"\x40"                      # 17 inc eax
            b"\x84\xC9"                  # 18 test cl, cl
            b"\x75\xF9"                  # 20 jnz short loc_849027
            b"\x2B\xC2"                  # 22 sub eax, edx
            b"\x83\xF8\xAB"              # 24 cmp eax, 18h
            b"\x0F\x83\xAB\xAB\xAB\xAB"  # 27 jnb loc_848EEE
            b"\x8B\xB5\xAB\xAB\xAB\xAB"  # 33 mov esi, [ebp+Str1]
            b"\x6A\xAB"                  # 39 push 18h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 41 lea eax, [ebp+var1+2]
            b"\x56"                      # 47 push esi
            b"\x50"                      # 48 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 49 call strncpy
            b"\x0F\xBF\x95\xAB\xAB\xAB\xAB"  # 55 movsx edx, word ptr [ebp+var1
            b"\x83\xC4\xAB"              # 62 add esp, 0Ch
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 65 lea ecx, [ebp+var1]
            b"\x51"                      # 71 push ecx
            b"\x52"                      # 72 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 73 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 78 mov ecx, eax
            b"\xE8"                      # 80 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 80,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 74,
        }
    ],
    # 2015-06-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 103h
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_10A8+2], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1F9h
            b"\x52"                      # 5  push edx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 7  mov word ptr [ebp+var1+4], ax
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 14 mov dword ptr [ebp+var1+6], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 25 mov ecx, eax
            b"\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 9B4h
            b"\x50"                      # 5  push eax
            b"\x51"                      # 6  push ecx
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 7  mov dword ptr [ebp+var1+6], edx
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 13 mov word ptr [ebp+var1+4], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 25 mov ecx, eax
            b"\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
            # many goto/jmp
        },
        {
            "instanceR": 21,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 22Dh
            b"\x89\x43\xAB"              # 5  mov [ebx+1Ch], eax
            b"\x66\x89\x55\xAB"          # 8  mov [ebp+var_54], dx
            b"\xFF\xD7"                  # 12 call edi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 14 mov ecx, dword_D43034
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 20 mov edx, dword_D4285C
            b"\x89\x45\xAB"              # 26 mov [ebp+var_46], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 29 mov eax, dword_D43030
            b"\x89\x4D\xAB"              # 34 mov [ebp+var_4E], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 37 mov ecx, offset dword_D41C90
            b"\x89\x45\xAB"              # 42 mov [ebp+var_52], eax
            b"\x89\x55\xAB"              # 45 mov [ebp+var_4A], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 48 call sub_9028A0
            b"\x0F\xBF\x4D\xAB"          # 53 movsx ecx, [ebp+var_54]
            b"\x88\x45\xAB"              # 57 mov [ebp+var_42], al
            b"\x8D\x45\xAB"              # 60 lea eax, [ebp+var_54]
            b"\x50"                      # 63 push eax
            b"\x51"                      # 64 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 65 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 70 mov ecx, eax
            b"\xE8"                      # 72 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 72,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 66,
        }
    ],
    # 2015-06-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 9EFh
            b"\x88\x55\xAB"              # 5  mov [ebp+var_E], dl
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 8  mov [ebp+var_D], 0
            b"\xC7\x45\xF7\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_9], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 27 mov ecx, eax
            b"\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1BFh
            b"\x51"                      # 5  push ecx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x45\xAB"          # 7  mov [ebp+var_4], ax
            b"\xC6\x45\xAB\xAB"          # 11 mov [ebp+var_2], 1
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 20 mov ecx, eax
            b"\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 1DDh
            b"\x66\x89\x55\xAB"          # 5  mov [ebp+var_34], dx
            b"\x8B\x96\xAB\xAB\xAB\xAB"  # 9  mov edx, [esi+180h]
            b"\x89\x45\xAB"              # 15 mov [ebp+var_32], eax
            b"\x8B\x86\xAB\xAB\xAB\xAB"  # 18 mov eax, [esi+184h]
            b"\x89\x4D\xAB"              # 24 mov [ebp+var_22], ecx
            b"\x8D\x4D\xAB"              # 27 lea ecx, [ebp+var_16]
            b"\x89\x55\xAB"              # 30 mov [ebp+var_2A], edx
            b"\x8B\x96\xAB\xAB\xAB\xAB"  # 33 mov edx, [esi+18Ch]
            b"\x89\x45\xAB"              # 39 mov [ebp+var_26], eax
            b"\x8B\x86\xAB\xAB\xAB\xAB"  # 42 mov eax, [esi+190h]
            b"\x51"                      # 48 push ecx
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 49 lea ecx, [ebp+var_98]
            b"\x89\x55\xAB"              # 55 mov [ebp+var_1E], edx
            b"\x89\x45\xAB"              # 58 mov [ebp+var_1A], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 61 call sub_7C0970
            b"\xE8\xAB\xAB\xAB\xAB"      # 66 call sub_8AC310
            b"\x88\x45\xAB"              # 71 mov [ebp+var_6], al
            b"\x0F\xBF\x45\xAB"          # 74 movsx eax, [ebp+var_34]
            b"\x8D\x55\xAB"              # 78 lea edx, [ebp+var_34]
            b"\x52"                      # 81 push edx
            b"\x50"                      # 82 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 83 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 88 mov ecx, eax
            b"\xE8"                      # 90 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 90,
            "packetId": 1,
            "retOffset": 0,
            "goto": (83, 4)
        },
        {
            "instanceR": 84,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 65h
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+cp], dx
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 12 mov edx, dword_D4285C
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 18 mov word ptr [ebp+var_28A], a
            b"\xA1\xAB\xAB\xAB\xAB"      # 25 mov eax, dword_D42860
            b"\x51"                      # 30 push ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 31 mov ecx, offset dword_D41C90
            b"\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 36 mov A, 1
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 46 mov [ebp+var_292], edx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 52 mov [ebp+var_28E], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 58 call sub_902880
            b"\xB9\xAB\xAB\xAB\xAB"      # 63 mov ecx, offset dword_D41C90
            b"\xE8\xAB\xAB\xAB\xAB"      # 68 call sub_9028A0
            b"\x88\x85\xAB\xAB\xAB\xAB"  # 73 mov [ebp+var_28A+2], al
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 79 movsx eax, word ptr [ebp+cp]
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 86 lea edx, [ebp+cp]
            b"\x52"                      # 92 push edx
            b"\x50"                      # 93 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 94 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 99 mov ecx, eax
            b"\xE8"                      # 101 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 101,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 95,
        }
    ],
    # 2015-06-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 970h
            b"\x66\x89\x4D\xAB"          # 5  mov [ebp-13h], cx
            b"\x88\x55\xAB"              # 9  mov [ebp-16h], dl
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 1FBh
            b"\x56"                      # 5  push esi
            b"\x50"                      # 6  push eax
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 7  mov [ebp+var_2D2], ecx
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 13 mov [ebp+var_2D4], dx
            b"\xFF\xD3"                  # 20 call ebx
            b"\x0F\xBF\x95\xAB\xAB\xAB\xAB"  # 22 movsx edx, [ebp+var_2D4]
            b"\x83\xC4\xAB"              # 29 add esp, 0Ch
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 32 lea ecx, [ebp+var_2D4]
            b"\x51"                      # 38 push ecx
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 39 mov byte ptr [ebp+a+6], 0
            b"\x52"                      # 46 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 47 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 52 mov ecx, eax
            b"\xE8"                      # 54 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 54,
            "packetId": 1,
            "retOffset": 0,
            "goto": (47, 1)
        },
        {
            "instanceR": 48,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 66h
            b"\x50"                      # 5  push eax
            b"\x51"                      # 6  push ecx
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 7  mov word ptr [ebp+Data], cx
            b"\x88\x95\xAB\xAB\xAB\xAB"  # 14 mov [ebp+Data+2], dl
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 25 mov ecx, eax
            b"\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 22Dh
            b"\x83\xC4\xAB"              # 5  add esp, 4
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 8  mov word ptr [ebp+cp], ax
            b"\xFF\xD7"                  # 15 call edi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 17 mov ecx, dword_D43030
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 23 mov edx, dword_D43034
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 29 mov dword ptr [ebp+var_28A], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 35 mov eax, dword_D4285C
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 40 mov [ebp+var_296], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 46 mov ecx, offset dword_D41C90
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 51 mov [ebp+var_292], edx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 57 mov [ebp+var_28E], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 63 call sub_9028A0
            b"\x0F\xBF\x95\xAB\xAB\xAB\xAB"  # 68 movsx edx, word ptr [ebp+cp]
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 75 lea ecx, [ebp+cp]
            b"\x51"                      # 81 push ecx
            b"\x52"                      # 82 push edx
            b"\x88\x85\xAB\xAB\xAB\xAB"  # 83 mov [ebp+var_28A+4], al
            b"\xE8\xAB\xAB\xAB\xAB"      # 89 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 94 mov ecx, eax
            b"\xE8"                      # 96 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 96,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 90,
        }
    ],
    # 2015-06-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 841h
            b"\x88\x8D\xAB\xAB\xAB\xAB"  # 5  mov byte ptr [ebp+A+3], cl
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 234h
            b"\x52"                      # 5  push edx
            b"\x51"                      # 6  push ecx
            b"\x66\x89\x4D\xAB"          # 7  mov [ebp+var_14], cx
            b"\x89\x75\xAB"              # 11 mov [ebp+var_12], esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 232h
            b"\x89\x7D\xAB"              # 5  mov [ebp+var_E], edi
            b"\x88\x55\xAB"              # 8  mov [ebp+var_A], dl
            b"\x88\x45\xAB"              # 11 mov [ebp+var_9], al
            b"\x88\x5D\xAB"              # 14 mov [ebp+var_8], bl
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 22 mov ecx, eax
            b"\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 233h
            b"\x52"                      # 5  push edx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x45\xAB"          # 7  mov word ptr [ebp+var_10], ax
            b"\x89\x5D\xAB"              # 11 mov dword ptr [ebp+var_10+2], ebx
            b"\x89\x4D\xAB"              # 14 mov dword ptr [ebp+var_10+6], ecx
            b"\xC6\x45\xAB\xAB"          # 17 mov [ebp+var_6], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 26 mov ecx, eax
            b"\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2015-06-24
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 21Dh
            b"\x51"                      # 5  push ecx
            b"\x52"                      # 6  push edx
            b"\x66\x89\x55\xAB"          # 7  mov [ebp+var_14], dx
            b"\x89\x45\xAB"              # 11 mov [ebp+var_12], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 974h
            b"\x51"                      # 5  push ecx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x44\xAB\xAB"      # 7  mov [esp+0Ch+var_4], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 827h
            b"\x66\x89\x4C\xAB\xAB"      # 5  mov word ptr [esp+13Ch+Src], cx
            b"\x8B\x10"                  # 10 mov edx, [eax]
            b"\x8D\x44\xAB\xAB"          # 12 lea eax, [esp+13Ch+Src]
            b"\x50"                      # 16 push eax
            b"\x51"                      # 17 push ecx
            b"\x89\x54\xAB\xAB"          # 18 mov [esp+144h+Src+2], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 27 mov ecx, eax
            b"\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 82Bh
            b"\x8D\x44\xAB\xAB"          # 5  lea eax, [esp+13Ch+var_124]
            b"\x66\x89\x4C\x24\xAB"      # 9  mov word ptr [esp+13Ch+Src], cx
            b"\x50"                      # 14 push eax
            b"\x8D\x8E\xAB\xAB\xAB\xAB"  # 15 lea ecx, [esi+0F8h]
            b"\x89\x54\x24\xAB"          # 21 mov [esp+140h+var_124], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 25 call sub_489990
            b"\x8B\x08"                  # 30 mov ecx, [eax]
            b"\x0F\xBF\x44\x24\xAB"      # 32 movsx eax, word ptr [esp+13Ch+Src
            b"\x8D\x54\x24\xAB"          # 37 lea edx, [esp+13Ch+Src]
            b"\x52"                      # 41 push edx
            b"\x50"                      # 42 push eax
            b"\x89\x4C\x24\xAB"          # 43 mov [esp+144h+Src+2], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 47 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 52 mov ecx, eax
            b"\xE8"                      # 54 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 54,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 48,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8C5h
            b"\x50"                      # 5  push eax
            b"\x51"                      # 6  push ecx
            b"\x66\x89\x4C\x24\xAB"      # 7  mov word ptr [esp+144h+Src], cx
            b"\x89\x54\x24\xAB"          # 12 mov [esp+144h+Src+2], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 16 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 21 mov ecx, eax
            b"\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 17,
        }
    ],
    # 2012-05-25
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 8D4h
            b"\x66\x89\x54\x24\xAB"      # 5  mov word ptr [esp+144h+Src+2], dx
            b"\x66\x89\x7C\x24\xAB"      # 10 mov [esp+144h+var_128], di
            b"\x66\x89\x44\x24\xAB"      # 15 mov [esp+144h+var_126], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 25 mov ecx, eax
            b"\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2012-05-25
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0A9h
            b"\x66\x89\x4C\x24\xAB"      # 5  mov [esp+44h+var_32], cx
            b"\x66\x89\x54\x24\xAB"      # 10 mov [esp+44h+var_30], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 20 mov ecx, eax
            b"\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2012-05-25
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 8C3h
            b"\x89\x84\x24\xAB\xAB\xAB\x00"  # 5  mov [esp+3A8h+var_1E], eax
            b"\x89\x8C\x24\xAB\xAB\xAB\x00"  # 12 mov [esp+3A8h+var_1A], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 24 mov ecx, eax
            b"\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2012-05-25
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 8B8h
            b"\x89\x94\x24\xAB\xAB\xAB\x00"  # 5  mov [esp+3A8h+var_1E], edx
            b"\x89\x84\x24\xAB\xAB\xAB\x00"  # 12 mov [esp+3A8h+var_1A], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 24 mov ecx, eax
            b"\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 28Ch
            b"\x89\x7C\x24\xAB"          # 5  mov [esp+60h+var_2A], edi
            b"\x66\x89\x54\x24\xAB"      # 9  mov [esp+60h+Src], dx
            b"\x89\x44\x24\xAB"          # 14 mov [esp+60h+var_2E], eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 18 mov ecx, 9
            b"\x8D\x74\x24\xAB"          # 23 lea esi, [esp+60h+var_54]
            b"\x8D\x7C\x24\xAB"          # 27 lea edi, [esp+60h+var_26]
            b"\xF3\xA5"                  # 31 rep movsd
            b"\x8D\x4C\x24\xAB"          # 33 lea ecx, [esp+60h+Src]
            b"\x51"                      # 37 push ecx
            b"\x52"                      # 38 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 39 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 44 mov ecx, eax
            b"\xE8"                      # 46 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 46,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 40,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8F7h
            b"\x52"                      # 5  push edx
            b"\x51"                      # 6  push ecx
            b"\x66\x89\x4C\xAB\xAB"      # 7  mov word ptr [esp+8+Src], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8E5h
            b"\x66\x89\x44\xAB\xAB"      # 5  mov [esp+68h+Src], ax
            b"\xB9\xAB\xAB\xAB\x00"      # 10 mov ecx, 9
            b"\x8D\x74\x24\xAB"          # 15 lea esi, [esp+68h+var_58]
            b"\x8D\x7C\x24\xAB"          # 19 lea edi, [esp+68h+var_2E]
            b"\xF3\xA5"                  # 23 rep movsd
            b"\x83\xC4\xAB"              # 25 add esp, 8
            b"\x66\xA5"                  # 28 movs movsw
            b"\x8D\x4C\x24\xAB"          # 30 lea ecx, [esp+60h+Src]
            b"\x51"                      # 34 push ecx
            b"\x50"                      # 35 push eax
            b"\xA4"                      # 36 movs movsb
            b"\xE8\xAB\xAB\xAB\xAB"      # 37 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 42 mov ecx, eax
            b"\xE8"                      # 44 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 44,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 38,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 8EBh
            b"\x66\x89\x54\x24\xAB"      # 5  mov [esp+48h+Src], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 10 call sub_452AB0
            b"\x50"                      # 15 push eax
            b"\x8D\x44\x24\xAB"          # 16 lea eax, [esp+4Ch+Dest]
            b"\x50"                      # 20 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 21 call ds:sprintf
            b"\x0F\xBF\x54\x24\xAB"      # 27 movsx edx, [esp+50h+Src]
            b"\x83\xC4\xAB"              # 32 add esp, 8
            b"\x8D\x4C\x24\xAB"          # 35 lea ecx, [esp+48h+Src]
            b"\x51"                      # 39 push ecx
            b"\x52"                      # 40 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 41 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 46 mov ecx, eax
            b"\xE8"                      # 48 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 48,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 42,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 91Eh
            b"\x89\x4C\x24\xAB"          # 5  mov [esp+0A4h+var_96], ecx
            b"\x8D\x4E\xAB"              # 9  lea ecx, [esi+60h]
            b"\x66\x89\x44\x24\xAB"      # 12 mov [esp+0A4h+Src], ax
            b"\x89\x54\x24\xAB"          # 17 mov [esp+0A4h+var_92], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call sub_447360
            b"\x0F\xBF\x4C\x24\xAB"      # 26 movsx ecx, [esp+0A4h+Src]
            b"\x66\x89\x44\x24\xAB"      # 31 mov [esp+0A4h+var_8E], ax
            b"\x8D\x44\x24\xAB"          # 36 lea eax, [esp+0A4h+Src]
            b"\x50"                      # 40 push eax
            b"\x51"                      # 41 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 42 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 47 mov ecx, eax
            b"\xE8"                      # 49 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 49,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 43,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 83Bh
            b"\x50"                      # 5  push eax
            b"\x52"                      # 6  push edx
            b"\x66\x89\x54\x24\xAB"      # 7  mov word ptr [esp+8C0h+Src], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 8F0h
            b"\x8D\x4C\x24\xAB"          # 5  lea ecx, [esp+9Ch+Src]
            b"\x66\x89\x54\xAB\xAB"      # 9  mov [esp+9Ch+Src], dx
            b"\x8B\x47\xAB"              # 14 mov eax, [edi+0Ch]
            b"\x51"                      # 17 push ecx
            b"\x52"                      # 18 push edx
            b"\x89\x44\x24\xAB"          # 19 mov [esp+0A4h+var_82], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 28 mov ecx, eax
            b"\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8F9h
            b"\x52"                      # 5  push edx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x44\xAB\xAB"      # 7  mov word ptr [esp+210h+var1], ax
            b"\x89\x4C\x24\xAB"          # 12 mov [esp+210h+var_1F4+2], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 16 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 21 mov ecx, eax
            b"\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 17,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8E7h
            b"\x66\x89\x4C\x24\xAB"      # 5  mov word ptr [esp+3A4h+Src], cx
            b"\x8B\x8E\xAB\xAB\xAB\xAB"  # 10 mov ecx, [esi+88h]
            b"\xE8\xAB\xAB\xAB\xAB"      # 16 call sub_452AB0
            b"\x50"                      # 21 push eax
            b"\xFF\xD7"                  # 22 call edi
            b"\xBA\xAB\xAB\xAB\xAB"      # 24 mov edx, 96h
            b"\x83\xC4\xAB"              # 29 add esp, 4
            b"\x66\x89\x44\x24\xAB"      # 32 mov word ptr [esp+3A4h+Src+2], ax
            b"\x66\x3B\xC2"              # 37 cmp ax, dx
            b"\x0F\x87\xAB\xAB\xAB\xAB"  # 40 ja loc_4FD40E
            b"\xB9\xAB\xAB\xAB\xAB"      # 46 mov ecx, offset dword_9ADEB0
            b"\xC7\x44\x24\xAB\xAB\xAB\xAB\xAB"  # 51 mov [esp+3A4h+var_38C], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 59 call sub_5CDA80
            b"\x0F\xBF\x54\x24\xAB"      # 64 movsx edx, word ptr [esp+3A4h+Src
            b"\x8D\x4C\x24\xAB"          # 69 lea ecx, [esp+3A4h+Src]
            b"\x51"                      # 73 push ecx
            b"\xB8\xAB\xAB\xAB\x00"      # 74 mov eax, 0Bh
            b"\x52"                      # 79 push edx
            b"\x66\x89\x44\x24\xAB"      # 80 mov [esp+3ACh+var_388], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 85 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 90 mov ecx, eax
            b"\xE8"                      # 92 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 92,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 86,
        }
    ],
    # 2012-05-25
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 2B6h
            b"\x89\x4C\x24\xAB"          # 5  mov [esp+1Eh], ecx
            b"\x88\x54\x24\xAB"          # 9  mov ptr [esp+0ACh+var1+2], dl
            b"\xE8\xAB\xAB\xAB\xAB"      # 13 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 18 mov ecx, eax
            b"\xE8"                      # 20 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 20,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 14,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 907h
            b"\x66\x89\x14\xAB"          # 5  mov [esp+8+var_8], dx
            b"\x8B\x54\x24\xAB"          # 9  mov edx, [esp+8+arg_4]
            b"\x66\x89\x44\x24\xAB"      # 13 mov [esp+8+var_6], ax
            b"\x8B\x44\x24\xAB"          # 18 mov eax, [esp+8+arg_0]
            b"\x52"                      # 22 push edx
            b"\x50"                      # 23 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 24 call sub_465130
            b"\x83\xF8\xAB"              # 29 cmp eax, 3
            b"\x0F\xBF\xAB\xAB"          # 32 movsx eax, [esp+8+var_8]
            b"\x8D\x14\xAB"              # 36 lea edx, [esp+8+var_8]
            b"\x52"                      # 39 push edx
            b"\x0F\x95\xC1"              # 40 setnz cl
            b"\x50"                      # 43 push eax
            b"\x88\x4C\x24\xAB"          # 44 mov [esp+10h+var_4], cl
            b"\xE8\xAB\xAB\xAB\xAB"      # 48 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 53 mov ecx, eax
            b"\xE8"                      # 55 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 55,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 49,
        }
    ],
    # 2012-05-25
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 94Dh
            b"\x89\x54\x24\xAB"          # 5  mov [esp+28h+var_E], edx
            b"\x89\x44\x24\xAB"          # 9  mov [esp+28h+var_A], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 13 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 18 mov ecx, eax
            b"\xE8"                      # 20 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 20,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 14,
        }
    ],
    # 2012-05-25
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 2B6h
            b"\x88\x54\x24\xAB"          # 5  mov [esp+0F8h+var_DA], dl
            b"\xE8\xAB\xAB\xAB\xAB"      # 9  call CRagConnection_instanceR
            b"\x8B\xC8"                  # 14 mov ecx, eax
            b"\xE8"                      # 16 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 16,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 10,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8D7h
            b"\x6A\xAB"                  # 5  push 18h
            b"\x50"                      # 7  push eax
            b"\x66\x89\x4C\x24\xAB"      # 8  mov [esp+2Ch+var_20], cx
            b"\x66\x89\x54\x24\xAB"      # 13 mov [esp+2Ch+var_1E], dx
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 18 call ds:strcpy_s
            b"\x0F\xBF\x54\x24\xAB"      # 24 movsx edx, [esp+2Ch+var_20]
            b"\x83\xC4\xAB"              # 29 add esp, 0Ch
            b"\x8D\x0C\xAB"              # 32 lea ecx, [esp+20h+var_20]
            b"\x51"                      # 35 push ecx
            b"\x52"                      # 36 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 37 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 42 mov ecx, eax
            b"\xE8"                      # 44 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 44,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 38,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 90Ah
            b"\x6A\xAB"                  # 5  push 18h
            b"\x52"                      # 7  push edx
            b"\x66\x89\x44\x24\xAB"      # 8  mov [esp+2Ch+var_20], ax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 13 call ds:strcpy_s
            b"\x0F\xBF\x4C\x24\xAB"      # 19 movsx ecx, [esp+2Ch+var_20]
            b"\x83\xC4\xAB"              # 24 add esp, 0Ch
            b"\x8D\x04\xAB"              # 27 lea eax, [esp+20h+var_20]
            b"\x50"                      # 30 push eax
            b"\x51"                      # 31 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 32 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 37 mov ecx, eax
            b"\xE8"                      # 39 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 39,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 33,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8DDh
            b"\x6A\xAB"                  # 5  push 18h
            b"\x52"                      # 7  push edx
            b"\x66\x89\x4C\x24\xAB"      # 8  mov [esp+2Ch+var_20], cx
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 13 call ds:strcpy_s
            b"\x0F\xBF\x54\x24\xAB"      # 19 movsx edx, [esp+2Ch+var_20]
            b"\x8A\x44\x24\xAB"          # 24 mov al, [esp+2Ch+arg_4]
            b"\x83\xC4\xAB"              # 28 add esp, 0Ch
            b"\x8D\x0C\xAB"              # 31 lea ecx, [esp+20h+var_20]
            b"\x51"                      # 34 push ecx
            b"\x52"                      # 35 push edx
            b"\x88\x44\x24\xAB"          # 36 mov [esp+28h+var_1E], al
            b"\xE8\xAB\xAB\xAB\xAB"      # 40 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 45 mov ecx, eax
            b"\xE8"                      # 47 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 47,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 41,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8E0h
            b"\x6A\xAB"                  # 5  push 18h
            b"\x52"                      # 7  push edx
            b"\x66\x89\x44\x24\xAB"      # 8  mov [esp+4Ch+Src], ax
            b"\xFF\xD7"                  # 13 call edi
            b"\x8D\x46\xAB"              # 15 lea eax, [esi+1Ch]
            b"\x50"                      # 18 push eax
            b"\x8D\x4C\x24\xAB"          # 19 lea ecx, [esp+50h+var_1D]
            b"\x6A\xAB"                  # 23 push 18h
            b"\x51"                      # 25 push ecx
            b"\xFF\xD7"                  # 26 call edi
            b"\x8B\x44\x24\xAB"          # 28 mov eax, [esp+58h+arg_0]
            b"\x83\xC4\xAB"              # 32 add esp, 18h
            b"\x88\x44\x24\xAB"          # 35 mov [esp+40h+var_36], al
            b"\x83\xF8\xAB"              # 39 cmp eax, 2
            b"\x75\x04"                  # 42 jnz short loc_5FF413
            b"\xC6\x46\xAB\xAB"          # 44 mov byte ptr [esi+38h], 0
            b"\x0F\xBF\x44\x24\xAB"      # 48 movsx eax, [esp+40h+Src]
            b"\x8D\x54\x24\xAB"          # 53 lea edx, [esp+40h+Src]
            b"\x52"                      # 57 push edx
            b"\x50"                      # 58 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 59 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 64 mov ecx, eax
            b"\xE8"                      # 66 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 66,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 60,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 90h
            b"\x52"                      # 5  push edx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x44\x24\xAB"      # 7  mov [esp+3Ch+Src], ax
            b"\x89\x4C\x24\xAB"          # 12 mov [esp+3Ch+var_1E], ecx
            b"\xC6\x44\x24\xAB\xAB"      # 16 mov [esp+3Ch+var_1A], 1
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 26 mov ecx, eax
            b"\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0D0h
            b"\x51"                      # 5  push ecx
            b"\x50"                      # 6  push eax
            b"\xC7\x86\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  mov [esi+100h], 1
            b"\x66\x89\x44\x24\xAB"      # 17 mov word ptr [esp+6D0h+Src], ax
            b"\xC6\x44\x24\xAB\xAB"      # 22 mov byte ptr [esp+6D0h+Src+2], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 27 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 32 mov ecx, eax
            b"\xE8"                      # 34 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 34,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 28,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 0D0h
            b"\x50"                      # 5  push eax
            b"\x52"                      # 6  push edx
            b"\xC7\x86\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  mov [esi+100h], 1
            b"\x66\x89\x54\x24\xAB"      # 17 mov word ptr [esp+6D0h+Src], dx
            b"\xC6\x44\x24\xAB\xAB"      # 22 mov byte ptr [esp+6D0h+Src+2], 1
            b"\xE8\xAB\xAB\xAB\xAB"      # 27 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 32 mov ecx, eax
            b"\xE8"                      # 34 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 34,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 28,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 87Fh
            b"\x89\x87\xAB\xAB\xAB\xAB"  # 5  mov [edi+2E8h], eax
            b"\x66\x89\x54\x24\xAB"      # 11 mov [esp+10h+Src], dx
            b"\xFF\xD6"                  # 16 call esi
            b"\x0F\xBF\x4C\x24\xAB"      # 18 movsx ecx, [esp+10h+Src]
            b"\x89\x44\x24\xAB"          # 23 mov [esp+10h+var_6], eax
            b"\x8D\x44\x24\xAB"          # 27 lea eax, [esp+10h+Src]
            b"\x50"                      # 31 push eax
            b"\x51"                      # 32 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 33 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 38 mov ecx, eax
            b"\xE8"                      # 40 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 40,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 34,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 891h
            b"\x51"                      # 5  push ecx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x44\xAB\xAB"      # 7  mov [esp+28h+Src], ax
            b"\x89\x5C\x24\xAB"          # 12 mov [esp+28h+var_6], ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 16 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 21 mov ecx, eax
            b"\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 17,
        }
    ],
    # 2012-05-25
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 149h
            b"\x89\x54\x24\xAB"          # 5  mov [esp+14h+var_A], edx
            b"\xC6\x44\x24\xAB\xAB"      # 9  mov [esp+14h+var_6], 2
            b"\x66\x89\x44\xAB\xAB"      # 14 mov [esp+14h+var_5], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 24 mov ecx, eax
            b"\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 0ABh
            b"\x50"                      # 5  push eax
            b"\x51"                      # 6  push ecx
            b"\x66\x89\x4C\xAB\xAB"      # 7  mov ptr [esp+14D4h+var1], cx
            b"\x66\x89\x54\xAB\xAB"      # 12 mov ptr [esp+14D4h+var1+2], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 22 mov ecx, eax
            b"\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A9h
            b"\x52"                      # 5  push edx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 7  mov [esp+14D4h+var1], ax
            b"\x66\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 15 mov [esp+14D4h+var1+2], b
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 23 mov [esp+14D4h+var2], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 31 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 36 mov ecx, eax
            b"\xE8"                      # 38 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 38,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 32,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 956h
            b"\x51"                      # 5  push ecx
            b"\x52"                      # 6  push edx
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  mov [ebp+354h], 1
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 17 mov [esp+14D4h+var1], dx
            b"\x66\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 25 mov [esp+14D4h+var1+2], b
            b"\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 33 mov [esp+14D4h+var2], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 41 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 46 mov ecx, eax
            b"\xE8"                      # 48 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 48,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 42,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 439h
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+350h], 1
            b"\xA1\xAB\xAB\xAB\xAB"      # 15 mov eax, dword_9AB40C
            b"\x51"                      # 20 push ecx
            b"\x52"                      # 21 push edx
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 22 mov [esp+14D4h+var1], dx
            b"\x89\x84\x24\xAB\xAB\xAB\xAB"  # 30 mov [esp+14D4h+var_1014], eax
            b"\x66\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 37 mov [esp+14D4h+var1+2], b
            b"\xE8\xAB\xAB\xAB\xAB"      # 45 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 50 mov ecx, eax
            b"\xE8"                      # 52 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 52,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 46,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0E4h
            b"\x51"                      # 5  push ecx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 7  mov [esp+14D4h+var1], ax
            b"\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 15 mov [esp+14D4h+var1+2], ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 27 mov ecx, eax
            b"\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2012-05-25
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0E8h
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+108h], 1
            b"\x66\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 15 mov [esp+14D4h+var1+2], b
            b"\x89\x84\x24\xAB\xAB\xAB\xAB"  # 23 mov [esp+14D4h+var2], eax
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 30 mov [ebp+118h], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 36 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 41 mov ecx, eax
            b"\xE8"                      # 43 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 43,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 37,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 897h
            b"\x8B\xC3"                  # 5  mov eax, ebx
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 7  mov [esp+14CCh+var1], dx
            b"\x8D\x50\xAB"              # 15 lea edx, [eax+1]
            b"\xEB\x03"                  # 18 jmp short loc_742E30
            b"\xAB\xAB\xAB"              # 20 align 10h
            b"\x8A\x08"                  # 23 mov cl, [eax]
            b"\x40"                      # 25 inc eax
            b"\x84\xC9"                  # 26 test cl, cl
            b"\x75\xF9"                  # 28 jnz short loc_742E30
            b"\x2B\xC2"                  # 30 sub eax, edx
            b"\x83\xF8\xAB"              # 32 cmp eax, 18h
            b"\x0F\x83\xAB\xAB\xAB\xAB"  # 35 jnb loc_742D1E
            b"\x6A\xAB"                  # 41 push 18h
            b"\x8D\x84\x24\xAB\xAB\xAB\xAB"  # 43 lea eax, [esp+14D0h+var1+2]
            b"\x53"                      # 50 push ebx
            b"\x50"                      # 51 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 52 call ds:strncpy
            b"\x0F\xBF\x94\x24\xAB\xAB\xAB\xAB"  # 58 movsx edx, [esp+14D8+var1
            b"\x83\xC4\xAB"              # 66 add esp, 0Ch
            b"\x8D\x8C\x24\xAB\xAB\xAB\xAB"  # 69 lea ecx, [esp+14CCh+var1]
            b"\x51"                      # 76 push ecx
            b"\x52"                      # 77 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 78 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 83 mov ecx, eax
            b"\xE8"                      # 85 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 85,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 79,
        }
    ],
    # 2012-05-25
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 103h
            b"\x89\x94\x24\xAB\xAB\xAB\xAB"  # 5  mov [esp+14D4h+var1+2], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 1E5h
            b"\x50"                      # 5  push eax
            b"\x51"                      # 6  push ecx
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 7  mov [esp+14D4h+var1], cx
            b"\x89\x94\x24\xAB\xAB\xAB\xAB"  # 15 mov [esp+14D4h+var1+2], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 27 mov ecx, eax
            b"\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1F9h
            b"\x52"                      # 5  push edx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 7  mov [esp+14D4h+var1], ax
            b"\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 15 mov [esp+14D4h+var1+2], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 27 mov ecx, eax
            b"\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 8B5h
            b"\x50"                      # 5  push eax
            b"\x52"                      # 6  push edx
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 7  mov [esp+14D4h+var1], dx
            b"\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 15 mov [esp+14D4h+var1+2], ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 27 mov ecx, eax
            b"\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
            # many goto
        },
        {
            "instanceR": 23,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 869h
            b"\x89\x47\xAB"              # 5  mov [edi+1Ch], eax
            b"\x66\x89\x4C\x24\xAB"      # 8  mov [esp+68h+Src], cx
            b"\xFF\xD6"                  # 13 call esi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 15 mov ecx, dword_9AAC44
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 21 mov edx, dword ptr qword_9AB40C
            b"\x89\x44\x24\xAB"          # 27 mov [esp+68h+var_46], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 31 mov eax, dword ptr qword_9AB40C+4
            b"\x89\x4C\x24\xAB"          # 36 mov [esp+68h+var_4A], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 40 mov ecx, offset dword_9A93E8
            b"\x89\x54\x24\xAB"          # 45 mov ptr [esp+68h+var1], edx
            b"\x89\x44\x24\xAB"          # 49 mov ptr [esp+68h+var1+4], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call sub_7D2640
            b"\x88\x44\x24\xAB"          # 58 mov [esp+68h+var_42], al
            b"\x0F\xBF\x44\x24\xAB"      # 62 movsx eax, [esp+68h+Src]
            b"\x8D\x54\x24\xAB"          # 67 lea edx, [esp+68h+Src]
            b"\x52"                      # 71 push edx
            b"\x50"                      # 72 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 73 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 78 mov ecx, eax
            b"\xE8"                      # 80 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 80,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 74,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1BFh
            b"\x51"                      # 5  push ecx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x44\x24\xAB"      # 7  mov [esp+10h+Src], ax
            b"\xC6\x44\x24\xAB\xAB"      # 12 mov [esp+10h+var_2], 1
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 22 mov ecx, eax
            b"\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1DDh
            b"\x89\x54\x24\xAB"          # 5  mov [esp+0A4h+var_92], edx
            b"\x8B\x96\xAB\xAB\xAB\xAB"  # 9  mov edx, [esi+188h]
            b"\x66\x89\x44\x24\xAB"      # 15 mov [esp+0A4h+Src], ax
            b"\x8B\x86\xAB\xAB\xAB\xAB"  # 20 mov eax, [esi+180h]
            b"\x89\x4C\x24\xAB"          # 26 mov [esp+0A4h+var_8A], ecx
            b"\x8B\x8E\xAB\xAB\xAB\xAB"  # 30 mov ecx, [esi+190h]
            b"\x89\x54\x24\xAB"          # 36 mov [esp+0A4h+var_86], edx
            b"\x89\x44\x24\xAB"          # 40 mov [esp+0A4h+var_8E], eax
            b"\x8B\x86\xAB\xAB\xAB\xAB"  # 44 mov eax, [esi+18Ch]
            b"\x8D\x54\x24\xAB"          # 50 lea edx, [esp+0A4h+var_7A]
            b"\x89\x4C\x24\xAB"          # 54 mov [esp+0A4h+var_7E], ecx
            b"\x52"                      # 58 push edx
            b"\x8D\x4C\x24\xAB"          # 59 lea ecx, [esp+0A8h+var_5C]
            b"\x89\x44\x24\xAB"          # 63 mov [esp+0A8h+var_82], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 67 call sub_6D1C80
            b"\xE8\xAB\xAB\xAB\xAB"      # 72 call sub_788240
            b"\x0F\xBF\x4C\x24\xAB"      # 77 movsx ecx, [esp+0A4h+Src]
            b"\x88\x44\x24\xAB"          # 82 mov [esp+0A4h+var_6A], al
            b"\x8D\x44\x24\xAB"          # 86 lea eax, [esp+0A4h+Src]
            b"\x50"                      # 90 push eax
            b"\x51"                      # 91 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 92 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 97 mov ecx, eax
            b"\xE8"                      # 99 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 99,
            "packetId": 1,
            "retOffset": 0,
            "goto": (92, 4)
        },
        {
            "instanceR": 93,
        }
    ],
    # 2012-05-25
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 200h
            b"\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 5  mov [esp+3F4h+var_272], ecx
            b"\x89\x94\x24\xAB\xAB\xAB\xAB"  # 12 mov [esp+3F4h+var_26E], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 24 mov ecx, eax
            b"\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 65h
            b"\x66\x89\x54\xAB\xAB"      # 5  mov word ptr [esp+3ECh+cp], dx
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 10 mov edx, dword_9AAC44
            b"\x66\x89\x44\x24\xAB"      # 16 mov ptr [esp+3ECh+var_396], ax
            b"\xA1\xAB\xAB\xAB\xAB"      # 21 mov eax, dword_9AAC48
            b"\x89\x4C\x24\xAB"          # 26 mov dword ptr [esp+3ECh+var1], ec
            b"\xB9\xAB\xAB\xAB\xAB"      # 30 mov ecx, offset dword_9A93E8
            b"\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 35 mov addr1, 1
            b"\x89\x54\x24\xAB"          # 45 mov ptr [esp+3ECh+var1+4], edx
            b"\x89\x44\x24\xAB"          # 49 mov [esp+3ECh+var_39A], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call sub_7D2640
            b"\x0F\xBF\x54\x24\xAB"      # 58 movsx edx, word ptr [esp+3ECh+cp]
            b"\x8D\x4C\x24\xAB"          # 63 lea ecx, [esp+3ECh+cp]
            b"\x51"                      # 67 push ecx
            b"\x52"                      # 68 push edx
            b"\x88\x44\x24\xAB"          # 69 mov [esp+3F4h+var_396+2], al
            b"\xE8\xAB\xAB\xAB\xAB"      # 73 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 78 mov ecx, eax
            b"\xE8"                      # 80 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 80,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 74,
        }
    ],
    # 2012-05-25
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 970h
            b"\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 5  mov [esp+3F4h+var_267], a
            b"\x88\x8C\x24\xAB\xAB\xAB\xAB"  # 13 mov [esp+3F4h+var_26E+4], cl
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 25 mov ecx, eax
            b"\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 1FBh
            b"\x56"                      # 5  push esi
            b"\x52"                      # 6  push edx
            b"\x89\x44\x24\xAB"          # 7  mov [esp+3F8h+var_38E], eax
            b"\x66\x89\x4C\x24\xAB"      # 11 mov [esp+3F8h+Dst], cx
            b"\xFF\xD5"                  # 16 call ebp
            b"\x0F\xBF\x4C\x24\xAB"      # 18 movsx ecx, [esp+3F8h+Dst]
            b"\x83\xC4\xAB"              # 23 add esp, 0Ch
            b"\x8D\x44\x24\xAB"          # 26 lea eax, [esp+3ECh+Dst]
            b"\x50"                      # 30 push eax
            b"\xC6\x84\x24\xAB\xAB\xAB\xAB\xAB"  # 31 mov [esp+3F0h+var_359], 0
            b"\x51"                      # 39 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 40 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 45 mov ecx, eax
            b"\xE8"                      # 47 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 47,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 41,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 66h
            b"\x52"                      # 5  push edx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x44\x24\xAB"      # 7  mov word ptr [esp+3F4h+var_1], ax
            b"\x88\x4C\x24\xAB"          # 12 mov ptr [esp+3F4h+var_1+2], cl
            b"\xE8\xAB\xAB\xAB\xAB"      # 16 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 21 mov ecx, eax
            b"\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 17,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 869h
            b"\x83\xC4\xAB"              # 5  add esp, 4
            b"\x66\x89\x4C\x24\xAB"      # 8  mov word ptr [esp+3ECh+cp], cx
            b"\xFF\xD7"                  # 13 call edi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 15 mov ecx, dword_9AAC44
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 21 mov edx, dword ptr qword_9AB40C
            b"\x89\x44\x24\xAB"          # 27 mov ptr [esp+3ECh+var_396], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 31 mov eax, dword ptr qword_9AB40C+4
            b"\x89\x4C\x24\xAB"          # 36 mov [esp+3ECh+var_39A], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 40 mov ecx, offset dword_9A93E8
            b"\x89\x54\x24\xAB"          # 45 mov ptr [esp+3ECh+var_3A2], edx
            b"\x89\x44\x24\xAB"          # 49 mov ptr [esp+3ECh+var_3A2+4], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call sub_7D2640
            b"\x88\x44\x24\xAB"          # 58 mov [esp+3ECh+var_396+4], al
            b"\x0F\xBF\x44\xAB\xAB"      # 62 movsx eax, word ptr [esp+3ECh+cp]
            b"\x8D\x54\xAB\xAB"          # 67 lea edx, [esp+3ECh+cp]
            b"\x52"                      # 71 push edx
            b"\x50"                      # 72 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 73 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 78 mov ecx, eax
            b"\xE8"                      # 80 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 80,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 74,
        }
    ],
    # 2012-05-25
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 841h
            b"\x88\x4C\x24\xAB"          # 5  mov byte ptr [esp+3F4h+var1+3], c
            b"\xE8\xAB\xAB\xAB\xAB"      # 9  call CRagConnection_instanceR
            b"\x8B\xC8"                  # 14 mov ecx, eax
            b"\xE8"                      # 16 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 16,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 10,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 234h
            b"\x52"                      # 5  push edx
            b"\x51"                      # 6  push ecx
            b"\x66\x89\x4C\x24\xAB"      # 7  mov [esp+20h+Src], cx
            b"\x89\x74\x24\xAB"          # 12 mov [esp+20h+var_6], esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 16 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 21 mov ecx, eax
            b"\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 17,
        }
    ],
    # 2012-05-25
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 232h
            b"\x89\x7C\x24\xAB"          # 5  mov [esp+40h+var_E], edi
            b"\x88\x54\x24\xAB"          # 9  mov [esp+40h+var_A], dl
            b"\x88\x44\x24\xAB"          # 13 mov [esp+40h+var_9], al
            b"\x88\x5C\x24\xAB"          # 17 mov [esp+40h+var_8], bl
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 26 mov ecx, eax
            b"\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 233h
            b"\x51"                      # 5  push ecx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x44\x24\xAB"      # 7  mov word ptr [esp+24h+Src], ax
            b"\x89\x5C\x24\xAB"          # 12 mov dword ptr [esp+24h+Src+2], eb
            b"\x89\x6C\x24\xAB"          # 16 mov dword ptr [esp+24h+Src+6], eb
            b"\xC6\x44\x24\xAB\xAB"      # 20 mov [esp+24h+var_2], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 25 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 30 mov ecx, eax
            b"\xE8"                      # 32 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 32,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 26,
        }
    ],
    # 2012-05-25
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 21Dh
            b"\x51"                      # 5  push ecx
            b"\x52"                      # 6  push edx
            b"\x66\x89\x54\x24\xAB"      # 7  mov [esp+34h+Src], dx
            b"\x89\x44\x24\xAB"          # 12 mov [esp+34h+var_12], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 16 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 21 mov ecx, eax
            b"\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 17,
        }
    ],
    # 2008-07-02
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 2B6h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_C], 2B6h
            b"\x89\x55\xAB"              # 12 mov [ebp+var_8], edx
            b"\x88\x45\xAB"              # 15 mov [ebp+var_4], al
            b"\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 23 mov ecx, eax
            b"\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 827h
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_110], cx
            b"\x8B\x00"                  # 12 mov eax, [eax]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 14 mov [ebp+var_10E], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 20 lea eax, [ebp+var_110]
            b"\x50"                      # 26 push eax
            b"\x51"                      # 27 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 28 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 33 mov ecx, eax
            b"\xE8"                      # 35 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 35,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 29,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 82Bh
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_110], ax
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"  # 12 movzx eax, byte_F3B5E6
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 19 mov [ebp+var_114], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 25 lea eax, [ebp+var_114]
            b"\x50"                      # 31 push eax
            b"\x8D\x8E\xAB\xAB\xAB\xAB"  # 32 lea ecx, [esi+0A4h]
            b"\xE8\xAB\xAB\xAB\xAB"      # 38 call sub_4A0400
            b"\x8B\x00"                  # 43 mov eax, [eax]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 45 mov [ebp+var_10E], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 51 lea eax, [ebp+var_110]
            b"\x50"                      # 57 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 58 movsx eax, [ebp+var_110]
            b"\x50"                      # 65 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 66 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 71 mov ecx, eax
            b"\xE8"                      # 73 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 73,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 67,
        }
    ],
    # 2016-04-27
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 8D4h
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var1], cx
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 12 mov word ptr [ebp+var1+2], di
            b"\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 24 mov ecx, eax
            b"\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A52h
            b"\x6A\xAB"                  # 5  push 10h
            b"\x66\x89\x45\xAB"          # 7  mov [ebp+var_24], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call sub_4DFE20
            b"\x50"                      # 16 push eax
            b"\x8D\x45\xAB"              # 17 lea eax, [ebp+var_22]
            b"\x6A\xAB"                  # 20 push 10h
            b"\x50"                      # 22 push eax
            b"\x90"                      # 23 no nop
            b"\xE8\xAB\xAB\xAB\xAB"      # 24 call near ptr 35AE380h
            b"\x66\x8B\x86\xAB\xAB\xAB\xAB"  # 29 mov ax, [esi+0B0h]
            b"\x66\x89\x45\xAB"          # 36 mov word ptr [ebp+var_14+2], ax
            b"\x83\xC4\xAB"              # 40 add esp, 10h
            b"\x8D\x45\xAB"              # 43 lea eax, [ebp+var_24]
            b"\x50"                      # 46 push eax
            b"\x0F\xBF\x45\xAB"          # 47 movsx eax, [ebp+var_24]
            b"\x50"                      # 51 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 52 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 57 mov ecx, eax
            b"\xE8"                      # 59 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 59,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 53,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A5Ch
            b"\x6A\xAB"                  # 5  push 10h
            b"\x66\x89\x45\xAB"          # 7  mov [ebp+var_18], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call sub_4DFE20
            b"\x50"                      # 16 push eax
            b"\x8D\x45\xAB"              # 17 lea eax, [ebp+var_16]
            b"\x6A\xAB"                  # 20 push 10h
            b"\x50"                      # 22 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call near ptr 35AE380h
            b"\x90"                      # 28 no nop
            b"\x83\xC4\xAB"              # 29 add esp, 10h
            b"\x8D\x45\xAB"              # 32 lea eax, [ebp+var_18]
            b"\x50"                      # 35 push eax
            b"\x0F\xBF\x45\xAB"          # 36 movsx eax, [ebp+var_18]
            b"\x50"                      # 40 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 41 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 46 mov ecx, eax
            b"\xE8"                      # 48 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 48,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 42,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A13h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+var_44], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 9  call sub_4DFE20
            b"\x50"                      # 14 push eax
            b"\x8D\x45\xAB"              # 15 lea eax, [ebp+var_42]
            b"\x6A\xAB"                  # 18 push 18h
            b"\x50"                      # 20 push eax
            b"\x90"                      # 21 no nop
            b"\xE8\xAB\xAB\xAB\xAB"      # 22 call near ptr 35B507Ch
            b"\x83\xC4\xAB"              # 27 add esp, 0Ch
            b"\x8D\x45\xAB"              # 30 lea eax, [ebp+var_44]
            b"\x50"                      # 33 push eax
            b"\x0F\xBF\x45\xAB"          # 34 movsx eax, [ebp+var_44]
            b"\x50"                      # 38 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 39 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 44 mov ecx, eax
            b"\xE8"                      # 46 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 46,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 40,
        }
    ],
    # 2016-04-27
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 9EEh
            b"\x89\x4D\xAB"              # 5  mov dword ptr [ebp+var_1C+3], ecx
            b"\x89\x75\xAB"              # 8  mov dword ptr [ebp+var_1C+7], esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 0A08h
            b"\x50"                      # 5  push eax
            b"\x66\x89\xAB\xAB"          # 6  mov [ebp+var_38], cx
            b"\x66\x0F\xD6\x45\xAB"      # 10 movq [ebp+var_36], xmm0
            b"\x66\x0F\xD6\x45\xAB"      # 15 movq [ebp+var_2E], xmm0
            b"\x66\x0F\xD6\x45\xAB"      # 20 movq [ebp+var_26], xmm0
            b"\x0F\xBF\xC1"              # 25 movsx eax, cx
            b"\x50"                      # 28 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 29 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 34 mov ecx, eax
            b"\xE8"                      # 36 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 36,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 30,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 9B0h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_29C], ax
            b"\xA1\xAB\xAB\xAB\xAB"      # 12 mov eax, dword_F3E874
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 17 mov [ebp-29Ah], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 23 lea eax, [ebp+var_290]
            b"\x50"                      # 29 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 30 call sub_4B9C00
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 35 mov [ebp+var_4], 1
            b"\x83\x78\xAB\xAB"          # 42 cmp dword ptr [eax+14h], 10h
            b"\x72\xAB"                  # 46 jb short loc_52ED55
            b"\x8B\x00"                  # 48 mov eax, [eax]
            b"\x50"                      # 50 push eax
            b"\x90"                      # 51 no nop
            b"\xE8\xAB\xAB\xAB\xAB"      # 52 call near ptr 35C9001h
            b"\x83\xC4\xAB"              # 57 add esp, 4
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 60 mov ptr [ebp+var_298+2], ax
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 67 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 74 lea ecx, [ebp+var_290]
            b"\xE8\xAB\xAB\xAB\xAB"      # 80 call sub_402620
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 85 lea eax, [ebp+var_29C]
            b"\x50"                      # 91 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 92 movsx eax, [ebp+var_29C]
            b"\x50"                      # 99 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 100 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 105 mov ecx, eax
            b"\xE8"                      # 107 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 107,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 101,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xBF\xAB\xAB\x00\x00"      # 0  mov edi, 9C3h
            b"\xEB\x0A"                  # 5  jmp short loc_56F8E0
            b"\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  align 10h
            b"\xA1\xAB\xAB\xAB\xAB"      # 17 mov eax, dword_F3E874
            b"\x8D\x4E\xAB"              # 22 lea ecx, [esi+8]
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 25 mov word ptr [ebp+var1], di
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 32 mov dword ptr [ebp+var1+2], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 38 call sub_4A9120
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 43 mov word ptr [ebp+var1+6], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 50 lea eax, [ebp+var1]
            b"\x50"                      # 56 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 57 movsx eax, word ptr [ebp+var1
            b"\x50"                      # 64 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 65 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 70 mov ecx, eax
            b"\xE8"                      # 72 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 72,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 66,
        }
    ],
    # 2016-04-27
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 9E8h
            b"\xC6\x45\xAB\xAB"          # 5  mov [ebp+var_E], 0
            b"\x66\x0F\x13\x45\xAB"      # 9  movlpd qword ptr [ebp-0Dh], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A08h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_AC], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 12 lea eax, [ebp+var_88]
            b"\x0F\x43\x85\xAB\xAB\xAB\xAB"  # 18 cmovnb eax, [ebp+var_88]
            b"\x50"                      # 25 push eax
            b"\x0F\x57\xC0"              # 26 xorps xmm0, xmm0
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 29 lea eax, [ebp+var_AA]
            b"\x50"                      # 35 push eax
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 36 movq [ebp+var_AA], xmm0
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 44 movq [ebp+var_A2], xmm0
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 52 movq [ebp+var_9A], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"      # 60 call sub_A4339C
            b"\x83\xC4\xAB"              # 65 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 68 lea eax, [ebp+var_AC]
            b"\x50"                      # 74 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 75 movsx eax, [ebp+var_AC]
            b"\x50"                      # 82 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 83 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 88 mov ecx, eax
            b"\xE8"                      # 90 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 90,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 84,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8DDh
            b"\x6A\xAB"                  # 5  push 18h
            b"\x50"                      # 7  push eax
            b"\x66\x89\x4D\xAB"          # 8  mov [ebp+var_20], cx
            b"\x90"                      # 12 no nop
            b"\xE8\xAB\xAB\xAB\xAB"      # 13 call near ptr 35B507Ch
            b"\x8A\x45\xAB"              # 18 mov al, [ebp+arg_4]
            b"\x88\x45\xAB"              # 21 mov [ebp+var_1E], al
            b"\x83\xC4\xAB"              # 24 add esp, 0Ch
            b"\x8D\x45\xAB"              # 27 lea eax, [ebp+var_20]
            b"\x50"                      # 30 push eax
            b"\x0F\xBF\x45\xAB"          # 31 movsx eax, [ebp+var_20]
            b"\x50"                      # 35 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 36 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 41 mov ecx, eax
            b"\xE8"                      # 43 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 43,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 37,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8E0h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+var_38], ax
            b"\x8B\x47\xAB"              # 9  mov eax, [edi+34h]
            b"\x83\xC0\xAB"              # 12 add eax, 4
            b"\x50"                      # 15 push eax
            b"\x8D\x45\xAB"              # 16 lea eax, [ebp+var_35]
            b"\x6A\xAB"                  # 19 push 18h
            b"\x50"                      # 21 push eax
            b"\x90"                      # 22 no nop
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call near ptr 35B507Ch
            b"\x8D\x47\xAB"              # 28 lea eax, [edi+1Ch]
            b"\x50"                      # 31 push eax
            b"\x8D\x45\xAB"              # 32 lea eax, [ebp+var_1D]
            b"\x6A\xAB"                  # 35 push 18h
            b"\x50"                      # 37 push eax
            b"\x90"                      # 38 no nop
            b"\xE8\xAB\xAB\xAB\xAB"      # 39 call near ptr 35B507Ch
            b"\x8B\x45\xAB"              # 44 mov eax, [ebp+arg_0]
            b"\x83\xC4\xAB"              # 47 add esp, 18h
            b"\x88\x45\xAB"              # 50 mov [ebp+var_36], al
            b"\x83\xAB\xAB"              # 53 cmp eax, 2
            b"\x75\x04"                  # 56 jnz short loc_7ED871
            b"\xC6\x47\xAB\xAB"          # 58 mov byte ptr [edi+38h], 0
            b"\x8D\x45\xAB"              # 62 lea eax, [ebp+var_38]
            b"\x50"                      # 65 push eax
            b"\x0F\xBF\x45\xAB"          # 66 movsx eax, [ebp+var_38]
            b"\x50"                      # 70 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 71 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 76 mov ecx, eax
            b"\xE8"                      # 78 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 78,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 72,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8D7h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+var_20], ax
            b"\x66\x8B\x45\xAB"          # 9  mov ax, [ebp+arg_0]
            b"\x6A\xAB"                  # 13 push 17h
            b"\x51"                      # 15 push ecx
            b"\x66\x89\x45\xAB"          # 16 mov [ebp+var_1E], ax
            b"\x8D\x45\xAB"              # 20 lea eax, [ebp+var_1C]
            b"\x6A\xAB"                  # 23 push 18h
            b"\x50"                      # 25 push eax
            b"\x90"                      # 26 no nop
            b"\xE8\xAB\xAB\xAB\xAB"      # 27 call near ptr 35B3F41h
            b"\x83\xC4\xAB"              # 32 add esp, 10h
            b"\x8D\x45\xAB"              # 35 lea eax, [ebp+var_20]
            b"\x50"                      # 38 push eax
            b"\x0F\xBF\x45\xAB"          # 39 movsx eax, [ebp+var_20]
            b"\x50"                      # 43 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 44 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 49 mov ecx, eax
            b"\xE8"                      # 51 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 51,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 45,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8DAh
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+var_20], ax
            b"\x8B\x41\xAB"              # 9  mov eax, [ecx+34h]
            b"\x83\xC0\xAB"              # 12 add eax, 4
            b"\x50"                      # 15 push eax
            b"\x8D\x45\xAB"              # 16 lea eax, [ebp+var_1E]
            b"\x6A\xAB"                  # 19 push 18h
            b"\x50"                      # 21 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 22 call near ptr 35B507Ch
            b"\x90"                      # 27 no nop
            b"\x83\xC4\xAB"              # 28 add esp, 0Ch
            b"\x8D\x45\xAB"              # 31 lea eax, [ebp+var_20]
            b"\x50"                      # 34 push eax
            b"\x0F\xBF\x45\xAB"          # 35 movsx eax, [ebp+var_20]
            b"\x50"                      # 39 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 40 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 45 mov ecx, eax
            b"\xE8"                      # 47 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 47,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 41,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 90Ah
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+var_20], ax
            b"\x8B\x41\xAB"              # 9  mov eax, [ecx+34h]
            b"\x83\xC0\xAB"              # 12 add eax, 4
            b"\x50"                      # 15 push eax
            b"\x8D\x45\xAB"              # 16 lea eax, [ebp+var_1E]
            b"\x6A\xAB"                  # 19 push 18h
            b"\x50"                      # 21 push eax
            b"\x90"                      # 22 no nop
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call near ptr 35B507Ch
            b"\x83\xC4\xAB"              # 28 add esp, 0Ch
            b"\x8D\x45\xAB"              # 31 lea eax, [ebp+var_20]
            b"\x50"                      # 34 push eax
            b"\x0F\xBF\x45\xAB"          # 35 movsx eax, [ebp+var_20]
            b"\x50"                      # 39 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 40 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 45 mov ecx, eax
            b"\xE8"                      # 47 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 47,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 41,
        }
    ],
    # 2016-04-27
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 362h
            b"\xC7\x87\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [edi+2E4h], 1
            b"\x66\x89\x9D\xAB\xAB\xAB\xAB"  # 15 mov word ptr [ebp+var1+2], bx
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 22 mov [ebp+var_10C0], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 29 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 34 mov ecx, eax
            b"\xE8"                      # 36 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 36,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 30,
        }
    ],
    # 2016-04-27
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0E8h
            b"\xC7\x87\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [edi+104h], 1
            b"\x66\x89\x9D\xAB\xAB\xAB\xAB"  # 15 mov word ptr [ebp+var1+2], bx
            b"\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 27 mov ecx, eax
            b"\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 802h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var1+4], ax
            b"\x8D\x51\xAB"              # 12 lea edx, [ecx+1]
            b"\xEB\x03"                  # 15 jmp short loc_954840
            b"\xAB\xAB\xAB"              # 17 align 10h
            b"\x8A\x01"                  # 20 mov al, [ecx]
            b"\x41"                      # 22 inc ecx
            b"\x84\xC0"                  # 23 test al, al
            b"\x75\xF9"                  # 25 jnz short loc_954840
            b"\x2B\xCA"                  # 27 sub ecx, edx
            b"\x83\xF9\xAB"              # 29 cmp ecx, 18h
            b"\x0F\x83\xAB\xAB\xAB\xAB"  # 32 jnb loc_95471E
            b"\x6A\xAB"                  # 38 push 18h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 40 lea eax, [ebp+var_1118+6]
            b"\x53"                      # 46 push ebx
            b"\x50"                      # 47 push eax
            b"\x90"                      # 48 no nop
            b"\xE8\xAB\xAB\xAB\xAB"      # 49 call near ptr 35B9119h
            b"\x83\xC4\xAB"              # 54 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 57 lea eax, [ebp+var_1118+4]
            b"\x50"                      # 63 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 64 movsx eax, [ebp+var1+4]
            b"\x50"                      # 71 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 72 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 77 mov ecx, eax
            b"\xE8"                      # 79 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 79,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 73,
        }
    ],
    # 2016-04-27
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 9EFh
            b"\x66\x0F\x13\x45\xAB"      # 5  movlpd [ebp+var_D], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"      # 10 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 15 mov ecx, eax
            b"\xE8"                      # 17 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 17,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 11,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1DDh
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+var_34], ax
            b"\xA1\xAB\xAB\xAB\xAB"      # 9  mov eax, dword_EE39A0
            b"\x66\x0F\xD6\x45\xAB"      # 14 movq [ebp+var_32+4], xmm0
            b"\xF3\x0F\x7E\xAB\xAB\xAB\xAB\xAB"  # 19 movq xmm0, ptr [esi+180h]
            b"\x89\x45\xAB"              # 27 mov dword ptr [ebp+var_32], eax
            b"\x66\x0F\xD6\x45\xAB"      # 30 movq [ebp+var_26], xmm0
            b"\xF3\x0F\x7E\xAB\xAB\xAB\xAB\xAB"  # 35 movq xmm0, ptr [esi+188h]
            b"\x8D\x45\xAB"              # 43 lea eax, [ebp+var_16]
            b"\x50"                      # 46 push eax
            b"\x66\x0F\xD6\x45\xAB"      # 47 movq [ebp+var_1E], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"      # 52 call sub_8C5960
            b"\xE8\xAB\xAB\xAB\xAB"      # 57 call sub_9BCCF0
            b"\x88\x45\xAB"              # 62 mov [ebp+var_6], al
            b"\x8D\x45\xAB"              # 65 lea eax, [ebp+var_34]
            b"\x50"                      # 68 push eax
            b"\x0F\xBF\x45\xAB"          # 69 movsx eax, [ebp+var_34]
            b"\x50"                      # 73 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 74 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 79 mov ecx, eax
            b"\xE8"                      # 81 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 81,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 75,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xBE\xAB\xAB\x00\x00"      # 0  mov esi, 64h
            b"\x66\x89\xB5\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_2B4], si
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 12 mov [ebp+var_2B2], eax
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 18 movq [ebp+var_29E], xmm0
            b"\x83\xF9\xAB"              # 26 cmp ecx, 7
            b"\x75\x49"                  # 29 jnz short loc_9C0212
            b"\x6A\xAB"                  # 31 push 18h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 33 lea eax, [ebp+var_2F2+2]
            b"\x50"                      # 39 push eax
            b"\x8D\x87\xAB\xAB\xAB\xAB"  # 40 lea eax, [edi+138h]
            b"\x50"                      # 46 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 47 call sub_784EA0
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 52 movq xmm0, [ebp+var_2F2+2
            b"\x66\x8B\xB5\xAB\xAB\xAB\xAB"  # 60 mov si, [ebp+var_2B4]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 67 movq [ebp+var_296], xmm0
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 75 movq xmm0, [ebp+var_2EA+2
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 83 movq [ebp+var_28E], xmm0
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 91 movq xmm0, [ebp+var_2E2+2
            b"\x83\xC4\xAB"              # 99 add esp, 0Ch
            b"\xEB\x28"                  # 102 jmp short loc_9C023A
            b"\xF3\x0F\x7E\x87\xAB\xAB\xAB\xAB"  # 104 movq xmm0, [edi+138h]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 112 movq [ebp+var1], xmm0
            b"\xF3\x0F\x7E\x87\xAB\xAB\xAB\xAB"  # 120 movq xmm0, [edi+140h]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 128 movq [ebp+var2], xmm0
            b"\xF3\x0F\x7E\x87\xAB\xAB\xAB\xAB"  # 136 movq xmm0, [edi+148h]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 144 movq [ebp+var_286], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"      # 152 call sub_9BCCF0
            b"\x88\x85\xAB\xAB\xAB\xAB"  # 157 mov [ebp+var_27E], al
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 163 lea eax, [ebp+var_2B4]
            b"\x50"                      # 169 push eax
            b"\x0F\xBF\xC6"              # 170 movsx eax, si
            b"\x50"                      # 173 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 174 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 179 mov ecx, eax
            b"\xE8"                      # 181 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 181,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 175,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1FBh
            b"\x6A\xAB"                  # 5  push 32h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 7  mov [ebp+var_2B4], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 14 lea eax, [ebp+var_2AE]
            b"\x56"                      # 20 push esi
            b"\x50"                      # 21 push eax
            b"\xFF\xD3"                  # 22 call ebx
            b"\x83\xC4\xAB"              # 24 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 27 lea eax, [ebp+var_2B4]
            b"\x50"                      # 33 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 34 movsx eax, [ebp+var_2B4]
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 41 mov [ebp+var_27D], 0
            b"\x50"                      # 48 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 49 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 54 mov ecx, eax
            b"\xE8"                      # 56 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 56,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 50,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 22Dh
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_274], ax
            b"\xFF\xD3"                  # 12 call ebx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 14 mov dword ptr [ebp+var_268+2], ea
            b"\xA1\xAB\xAB\xAB\xAB"      # 20 mov eax, dword_F3E874
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 25 mov [ebp+var_272], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 31 mov eax, dword_F3E878
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 36 mov [ebp+var_26E], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 42 mov eax, dword_F3E138
            b"\xB9\xAB\xAB\xAB\xAB"      # 47 mov ecx, offset dword_F3D670
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 52 mov [ebp-26Ah], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 58 call sub_A1BFC0
            b"\x88\x85\xAB\xAB\xAB\xAB"  # 63 mov byte ptr [ebp+var_268+6], al
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 69 lea eax, [ebp+var_274]
            b"\x50"                      # 75 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 76 movsx eax, [ebp+var_274]
            b"\x50"                      # 83 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 84 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 89 mov ecx, eax
            b"\xE8"                      # 91 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 91,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 85,
        }
    ],
    # 2009-01-20
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 28Dh
            b"\x89\x15\xAB\xAB\xAB\xAB"  # 5  mov ds:dword_813538, edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2009-01-20
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 228h
            b"\x89\x4C\x24\xAB"          # 5  mov [esp+424h+var_40E], ecx
            b"\x89\x54\x24\xAB"          # 9  mov [esp+424h+var_40A], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 13 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 18 mov ecx, eax
            b"\xE8"                      # 20 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 20,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 14,
        }
    ],
    # 2009-01-20
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 2F1h
            b"\x52"                      # 5  push edx
            b"\x51"                      # 6  push ecx
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 7  mov ptr [esp+7Ch+A], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 20 mov ecx, eax
            b"\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2009-01-20
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0D3h
            b"\x89\x4C\x24\xAB"          # 5  mov [esp+34h+var_20+2], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset off_813544
            b"\x66\x89\x44\x24\xAB"      # 14 mov word ptr [esp+34h+var_20], ax
            b"\xC6\x44\x24\xAB\xAB"      # 19 mov byte ptr [esp+34h+var_1C+2],
            b"\xE8\xAB\xAB\xAB\xAB"      # 24 call sub_54A790
            b"\x0F\xBF\x4C\x24\xAB"      # 29 movsx ecx, ptr [esp+34h+var_20]
            b"\x8B\xD0"                  # 34 mov edx, eax
            b"\x33\x54\x24\xAB"          # 36 xor edx, [esp+34h+var_20+2]
            b"\x04\x37"                  # 40 add al, 37h
            b"\x30\x44\x24\xAB"          # 42 xor ptr [esp+34h+var_1C+2], al
            b"\x8D\x44\x24\xAB"          # 46 lea eax, [esp+34h+var_20]
            b"\x50"                      # 50 push eax
            b"\x81\xF2\xAB\xAB\xAB\xAB"  # 51 xor edx, 75AAh
            b"\x51"                      # 57 push ecx
            b"\x89\x54\x24\xAB"          # 58 mov [esp+3Ch+var_20+2], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 62 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 67 mov ecx, eax
            b"\xE8"                      # 69 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 69,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 63,
        }
    ],
    # 2009-01-20
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 89h
            b"\x50"                      # 5  push eax
            b"\x8B\xCE"                  # 6  mov ecx, esi
            b"\x66\x89\x54\x24\xAB"      # 8  mov [esp+24h+var_10], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 13 call sub_584020
            b"\xFF\xD7"                  # 18 call edi
            b"\x0F\xBF\x54\x24\xAB"      # 20 movsx edx, [esp+1Ch+var_10]
            b"\x8D\x4C\x24\xAB"          # 25 lea ecx, [esp+1Ch+var_10]
            b"\x51"                      # 29 push ecx
            b"\x52"                      # 30 push edx
            b"\x89\x44\x24\xAB"          # 31 mov [esp+24h+var_9], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 35 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 40 mov ecx, eax
            b"\xE8"                      # 42 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 42,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 36,
        }
    ],
    # 2009-01-20
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8Ch
            b"\x51"                      # 5  push ecx
            b"\x8B\xCE"                  # 6  mov ecx, esi
            b"\x66\x89\x44\x24\xAB"      # 8  mov word ptr [esp+34h+var_14], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 13 call sub_584020
            b"\x0F\xBF\x44\x24\xAB"      # 18 movsx eax, ptr [esp+2Ch+var_14]
            b"\x8D\x54\x24\xAB"          # 23 lea edx, [esp+2Ch+var_14]
            b"\x52"                      # 27 push edx
            b"\x50"                      # 28 push eax
            b"\x89\x5C\x24\xAB"          # 29 mov [esp+34h+var_A], ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 33 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 38 mov ecx, eax
            b"\xE8"                      # 40 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 40,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 34,
        }
    ],
    # 2009-01-20
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 116h
            b"\x50"                      # 5  push eax
            b"\x8B\xCD"                  # 6  mov ecx, ebp
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 8  mov [ebp+30Ch], 1
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 18 mov [esp+1414h+var1], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 26 call sub_584020
            b"\x6A\x07"                  # 31 push 7
            b"\x8D\x8C\x24\xAB\xAB\xAB\xAB"  # 33 lea ecx, [esp+30Ch]
            b"\x51"                      # 40 push ecx
            b"\x8B\xCD"                  # 41 mov ecx, ebp
            b"\xE8\xAB\xAB\xAB\xAB"      # 43 call sub_584020
            b"\x0F\xBF\x8C\x24\xAB\xAB\xAB\xAB"  # 48 movsx ecx, [esp+var2]
            b"\x66\x8B\x54\x24\xAB"      # 56 mov dx, [esp+140Ch+var_13F8]
            b"\x8D\x84\x24\xAB\xAB\xAB\xAB"  # 61 lea eax, [esp+140Ch+var1]
            b"\x50"                      # 68 push eax
            b"\x51"                      # 69 push ecx
            b"\x66\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 70 mov [esp+1414h+var1+6], b
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 78 mov [esp+317h], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 86 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 91 mov ecx, eax
            b"\xE8"                      # 93 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 93,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 87,
        }
    ],
    # 2009-01-20
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 41Fh
            b"\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset off_813544
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 10 mov [esp+var3], dx
            b"\x89\x84\x24\xAB\xAB\xAB\xAB"  # 18 mov [esp+var3+4], eax
            b"\x66\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 25 mov [esp+var3+2], bx
            b"\xE8\xAB\xAB\xAB\xAB"      # 33 call sub_54A790
            b"\x8B\x8C\x24\xAB\xAB\xAB\xAB"  # 38 mov ecx, [esp+var3+4]
            b"\x33\xC8"                  # 45 xor ecx, eax
            b"\x05\xAB\xAB\xAB\xAB"      # 47 add eax, 3B6Ch
            b"\x66\x31\x84\x24\xAB\xAB\xAB\xAB"  # 52 xor [esp+var3+2], ax
            b"\x0F\xBF\x84\x24\xAB\xAB\xAB\xAB"  # 60 movsx eax, [esp+var3]
            b"\x8D\x94\x24\xAB\xAB\xAB\xAB"  # 68 lea edx, [esp+var3]
            b"\x52"                      # 75 push edx
            b"\x81\xF1\xAB\xAB\xAB\xAB"  # 76 xor ecx, 3643h
            b"\x50"                      # 82 push eax
            b"\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 83 mov [esp+1414h+var1+4], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 90 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 95 mov ecx, eax
            b"\xE8"                      # 97 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 97,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 91,
        }
    ],
    # 2009-01-20
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 409h
            b"\x89\x84\x24\xAB\xAB\xAB\xAB"  # 5  mov [esp+1414h+var1+2], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2009-01-20
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 1F9h
            b"\x51"                      # 5  push ecx
            b"\x52"                      # 6  push edx
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 7  mov ptr [esp+A+var1], dx
            b"\x89\x84\x24\xAB\xAB\xAB\xAB"  # 15 mov ptr [esp+A+var1+2], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 27 mov ecx, eax
            b"\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2009-01-20
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 288h
            b"\x6A\xAB"                  # 5  push 0
            b"\x8D\x94\x24\xAB\xAB\xAB\xAB"  # 7  lea edx, [esp+A+var_10F0]
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 14 mov [esp+A+var1], cx
            b"\x52"                      # 22 push edx
            b"\xB9\xAB\xAB\xAB\xAB"      # 23 mov ecx, offset dword_8106C0
            b"\xE8\xAB\xAB\xAB\xAB"      # 28 call sub_69ADD0
            b"\xC7\x84\x24\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 33 mov[esp+v],34
            b"\x83\xBC\x24\xAB\xAB\xAB\xAB\xAB"  # 44 cmp [esp+var6], 10h
            b"\x66\x8B\x84\x24\xAB\xAB\xAB\xAB"  # 52 mov ax, ptr [esp+var5]
            b"\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 60 mov [esp+var4], ax
            b"\x8B\x84\x24\xAB\xAB\xAB\xAB"  # 68 mov eax, [esp+140Ch+var_10C0]
            b"\x73\x07"                  # 75 jnb short loc_5A3553
            b"\x8D\x84\x24\xAB\xAB\xAB\xAB"  # 77 lea eax, [esp+140Ch+var_10C0]
            b"\x50"                      # 84 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 85 call ds:dword_70D3B8
            b"\x0F\xBF\x94\x24\xAB\xAB\xAB\xAB"  # 91 movsx edx, [esp+A+var1]
            b"\x83\xC4\xAB"              # 99 add esp, 4
            b"\x8D\x8C\x24\xAB\xAB\xAB\xAB"  # 102 lea ecx, [esp+var3]
            b"\x51"                      # 109 push ecx
            b"\x52"                      # 110 push edx
            b"\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 111 mov [esp+var3], ax
            b"\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 119 mov [esp+1414h+var1+6], ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 126 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 131 mov ecx, eax
            b"\xE8"                      # 133 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 133,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 127,
        }
    ],
    # 2009-01-20
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 2BAh
            b"\x66\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 5  mov ptr [esp+var1], bx
            b"\xE8\xAB\xAB\xAB\xAB"      # 13 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 18 mov ecx, eax
            b"\xE8"                      # 20 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 20,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 14,
        }
    ],
    # 2009-01-20
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 2D8h
            b"\x50"                      # 5  push eax
            b"\x52"                      # 6  push edx
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 7  mov ptr [esp+var1], dx
            b"\xC7\x84\x24\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 15 mov [esp+v],0
            b"\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 26 mov ptr [esp+var1+6], ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 33 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 38 mov ecx, eax
            b"\xE8"                      # 40 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 40,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 34,
        }
    ],
    # 2009-01-20
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 345h
            b"\x89\x47\xAB"              # 5  mov [edi+1Ch], eax
            b"\x66\x89\x4C\x24\xAB"      # 8  mov [esp+60h+var_54], cx
            b"\xFF\xD6"                  # 13 call esi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 15 mov ecx, ds:dword_811734
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 21 mov edx, ds:dword_811F4C
            b"\x89\x44\x24\xAB"          # 27 mov [esp+60h+var_52], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 31 mov eax, ds:dword_811F50
            b"\x89\x4C\x24\xAB"          # 36 mov [esp+60h+var_4A], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 40 mov ecx, offset dword_8106C0
            b"\x89\x54\x24\xAB"          # 45 mov dword ptr [esp+var3], edx
            b"\x89\x44\x24\xAB"          # 49 mov [esp+60h+var_4E], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call sub_6BD050
            b"\x8B\x54\x24\xAB"          # 58 mov edx, dword ptr [esp+var2]
            b"\x33\x54\x24\xAB"          # 62 xor edx, [esp+60h+var_52]
            b"\x88\x44\x24\xAB"          # 66 mov byte ptr [esp+60h+var_46], al
            b"\x8B\x44\x24\xAB"          # 70 mov eax, [esp+60h+var_4A]
            b"\x33\x44\x24\xAB"          # 74 xor eax, [esp+60h+var_4E]
            b"\x81\xF2\xAB\xAB\xAB\xAB"  # 78 xor edx, 2290h
            b"\x89\x54\x24\xAB"          # 84 mov dword ptr [esp+var1], edx
            b"\x0F\xBF\x54\x24\xAB"      # 88 movsx edx, [esp+60h+var_54]
            b"\x8D\x4C\x24\xAB"          # 93 lea ecx, [esp+60h+var_54]
            b"\x51"                      # 97 push ecx
            b"\x35\xAB\xAB\xAB\xAB"      # 98 xor eax, 2290h
            b"\x52"                      # 103 push edx
            b"\x89\x44\x24\xAB"          # 104 mov [esp+68h+var_4E], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 108 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 113 mov ecx, eax
            b"\xE8"                      # 115 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 115,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 109,
        }
    ],
    # 2009-01-20
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 65h
            b"\x66\x89\x44\x24\xAB"      # 5  mov ptr [esp+164h+var_14C], ax
            b"\xA1\xAB\xAB\xAB\xAB"      # 10 mov eax, ds:dword_811734
            b"\x89\x4C\x24\xAB"          # 15 mov [esp+164h+var_142], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 19 mov ecx, offset dword_8106C0
            b"\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 24 mov ds:A, 1
            b"\x89\x54\x24\xAB"          # 34 mov [esp+164h+var_14C+2], edx
            b"\x89\x44\x24\xAB"          # 38 mov [esp+164h+var_146], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 42 call sub_6BD050
            b"\x88\x44\x24\xAB"          # 47 mov ptr [esp+164h+var_13E+2], al
            b"\x0F\xBF\x44\x24\xAB"      # 51 movsx eax, ptr [esp+164h+var_14C]
            b"\x8D\x54\x24\xAB"          # 56 lea edx, [esp+164h+var_14C]
            b"\x52"                      # 60 push edx
            b"\x50"                      # 61 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 62 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 67 mov ecx, eax
            b"\xE8"                      # 69 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 69,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 63,
        }
    ],
    # 2009-01-20
    [
        (
            b"\x6A\xAB"                  # 0  push 67h
            b"\x66\x89\x4C\x24\xAB"      # 2  mov [esp+55h], cx
            b"\x66\x89\x54\x24\xAB"      # 7  mov word ptr [esp+16Ch+A+2], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2009-01-20
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 66h
            b"\x51"                      # 5  push ecx
            b"\x52"                      # 6  push edx
            b"\x66\x89\x54\x24\xAB"      # 7  mov word ptr [esp+16Ch+var1], dx
            b"\x88\x44\x24\xAB"          # 12 mov byte ptr [esp+16Ch+var2], al
            b"\xE8\xAB\xAB\xAB\xAB"      # 16 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 21 mov ecx, eax
            b"\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 17,
        }
    ],
    # 2009-01-20
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 345h
            b"\x83\xC4\xAB"              # 5  add esp, 4
            b"\x66\x89\x44\x24\xAB"      # 8  mov ptr [esp+164h+var_14C], ax
            b"\xFF\xD7"                  # 13 call edi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 15 mov ecx, ds:dword_811F4C
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 21 mov edx, ds:dword_811F50
            b"\x89\x44\x24\xAB"          # 27 mov [esp+164h+var_14C+2], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 31 mov eax, ds:dword_811734
            b"\x89\x4C\x24\xAB"          # 36 mov ptr [esp+164h+var_13E+1], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 40 mov ecx, offset dword_8106C0
            b"\x89\x54\x24\xAB"          # 45 mov [esp+164h+var_146], edx
            b"\x89\x44\x24\xAB"          # 49 mov [esp+164h+var_142], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call sub_6BD050
            b"\x8B\x4C\x24\xAB"          # 58 mov ecx, ptr [esp+164h+var_13E+1]
            b"\x33\x4C\x24\xAB"          # 62 xor ecx, [esp+164h+var_14C+2]
            b"\x8B\x54\x24\xAB"          # 66 mov edx, [esp+164h+var_142]
            b"\x33\x54\x24\xAB"          # 70 xor edx, [esp+164h+var_146]
            b"\x81\xF1\xAB\xAB\xAB\xAB"  # 74 xor ecx, 2290h
            b"\x88\x44\x24\xAB"          # 80 mov ptr [esp+164h+var_13E], al
            b"\x89\x4C\x24\xAB"          # 84 mov ptr [esp+164h+var_13E+1], ecx
            b"\x0F\xBF\x4C\x24\xAB"      # 88 movsx ecx, ptr [esp+164h+var_14C]
            b"\x8D\x44\x24\xAB"          # 93 lea eax, [esp+164h+var_14C]
            b"\x50"                      # 97 push eax
            b"\x81\xF2\xAB\xAB\xAB\xAB"  # 98 xor edx, 2290h
            b"\x51"                      # 104 push ecx
            b"\x89\x54\x24\xAB"          # 105 mov [esp+16Ch+var_146], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 109 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 114 mov ecx, eax
            b"\xE8"                      # 116 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 116,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 110,
        }
    ],
    # 2009-01-20
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 3F2h
            b"\x89\x7C\x24\xAB"          # 5  mov [esp+38h+var_E], edi
            b"\x88\x54\x24\xAB"          # 9  mov [esp+38h+var_A], dl
            b"\x88\x44\x24\xAB"          # 13 mov [esp+38h+var_9], al
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 22 mov ecx, eax
            b"\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2008-10-22
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_24], 8Ch
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_581840
            b"\x0F\xBF\x45\xAB"          # 11 movsx eax, [ebp+var_24]
            b"\x8B\x4D\xAB"              # 15 mov ecx, [ebp+arg_0]
            b"\x8D\x55\xAB"              # 18 lea edx, [ebp+var_24]
            b"\x52"                      # 21 push edx
            b"\x50"                      # 22 push eax
            b"\x89\x4D\xAB"              # 23 mov [ebp+var_1A], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 26 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 31 mov ecx, eax
            b"\xE8"                      # 33 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 33,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 27,
        }
    ],
    # 2008-10-22
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_20], 0A2h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_581840
            b"\x0F\xBF\x4D\xAB"          # 11 movsx ecx, [ebp+var_20]
            b"\x8D\x45\xAB"              # 15 lea eax, [ebp+var_20]
            b"\x89\x75\xAB"              # 18 mov [ebp+var_16], esi
            b"\x50"                      # 21 push eax
            b"\x51"                      # 22 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 28 mov ecx, eax
            b"\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # 2008-10-22
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_28], 0A2h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_581840
            b"\x0F\xBF\x55\xAB"          # 11 movsx edx, [ebp+var_28]
            b"\x8D\x4D\xAB"              # 15 lea ecx, [ebp+var_28]
            b"\x89\x5D\xAB"              # 18 mov [ebp+var_1E], ebx
            b"\x51"                      # 21 push ecx
            b"\x52"                      # 22 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 28 mov ecx, eax
            b"\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # 2008-10-22
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_10], 16Ah
            b"\x89\x45\xAB"              # 6  mov [ebp+var_E], eax
            b"\xC6\x45\xAB\xAB"          # 9  mov [ebp+var_A], 1
            b"\xE8\xAB\xAB\xAB\xAB"      # 13 call sub_538DB0
            b"\x8A\x5D\xAB"              # 18 mov bl, [ebp+var_A]
            b"\x8B\x75\xAB"              # 21 mov esi, [ebp+var_E]
            b"\x8B\xC8"                  # 24 mov ecx, eax
            b"\x04\xAB"                  # 26 add al, 6Ch
            b"\x32\xD8"                  # 28 xor bl, al
            b"\x33\xCE"                  # 30 xor ecx, esi
            b"\x0F\xBF\x45\xAB"          # 32 movsx eax, [ebp+var_10]
            b"\x8D\x55\xAB"              # 36 lea edx, [ebp+var_10]
            b"\x81\xF1\xAB\xAB\xAB\xAB"  # 39 xor ecx, 0FEF4h
            b"\x52"                      # 45 push edx
            b"\x50"                      # 46 push eax
            b"\x89\x4D\xAB"              # 47 mov [ebp+var_E], ecx
            b"\x88\x5D\xAB"              # 50 mov [ebp+var_A], bl
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 58 mov ecx, eax
            b"\xE8"                      # 60 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 60,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 54,
        }
    ],
    # 2008-10-22
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 334h
            b"\x66\x89\x55\xAB"          # 5  mov word ptr [ebp+arg_4+2], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 9  call CRagConnection_instanceR
            b"\x8B\xC8"                  # 14 mov ecx, eax
            b"\xE8"                      # 16 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 16,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 10,
        }
    ],
    # 2008-10-22
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_2C], 116h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_581840
            b"\x8D\x4D\xAB"              # 11 lea ecx, [ebp+var_28+4]
            b"\x6A\xAB"                  # 14 push 7
            b"\x51"                      # 16 push ecx
            b"\x8B\xCB"                  # 17 mov ecx, ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 19 call sub_581840
            b"\x0F\xBF\x4D\xAB"          # 24 movsx ecx, [ebp+var_2C]
            b"\x66\x8B\x55\xAB"          # 28 mov dx, word ptr [ebp+arg_4+4]
            b"\x8D\x45\xAB"              # 32 lea eax, [ebp+var_2C]
            b"\x50"                      # 35 push eax
            b"\x51"                      # 36 push ecx
            b"\x66\x89\x75\xAB"          # 37 mov word ptr [ebp+var_28+2], si
            b"\x66\x89\x55\xAB"          # 41 mov word ptr [ebp+var_28+0Bh], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 45 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 50 mov ecx, eax
            b"\xE8"                      # 52 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 52,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 46,
        }
    ],
    # 2008-10-22
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_18+4], 3F2h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_571D70
            b"\xB9\xAB\xAB\xAB\xAB"      # 11 mov ecx, offset off_7CB2C4
            b"\x89\x45\xAB"              # 16 mov dword ptr [ebp+var_18+8], eax
            b"\x66\x89\x75\xAB"          # 19 mov word ptr [ebp+var_18+6], si
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call sub_538DB0
            b"\x8B\x4D\xAB"              # 28 mov ecx, dword ptr [ebp+var_18+8]
            b"\x8B\xD0"                  # 31 mov edx, eax
            b"\x05\xAB\xAB\xAB\xAB"      # 33 add eax, 11A4h
            b"\x33\xD1"                  # 38 xor edx, ecx
            b"\x66\x31\x45\xAB"          # 40 xor word ptr [ebp+var_18+6], ax
            b"\x8D\x45\xAB"              # 44 lea eax, [ebp+var_18+4]
            b"\x0F\xBF\x4D\xAB"          # 47 movsx ecx, word ptr [ebp+var_18+4
            b"\x81\xF2\xAB\xAB\xAB\xAB"  # 51 xor edx, 5F8Ch
            b"\x50"                      # 57 push eax
            b"\x51"                      # 58 push ecx
            b"\x89\x55\xAB"              # 59 mov dword ptr [ebp+var_18+8], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 62 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 67 mov ecx, eax
            b"\xE8"                      # 69 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 69,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 63,
        }
    ],
    # 2008-10-22
    [
        (
            b"\xBF\xAB\xAB\x00\x00"      # 0  mov edi, 286h
            b"\x56"                      # 5  push esi
            b"\xB9\xAB\xAB\xAB\xAB"      # 6  mov ecx, offset byte_77F128
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call sub_59B620
            b"\x81\x38\xAB\xAB\xAB\xAB"  # 16 cmp dword ptr [eax], 0E1h
            b"\x75\xAB"                  # 22 jnz short loc_596A1A
            b"\xB9\xAB\xAB\xAB\xAB"      # 24 mov ecx, offset off_77EB18
            b"\xE8\xAB\xAB\xAB\xAB"      # 29 call sub_59AB90
            b"\x50"                      # 34 push eax
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 35 lea edx, [ebp+var_6A8]
            b"\x68\xAB\xAB\xAB\xAB"      # 41 push offset dword_70A93C
            b"\x52"                      # 46 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 47 call sub_6BA4E8
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 52 lea eax, [ebp+var_6A8]
            b"\x50"                      # 58 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 59 call sub_6BB387
            b"\x83\xC4\xAB"              # 64 add esp, 10h
            b"\x8D\x4D\xAB"              # 67 lea ecx, [ebp+arg_4]
            b"\x66\x89\x7D\xAB"          # 70 mov word ptr [ebp+arg_4], di
            b"\x66\x89\x45\xAB"          # 74 mov word ptr [ebp+arg_4+2], ax
            b"\x51"                      # 78 push ecx
            b"\x57"                      # 79 push edi
            b"\xE8\xAB\xAB\xAB\xAB"      # 80 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 85 mov ecx, eax
            b"\xE8"                      # 87 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 87,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 81,
        }
    ],
    # 2008-10-22
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_18], 288h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_669AB0
            b"\x66\x8B\x8D\xAB\xAB\xAB\xAB"  # 11 mov cx, word ptr [ebp+var_98]
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 18 mov [ebp+var_4], 41h
            b"\x66\x89\x4D\xAB"          # 25 mov word ptr [ebp+var_18+4], cx
            b"\x8D\x4D\xAB"              # 29 lea ecx, [ebp+var_7C]
            b"\xE8\xAB\xAB\xAB\xAB"      # 32 call sub_4BF830
            b"\x50"                      # 37 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 38 call sub_6BB387
            b"\x0F\xBF\x4D\xAB"          # 43 movsx ecx, word ptr [ebp+var_18]
            b"\x8B\x55\xAB"              # 47 mov edx, dword ptr [ebp+arg_4]
            b"\x83\xC4\xAB"              # 50 add esp, 4
            b"\x66\x89\x45\xAB"          # 53 mov word ptr [ebp+var_18+2], ax
            b"\x8D\x45\xAB"              # 57 lea eax, [ebp+var_18]
            b"\x50"                      # 60 push eax
            b"\x51"                      # 61 push ecx
            b"\x89\x55\xAB"              # 62 mov dword ptr [ebp+var_18+6], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 65 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 70 mov ecx, eax
            b"\xE8"                      # 72 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 72,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 66,
        }
    ],
    # 2008-10-22
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 2D8h
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var1], 0
            b"\x89\xBD\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_DE], edi
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 26 mov ecx, eax
            b"\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2008-10-22
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_48], 170h
            b"\xFF\xD7"                  # 6  call edi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 8  mov ecx, ds:dword_7C9F20
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 14 mov edx, ds:dword_7C9854
            b"\x89\x45\xAB"              # 20 mov [ebp+var_46], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 23 mov eax, ds:dword_7C9F1C
            b"\x89\x4D\xAB"              # 28 mov [ebp+var_3E], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 31 mov ecx, offset dword_7C8928
            b"\x89\x45\xAB"              # 36 mov [ebp+var_42], eax
            b"\x89\x55\xAB"              # 39 mov [ebp+var_3A], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 42 call sub_685E90
            b"\x8B\x7D\xAB"              # 47 mov edi, [ebp+var_46]
            b"\x8B\x4D\xAB"              # 50 mov ecx, [ebp+var_3A]
            b"\x8B\x55\xAB"              # 53 mov edx, [ebp+var_3E]
            b"\x88\x45\xAB"              # 56 mov [ebp+var_36], al
            b"\x8B\x45\xAB"              # 59 mov eax, [ebp+var_42]
            b"\x33\xCA"                  # 62 xor ecx, edx
            b"\x33\xC7"                  # 64 xor eax, edi
            b"\x8D\x55\xAB"              # 66 lea edx, [ebp+var_48]
            b"\x35\xAB\xAB\xAB\xAB"      # 69 xor eax, 3604h
            b"\x81\xF1\xAB\xAB\xAB\xAB"  # 74 xor ecx, 3604h
            b"\x89\x45\xAB"              # 80 mov [ebp+var_42], eax
            b"\x52"                      # 83 push edx
            b"\x0F\xBF\x45\xAB"          # 84 movsx eax, [ebp+var_48]
            b"\x50"                      # 88 push eax
            b"\x89\x4D\xAB"              # 89 mov [ebp+var_3E], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 92 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 97 mov ecx, eax
            b"\xE8"                      # 99 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 99,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 93,
        }
    ],
    # 2008-10-22
    [
        (
            b"\x66\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB"  # 0  mov [ebp+var_94], 64h
            b"\xF3\xA5"                  # 9  rep movsd
            b"\x8D\xB3\xAB\xAB\xAB\xAB"  # 11 lea esi, [ebx+120h]
            b"\xB9\xAB\xAB\xAB\xAB"      # 17 mov ecx, 6
            b"\x8D\x7D\xAB"              # 22 lea edi, [ebp+var_76]
            b"\xF3\xA5"                  # 25 rep movsd
            b"\xE8\xAB\xAB\xAB\xAB"      # 27 call sub_5DE5D0
            b"\x8D\x95\xAB\xAB\xAB\xAB"  # 32 lea edx, [ebp+var_94]
            b"\x88\x45\xAB"              # 38 mov [ebp+var_5E], al
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 41 movsx eax, [ebp+var_94]
            b"\x52"                      # 48 push edx
            b"\x50"                      # 49 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 50 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 55 mov ecx, eax
            b"\xE8"                      # 57 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 57,
            "packetId": (7, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 51,
        }
    ],
    # 2008-10-22
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_1C], 65h
            b"\x66\x89\x7D\xAB"          # 6  mov word ptr [ebp+var_E], di
            b"\x89\x45\xAB"              # 10 mov [ebp+var_1A], eax
            b"\x89\x55\xAB"              # 13 mov [ebp+var_12], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 16 call sub_685E90
            b"\x0F\xBF\x4D\xAB"          # 21 movsx ecx, [ebp+var_1C]
            b"\x88\x45\xAB"              # 25 mov [ebp+var_E+2], al
            b"\x8D\x45\xAB"              # 28 lea eax, [ebp+var_1C]
            b"\x50"                      # 31 push eax
            b"\x51"                      # 32 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 33 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 38 mov ecx, eax
            b"\xE8"                      # 40 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 40,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 34,
        }
    ],
    # 2008-10-22
    [
        (
            b"\x6A\xAB"                  # 0  push 66h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 2  mov word ptr [ebp+arg_0], 66h
            b"\x88\x55\xAB"              # 8  mov byte ptr [ebp+arg_0+2], dl
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2008-10-22
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_1C], 170h
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 6  call ds:dword_6E0440
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 12 mov ecx, ds:dword_7C9854
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 18 mov edx, ds:dword_7C9F1C
            b"\x89\x45\xAB"              # 24 mov [ebp+var_1A], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 27 mov eax, ds:dword_7C9F20
            b"\x89\x4D\xAB"              # 32 mov dword ptr [ebp+var_E], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 35 mov ecx, offset dword_7C8928
            b"\x89\x55\xAB"              # 40 mov [ebp+var_16], edx
            b"\x89\x45\xAB"              # 43 mov [ebp+var_12], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 46 call sub_685E90
            b"\x8B\x55\xAB"              # 51 mov edx, [ebp+var_16]
            b"\x8B\x7D\xAB"              # 54 mov edi, [ebp+var_1A]
            b"\x33\xD7"                  # 57 xor edx, edi
            b"\x88\x45\xAB"              # 59 mov [ebp+var_E+4], al
            b"\x8B\x45\xAB"              # 62 mov eax, dword ptr [ebp+var_E]
            b"\x81\xF2\xAB\xAB\xAB\xAB"  # 65 xor edx, 3604h
            b"\x89\x55\xAB"              # 71 mov [ebp+var_16], edx
            b"\x8B\x55\xAB"              # 74 mov edx, [ebp+var_12]
            b"\x33\xC2"                  # 77 xor eax, edx
            b"\x8D\x4D\xAB"              # 79 lea ecx, [ebp+var_1C]
            b"\x0F\xBF\x55\xAB"          # 82 movsx edx, [ebp+var_1C]
            b"\x35\xAB\xAB\xAB\xAB"      # 86 xor eax, 3604h
            b"\x51"                      # 91 push ecx
            b"\x52"                      # 92 push edx
            b"\x89\x45\xAB"              # 93 mov [ebp+var_12], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 96 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 101 mov ecx, eax
            b"\xE8"                      # 103 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 103,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 97,
        }
    ],
    # 2008-10-22
    [
        (
            b"\x6A\xAB"                  # 0  push 7Dh
            b"\x89\x75\xAB"              # 2  mov [ebp+var_6], esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 5  call CRagConnection_instanceR
            b"\x8B\xC8"                  # 10 mov ecx, eax
            b"\xE8"                      # 12 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 12,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 6,
        }
    ],
    # 2015-07-22
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A13h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+var_2C], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 9  call sub_4AEC20
            b"\x50"                      # 14 push eax
            b"\x8D\x45\xAB"              # 15 lea eax, [ebp+var_2A]
            b"\x6A\xAB"                  # 18 push 18h
            b"\x50"                      # 20 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call near ptr 32E507Ch
            b"\x90"                      # 26 no nop
            b"\x83\xC4\xAB"              # 27 add esp, 0Ch
            b"\x8D\x45\xAB"              # 30 lea eax, [ebp+var_2C]
            b"\x50"                      # 33 push eax
            b"\x0F\xBF\x45\xAB"          # 34 movsx eax, [ebp+var_2C]
            b"\x50"                      # 38 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 39 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 44 mov ecx, eax
            b"\xE8"                      # 46 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 46,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 40,
        }
    ],
    # 2015-07-22
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 9EEh
            b"\x89\x4D\xAB"              # 5  mov dword ptr [ebp+var_10+3], ecx
            b"\x89\x7D\xAB"              # 8  mov dword ptr [ebp+var_10+7], edi
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2015-07-22
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 9B0h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_29C], ax
            b"\xA1\xAB\xAB\xAB\xAB"      # 12 mov eax, dword_E1513C
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 17 mov [ebp-29Ah], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 23 lea eax, [ebp+var_290]
            b"\x50"                      # 29 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 30 call sub_49BC80
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 35 mov [ebp+var_4], 1
            b"\x83\x78\xAB\xAB"          # 42 cmp dword ptr [eax+14h], 10h
            b"\x72\x02"                  # 46 jb short loc_4F09B2
            b"\x8B\x00"                  # 48 mov eax, [eax]
            b"\x50"                      # 50 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 51 call near ptr 32F9001h
            b"\x90"                      # 56 no nop
            b"\x83\xC4\xAB"              # 57 add esp, 4
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 60 mov word ptr [ebp+var1], ax
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 67 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 74 lea ecx, [ebp+var_290]
            b"\xE8\xAB\xAB\xAB\xAB"      # 80 call sub_402620
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 85 lea eax, [ebp+var_29C]
            b"\x50"                      # 91 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 92 movsx eax, [ebp+var_29C]
            b"\x50"                      # 99 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 100 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 105 mov ecx, eax
            b"\xE8"                      # 107 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 107,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 101,
        }
    ],
    # 2015-07-22
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8E0h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+var_38], ax
            b"\x8B\x47\xAB"              # 9  mov eax, [edi+34h]
            b"\x83\xC0\xAB"              # 12 add eax, 4
            b"\x50"                      # 15 push eax
            b"\x8D\x45\xAB"              # 16 lea eax, [ebp+var_35]
            b"\x6A\xAB"                  # 19 push 18h
            b"\x50"                      # 21 push eax
            b"\x90"                      # 22 no nop
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call near ptr 32E507Ch
            b"\x8D\x47\xAB"              # 28 lea eax, [edi+1Ch]
            b"\x50"                      # 31 push eax
            b"\x8D\x45\xAB"              # 32 lea eax, [ebp+var_1D]
            b"\x6A\xAB"                  # 35 push 18h
            b"\x50"                      # 37 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 38 call near ptr 32E507Ch
            b"\x90"                      # 43 no nop
            b"\x8B\x45\xAB"              # 44 mov eax, [ebp+arg_0]
            b"\x83\xC4\xAB"              # 47 add esp, 18h
            b"\x88\x45\xAB"              # 50 mov [ebp+var_36], al
            b"\x83\xF8\xAB"              # 53 cmp eax, 2
            b"\x75\x04"                  # 56 jnz short loc_715A01
            b"\xC6\x47\xAB\xAB"          # 58 mov byte ptr [edi+38h], 0
            b"\x8D\x45\xAB"              # 62 lea eax, [ebp+var_38]
            b"\x50"                      # 65 push eax
            b"\x0F\xBF\x45\xAB"          # 66 movsx eax, [ebp+var_38]
            b"\x50"                      # 70 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 71 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 76 mov ecx, eax
            b"\xE8"                      # 78 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 78,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 72,
        }
    ],
    # 2015-07-22
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8D7h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+var_20], ax
            b"\x66\x8B\x45\xAB"          # 9  mov ax, [ebp+arg_0]
            b"\x6A\xAB"                  # 13 push 17h
            b"\x51"                      # 15 push ecx
            b"\x66\x89\x45\xAB"          # 16 mov [ebp+var_1E], ax
            b"\x8D\x45\xAB"              # 20 lea eax, [ebp+var_1C]
            b"\x6A\xAB"                  # 23 push 18h
            b"\x50"                      # 25 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 26 call near ptr 32E3F41h
            b"\x90"                      # 31 no nop
            b"\x83\xC4\xAB"              # 32 add esp, 10h
            b"\x8D\x45\xAB"              # 35 lea eax, [ebp+var_20]
            b"\x50"                      # 38 push eax
            b"\x0F\xBF\x45\xAB"          # 39 movsx eax, [ebp+var_20]
            b"\x50"                      # 43 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 44 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 49 mov ecx, eax
            b"\xE8"                      # 51 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 51,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 45,
        }
    ],
    # 2015-07-22
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 368h
            b"\x66\x89\x45\xAB"          # 5  mov word ptr [ebp+var_30+4], ax
            b"\x8D\x51\xAB"              # 9  lea edx, [ecx+1]
            b"\x8A\x01"                  # 12 mov al, [ecx]
            b"\x41"                      # 14 inc ecx
            b"\x84\xC0"                  # 15 test al, al
            b"\x75\xF9"                  # 17 jnz short loc_8843F1
            b"\x2B\xCA"                  # 19 sub ecx, edx
            b"\x83\xF9\x18"              # 21 cmp ecx, 18h
            b"\x0F\x83\xAB\xAB\xAB\xAB"  # 24 jnb loc_8842CE
            b"\x6A\xAB"                  # 30 push 18h
            b"\x8D\x45\xAB"              # 32 lea eax, [ebp+var_30+6]
            b"\x53"                      # 35 push ebx
            b"\x50"                      # 36 push eax
            b"\x90"                      # 37 no nop
            b"\xE8\xAB\xAB\xAB\xAB"      # 38 call near ptr 32E9119h
            b"\x83\xC4\xAB"              # 43 add esp, 0Ch
            b"\x8D\x45\xAB"              # 46 lea eax, [ebp+var_30+4]
            b"\x50"                      # 49 push eax
            b"\x0F\xBF\x45\xAB"          # 50 movsx eax, word ptr [ebp+var_30+4
            b"\x50"                      # 54 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 55 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 60 mov ecx, eax
            b"\xE8"                      # 62 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 62,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 56,
        }
    ],
    # 2015-07-22
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 213h
            b"\x66\x0F\xD6\x45\xAB"      # 5  movq [ebp+var_1C+2], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"      # 10 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 15 mov ecx, eax
            b"\xE8"                      # 17 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 17,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 11,
        }
    ],
    # 2015-07-22
    [
        (
            b"\xBE\xAB\xAB\x00\x00"      # 0  mov esi, 64h
            b"\x66\x89\xB5\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_2D8], si
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 12 mov [ebp+var_2D6], eax
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 18 movq [ebp+var_2C2], xmm0
            b"\x83\xF9\xAB"              # 26 cmp ecx, 7
            b"\x75\x3D"                  # 29 jnz short loc_8EE368
            b"\x6A\xAB"                  # 31 push 18h
            b"\x8D\x45\xAB"              # 33 lea eax, [ebp+var_2C]
            b"\x50"                      # 36 push eax
            b"\x8D\x87\xAB\xAB\xAB\xAB"  # 37 lea eax, [edi+138h]
            b"\x50"                      # 43 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 44 call sub_6ADE30
            b"\xF3\x0F\x7E\x45\xAB"      # 49 movq xmm0, [ebp+var_2C]
            b"\x66\x8B\xB5\xAB\xAB\xAB\xAB"  # 54 mov si, [ebp+var_2D8]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 61 movq [ebp+var_2BA], xmm0
            b"\xF3\x0F\x7E\x45\xAB"      # 69 movq xmm0, qword ptr [ebp-24h]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 74 movq [ebp+var_2B2], xmm0
            b"\xF3\x0F\x7E\x45\xAB"      # 82 movq xmm0, [ebp+var_22+6]
            b"\x83\xC4\xAB"              # 87 add esp, 0Ch
            b"\xEB\x28"                  # 90 jmp short loc_8EE390
            b"\xF3\x0F\x7E\x87\xAB\xAB\xAB\xAB"  # 92 movq xmm0, [edi+138h]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 100 movq [ebp+var_2BA], xmm0
            b"\xF3\x0F\x7E\x87\xAB\xAB\xAB\xAB"  # 108 movq xmm0, [edi+140h]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 116 movq [ebp+var_2B2], xmm0
            b"\xF3\x0F\x7E\x87\xAB\xAB\xAB\xAB"  # 124 movq xmm0, [edi+148h]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 132 movq [ebp+var_2AA], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"      # 140 call sub_8EAE80
            b"\x88\x85\xAB\xAB\xAB\xAB"  # 145 mov [ebp+var_2A2], al
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 151 lea eax, [ebp+var_2D8]
            b"\x50"                      # 157 push eax
            b"\x0F\xBF\xC6"              # 158 movsx eax, si
            b"\x50"                      # 161 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 162 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 167 mov ecx, eax
            b"\xE8"                      # 169 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 169,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 163,
        }
    ],
    # 2015-07-22
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 65h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_298], ax
            b"\x33\xC0"                  # 12 xor eax, eax
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 14 mov ptr [ebp+var_28C+2], ax
            b"\xA1\xAB\xAB\xAB\xAB"      # 21 mov eax, dword_E1513C
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 26 mov [ebp+var_296], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 32 mov eax, dword_E14A00
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 37 mov [ebp+var_292], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 43 mov eax, dword_E14A04
            b"\xB9\xAB\xAB\xAB\xAB"      # 48 mov ecx, offset dword_E13F38
            b"\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 53 mov A, 1
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 63 mov [ebp-28Eh], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 69 call sub_958E90
            b"\xB9\xAB\xAB\xAB\xAB"      # 74 mov ecx, offset dword_E13F38
            b"\xE8\xAB\xAB\xAB\xAB"      # 79 call sub_94A210
            b"\x88\x85\xAB\xAB\xAB\xAB"  # 84 mov byte ptr [ebp+var_28C+4], al
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 90 lea eax, [ebp+var_298]
            b"\x50"                      # 96 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 97 movsx eax, [ebp+var_298]
            b"\x50"                      # 104 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 105 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 110 mov ecx, eax
            b"\xE8"                      # 112 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 112,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 106,
        }
    ],
    # 2015-07-22
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1FBh
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_2D8], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 12 lea eax, [ebp+var_2D2]
            b"\x56"                      # 18 push esi
            b"\x50"                      # 19 push eax
            b"\xFF\xD3"                  # 20 call ebx
            b"\x83\xC4\xAB"              # 22 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 25 lea eax, [ebp+var_2D8]
            b"\x50"                      # 31 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 32 movsx eax, [ebp+var_2D8]
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 39 mov [ebp+var_2A1], 0
            b"\x50"                      # 46 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 47 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 52 mov ecx, eax
            b"\xE8"                      # 54 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 54,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 48,
        }
    ],
    # 2005-10-04
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_14], 89h
            b"\xFF\xD3"                  # 6  call ebx
            b"\x89\x45\xAB"              # 8  mov [ebp+var_D], eax
            b"\x8D\x55\xAB"              # 11 lea edx, [ebp+var_14]
            b"\x0F\xBF\x45\xAB"          # 14 movsx eax, [ebp+var_14]
            b"\x52"                      # 18 push edx
            b"\x50"                      # 19 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 25 mov ecx, eax
            b"\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2005-10-04
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_18], 89h
            b"\xFF\xD7"                  # 6  call edi
            b"\x0F\xBF\x4D\xAB"          # 8  movsx ecx, [ebp+var_18]
            b"\x89\x45\xAB"              # 12 mov [ebp+var_14+3], eax
            b"\x8D\x45\xAB"              # 15 lea eax, [ebp+var_18]
            b"\x50"                      # 18 push eax
            b"\x51"                      # 19 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 25 mov ecx, eax
            b"\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2005-10-04
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_C], 89h
            b"\xFF\xD6"                  # 6  call esi
            b"\x89\x45\xAB"              # 8  mov [ebp+var_5], eax
            b"\x8D\x55\xAB"              # 11 lea edx, [ebp+var_C]
            b"\x0F\xBF\x45\xAB"          # 14 movsx eax, [ebp+var_C]
            b"\x52"                      # 18 push edx
            b"\x50"                      # 19 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 25 mov ecx, eax
            b"\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2005-10-04
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0A2h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov [ebp+var_1C], 0A2h
            b"\x89\x75\xAB"              # 11 mov [ebp+var_16], esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2005-10-04
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0A2h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov [ebp+var_24], 0A2h
            b"\x89\x5D\xAB"              # 11 mov [ebp+var_1E], ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2005-10-04
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 0ABh
            b"\x89\x83\xAB\xAB\xAB\xAB"  # 5  mov [ebx+0FCh], eax
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 11 mov word ptr [ebp+arg_4], 0ABh
            b"\x66\x89\x4D\xAB"          # 17 mov word ptr [ebp+arg_4+2], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 26 mov ecx, eax
            b"\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2005-10-04
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 116h
            b"\xC7\x83\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebx+2F4h], 1
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 15 mov word ptr [ebp+var_2C], 116h
            b"\x66\x89\x45\xAB"          # 21 mov [ebp+var_1B], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 25 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 30 mov ecx, eax
            b"\xE8"                      # 32 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 32,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 26,
        }
    ],
    # 2005-10-04
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_30], 9Fh
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5337E0
            b"\x89\x45\xAB"              # 11 mov [ebp-1Fh], eax
            b"\x8D\x55\xAB"              # 14 lea edx, [ebp+var_30]
            b"\x0F\xBF\x45\xAB"          # 17 movsx eax, word ptr [ebp+var_30]
            b"\x52"                      # 21 push edx
            b"\x50"                      # 22 push eax
            b"\x66\x89\x75\xAB"          # 23 mov word ptr [ebp+var_2C+3], si
            b"\xE8\xAB\xAB\xAB\xAB"      # 27 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 32 mov ecx, eax
            b"\xE8"                      # 34 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 34,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 28,
        }
    ],
    # 2005-10-04
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 103h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_4], 37h
            b"\x89\x55\xAB"              # 12 mov [ebp+var_38+2], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 20 mov ecx, eax
            b"\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2005-10-04
    [
        (
            b"\x66\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB"  # 0  mov [ebp+var1], 231h
            b"\xF2\xAE"                  # 9  repne scasb
            b"\xF7\xD1"                  # 11 not ecx
            b"\x49"                      # 13 dec ecx
            b"\x83\xF9\xAB"              # 14 cmp ecx, 18h
            b"\x7D\x17"                  # 17 jge short loc_5576F1
            b"\x41"                      # 19 inc ecx
            b"\x8D\xBD\xAB\xAB\xAB\xAB"  # 20 lea edi, [ebp+var_23A]
            b"\x8B\xD1"                  # 26 mov edx, ecx
            b"\xC1\xE9\xAB"              # 28 shr ecx, 2
            b"\xF3\xA5"                  # 31 rep movsd
            b"\x8B\xCA"                  # 33 mov ecx, edx
            b"\x83\xE1\xAB"              # 35 and ecx, 3
            b"\xF3\xA4"                  # 38 rep movsb
            b"\xEB\x14"                  # 40 jmp short loc_557705
            b"\xB9\xAB\xAB\xAB\x00"      # 42 mov ecx, 6
            b"\x8D\xBD\xAB\xAB\xAB\xAB"  # 47 lea edi, [ebp+var_23A]
            b"\xF3\xA5"                  # 53 rep movsd
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 55 mov [ebp+var_223], 0
            b"\x0F\xBF\x8D\xAB\xAB\xAB\xAB"  # 62 movsx ecx, [ebp+var_23C]
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 69 lea eax, [ebp+var_23C]
            b"\x50"                      # 75 push eax
            b"\x51"                      # 76 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 77 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 82 mov ecx, eax
            b"\xE8"                      # 84 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 84,
            "packetId": (7, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 78,
        }
    ],
    # 2005-10-04
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_58], 9Bh
            b"\xFF\xD7"                  # 6  call edi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 8  mov ecx, dword_7367E4
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 14 mov edx, dword_736124
            b"\x89\x45\xAB"              # 20 mov [ebp+var_3A], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 23 mov eax, dword_7367E0
            b"\x89\x4D\xAB"              # 28 mov [ebp+var_43], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 31 mov ecx, offset dword_735A18
            b"\x89\x45\xAB"              # 36 mov [ebp+var_51], eax
            b"\x89\x55\xAB"              # 39 mov [ebp+var_3E], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 42 call sub_612BF0
            b"\x0F\xBF\x4D\xAB"          # 47 movsx ecx, [ebp+var_58]
            b"\x88\x45\xAB"              # 51 mov [ebp+var_36], al
            b"\x8D\x45\xAB"              # 54 lea eax, [ebp+var_58]
            b"\x50"                      # 57 push eax
            b"\x51"                      # 58 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 59 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 64 mov ecx, eax
            b"\xE8"                      # 66 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 66,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 60,
        }
    ],
    # 2005-10-04
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 233h
            b"\x89\x7D\xAB"              # 5  mov [ebp+var_A], edi
            b"\x89\x75\xAB"              # 8  mov [ebp+var_6], esi
            b"\xC6\x45\xAB\xAB"          # 11 mov [ebp+var_2], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 20 mov ecx, eax
            b"\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2005-10-04
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+cp], 65h
            b"\x66\x89\x75\xAB"          # 6  mov [ebp+var_46], si
            b"\x89\x55\xAB"              # 10 mov [ebp+var_52], edx
            b"\x89\x45\xAB"              # 13 mov [ebp+var_4E], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 16 call sub_612BF0
            b"\x8D\x55\xAB"              # 21 lea edx, [ebp+cp]
            b"\x88\x45\xAB"              # 24 mov byte ptr [ebp+var_44], al
            b"\x0F\xBF\x45\xAB"          # 27 movsx eax, word ptr [ebp+cp]
            b"\x52"                      # 31 push edx
            b"\x50"                      # 32 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 33 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 38 mov ecx, eax
            b"\xE8"                      # 40 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 40,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 34,
        }
    ],
    # 2005-10-04
    [
        (
            b"\x6A\xAB"                  # 0  push 67h
            b"\x66\x89\x4D\xAB"          # 2  mov word ptr [ebp+var_12+3], cx
            b"\x66\x89\x55\xAB"          # 6  mov [ebp+var_D], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 10 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 15 mov ecx, eax
            b"\xE8"                      # 17 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 17,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 11,
        }
    ],
    # 2005-10-04
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_30], 1DDh
            b"\xF3\xA5"                  # 6  rep movsd
            b"\x8D\x55\xAB"              # 8  lea edx, [ebp+var_12]
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 11 lea ecx, [ebp+var_88]
            b"\x52"                      # 17 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 18 call sub_41A650
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call sub_581DE0
            b"\x0F\xBF\x4D\xAB"          # 28 movsx ecx, [ebp+var_30]
            b"\x88\x45\xAB"              # 32 mov [ebp+var_2], al
            b"\x8D\x45\xAB"              # 35 lea eax, [ebp+var_30]
            b"\x50"                      # 38 push eax
            b"\x51"                      # 39 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 40 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 45 mov ecx, eax
            b"\xE8"                      # 47 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 47,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 41,
        }
    ],
    # 2011-09-29
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 827h
            b"\x66\x89\x4C\x24\xAB"      # 5  mov word ptr [esp+13Ch+Src], cx
            b"\x8B\x10"                  # 10 mov edx, [eax]
            b"\x8D\x44\x24\xAB"          # 12 lea eax, [esp+13Ch+Src]
            b"\x50"                      # 16 push eax
            b"\x89\x54\x24\xAB"          # 17 mov [esp+140h+Src+2], edx
            b"\x51"                      # 21 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 27 mov ecx, eax
            b"\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2011-09-29
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 846h
            b"\x51"                      # 5  push ecx
            b"\x8D\x83\xAB\xAB\xAB\xAB"  # 6  lea eax, [ebx-14Dh]
            b"\x52"                      # 12 push edx
            b"\x66\x89\x54\x24\xAB"      # 13 mov word ptr [esp+12C4h+p], dx
            b"\x66\x89\x44\x24\xAB"      # 18 mov word ptr [esp+12C4h+p+2], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 28 mov ecx, eax
            b"\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # 2011-09-29
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8B5h
            b"\x52"                      # 5  push edx
            b"\x51"                      # 6  push ecx
            b"\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 7  mov [esp+148Ch+var1], ebx
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 14 mov [esp+148Ch+var2], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 27 mov ecx, eax
            b"\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2011-09-29
    [
        (
            b"\x6A\xAB"                  # 0  push 67h
            b"\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 2  mov [esp+454h+var1+3], ax
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 10 mov [esp+454h+var2+1], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 23 mov ecx, eax
            b"\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2011-09-29
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 8A2h
            b"\x83\xC4\xAB"              # 5  add esp, 4
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 8  mov [esp+44Ch+cp], dx
            b"\xFF\xD7"                  # 16 call edi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 18 mov ecx, dword ptr qword_8DB2F4+4
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 24 mov edx, dword ptr qword_8DAAE0
            b"\x89\x84\x24\xAB\xAB\xAB\xAB"  # 30 mov [esp+0D2h], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 37 mov eax, dword ptr qword_8DB2F4
            b"\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 42 mov [esp+44Ch+cp+6], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 49 mov ecx, offset dword_8D9390
            b"\x89\x84\x24\xAB\xAB\xAB\xAB"  # 54 mov [esp+44Ch+cp+2], eax
            b"\x89\x94\x24\xAB\xAB\xAB\xAB"  # 61 mov [esp+44Ch+var2], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 68 call sub_7385E0
            b"\x0F\xBF\x8C\x24\xAB\xAB\xAB\xAB"  # 73 movsx ecx, [esp+44Ch+cp]
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"  # 81 mov ptr [esp+44Ch+var1+2], al
            b"\x8D\x84\x24\xAB\xAB\xAB\xAB"  # 88 lea eax, [esp+44Ch+cp]
            b"\x50"                      # 95 push eax
            b"\x51"                      # 96 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 97 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 102 mov ecx, eax
            b"\xE8"                      # 104 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 104,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 98,
        }
    ],
    # 2011-09-29
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 8D4h
            b"\x66\x89\x4C\x24\xAB"      # 5  mov ptr [esp+148h+var_12C+2], cx
            b"\x66\x89\x7C\x24\xAB"      # 10 mov ptr [esp+148h+var_128], di
            b"\x66\x89\x54\x24\xAB"      # 15 mov ptr [esp+148h+var_128+2], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 25 mov ecx, eax
            b"\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2011-09-29
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 65h
            b"\x66\x89\x54\x24\xAB"      # 5  mov word ptr [esp+3F0h+cp], dx
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 10 mov edx, dword_8D2818
            b"\x66\x89\x44\x24\xAB"      # 16 mov ptr [esp+3F0h+var_372], ax
            b"\xA1\xAB\xAB\xAB\xAB"      # 21 mov eax, dword_8D281C
            b"\x89\x4C\x24\xAB"          # 26 mov [esp+3F0h+var_37E], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 30 mov ecx, offset dword_8D10C8
            b"\x89\x2D\xAB\xAB\xAB\xAB"  # 35 mov dword_8A4614, ebp
            b"\x89\x54\x24\xAB"          # 41 mov ptr [esp+3F0h+var_37A], edx
            b"\x89\x44\x24\xAB"          # 45 mov [esp+3F0h+var_376], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 49 call sub_73CA20
            b"\x0F\xBF\x54\x24\xAB"      # 54 movsx edx, word ptr [esp+3F0h+cp]
            b"\x8D\x4C\x24\xAB"          # 59 lea ecx, [esp+3F0h+cp]
            b"\x51"                      # 63 push ecx
            b"\x52"                      # 64 push edx
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"  # 65 mov [esp+3F8h+var_372+2], al
            b"\xE8\xAB\xAB\xAB\xAB"      # 72 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 77 mov ecx, eax
            b"\xE8"                      # 79 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 79,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 73,
        }
    ],
    # 2011-09-29
    [
        (
            b"\x6A\xAB"                  # 0  push 67h
            b"\x66\x89\x44\x24\xAB"      # 2  mov [esp+3F8h+var_387], ax
            b"\x66\x89\x4C\x24\xAB"      # 7  mov [esp+3F8h+var_385], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2011-09-29
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 1FBh
            b"\x56"                      # 5  push esi
            b"\x52"                      # 6  push edx
            b"\x89\x84\x24\xAB\xAB\xAB\xAB"  # 7  mov [esp+3FCh+var_36A], eax
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 14 mov [esp+3FCh+Dst], cx
            b"\xFF\xD5"                  # 22 call ebp
            b"\x0F\xBF\x8C\x24\xAB\xAB\xAB\xAB"  # 24 movsx ecx, [esp+3FCh+Dst]
            b"\x83\xC4\xAB"              # 32 add esp, 0Ch
            b"\x8D\x84\x24\xAB\xAB\xAB\xAB"  # 35 lea eax, [esp+3F0h+Dst]
            b"\x50"                      # 42 push eax
            b"\xC6\x84\x24\xAB\xAB\xAB\xAB\xAB"  # 43 mov [esp+3F4h+var_335], 0
            b"\x51"                      # 51 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 52 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 57 mov ecx, eax
            b"\xE8"                      # 59 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 59,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 53,
        }
    ],
    # 2011-09-29
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8A2h
            b"\x83\xC4\xAB"              # 5  add esp, 4
            b"\x66\x89\x4C\x24\xAB"      # 8  mov word ptr [esp+3F0h+cp], cx
            b"\xFF\xD7"                  # 13 call edi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 15 mov ecx, dword_8D2818
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 21 mov edx, dword_8D302C
            b"\x89\x44\x24\xAB"          # 27 mov ptr [esp+3F0h+var_372], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 31 mov eax, dword_8D3030
            b"\x89\x4C\x24\xAB"          # 36 mov [esp+3F0h+var_376], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 40 mov ecx, offset dword_8D10C8
            b"\x89\x54\x24\xAB"          # 45 mov [esp+3F0h+var_37E], edx
            b"\x89\x44\x24\xAB"          # 49 mov ptr [esp+3F0h+var_37A], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call sub_73CA20
            b"\x88\x84\x24\xAB\xAB\xAB\xAB"  # 58 mov [esp+3F0h+var_372+4], al
            b"\x0F\xBF\x44\x24\xAB"      # 65 movsx eax, word ptr [esp+3F0h+cp]
            b"\x8D\x54\x24\xAB"          # 70 lea edx, [esp+3F0h+cp]
            b"\x52"                      # 74 push edx
            b"\x50"                      # 75 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 76 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 81 mov ecx, eax
            b"\xE8"                      # 83 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 83,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 77,
        }
    ],
    # 2005-12-19
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_20], 0A2h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5447E0
            b"\x0F\xBF\x45\xAB"          # 11 movsx eax, [ebp+var_20]
            b"\x8D\x55\xAB"              # 15 lea edx, [ebp+var_20]
            b"\x89\x5D\xAB"              # 18 mov [ebp+var_18], ebx
            b"\x52"                      # 21 push edx
            b"\x50"                      # 22 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 28 mov ecx, eax
            b"\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # 2005-12-19
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_28], 116h
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5447E0
            b"\x8D\x4D\xAB"              # 11 lea ecx, [ebp+var_24+3]
            b"\x51"                      # 14 push ecx
            b"\x8B\xCB"                  # 15 mov ecx, ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call sub_5447E0
            b"\x0F\xBF\x4D\xAB"          # 22 movsx ecx, word ptr [ebp+var_28]
            b"\x66\x8B\x55\xAB"          # 26 mov dx, word ptr [ebp+arg_8]
            b"\x8D\x45\xAB"              # 30 lea eax, [ebp+var_28]
            b"\x50"                      # 33 push eax
            b"\x51"                      # 34 push ecx
            b"\x66\x89\x75\xAB"          # 35 mov word ptr [ebp+var_24+1], si
            b"\x66\x89\x55\xAB"          # 39 mov word ptr [ebp+var_1C], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 43 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 48 mov ecx, eax
            b"\xE8"                      # 50 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 50,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 44,
        }
    ],
    # 2005-12-19
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_2C], 9Fh
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5447E0
            b"\x8D\x45\xAB"              # 11 lea eax, [ebp+var_24]
            b"\x8B\xCB"                  # 14 mov ecx, ebx
            b"\x50"                      # 16 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call sub_5447E0
            b"\xB9\xAB\xAB\xAB\xAB"      # 22 mov ecx, offset dword_73A978
            b"\xE8\xAB\xAB\xAB\xAB"      # 27 call sub_536310
            b"\x0F\xBF\x55\xAB"          # 32 movsx edx, word ptr [ebp+var_2C]
            b"\x8D\x4D\xAB"              # 36 lea ecx, [ebp+var_2C]
            b"\x89\x45\xAB"              # 39 mov [ebp+var_20+1], eax
            b"\x51"                      # 42 push ecx
            b"\x52"                      # 43 push edx
            b"\x66\x89\x75\xAB"          # 44 mov word ptr [ebp+var_28+2], si
            b"\xE8\xAB\xAB\xAB\xAB"      # 48 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 53 mov ecx, eax
            b"\xE8"                      # 55 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 55,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 49,
        }
    ],
    # 2005-12-19
    [
        (
            b"\x66\xC7\x45\xA8\xAB\xAB"  # 0  mov [ebp+var_58], 9Bh
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5447E0
            b"\x8D\x4D\xAB"              # 11 lea ecx, [ebp+var_48]
            b"\x51"                      # 14 push ecx
            b"\x8B\xCB"                  # 15 mov ecx, ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call sub_5447E0
            b"\x8D\x55\xAB"              # 22 lea edx, [ebp+var_42]
            b"\x8B\xCB"                  # 25 mov ecx, ebx
            b"\x52"                      # 27 push edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 28 call sub_5447E0
            b"\xFF\xD6"                  # 33 call esi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 35 mov ecx, dword_73B744
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 41 mov edx, dword_73B084
            b"\x89\x45\xAB"              # 47 mov [ebp+var_3C], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 50 mov eax, dword_73B740
            b"\x89\x4D\xAB"              # 55 mov [ebp+var_46], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 58 mov ecx, offset dword_73A978
            b"\x89\x45\xAB"              # 63 mov [ebp+var_4C], eax
            b"\x89\x55\xAB"              # 66 mov [ebp+var_40], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 69 call sub_6168E0
            b"\x0F\xBF\x4D\xAB"          # 74 movsx ecx, [ebp+var_58]
            b"\x88\x45\xAB"              # 78 mov [ebp+var_38], al
            b"\x8D\x45\xAB"              # 81 lea eax, [ebp+var_58]
            b"\x50"                      # 84 push eax
            b"\x51"                      # 85 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 86 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 91 mov ecx, eax
            b"\xE8"                      # 93 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 93,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 87,
        }
    ],
    # 2005-12-19
    [
        (
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+cp], 65h
            b"\x66\xC7\x45\xAB\xAB\xAB"  # 6  mov [ebp+var_A], 0
            b"\x89\x55\xAB"              # 12 mov [ebp+var_16], edx
            b"\x89\x45\xAB"              # 15 mov [ebp+var_12], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 18 call sub_6168E0
            b"\x8D\x55\xAB"              # 23 lea edx, [ebp+cp]
            b"\x88\x45\xAB"              # 26 mov byte ptr [ebp+var_8], al
            b"\x0F\xBF\x45\xAB"          # 29 movsx eax, word ptr [ebp+cp]
            b"\x52"                      # 33 push edx
            b"\x50"                      # 34 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 35 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 40 mov ecx, eax
            b"\xE8"                      # 42 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 42,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 36,
        }
    ],
    # 2014-01-08
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 970h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp-13h], ax
            b"\x88\x4D\xAB"              # 9  mov [ebp-16h], cl
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2011-01-04
    [
        (
            b"\x6A\xAB"                  # 0  push 67h
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 2  mov [esp+4A0h+var1+3], cx
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 10 mov [esp+4A0h+var2+1], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 23 mov ecx, eax
            b"\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2010-10-19
    [
        (
            b"\x6A\xAB"                  # 0  push 67h
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 2  mov [esp+378h+var1], dx
            b"\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 10 mov [esp+378h+var2], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 23 mov ecx, eax
            b"\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2018-06-20
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 9B0h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+buf], ax
            b"\xA1\xAB\xAB\xAB\xAB"      # 12 mov eax, dword ptr account_id
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 17 mov [ebp-29Ah], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 23 lea eax, [ebp+var_290]
            b"\x50"                      # 29 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 30 call sub_511740
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 35 mov [ebp+var_4], 1
            b"\x83\x78\xAB\xAB"          # 42 cmp dword ptr [eax+14h], 10h
            b"\x72\x02"                  # 46 jb short loc_5E0A58
            b"\x8B\x00"                  # 48 mov eax, [eax]
            b"\x50"                      # 50 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 51 call ds:atoi
            b"\x83\xC4\x04"              # 57 add esp, 4
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 60 mov ptr [ebp+var_298+2], ax
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 67 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 74 lea ecx, [ebp+var_290]
            b"\xE8\xAB\xAB\xAB\xAB"      # 80 call sub_41F0C0
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 85 lea eax, [ebp+buf]
            b"\x50"                      # 91 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 92 movsx eax, [ebp+buf]
            b"\x50"                      # 99 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 100 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 105 mov ecx, eax
            b"\xE8"                      # 107 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 107,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 101,
        }
    ],
    # 2018-06-20
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A49h
            b"\x66\x89\x45\xAB"          # 5  mov word ptr [ebp+buf], ax
            b"\x8D\x45\xAB"              # 9  lea eax, [ebp+Src]
            b"\x0F\x43\x45\xAB"          # 12 cmovnb eax, [ebp+Src]
            b"\x50"                      # 16 push eax
            b"\x8D\x45\xAB"              # 17 lea eax, [ebp+Dst]
            b"\x6A\xAB"                  # 20 push 10h
            b"\x50"                      # 22 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 23 call ds:strcpy_s
            b"\x66\x8B\x45\xAB"          # 29 mov ax, [ebp+arg_18]
            b"\x66\x89\x45\xAB"          # 33 mov [ebp+var_12], ax
            b"\x83\xC4\x0C"              # 37 add esp, 0Ch
            b"\x8D\x45\xAB"              # 40 lea eax, [ebp+buf]
            b"\x50"                      # 43 push eax
            b"\x0F\xBF\x45\xAB"          # 44 movsx eax, word ptr [ebp+buf]
            b"\x50"                      # 48 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 49 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 54 mov ecx, eax
            b"\xE8"                      # 56 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 56,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 50,
        }
    ],
    # 2018-06-20
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 2C4h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var1], ax
            b"\x8D\x51\xAB"              # 12 lea edx, [ecx+1]
            b"\x8D\x49\xAB"              # 15 lea ecx, [ecx+0]
            b"\x8A\x01"                  # 18 mov al, [ecx]
            b"\x41"                      # 20 inc ecx
            b"\x84\xC0"                  # 21 test al, al
            b"\x75\xF9"                  # 23 jnz short loc_958080
            b"\x2B\xCA"                  # 25 sub ecx, edx
            b"\x83\xF9\x18"              # 27 cmp ecx, 18h
            b"\x0F\x83\xAB\xAB\xAB\xAB"  # 30 jnb loc_957F5E
            b"\x6A\x18"                  # 36 push 18h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 38 lea eax, [ebp+var_BF16]
            b"\x53"                      # 44 push ebx
            b"\x50"                      # 45 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 46 call ds:strncpy
            b"\x83\xC4\x0C"              # 52 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 55 lea eax, [ebp+var1]
            b"\x50"                      # 61 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 62 movsx eax, word ptr [ebp+var1
            b"\x50"                      # 69 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 70 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 75 mov ecx, eax
            b"\xE8"                      # 77 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 77,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 71,
        }
    ],
    # 2018-06-20
    [
        (
            b"\xBE\xAB\xAB\x00\x00"      # 0  mov esi, 64h
            b"\x66\x89\xB5\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_33C], s
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 12 mov [ebp+var_33A], eax
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 18 movq [ebp+var_326], xmm0
            b"\x83\xF9\x07"              # 26 cmp ecx, 7
            b"\x75\x49"                  # 29 jnz short loc_9CF3FF
            b"\x6A\x18"                  # 31 push 18h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 33 lea eax, [ebp+var_2DC]
            b"\x50"                      # 39 push eax
            b"\x8D\x83\xAB\xAB\xAB\xAB"  # 40 lea eax, [ebx+138h]
            b"\x50"                      # 46 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 47 call sub_796F90
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 52 movq xmm0, [ebp+var_2DC]
            b"\x66\x8B\xB5\xAB\xAB\xAB\xAB"  # 60 mov si, word ptr [ebp+var_33C
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 67 movq [ebp+var_31E], xmm0
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 75 movq xmm0, [ebp+var_2D4]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 83 movq [ebp+var_316], xmm0
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 91 movq xmm0, [ebp+var_2CC]
            b"\x83\xC4\x0C"              # 99 add esp, 0Ch
            b"\xEB\x28"                  # 102 jmp short loc_9CF427
            b"\xF3\x0F\x7E\x83\xAB\xAB\xAB\xAB"  # 104 movq xmm0, [ebx+138h]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 112 movq [ebp+var_31E], xmm0
            b"\xF3\x0F\x7E\x83\xAB\xAB\xAB\xAB"  # 120 movq xmm0, [ebx+140h]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 128 movq [ebp+var_316], xmm0
            b"\xF3\x0F\x7E\x83\xAB\xAB\xAB\xAB"  # 136 movq xmm0, [ebx+148h]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 144 movq [ebp+var_30E], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"      # 152 call sub_9CB920
            b"\x88\x85\xAB\xAB\xAB\xAB"  # 157 mov [ebp+var_306], al
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 163 lea eax, [ebp+var_33C]
            b"\x50"                      # 169 push eax
            b"\x0F\xBF\xC6"              # 170 movsx eax, si
            b"\x50"                      # 173 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 174 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 179 mov ecx, eax
            b"\xE8"                      # 181 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 181,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 175,
        }
    ],
    # 2018-06-20
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1FBh
            b"\x6A\xAB"                  # 5  push 32h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 7  mov word ptr [ebp+var1], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 14 lea eax, [ebp+var_336]
            b"\x56"                      # 20 push esi
            b"\x50"                      # 21 push eax
            b"\xFF\xD7"                  # 22 call edi
            b"\x83\xC4\x0C"              # 24 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 27 lea eax, [ebp+var1]
            b"\x50"                      # 33 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 34 movsx eax, word ptr [ebp+var1
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 41 mov [ebp+var_305], 0
            b"\x50"                      # 48 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 49 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 54 mov ecx, eax
            b"\xE8"                      # 56 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 56,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 50,
        }
    ],
    # 2018-07-04
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A49h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+p.packet_id], ax
            b"\x8D\x45\xAB"              # 9  lea eax, [ebp+a1]
            b"\x0F\x43\x45\xAB"          # 12 cmovnb eax, [ebp+a1]
            b"\x50"                      # 16 push eax
            b"\x8D\x45\xAB"              # 17 lea eax, [ebp+p.map_name]
            b"\x6A\xAB"                  # 20 push 10h
            b"\x50"                      # 22 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 23 call strcpy_s
            b"\x8B\x45\xAB"              # 29 mov eax, [ebp+item_id]
            b"\x89\x45\xAB"              # 32 mov [ebp+p.item_id], eax
            b"\x83\xC4\x0C"              # 35 add esp, 0Ch
            b"\x8D\x45\xAB"              # 38 lea eax, [ebp+p]
            b"\x50"                      # 41 push eax
            b"\x0F\xBF\x45\xAB"          # 42 movsx eax, [ebp+p.packet_id]
            b"\x50"                      # 46 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 47 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 52 mov ecx, eax
            b"\xE8"                      # 54 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 54,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 48,
        }
    ],
    # 2018-07-04
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 83Ch
            b"\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov A, 0FFFFFFFFh
            b"\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 15 mov B, 0FFFFFFFFh
            b"\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 25 mov C, 0FFFFFFFFh
            b"\x66\x89\x45\xAB"          # 35 mov word ptr [ebp+var_20], ax
            b"\x8B\x46\xAB"              # 39 mov eax, [esi+4]
            b"\x89\x45\xAB"              # 42 mov [ebp+var_20+2], eax
            b"\x8B\x06"                  # 45 mov eax, [esi]
            b"\x8D\x4E\xAB"              # 47 lea ecx, [esi+60h]
            b"\x89\x45\xAB"              # 50 mov [ebp+var_1A], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call sub_508EE0
            b"\x89\x45\xAB"              # 58 mov [ebp+var_16], eax
            b"\x8D\x45\xAB"              # 61 lea eax, [ebp+var_20]
            b"\x50"                      # 64 push eax
            b"\x0F\xBF\x45\xAB"          # 65 movsx eax, word ptr [ebp+var_20]
            b"\x50"                      # 69 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 70 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 75 mov ecx, eax
            b"\xE8"                      # 77 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 77,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 71,
        }
    ],
    # 2018-07-04
    [
        (
            b"\xBF\xAB\xAB\x00\x00"      # 0  mov edi, 9C3h
            b"\x8D\x49\xAB"              # 5  lea ecx, [ecx+0]
            b"\x56"                      # 8  push esi
            b"\x6A\x08"                  # 9  push 8
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 11 lea eax, [ebp+var_95C]
            b"\x50"                      # 17 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 18 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call CSession_sub_AC5C40
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 28 mov [ebp+var_4], 9
            b"\xA1\xAB\xAB\xAB\xAB"      # 35 mov eax, g_session.m_account_id
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 40 lea ecx, [ebp+var_95C]
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 46 mov [ebp+var_81C], di
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 53 mov dword ptr [ebp+var_81C+2], ea
            b"\xE8\xAB\xAB\xAB\xAB"      # 59 call sub_508EE0
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 64 mov [ebp+var_816], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 70 lea eax, [ebp+var_81C]
            b"\x50"                      # 76 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 77 movsx eax, [ebp+var_81C]
            b"\x50"                      # 84 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 85 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 90 mov ecx, eax
            b"\xE8"                      # 92 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 92,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 86,
        }
    ],
    # 2018-07-04
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 9B0h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_23C], a
            b"\xA1\xAB\xAB\xAB\xAB"      # 12 mov eax, g_session.m_account_id
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 17 mov [ebp+var_23C+2], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 23 lea eax, [ebp+var_290]
            b"\x50"                      # 29 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 30 call sub_5113C0
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 35 mov [ebp+var_4], 1
            b"\x83\x78\xAB\xAB"          # 42 cmp dword ptr [eax+14h], 10h
            b"\x72\x02"                  # 46 jb short loc_5E0D48
            b"\x8B\xAB"                  # 48 mov eax, [eax]
            b"\x50"                      # 50 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 51 call atoi
            b"\x83\xC4\x04"              # 57 add esp, 4
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 60 mov [ebp-236h], eax
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 66 mov [ebp+var_4], 0FFFFFFFFh
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 73 lea ecx, [ebp+var_290]
            b"\xE8\xAB\xAB\xAB\xAB"      # 79 call sub_41F0C0
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 84 lea eax, [ebp+var_23C]
            b"\x50"                      # 90 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 91 movsx eax, word ptr [ebp+var1
            b"\x50"                      # 98 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 99 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 104 mov ecx, eax
            b"\xE8"                      # 106 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 106,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 100,
        }
    ],
    # 2018-07-04
    [
        (
            b"\xBF\xAB\xAB\x00\x00"      # 0  mov edi, 9C3h
            b"\xEB\x08"                  # 5  jmp short loc_623FA0
            b"\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  align 10h
            b"\xA1\xAB\xAB\xAB\xAB"      # 15 mov eax, g_session.m_account_id
            b"\x8D\x4E\xAB"              # 20 lea ecx, [esi+8]
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 23 mov word ptr [ebp+A], di
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 30 mov [ebp+A+2], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 36 call sub_508EE0
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 41 mov [ebp+A+6], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 47 lea eax, [ebp+A]
            b"\x50"                      # 53 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 54 movsx eax, word ptr [ebp+A]
            b"\x50"                      # 61 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 62 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 67 mov ecx, eax
            b"\xE8"                      # 69 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 69,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 63,
        }
    ],
    # 2018-05-10 iro
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 9EEh
            b"\x8B\x48\xAB"              # 5  mov ecx, [eax+10h]
            b"\x8B\x40\xAB"              # 8  mov eax, [eax+14h]
            b"\x89\x45\xAB"              # 11 mov dword ptr [ebp+Src+7], eax
            b"\x8D\x45\xAB"              # 14 lea eax, [ebp+Src]
            b"\x50"                      # 17 push eax
            b"\x52"                      # 18 push edx
            b"\x66\x89\x55\xAB"          # 19 mov word ptr [ebp+Src], dx
            b"\x88\x5D\xAB"              # 23 mov byte ptr [ebp+Src+2], bl
            b"\x89\x4D\xAB"              # 26 mov dword ptr [ebp+Src+3], ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 29 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 34 mov ecx, eax
            b"\xE8"                      # 36 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 36,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 30,
        }
    ],
    # 2018-05-10 iro
    [
        (
            b"\x6A\xAB"                  # 0  push 7Dh
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 2  movq [ebp+var_19A], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"      # 10 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 15 mov ecx, eax
            b"\xE8"                      # 17 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 17,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 11,
        }
    ],
    # 2018-05-10 iro
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 437h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var1], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 12 lea eax, [ebp+var_10A0+6]
            b"\x50"                      # 18 push eax
            b"\x6A\x00"                  # 19 push 0
            b"\x51"                      # 21 push ecx
            b"\x53"                      # 22 push ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call sub_87A4B0
            b"\x83\xC4\x10"              # 28 add esp, 10h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 31 lea eax, [ebp+var_10A0+4]
            b"\x50"                      # 37 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 38 movsx eax, word ptr [ebp+var1
            b"\x50"                      # 45 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 46 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 51 mov ecx, eax
            b"\xE8"                      # 53 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 53,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 47,
        }
    ],
    # 2018-05-10 iro
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 288h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+A+4], ax
            b"\x6A\x00"                  # 12 push 0
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 14 lea eax, [ebp+var_EC]
            b"\x50"                      # 20 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 21 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 26 call CSession_sub_938CA0
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 31 mov [ebp+var_4], 30h
            b"\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 38 mov ax, word ptr [ebp+var_DC]
            b"\x83\xBD\xAB\xAB\xAB\xAB\xAB"  # 45 cmp [ebp+var_AC], 10h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 52 mov [ebp+var_1098], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 59 lea eax, [ebp+Str]
            b"\x0F\x43\x85\xAB\xAB\xAB\xAB"  # 65 cmovnb eax, [ebp+Str]
            b"\x50"                      # 72 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 73 call ds:atoi
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 79 mov word ptr [ebp+A+6], ax
            b"\x83\xC4\x04"              # 86 add esp, 4
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 89 lea eax, [ebp+A+4]
            b"\x50"                      # 95 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 96 movsx eax, word ptr [ebp+A+4]
            b"\x50"                      # 103 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 104 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 109 mov ecx, eax
            b"\xE8"                      # 111 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 111,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 105,
        }
    ],
    # 2018-05-10 iro
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8A3h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+Src], ax
            b"\x83\xC8\xAB"              # 9  or eax, 0FFFFFFFFh
            b"\x38\x0D\xAB\xAB\xAB\xAB"  # 12 cmp byte ptr g_session+3EF0h, cl
            b"\x0F\x45\xF0"              # 18 cmovnz esi, eax
            b"\x8D\x45\xAB"              # 21 lea eax, [ebp+var_30]
            b"\x50"                      # 24 push eax
            b"\x56"                      # 25 push esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 26 call sub_80FC80
            b"\x8D\x45\xAB"              # 31 lea eax, [ebp+var_24+4]
            b"\x50"                      # 34 push eax
            b"\xFF\x75\xAB"              # 35 push [ebp+arg_C]
            b"\xE8\xAB\xAB\xAB\xAB"      # 38 call sub_80FC80
            b"\x66\x8B\x45\xAB"          # 43 mov ax, word ptr [ebp+arg_8]
            b"\x66\x89\x45\xAB"          # 47 mov [ebp+var_32], ax
            b"\x83\xC4\x10"              # 51 add esp, 10h
            b"\x8D\x45\xAB"              # 54 lea eax, [ebp+Src]
            b"\x50"                      # 57 push eax
            b"\x0F\xBF\x45\xAB"          # 58 movsx eax, [ebp+Src]
            b"\x50"                      # 62 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 63 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 68 mov ecx, eax
            b"\xE8"                      # 70 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 70,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 64,
        }
    ],
    # aro
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0ABh
            b"\x52"                      # 5  push edx
            b"\x50"                      # 6  push eax
            b"\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 7  mov ptr [esp+var1], ax
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 15 mov ptr [esp+var2], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 28 mov ecx, eax
            b"\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # aro
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 85h
            b"\x53"                      # 5  push ebx
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 6  mov word [esp+var1], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call sub_6C7060
            b"\x0F\xBF\x84\x24\xAB\xAB\xAB\xAB"  # 19 movsx eax, word [esp+var1
            b"\x83\xC4\x10"              # 27 add esp, 10h
            b"\x8D\x94\x24\xAB\xAB\xAB\xAB"  # 30 lea edx, [esp+var2]
            b"\x52"                      # 37 push edx
            b"\x50"                      # 38 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 39 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 44 mov ecx, eax
            b"\xE8"                      # 46 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 46,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 40,
        }
    ],
    # aro
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 288h
            b"\x50"                      # 5  push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 6  mov ecx, offset g_session
            b"\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 11 mov word [esp+var3], dx
            b"\xE8\xAB\xAB\xAB\xAB"      # 19 call CSession_sub_7345B0
            b"\xC7\x84\x24\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 24 mov [esp+v4],
            b"\x83\xBC\x24\xAB\xAB\xAB\xAB\xAB"  # 35 cmp dword [esp+var5], 10h
            b"\x66\x8B\x8C\x24\xAB\xAB\xAB\xAB"  # 43 mov cx, word [esp+var2]
            b"\x8B\x84\x24\xAB\xAB\xAB\xAB"  # 51 mov eax, [esp+var7]
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 58 mov word [esp+var6], cx
            b"\x73\x07"                  # 66 jnb short loc_6CFC48
            b"\x8D\x84\x24\xAB\xAB\xAB\xAB"  # 68 lea eax, [esp+1274h+Str]
            b"\x50"                      # 75 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 76 call ds:atoi
            b"\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 82 mov word [esp+var1+2], ax
            b"\x0F\xBF\x84\x24\xAB\xAB\xAB\xAB"  # 90 movsx eax, word [esp+var1
            b"\x83\xC4\x04"              # 98 add esp, 4
            b"\x8D\x94\x24\xAB\xAB\xAB\xAB"  # 101 lea edx, [esp+1274h+var_104C
            b"\x52"                      # 108 push edx
            b"\x50"                      # 109 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 110 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 115 mov ecx, eax
            b"\xE8"                      # 117 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 117,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 111,
        }
    ],
    # aro
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 23Bh
            b"\x66\x89\x44\x24\xAB"      # 5  mov word ptr [esp+48h+Src], ax
            b"\x74\x03"                  # 10 jz short loc_71105E
            b"\x83\xCF\xAB"              # 12 or edi, 0FFFFFFFFh
            b"\x8D\x4C\x24\xAB"          # 15 lea ecx, [esp+48h+var_30]
            b"\x51"                      # 19 push ecx
            b"\x57"                      # 20 push edi
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call sub_65FA60
            b"\x8B\x44\x24\xAB"          # 26 mov eax, [esp+50h+arg_C]
            b"\x8D\x54\x24\xAB"          # 30 lea edx, [esp+50h+var_20]
            b"\x52"                      # 34 push edx
            b"\x50"                      # 35 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 36 call sub_65FA60
            b"\x0F\xBF\x44\x24\xAB"      # 41 movsx eax, word ptr [esp+58h+Src]
            b"\x66\x8B\x4C\x24\xAB"      # 46 mov cx, word ptr [esp+58h+arg_8]
            b"\x83\xC4\x10"              # 51 add esp, 10h
            b"\x8D\x54\x24\xAB"          # 54 lea edx, [esp+48h+Src]
            b"\x52"                      # 58 push edx
            b"\x50"                      # 59 push eax
            b"\x66\x89\x4C\x24\xAB"      # 60 mov word ptr [esp+50h+Src+2], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 65 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 70 mov ecx, eax
            b"\xE8"                      # 72 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 72,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 66,
        }
    ],
    # aro
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 65h
            b"\x66\x89\x4C\x24\xAB"      # 5  mov [esp+48h], cx
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 10 mov ecx, g_session.m_auth_code1
            b"\x33\xD2"                  # 16 xor edx, edx
            b"\x66\x89\x54\x24\xAB"      # 18 mov word ptr [esp+16Ch+var1], dx
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 23 mov edx, dword ptr g_session+1854
            b"\x89\x4C\x24\xAB"          # 29 mov [esp+16Ch+var_11E], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 33 mov ecx, offset g_session
            b"\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 38 mov A, 1
            b"\x89\x44\x24\xAB"          # 48 mov [esp+4Ah], eax
            b"\x89\x54\x24\xAB"          # 52 mov [esp+16Ch+var_11A], edx
            b"\xE8\xAB\xAB\xAB\xAB"      # 56 call CSession_GetSex
            b"\x0F\xBF\x4C\x24\xAB"      # 61 movsx ecx, word ptr [esp+48h]
            b"\x88\x44\x24\xAB"          # 66 mov [esp+16Ch+var_116+2], al
            b"\x8D\x44\x24\xAB"          # 70 lea eax, [esp+48h]
            b"\x50"                      # 74 push eax
            b"\x51"                      # 75 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 76 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 81 mov ecx, eax
            b"\xE8"                      # 83 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 83,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 77,
        }
    ],
    # aro
    [
        (
            b"\x6A\xAB"                  # 0  push 67h
            b"\x66\x89\x54\x24\xAB"      # 2  mov [esp+174h+p.hair_color], dx
            b"\x66\x89\x44\x24\xAB"      # 7  mov [esp+174h+p.hair_style], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # aro
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 1FBh
            b"\x57"                      # 5  push edi
            b"\x52"                      # 6  push edx
            b"\x89\x84\x24\xAB\xAB\xAB\xAB"  # 7  mov dword ptr [esp+D], eax
            b"\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 14 mov word ptr [esp+C], cx
            b"\xFF\xD3"                  # 22 call ebx
            b"\x0F\xBF\x8C\x24\xAB\xAB\xAB\xAB"  # 24 movsx ecx, word [esp+B]
            b"\x83\xC4\x0C"              # 32 add esp, 0Ch
            b"\x8D\x84\x24\xAB\xAB\xAB\xAB"  # 35 lea eax, [esp+16Ch+pncb]
            b"\x50"                      # 42 push eax
            b"\xC6\x84\x24\xAB\xAB\xAB\xAB\xAB"  # 43 mov [esp+A], 0
            b"\x51"                      # 51 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 52 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 57 mov ecx, eax
            b"\xE8"                      # 59 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 59,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 53,
        }
    ],
    # aro
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 72h
            b"\x83\xC4\x04"              # 5  add esp, 4
            b"\x66\x89\x4C\x24\xAB"      # 8  mov [esp+48h], cx
            b"\xFF\xD5"                  # 13 call ebp
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 15 mov ecx, g_session.m_auth_code1
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 21 mov edx, g_session.m_account_id
            b"\x89\x44\x24\xAB"          # 27 mov dword ptr [esp+16Ch+var1], ea
            b"\xA1\xAB\xAB\xAB\xAB"      # 31 mov eax, g_session.m_char_id
            b"\x89\x4C\x24\xAB"          # 36 mov [esp+16Ch+var_11A], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 40 mov ecx, offset g_session
            b"\x89\x54\x24\xAB"          # 45 mov [esp+4Ah], edx
            b"\x89\x44\x24\xAB"          # 49 mov [esp+16Ch+var_11E], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call CSession_GetSex
            b"\x88\x44\x24\xAB"          # 58 mov [esp+16Ch+var1+4], al
            b"\x0F\xBF\x44\x24\xAB"      # 62 movsx eax, word ptr [esp+48h]
            b"\x8D\x54\x24\xAB"          # 67 lea edx, [esp+48h]
            b"\x52"                      # 71 push edx
            b"\x50"                      # 72 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 73 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 78 mov ecx, eax
            b"\xE8"                      # 80 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 80,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 74,
        }
    ],
    # 2017-10-18 zero
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 2C4h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_BF18], ax
            b"\x8D\x51\xAB"              # 12 lea edx, [ecx+1]
            b"\xEB\x03"                  # 15 jmp short loc_9E7B80
            b"\xAB\xAB\xAB"              # 17 align 10h
            b"\x8A\x01"                  # 20 mov al, [ecx]
            b"\x41"                      # 22 inc ecx
            b"\x84\xC0"                  # 23 test al, al
            b"\x75\xF9"                  # 25 jnz short loc_9E7B80
            b"\x2B\xCA"                  # 27 sub ecx, edx
            b"\x83\xF9\xAB"              # 29 cmp ecx, 18h
            b"\x0F\x83\xAB\xAB\xAB\xAB"  # 32 jnb loc_9E7A5E
            b"\x6A\xAB"                  # 38 push 18h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 40 lea eax, [ebp+var_BF16]
            b"\x53"                      # 46 push ebx
            b"\x50"                      # 47 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 48 call ds:strncpy
            b"\x83\xC4\x0C"              # 54 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 57 lea eax, [ebp+var_BF18]
            b"\x50"                      # 63 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 64 movsx eax, [ebp+var_BF18]
            b"\x50"                      # 71 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 72 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 77 mov ecx, eax
            b"\xE8"                      # 79 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 79,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 73,
        }
    ],
    # 2017-10-18 zero
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 436h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+Src], ax
            b"\xFF\xD7"                  # 9  call edi
            b"\x89\x45\xAB"              # 11 mov [ebp+var_2E], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 14 mov eax, cchWideChar
            b"\x89\x45\xAB"              # 19 mov [ebp+var_3A], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 22 mov eax, dword_106A698
            b"\x89\x45\xAB"              # 27 mov [ebp+var_36], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 30 mov eax, dword_1069F58
            b"\xB9\xAB\xAB\xAB\xAB"      # 35 mov ecx, offset dword_1069218
            b"\x89\x45\xAB"              # 40 mov [ebp+var_32], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 43 call sub_ABDAC0
            b"\x88\x45\xAB"              # 48 mov [ebp+var_2A], al
            b"\x8D\x45\xAB"              # 51 lea eax, [ebp+Src]
            b"\x50"                      # 54 push eax
            b"\x0F\xBF\x45\xAB"          # 55 movsx eax, [ebp+Src]
            b"\x50"                      # 59 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 60 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 65 mov ecx, eax
            b"\xE8"                      # 67 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 67,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 61,
        }
    ],
    # 2017-10-18 zero
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0AD0h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+Src], ax
            b"\x6A\xAB"                  # 9  push 9
            b"\x8D\x83\xAB\xAB\xAB\xAB"  # 11 lea eax, [ebx+1B8h]
            b"\x50"                      # 17 push eax
            b"\x8D\x45\xAB"              # 18 lea eax, [ebp+Dst]
            b"\x6A\xAB"                  # 21 push 9
            b"\x50"                      # 23 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 24 call ds:memcpy_s
            b"\x83\xC4\x10"              # 30 add esp, 10h
            b"\x8D\x45\xAB"              # 33 lea eax, [ebp+Src]
            b"\x50"                      # 36 push eax
            b"\x0F\xBF\x45\xAB"          # 37 movsx eax, [ebp+Src]
            b"\x50"                      # 41 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 42 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 47 mov ecx, eax
            b"\xE8"                      # 49 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 49,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 43,
        }
    ],
    # 2017-10-18 zero
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 233h
            b"\x89\x7D\xAB"              # 5  mov dword ptr [ebp+Src+6], edi
            b"\xC6\x45\xAB\xAB"          # 8  mov [ebp+var_6], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2018-11-14
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 64h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_61C], ax
            b"\xA1\xAB\xAB\xAB\xAB"      # 12 mov eax, dword_108EEE8
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 17 movq [ebp+var_60E], xmm0
            b"\xF3\x0F\x7E\x83\xAB\xAB\xAB\xAB"  # 25 movq xmm0, qword ptr [ebx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 33 mov [ebp+var_61A], eax
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 39 movq [ebp+var_606], xmm0
            b"\x83\xF9\x07"              # 47 cmp ecx, 7
            b"\x75\xAB"                  # 50 jnz short loc_A92D80
            b"\x6A\x18"                  # 52 push 18h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 54 lea eax, [ebp+var_8C]
            b"\x50"                      # 60 push eax
            b"\x8D\x83\xAB\xAB\xAB\xAB"  # 61 lea eax, [ebx+138h]
            b"\x50"                      # 67 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 68 call sub_858520
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 73 movq xmm0, [ebp+var_8C]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 81 movq [ebp+var_5FE], xmm0
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 89 movq xmm0, [ebp+var_84]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 97 movq [ebp+var_5F6], xmm0
            b"\xF3\x0F\x7E\x45\xAB"      # 105 movq xmm0, [ebp+var_7C]
            b"\x83\xC4\x0C"              # 110 add esp, 0Ch
            b"\xEB\xAB"                  # 113 jmp short loc_A92DC5
            b"\x83\xF9\x04"              # 115 cmp ecx, 4
            b"\x75\xAB"                  # 118 jnz short loc_A92D9D
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 120 lea eax, [ebp+var_5FE]
            b"\x50"                      # 126 push eax
            b"\x8D\x83\xAB\xAB\xAB\xAB"  # 127 lea eax, [ebx+138h]
            b"\x50"                      # 133 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 134 call sub_46F8B0
            b"\x83\xC4\x08"              # 139 add esp, 8
            b"\xEB\xAB"                  # 142 jmp short loc_A92DCD
            b"\xF3\x0F\x7E\x83\xAB\xAB\xAB\xAB"  # 144 movq xmm0, qword ptr [eb
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 152 movq [ebp+var_5FE], xmm0
            b"\xF3\x0F\x7E\x83\xAB\xAB\xAB\xAB"  # 160 movq xmm0, qword ptr [eb
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 168 movq [ebp+var_5F6], xmm0
            b"\xF3\x0F\x7E\x83\xAB\xAB\xAB\xAB"  # 176 movq xmm0, qword ptr [eb
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 184 movq [ebp+var_5EE], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"      # 192 call sub_A8E620
            b"\x88\x85\xAB\xAB\xAB\xAB"  # 197 mov [ebp+var_5E6], al
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 203 lea eax, [ebp+var_61C]
            b"\x50"                      # 209 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 210 movsx eax, [ebp+var_61C]
            b"\x50"                      # 217 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 218 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 223 mov ecx, eax
            b"\xE8"                      # 225 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 225,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 219,
        }
    ],
    # 2018-11-14
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 808h
            b"\x66\x89\x45\xAB"          # 5  mov word ptr [ebp+buf], ax
            b"\x83\xC8\xFF"              # 9  or eax, 0FFFFFFFFh
            b"\x66\x8B\xC8"              # 12 mov cx, ax
            b"\x0F\xB7\xC1"              # 15 movzx eax, cx
            b"\x8B\xC8"                  # 18 mov ecx, eax
            b"\xC1\xE0\x10"              # 20 shl eax, 10h
            b"\x0B\xC8"                  # 23 or ecx, eax
            b"\x0F\x57\xC0"              # 25 xorps xmm0, xmm0
            b"\x66\x0F\xD6\x45\xAB"      # 28 movq qword ptr [ebp+buf+2], xmm0
            b"\xC7\x45\xAB\x00\x00\x00\x00"  # 33 mov [ebp+var_A], 0
            b"\x89\x4D\xAB"              # 40 mov dword ptr [ebp+buf+2], ecx
            b"\x89\x4D\xAB"              # 43 mov [ebp-0Eh], ecx
            b"\x89\x4D\xAB"              # 46 mov [ebp+var_A], ecx
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 49 mov ecx, _dword_F487F4
            b"\x33\xDB"                  # 55 xor ebx, ebx
            b"\x8B\x01"                  # 57 mov eax, [ecx]
            b"\x3B\xC1"                  # 59 cmp eax, ecx
            b"\x0F\x84\xAB\xAB\x00\x00"  # 61 jz loc_66491C
            b"\x56"                      # 67 push esi
            b"\x8D\x75\xAB"              # 68 lea esi, [ebp+buf+2]
            b"\x81\xC7\x98\x00\x00\x00"  # 71 add edi, 98h
            b"\x8B\x17"                  # 77 mov edx, [edi]
            b"\x85\xD2"                  # 79 test edx, edx
            b"\x74\xAB"                  # 81 jz short loc_66487E
            b"\x83\x7A\xAB\x00"          # 83 cmp dword ptr [edx+30h], 0
            b"\x74\xAB"                  # 87 jz short loc_66487E
            b"\x66\x8B\x50\xAB"          # 89 mov dx, [eax+10h]
            b"\x66\x89\x16"              # 93 mov [esi], dx
            b"\xC6\x40\xAB\x01"          # 96 mov byte ptr [eax+14h], 1
            b"\x43"                      # 100 inc ebx
            b"\xEB\xAB"                  # 101 jmp short loc_664888
            b"\x83\xCA\xFF"              # 103 or edx, 0FFFFFFFFh
            b"\x66\x89\x16"              # 106 mov [esi], dx
            b"\xC6\x40\xAB\x00"          # 109 mov byte ptr [eax+14h], 0
            b"\x80\x78\xAB\x00"          # 113 cmp byte ptr [eax+0Dh], 0
            b"\x75\xAB"                  # 117 jnz short loc_6648C8
            b"\x8B\x50\xAB"              # 119 mov edx, [eax+8]
            b"\x80\x7A\xAB\x00"          # 122 cmp byte ptr [edx+0Dh], 0
            b"\x75\xAB"                  # 126 jnz short loc_6648AD
            b"\x8B\xC2"                  # 128 mov eax, edx
            b"\x8B\x10"                  # 130 mov edx, [eax]
            b"\x80\x7A\xAB\x00"          # 132 cmp byte ptr [edx+0Dh], 0
            b"\x75\xAB"                  # 136 jnz short loc_6648C8
            b"\x8B\xC2"                  # 138 mov eax, edx
            b"\x8B\x10"                  # 140 mov edx, [eax]
            b"\x80\x7A\xAB\x00"          # 142 cmp byte ptr [edx+0Dh], 0
            b"\x74\xAB"                  # 146 jz short loc_6648A1
            b"\xEB\xAB"                  # 148 jmp short loc_6648C8
            b"\x8B\x50\xAB"              # 150 mov edx, [eax+4]
            b"\x80\x7A\xAB\x00"          # 153 cmp byte ptr [edx+0Dh], 0
            b"\x75\xAB"                  # 157 jnz short loc_6648C6
            b"\x3B\x42\xAB"              # 159 cmp eax, [edx+8]
            b"\x75\xAB"                  # 162 jnz short loc_6648C6
            b"\x8B\xC2"                  # 164 mov eax, edx
            b"\x8B\x52\xAB"              # 166 mov edx, [edx+4]
            b"\x80\x7A\xAB\x00"          # 169 cmp byte ptr [edx+0Dh], 0
            b"\x74\xAB"                  # 173 jz short loc_6648B6
            b"\x8B\xC2"                  # 175 mov eax, edx
            b"\x83\xC7\x04"              # 177 add edi, 4
            b"\x83\xC6\x02"              # 180 add esi, 2
            b"\x3B\xC1"                  # 183 cmp eax, ecx
            b"\x75\xAB"                  # 185 jnz short loc_664864
            b"\x5E"                      # 187 pop esi
            b"\x85\xDB"                  # 188 test ebx, ebx
            b"\x74\xAB"                  # 190 jz short loc_66491C
            b"\x8D\x45\xAB"              # 192 lea eax, [ebp+buf]
            b"\x50"                      # 195 push eax
            b"\x0F\xBF\x45\xAB"          # 196 movsx eax, word ptr [ebp+buf]
            b"\x50"                      # 200 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 201 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 206 mov ecx, eax
            b"\xE8"                      # 208 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 208,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 202,
        }
    ],
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 204h
            b"\x66\x89\x55\xAB"          # 5  mov word ptr [ebp+buf], dx
            b"\x85\xC0"                  # 9  test eax, eax
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 11 jz loc_A8E39B
            b"\x8D\x45\xAB"              # 17 lea eax, [ebp+buf+2]
            b"\x50"                      # 20 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call sub_A8E540
            b"\x85\xC0"                  # 26 test eax, eax
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 28 jz loc_A8E39B
            b"\xA1\xAB\xAB\xAB\xAB"      # 34 mov eax, g_serviceType
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 39 mov dword ptr [ebp+var_28], 3
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 46 mov dword ptr [ebp+var_28+4],
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 53 mov dword ptr [ebp+var_20], 1
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 60 mov dword ptr [ebp+var_20+4],
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 67 mov dword ptr [ebp+var_38], 9
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 74 mov dword ptr [ebp+var_38+4],
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 81 mov dword ptr [ebp+var_30], 7
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 88 mov dword ptr [ebp+var_30+4],
            b"\x85\xC0"                  # 95 test eax, eax
            b"\x75\xAB"                  # 97 jnz short loc_A8E33B
            b"\xA1\xAB\xAB\xAB\xAB"      # 99 mov eax, g_serverType
            b"\x83\xF8\x01"              # 104 cmp eax, 1
            b"\x75\xAB"                  # 107 jnz short loc_A8E32D
            b"\xF3\x0F\x7E\x45\xAB"      # 109 movq xmm0, [ebp+var_38]
            b"\x66\x0F\xD6\x45\xAB"      # 114 movq qword ptr [ebp+buf+2], xmm0
            b"\xF3\x0F\x7E\x45\xAB"      # 119 movq xmm0, [ebp+var_30]
            b"\xEB\xAB"                  # 124 jmp short loc_A8E374
            b"\x85\xC0"                  # 126 test eax, eax
            b"\x74\xAB"                  # 128 jz short loc_A8E365
            b"\x0F\x57\xC0"              # 130 xorps xmm0, xmm0
            b"\x66\x0F\xD6\x45\xAB"      # 133 movq qword ptr [ebp+buf+2], xmm0
            b"\xEB\xAB"                  # 138 jmp short loc_A8E374
            b"\x83\xF8\x06"              # 140 cmp eax, 6
            b"\x75\xAB"                  # 143 jnz short loc_A8E379
            b"\x83\xAB\xAB\xAB\xAB\xAB\x01"  # 145 cmp g_serverType, 1
            b"\x75\xAB"                  # 152 jnz short loc_A8E379
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 154 mov dword ptr [ebp+var_28],
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 161 mov dword ptr [ebp+var_28+4]
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 168 mov dword ptr [ebp+var_20],
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 175 mov dword ptr [ebp+var_20+4]
            b"\xF3\x0F\x7E\x45\xAB"      # 182 movq xmm0, [ebp+var_28]
            b"\x66\x0F\xD6\x45\xAB"      # 187 movq qword ptr [ebp+buf+2], xmm0
            b"\xF3\x0F\x7E\x45\xAB"      # 192 movq xmm0, [ebp+var_20]
            b"\x66\x0F\xD6\x45\xAB"      # 197 movq [ebp+var_E], xmm0
            b"\x8D\x45\xAB"              # 202 lea eax, [ebp+buf]
            b"\x50"                      # 205 push eax
            b"\x0F\xBF\x45\xAB"          # 206 movsx eax, word ptr [ebp+buf]
            b"\x50"                      # 210 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 211 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 216 mov ecx, eax
            b"\xE8"                      # 218 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 218,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 212,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 9A7h
            b"\x50"                      # 5  push eax
            b"\x51"                      # 6  push ecx
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 7  mov [ebp+Src], cx
            b"\x89\x95\xAB\xAB\xAB\xAB"  # 14 mov [ebp+var_132], edx
            b"\x89\xB5\xAB\xAB\xAB\xAB"  # 20 mov [ebp+var_12E], esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 26 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 31 mov ecx, eax
            b"\xE8"                      # 33 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 33,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 27,
        }
    ],
    # 2014-04-02
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 9A9h
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_132], ecx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 11 mov [ebp+var_12E], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 22 mov ecx, eax
            b"\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 85Dh
            b"\x51"                      # 5  push ecx
            b"\x47"                      # 6  inc edi
            b"\x50"                      # 7  push eax
            b"\x89\xBE\xAB\xAB\xAB\x00"  # 8  mov [esi+0ACh], edi
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 14 mov word ptr [ebp+Args], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 26 mov ecx, eax
            b"\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 9B4h
            b"\x51"                      # 5  push ecx
            b"\x52"                      # 6  push edx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 7  mov dword ptr [ebp+var_106C+6], e
            b"\x66\x89\x95\xAB\xAB\xAB\xAB"  # 13 mov word ptr [ebp+var_106C+4]
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 25 mov ecx, eax
            b"\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2014-04-02
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 920h
            b"\x83\xC4\x04"              # 5  add esp, 4
            b"\x66\x89\x45\xAB"          # 8  mov word ptr [ebp+var_26+2], ax
            b"\xFF\xD7"                  # 12 call edi
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 14 mov ecx, dword ptr _dword_CDB934
            b"\x8B\x15\xAB\xAB\xAB\xAB"  # 20 mov edx, dword ptr _dword_CDB934+
            b"\x89\x45\xAB"              # 26 mov [ebp+var_18+2], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 29 mov eax, dword_CDB16C
            b"\x89\x4D\xAB"              # 34 mov [ebp-22h], ecx
            b"\xB9\xAB\xAB\xAB\xAB"      # 37 mov ecx, offset dword_CDA5A8
            b"\x89\x55\xAB"              # 42 mov dword ptr [ebp+anonymous_1+4]
            b"\x89\x45\xAB"              # 45 mov dword ptr [ebp+anonymous_1+8]
            b"\xE8\xAB\xAB\xAB\xAB"      # 48 call sub_8AF090
            b"\x0F\xBF\x55\xAB"          # 53 movsx edx, word ptr [ebp+var_26+2
            b"\x8D\x4D\xAB"              # 57 lea ecx, [ebp+var_26+2]
            b"\x51"                      # 60 push ecx
            b"\x52"                      # 61 push edx
            b"\x88\x45\xAB"              # 62 mov byte ptr [ebp+var_13+1], al
            b"\xE8\xAB\xAB\xAB\xAB"      # 65 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 70 mov ecx, eax
            b"\xE8"                      # 72 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 72,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 66,
        }
    ],
    # 2016-04-27
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A08h
            b"\x66\x89\x45\xAB"          # 5  mov word ptr [ebp+buf], ax
            b"\x8B\x87\xAB\xAB\xAB\xAB"  # 9  mov eax, [edi+94h]
            b"\x0F\x57\xC0"              # 15 xorps xmm0, xmm0
            b"\x81\xC7\xAB\xAB\xAB\x00"  # 18 add edi, 84h
            b"\x66\x0F\xD6\x45\xAB"      # 24 movq qword ptr [ebp+buf+2], xmm0
            b"\x83\x7F\xAB\xAB"          # 29 cmp dword ptr [edi+14h], 10h
            b"\x66\x0F\xD6\x45\xAB"      # 33 movq [ebp+var_2E], xmm0
            b"\x66\x0F\xD6\x45\xAB"      # 38 movq [ebp+var_26], xmm0
            b"\x72\x02"                  # 43 jb short loc_4BD3D8
            b"\x8B\x3F"                  # 45 mov edi, [edi]
            b"\x50"                      # 47 push eax
            b"\x8D\x45\xAB"              # 48 lea eax, [ebp+buf+2]
            b"\x57"                      # 51 push edi
            b"\x50"                      # 52 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call sub_A4339C
            b"\x83\xC4\x0C"              # 58 add esp, 0Ch
            b"\x8D\x45\xAB"              # 61 lea eax, [ebp+buf]
            b"\x50"                      # 64 push eax
            b"\x0F\xBF\x45\xAB"          # 65 movsx eax, word ptr [ebp+buf]
            b"\x50"                      # 69 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 70 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 75 mov ecx, eax
            b"\xE8"                      # 77 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 77,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 71,
        }
    ],
    # 2014-09-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 9EEh
            b"\x88\x55\xAB"              # 5  mov [ebp+var_36], dl
            b"\x89\x5D\xAB"              # 8  mov [ebp+var_35], ebx
            b"\x89\x45\xAB"              # 11 mov [ebp+var_31], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2014-09-24
    [
        (
            b"\x68\xAB\xAB\x00\x00"      # 0  push 9E8h
            b"\x88\x45\xAB"              # 5  mov [ebp+var_E], al
            b"\x89\x45\xAB"              # 8  mov [ebp+var_D], eax
            b"\x89\x45\xAB"              # 11 mov [ebp+var_9], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 19 mov ecx, eax
            b"\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2016-01-13
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 88Bh
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_1118+4]
            b"\x8D\x51\xAB"              # 12 lea edx, [ecx+1]
            b"\x8A\x01"                  # 15 mov al, [ecx]
            b"\x41"                      # 17 inc ecx
            b"\x84\xC0"                  # 18 test al, al
            b"\x75\xF9"                  # 20 jnz short loc_8C12E3
            b"\x2B\xCA"                  # 22 sub ecx, edx
            b"\x83\xF9\xAB"              # 24 cmp ecx, 18h
            b"\x0F\x83\xAB\xAB\xAB\xAB"  # 27 jnb loc_8C11AE
            b"\x6A\xAB"                  # 33 push 18h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 35 lea eax, [ebp+var_1118+6]
            b"\x53"                      # 41 push ebx
            b"\x50"                      # 42 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 43 call strncpy
            b"\x83\xC4\x0C"              # 49 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 52 lea eax, [ebp+var_1118+4]
            b"\x50"                      # 58 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 59 movsx eax, word ptr [ebp+var_
            b"\x50"                      # 66 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 67 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 72 mov ecx, eax
            b"\xE8"                      # 74 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 74,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 68,
        }
    ],
    # 2015-04-08
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 878h
            b"\x66\x89\x45\xAB"          # 5  mov word ptr [ebp+var_30+4], ax
            b"\x8D\x51\xAB"              # 9  lea edx, [ecx+1]
            b"\x8A\x01"                  # 12 mov al, [ecx]
            b"\x41"                      # 14 inc ecx
            b"\x84\xC0"                  # 15 test al, al
            b"\x75\xAB"                  # 17 jnz short loc_87C721
            b"\x2B\xCA"                  # 19 sub ecx, edx
            b"\x83\xF9\xAB"              # 21 cmp ecx, 18h
            b"\x0F\x83\xAB\xAB\xAB\xAB"  # 24 jnb loc_87C5FE
            b"\x6A\xAB"                  # 30 push 18h
            b"\x8D\x45\xAB"              # 32 lea eax, [ebp+var_30+6]
            b"\x53"                      # 35 push ebx
            b"\x50"                      # 36 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 37 call strncpy
            b"\x83\xC4\x0C"              # 43 add esp, 0Ch
            b"\x8D\x45\xAB"              # 46 lea eax, [ebp+var_30+4]
            b"\x50"                      # 49 push eax
            b"\x0F\xBF\x45\xAB"          # 50 movsx eax, word ptr [ebp+var_30+4
            b"\x50"                      # 54 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 55 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 60 mov ecx, eax
            b"\xE8"                      # 62 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 62,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 56,
        }
    ],
    # 2018-12-12
    [
        (
            b"\xBF\xAB\xAB\x00\x00"      # 0  mov edi, 9C3h
            b"\x56"                      # 5  push esi
            b"\x6A\xAB"                  # 6  push 8
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 8  lea eax, [ebp+var_95C]
            b"\x50"                      # 14 push eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 15 mov ecx, offset g_session
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CSession_sub_AC9D50
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 25 mov [ebp+var_4], 9
            b"\xA1\xAB\xAB\xAB\xAB"      # 32 mov eax, g_session.m_account_id
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 37 lea ecx, [ebp+var_95C]
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 43 mov [ebp+Src], di
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 50 mov [ebp+var_81A], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 56 call sub_50BFB0
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 61 mov [ebp+var_816], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 67 lea eax, [ebp+Src]
            b"\x50"                      # 73 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 74 movsx eax, [ebp+Src]
            b"\x50"                      # 81 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 82 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 87 mov ecx, eax
            b"\xE8"                      # 89 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 89,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 83,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xBF\xAB\xAB\x00\x00"      # 0  mov edi, 9C3h
            b"\x56"                      # 5  push esi
            b"\x6A\xAB"                  # 6  push 8
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 8  lea eax, [ebp+var_92C]
            b"\xB9\xAB\xAB\xAB\xAB"      # 14 mov ecx, offset g_session
            b"\x50"                      # 19 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CSession_sub_A70780
            b"\xC7\x45\xAB\xAB\xAB\xAB\x00"  # 25 mov [ebp+var_4], 12h
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 32 lea ecx, [ebp+var_92C]
            b"\xA1\xAB\xAB\xAB\xAB"      # 38 mov eax, dword_F68524
            b"\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 43 mov word ptr [ebp+var_81C], d
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 50 mov dword ptr [ebp+var_81C+2], ea
            b"\xE8\xAB\xAB\xAB\xAB"      # 56 call sub_518820
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 61 mov dword ptr [ebp+var_81C+6], ea
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 67 lea eax, [ebp+var_81C]
            b"\x50"                      # 73 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 74 movsx eax, word ptr [ebp+var_
            b"\x50"                      # 81 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 82 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 87 mov ecx, eax
            b"\xE8"                      # 89 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 89,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 83,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 82Bh
            b"\x8D\x8E\xAB\xAB\xAB\x00"  # 5  lea ecx, [esi+0B0h]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 11 mov word ptr [ebp+Src+8], ax
            b"\x0F\xB6\x05\xAB\xAB\xAB\xAB"  # 18 movzx eax, g_charInfo.slot
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 25 mov [ebp+var_14C], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 31 lea eax, [ebp+var_14C]
            b"\x50"                      # 37 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 38 call sub_51E320
            b"\x8B\x00"                  # 43 mov eax, [eax]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 45 mov dword ptr [ebp+Src+0Ah], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 51 lea eax, [ebp+Src+8]
            b"\x50"                      # 57 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 58 movsx eax, word ptr [ebp+Src+
            b"\x50"                      # 65 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 66 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 71 mov ecx, eax
            b"\xE8"                      # 73 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 73,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 67,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 0A52h
            b"\x6A\xAB"                  # 5  push 10h
            b"\x66\x89\x45\xAB"          # 7  mov [ebp+Src], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call sub_582BC0
            b"\x50"                      # 16 push eax
            b"\x8D\x45\xAB"              # 17 lea eax, [ebp+Dst]
            b"\x6A\xAB"                  # 20 push 10h
            b"\x50"                      # 22 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call sub_53B0F0
            b"\x66\x8B\x87\xAB\xAB\xAB\x00"  # 28 mov ax, [edi+0C4h]
            b"\x83\xC4\x10"              # 35 add esp, 10h
            b"\x66\x89\x45\xAB"          # 38 mov word ptr [ebp+var_14+2], ax
            b"\x8D\x45\xAB"              # 42 lea eax, [ebp+Src]
            b"\x50"                      # 45 push eax
            b"\x0F\xBF\x45\xAB"          # 46 movsx eax, [ebp+Src]
            b"\x50"                      # 50 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 51 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 56 mov ecx, eax
            b"\xE8"                      # 58 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 58,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 52,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 0A5Ch
            b"\x6A\xAB"                  # 5  push 10h
            b"\x66\x89\x45\xAB"          # 7  mov [ebp+Src], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call sub_582BC0
            b"\x50"                      # 16 push eax
            b"\x8D\x45\xAB"              # 17 lea eax, [ebp+Dst]
            b"\x6A\xAB"                  # 20 push 10h
            b"\x50"                      # 22 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 23 call sub_53C1D0
            b"\x83\xC4\x10"              # 28 add esp, 10h
            b"\x8D\x45\xAB"              # 31 lea eax, [ebp+Src]
            b"\x50"                      # 34 push eax
            b"\x0F\xBF\x45\xAB"          # 35 movsx eax, [ebp+Src]
            b"\x50"                      # 39 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 40 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 45 mov ecx, eax
            b"\xE8"                      # 47 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 47,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 41,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 0A13h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+Src], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call sub_582BC0
            b"\x50"                      # 17 push eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 18 lea eax, [ebp+Dst]
            b"\x6A\xAB"                  # 24 push 18h
            b"\x50"                      # 26 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 27 call ds:strcpy_s
            b"\x83\xC4\x0C"              # 33 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 36 lea eax, [ebp+Src]
            b"\x50"                      # 42 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 43 movsx eax, [ebp+Src]
            b"\x50"                      # 50 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 51 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 56 mov ecx, eax
            b"\xE8"                      # 58 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 58,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 52,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 9C3h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_81C], a
            b"\x8D\x4E\xAB"              # 12 lea ecx, [esi+8]
            b"\xA1\xAB\xAB\xAB\xAB"      # 15 mov eax, dword_F68524
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 20 mov dword ptr [ebp+var_81C+2], ea
            b"\xE8\xAB\xAB\xAB\xAB"      # 26 call sub_518820
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 31 mov dword ptr [ebp+var_81C+6], ea
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 37 lea eax, [ebp+var_81C]
            b"\x50"                      # 43 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 44 movsx eax, word ptr [ebp+var_
            b"\x50"                      # 51 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 52 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 57 mov ecx, eax
            b"\xE8"                      # 59 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 59,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 53,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 83Ch
            b"\xC7\x05\xAB\xAB\xAB\xAB\xFF\xFF\xFF\xFF"  # 5  mov dword_F6C5E8,
            b"\xC7\x05\xAB\xAB\xAB\xAB\xFF\xFF\xFF\xFF"  # 15 mov dword_F6C5EC,
            b"\x8D\x4E\xAB"              # 25 lea ecx, [esi+60h]
            b"\xC7\x05\xAB\xAB\xAB\xAB\xFF\xFF\xFF\xFF"  # 28 mov dword_F6C5F0,
            b"\x66\x89\x45\xAB"          # 38 mov [ebp+Src], ax
            b"\x8B\x46\xAB"              # 42 mov eax, [esi+4]
            b"\x89\x45\xAB"              # 45 mov [ebp+var_1E], eax
            b"\x8B\x06"                  # 48 mov eax, [esi]
            b"\x89\x45\xAB"              # 50 mov [ebp+var_1A], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call sub_518820
            b"\x89\x45\xAB"              # 58 mov [ebp+var_16], eax
            b"\x8D\x45\xAB"              # 61 lea eax, [ebp+Src]
            b"\x50"                      # 64 push eax
            b"\x0F\xBF\x45\xAB"          # 65 movsx eax, [ebp+Src]
            b"\x50"                      # 69 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 70 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 75 mov ecx, eax
            b"\xE8"                      # 77 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 77,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 71,
        }
    ],
    # 2019-02-13
    [
        (
            b"\x68\xAB\xAB\xAB\x00"      # 0  push 8FCh
            b"\xF3\x0F\x7E\x47\xAB"      # 5  movq xmm0, qword ptr [edi+10h]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 10 movq [ebp+var_176], xmm0
            b"\x0F\x10\x07"              # 18 movups xmm0, xmmword ptr [edi]
            b"\x0F\x11\x05\xAB\xAB\xAB\xAB"  # 21 movups xmmword ptr unk_F6C314
            b"\xF3\x0F\x7E\x47\xAB"      # 28 movq xmm0, qword ptr [edi+10h]
            b"\x66\x0F\xD6\x05\xAB\xAB\xAB\xAB"  # 33 movq qword ptr unk_F6C324
            b"\xE8\xAB\xAB\xAB\xAB"      # 41 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 46 mov ecx, eax
            b"\xE8"                      # 48 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 48,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 42,
        }
    ],
    # 2019-02-13
    [
        (
            b"\x68\xAB\xAB\xAB\x00"      # 0  push 802h
            b"\x0F\x11\x85\xAB\xAB\xAB\xAB"  # 5  movups xmmword ptr [ebp-1A6h]
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 907h
            b"\xFF\x75\xAB"              # 5  push [ebp+arg_0]
            b"\x66\x89\x45\xAB"          # 8  mov [ebp+Src], ax
            b"\x66\x8B\x42\xAB"          # 12 mov ax, [edx+4]
            b"\x66\x89\x45\xAB"          # 16 mov [ebp+var_6], ax
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call sub_5A5980
            b"\x83\xF8\xAB"              # 25 cmp eax, 3
            b"\x8D\x45\xAB"              # 28 lea eax, [ebp+Src]
            b"\x50"                      # 31 push eax
            b"\x0F\xBF\x45\xAB"          # 32 movsx eax, [ebp+Src]
            b"\x0F\x95\x45\xAB"          # 36 setnz [ebp+var_4]
            b"\x50"                      # 40 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 41 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 46 mov ecx, eax
            b"\xE8"                      # 48 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 48,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 42,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB9\xAB\xAB\xAB\x00"      # 0  mov ecx, 202h
            b"\x66\x89\x4D\xAB"          # 5  mov [ebp+Src], cx
            b"\x0F\x10\x00"              # 9  movups xmm0, xmmword ptr [eax]
            b"\x0F\x11\x45\xAB"          # 12 movups [ebp+var_1E], xmm0
            b"\xF3\x0F\x7E\x40\xAB"      # 16 movq xmm0, qword ptr [eax+10h]
            b"\x8D\x45\xAB"              # 21 lea eax, [ebp+Src]
            b"\x50"                      # 24 push eax
            b"\x51"                      # 25 push ecx
            b"\x66\x0F\xD6\x45\xAB"      # 26 movq [ebp+var_E], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"      # 31 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 36 mov ecx, eax
            b"\xE8"                      # 38 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 38,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 32,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 0A49h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+var_28], ax
            b"\x8D\x45\xAB"              # 9  lea eax, [ebp+Src]
            b"\x0F\x43\x45\xAB"          # 12 cmovnb eax, [ebp+Src]
            b"\x50"                      # 16 push eax
            b"\x8D\x45\xAB"              # 17 lea eax, [ebp+Dst]
            b"\x6A\xAB"                  # 20 push 10h
            b"\x50"                      # 22 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 23 call ds:strcpy_s
            b"\x8B\x45\xAB"              # 29 mov eax, [ebp+arg_18]
            b"\x83\xC4\x0C"              # 32 add esp, 0Ch
            b"\x89\x45\xAB"              # 35 mov [ebp+var_16], eax
            b"\x8D\x45\xAB"              # 38 lea eax, [ebp+var_28]
            b"\x50"                      # 41 push eax
            b"\x0F\xBF\x45\xAB"          # 42 movsx eax, [ebp+var_28]
            b"\x50"                      # 46 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 47 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 52 mov ecx, eax
            b"\xE8"                      # 54 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 54,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 48,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB9\xAB\xAB\xAB\x00"      # 0  mov ecx, 8DDh
            b"\x50"                      # 5  push eax
            b"\x8D\x45\xAB"              # 6  lea eax, [ebp+Dst]
            b"\x66\x89\x4D\xAB"          # 9  mov [ebp+var_20], cx
            b"\x6A\xAB"                  # 13 push 18h
            b"\x50"                      # 15 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 16 call ds:strcpy_s
            b"\x8A\x45\xAB"              # 22 mov al, [ebp+arg_4]
            b"\x83\xC4\x0C"              # 25 add esp, 0Ch
            b"\x88\x45\xAB"              # 28 mov [ebp+var_1E], al
            b"\x8D\x45\xAB"              # 31 lea eax, [ebp+var_20]
            b"\x50"                      # 34 push eax
            b"\x0F\xBF\x45\xAB"          # 35 movsx eax, [ebp+var_20]
            b"\x50"                      # 39 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 40 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 45 mov ecx, eax
            b"\xE8"                      # 47 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 47,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 41,
        }
    ],
    # 2019-02-13
    [
        (
            b"\x68\xAB\xAB\xAB\x00"      # 0  push 998h
            b"\x89\xB5\xAB\xAB\xAB\xAB"  # 5  mov [ebp-0EC8Ch], esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB9\xAB\xAB\xAB\x00"      # 0  mov ecx, 363h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp-0EAFAh], ax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 12 mov eax, [ebp-0E9B0h]
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 18 mov [ebp-0EAF8h], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 25 lea eax, [ebp-0EAFCh]
            b"\x50"                      # 31 push eax
            b"\x51"                      # 32 push ecx
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 33 mov [ebp-0EAFCh], cx
            b"\xE8\xAB\xAB\xAB\xAB"      # 40 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 45 mov ecx, eax
            b"\xE8"                      # 47 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 47,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 41,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB9\xAB\xAB\xAB\x00"      # 0  mov ecx, 0E8h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp-0EBC2h], ax
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 12 mov eax, [ebp-0E9B0h]
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 18 mov [ebp-0EBC0h], eax
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 24 mov [ebp-0EBC4h], cx
            b"\x89\x87\xAB\xAB\xAB\x00"  # 31 mov [edi+114h], eax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 37 lea eax, [ebp-0EBC4h]
            b"\x50"                      # 43 push eax
            b"\x51"                      # 44 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 45 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 50 mov ecx, eax
            b"\xE8"                      # 52 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 52,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 46,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 2C4h
            b"\x8B\xCA"                  # 5  mov ecx, edx
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 7  mov [ebp-0CA48h], ax
            b"\x8D\x71\xAB"              # 14 lea esi, [ecx+1]
            b"\x0F\x1F\x00"              # 17 nop dword ptr [eax]
            b"\x8A\x01"                  # 20 mov al, [ecx]
            b"\x41"                      # 22 inc ecx
            b"\x84\xC0"                  # 23 test al, al
            b"\x75\xAB"                  # 25 jnz short loc_9C9AD0
            b"\x2B\xCE"                  # 27 sub ecx, esi
            b"\x83\xF9\xAB"              # 29 cmp ecx, 18h
            b"\x0F\x83\xAB\xAB\xAB\xAB"  # 32 jnb loc_9C999E
            b"\x6A\xAB"                  # 38 push 18h
            b"\x52"                      # 40 push edx
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 41 lea eax, [ebp-0CA46h]
            b"\x50"                      # 47 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 48 call ds:strncpy
            b"\x83\xC4\x0C"              # 54 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 57 lea eax, [ebp-0CA48h]
            b"\x50"                      # 63 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 64 movsx eax, word ptr [ebp-0CA4
            b"\x50"                      # 71 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 72 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 77 mov ecx, eax
            b"\xE8"                      # 79 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 79,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 73,
        }
    ],
    # 2019-02-13
    [
        (
            b"\x68\xAB\xAB\xAB\x00"      # 0  push 213h
            b"\x0F\x11\x85\xAB\xAB\xAB\xAB"  # 5  movups xmmword ptr [ebp-0C78A
            b"\xF3\x0F\x7E\x42\xAB"      # 12 movq xmm0, qword ptr [edx+10h]
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 17 movq qword ptr [ebp-0C77A
            b"\xE8\xAB\xAB\xAB\xAB"      # 25 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 30 mov ecx, eax
            b"\xE8"                      # 32 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 32,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 26,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB9\xAB\xAB\xAB\x00"      # 0  mov ecx, 0A99h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 5  lea eax, [ebp-0E9E0h]
            b"\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 11 mov [ebp-0E9E0h], cx
            b"\x50"                      # 18 push eax
            b"\x51"                      # 19 push ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 25 mov ecx, eax
            b"\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 436h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+var_3C], ax
            b"\xFF\xD7"                  # 9  call edi
            b"\x89\x45\xAB"              # 11 mov [ebp+var_2E], eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 14 mov ecx, offset g_session
            b"\xA1\xAB\xAB\xAB\xAB"      # 19 mov eax, dword ptr qword_F68524
            b"\x89\x45\xAB"              # 24 mov [ebp+var_3A], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 27 mov eax, dword ptr qword_F68524+4
            b"\x89\x45\xAB"              # 32 mov [ebp+var_36], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 35 mov eax, dword_F67DD4
            b"\x89\x45\xAB"              # 40 mov [ebp+var_32], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 43 call CSession_sub_A929B0
            b"\x88\x45\xAB"              # 48 mov [ebp+var_2A], al
            b"\x8D\x45\xAB"              # 51 lea eax, [ebp+var_3C]
            b"\x50"                      # 54 push eax
            b"\x0F\xBF\x45\xAB"          # 55 movsx eax, [ebp+var_3C]
            b"\x50"                      # 59 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 60 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 65 mov ecx, eax
            b"\xE8"                      # 67 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 67,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 61,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 1DDh
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+Src], ax
            b"\xA1\xAB\xAB\xAB\xAB"      # 9  mov eax, dword_F097D0
            b"\x89\x45\xAB"              # 14 mov [ebp+var_32], eax
            b"\x8D\x45\xAB"              # 17 lea eax, [ebp+var_16]
            b"\x0F\x11\x45\xAB"          # 20 movups [ebp+var_2E], xmm0
            b"\x50"                      # 24 push eax
            b"\xF3\x0F\x7E\x87\xAB\xAB\xAB\x00"  # 25 movq xmm0, qword ptr [edi
            b"\x66\x0F\xD6\x45\xAB"      # 33 movq [ebp+var_1E], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"      # 38 call sub_95CB10
            b"\xE8\xAB\xAB\xAB\xAB"      # 43 call sub_A36E40
            b"\x88\x45\xAB"              # 48 mov [ebp+var_6], al
            b"\x8D\x45\xAB"              # 51 lea eax, [ebp+Src]
            b"\x50"                      # 54 push eax
            b"\x0F\xBF\x45\xAB"          # 55 movsx eax, [ebp+Src]
            b"\x50"                      # 59 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 60 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 65 mov ecx, eax
            b"\xE8"                      # 67 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 67,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 61,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 1FBh
            b"\x6A\xAB"                  # 5  push 32h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 7  mov [ebp+var_654], ax
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 14 lea eax, [ebp+var_64E]
            b"\x56"                      # 20 push esi
            b"\x50"                      # 21 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 22 call ds:strncpy
            b"\x83\xC4\x0C"              # 28 add esp, 0Ch
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 31 mov [ebp+var_61D], 0
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 38 lea eax, [ebp+var_654]
            b"\x50"                      # 44 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 45 movsx eax, [ebp+var_654]
            b"\x50"                      # 52 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 53 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 58 mov ecx, eax
            b"\xE8"                      # 60 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 60,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 54,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 0AD0h
            b"\x66\x89\x45\xAB"          # 5  mov [ebp+var_1C], ax
            b"\x8D\x83\xAB\xAB\xAB\x00"  # 9  lea eax, [ebx+1B8h]
            b"\x6A\xAB"                  # 15 push 9
            b"\x50"                      # 17 push eax
            b"\x8D\x45\xAB"              # 18 lea eax, [ebp+Dst]
            b"\x6A\xAB"                  # 21 push 9
            b"\x50"                      # 23 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 24 call sub_A42810
            b"\x83\xC4\x10"              # 29 add esp, 10h
            b"\x8D\x45\xAB"              # 32 lea eax, [ebp+var_1C]
            b"\x50"                      # 35 push eax
            b"\x0F\xBF\x45\xAB"          # 36 movsx eax, [ebp+var_1C]
            b"\x50"                      # 40 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 41 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 46 mov ecx, eax
            b"\xE8"                      # 48 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 48,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 42,
        }
    ],
    # 2019-02-13
    [
        (
            b"\x68\xAB\xAB\xAB\x00"      # 0  push 233h
            b"\x89\x75\xAB"              # 5  mov dword ptr [ebp+Src+6], esi
            b"\xC6\x45\xAB\xAB"          # 8  mov [ebp+var_6], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 17 mov ecx, eax
            b"\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2019-02-13
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 436h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_5AC], ax
            b"\xA1\xAB\xAB\xAB\xAB"      # 12 mov eax, ds:timeGetTime
            b"\xFF\xD0"                  # 17 call eax
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 19 mov [ebp+var_59E], eax
            b"\xB9\xAB\xAB\xAB\xAB"      # 25 mov ecx, offset g_session
            b"\xA1\xAB\xAB\xAB\xAB"      # 30 mov eax, dword_E6A67C
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 35 mov [ebp+var_5AA], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 41 mov eax, dword_E6A680
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 46 mov [ebp+var_5A6], eax
            b"\xA1\xAB\xAB\xAB\xAB"      # 52 mov eax, dword ptr unk_E69F2C
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 57 mov [ebp+var_5A2], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 63 call CSession_sub_9A0ED0
            b"\x88\x85\xAB\xAB\xAB\xAB"  # 68 mov [ebp+var_59A], al
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 74 lea eax, [ebp+var_5AC]
            b"\x50"                      # 80 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 81 movsx eax, [ebp+var_5AC]
            b"\x50"                      # 88 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 89 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 94 mov ecx, eax
            b"\xE8"                      # 96 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 96,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 90,
        }
    ],
    # 2019-03-27
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 64h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_61C], ax
            b"\xA1\xAB\xAB\xAB\xAB"      # 12 mov eax, dword_E13980
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 17 mov [ebp+var_61A], eax
            b"\x0F\x10\x87\xAB\xAB\xAB\xAB"  # 23 movups xmm0, xmmword ptr [edi
            b"\x0F\x11\x85\xAB\xAB\xAB\xAB"  # 30 movups [ebp+var_616], xmm0
            b"\xF3\x0F\x7E\x87\xAB\xAB\xAB\xAB"  # 37 movq xmm0, qword ptr [edi
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 45 movq [ebp+var_606], xmm0
            b"\x83\xF9\xAB"              # 53 cmp ecx, 7
            b"\x75\x30"                  # 56 jnz short loc_94DCAC
            b"\x6A\xAB"                  # 58 push 18h
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 60 lea eax, [ebp+var_9C]
            b"\x50"                      # 66 push eax
            b"\x8D\x87\xAB\xAB\xAB\xAB"  # 67 lea eax, [edi+138h]
            b"\x50"                      # 73 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 74 call sub_74D200
            b"\x0F\x10\x85\xAB\xAB\xAB\xAB"  # 79 movups xmm0, xmmword ptr [ebp
            b"\x83\xC4\x0C"              # 86 add esp, 0Ch
            b"\x0F\x11\x85\xAB\xAB\xAB\xAB"  # 89 movups [ebp+var_5FE], xmm0
            b"\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 96 movq xmm0, [ebp+var_8C]
            b"\xEB\x33"                  # 104 jmp short loc_94DCDF
            b"\x83\xF9\xAB"              # 106 cmp ecx, 4
            b"\x75\x18"                  # 109 jnz short loc_94DCC9
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 111 lea eax, [ebp+var_5FE]
            b"\x50"                      # 117 push eax
            b"\x8D\x87\xAB\xAB\xAB\xAB"  # 118 lea eax, [edi+138h]
            b"\x50"                      # 124 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 125 call sub_4ABB60
            b"\x83\xC4\x08"              # 130 add esp, 8
            b"\xEB\x1E"                  # 133 jmp short loc_94DCE7
            b"\x0F\x10\x87\xAB\xAB\xAB\xAB"  # 135 movups xmm0, xmmword ptr [ed
            b"\x0F\x11\x85\xAB\xAB\xAB\xAB"  # 142 movups [ebp+var_5FE], xmm0
            b"\xF3\x0F\x7E\x87\xAB\xAB\xAB\xAB"  # 149 movq xmm0, qword ptr [ed
            b"\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 157 movq [ebp+var_5EE], xmm0
            b"\xE8\xAB\xAB\xAB\xAB"      # 165 call sub_949560
            b"\x88\x85\xAB\xAB\xAB\xAB"  # 170 mov [ebp+var_5E6], al
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 176 lea eax, [ebp+var_61C]
            b"\x50"                      # 182 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 183 movsx eax, [ebp+var_61C]
            b"\x50"                      # 190 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 191 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 196 mov ecx, eax
            b"\xE8"                      # 198 call CRagConnection_GetPacketSiz
        ),
        {
            "fixedOffset": 198,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 192,
        }
    ],
    # 2019-05-08
    [
        (
            b"\x68\xAB\xAB\xAB\x00"      # 0  push 9F3h
            b"\x89\x75\xAB"              # 5  mov [ebp+var_A], esi
            b"\x88\x5D\xAB"              # 8  mov [ebp+var_6], bl
            b"\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 16 mov ecx, eax
            b"\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2019-07-03
    [
        (
            b"\xB8\xAB\xAB\xAB\x00"      # 0  mov eax, 2C4h
            b"\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_CA8C],
            b"\x8D\x71\xAB"              # 12 lea esi, [ecx+1]
            b"\x8A\x01"                  # 15 mov al, [ecx]
            b"\x41"                      # 17 inc ecx
            b"\x84\xC0"                  # 18 test al, al
            b"\x75\xF9"                  # 20 jnz short loc_9EEAA7
            b"\x2B\xCE"                  # 22 sub ecx, esi
            b"\x83\xF9\xAB"              # 24 cmp ecx, 18h
            b"\x0F\x83\xAB\xAB\xAB\xAB"  # 27 jnb loc_9EE97E
            b"\x6A\x18"                  # 33 push 18h
            b"\x52"                      # 35 push edx
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 36 lea eax, [ebp+var_CA8A]
            b"\x50"                      # 42 push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 43 call ds:strncpy
            b"\x83\xC4\x0C"              # 49 add esp, 0Ch
            b"\x8D\x85\xAB\xAB\xAB\xAB"  # 52 lea eax, [ebp+var_CA8C]
            b"\x50"                      # 58 push eax
            b"\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 59 movsx eax, word ptr [ebp+var_
            b"\x50"                      # 66 push eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 67 call CRagConnection_instanceR
            b"\x8B\xC8"                  # 72 mov ecx, eax
            b"\xE8"                      # 74 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 74,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 68,
        }
    ],
]
