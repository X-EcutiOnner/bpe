#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


blocks = [
    # 2016-2018
    [
        (
            b"\xA1\xAB\xAB\xAB\xAB"      # 0  mov eax, g_soundMgr
            b"\x6A\x04"                  # 5  push 4
            b"\x83\xC0\xAB"              # 7  add eax, CSoundMgr.m_isBgmOn
            b"\x50"                      # 10 push eax
            b"\x6A\x04"                  # 11 push 4
            b"\x6A\x00"                  # 13 push 0
            b"\x68\xAB\xAB\xAB\xAB"      # 15 push offset aIsbgmon
            b"\xFF\xB5\xAB\xAB\xAB\xAB"  # 20 push [ebp+phkResult]
            b"\xFF\xD3"                  # 26 call ebx
        ),
        {
            "fixedOffset": 16,
            "retOffset": 1,
            "memberOffset": (9, 1)
        },
        {
            "strOffset": (16, False)
        }
    ],
    # 2015-01-07
    [
        (
            b"\xA1\xAB\xAB\xAB\xAB"      # 0  mov eax, g_soundMgr
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 5  mov ecx, [ebp+phkResult]
            b"\x6A\x04"                  # 11 push 4
            b"\x83\xC0\xAB"              # 13 add eax, 8
            b"\x50"                      # 16 push eax
            b"\x6A\x04"                  # 17 push 4
            b"\x6A\x00"                  # 19 push 0
            b"\x68\xAB\xAB\xAB\xAB"      # 21 push offset aIssoundon
            b"\x51"                      # 26 push ecx
            b"\xFF\xD3"                  # 27 call ebx
        ),
        {
            "fixedOffset": 22,
            "retOffset": 1,
            "memberOffset": (15, 1)
        },
        {
            "strOffset": (22, False)
        }
    ],
    # 2015-01-07
    [
        (
            b"\x8B\x0D\xAB\xAB\xAB\xAB"  # 0  mov ecx, g_soundMgr
            b"\x8B\x95\xAB\xAB\xAB\xAB"  # 6  mov edx, [ebp+phkResult]
            b"\x6A\x04"                  # 12 push 4
            b"\x83\xC1\xAB"              # 14 add ecx, 10h
            b"\x51"                      # 17 push ecx
            b"\x6A\x04"                  # 18 push 4
            b"\x6A\x00"                  # 20 push 0
            b"\x68\xAB\xAB\xAB\xAB"      # 22 push offset aIsbgmon
            b"\x52"                      # 27 push edx
            b"\xFF\xD3"                  # 28 call ebx
        ),
        {
            "fixedOffset": 23,
            "retOffset": 2,
            "memberOffset": (16, 1)
        },
        {
            "strOffset": (23, False)
        }
    ],
]
