#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


blocks = [
    # 2019
    [
        (
            b"\x6A\x04"                  # 0  push 4
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push (offset g_windowMgr+22h)
            b"\x6A\x04"                  # 7  push 4
            b"\x6A\x00"                  # 9  push 0
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push offset aM_bcansetchatm
            b"\xFF\xB5\xAB\xAB\xAB\xAB"  # 16 push [ebp+phkResult]
            b"\xFF\xD3"                  # 22 call ebx
        ),
        {
            "fixedOffset": 12,
            "retOffset": 0,
            "memberOffset": (3, 4),
        },
        {
            "strOffset": (12, False),
        }
    ],
    # 2010
    [
        (
            b"\x8B\x54\x24\xAB"          # 0  mov edx, [esp+324h+phkResult]
            b"\x6A\x04"                  # 4  push 4
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push (offset g_windowMgr+2Ah)
            b"\x6A\x04"                  # 11 push 4
            b"\x6A\x00"                  # 13 push 0
            b"\x68\xAB\xAB\xAB\xAB"      # 15 push offset aM_bcansetchatm
            b"\x52"                      # 20 push edx
            b"\xFF\xD3"                  # 21 call ebx
        ),
        {
            "fixedOffset": 16,
            "retOffset": 0,
            "memberOffset": (7, 4),
        },
        {
            "strOffset": (16, False),
        }
    ],
    # 2011
    [
        (
            b"\x6A\x04"                  # 0  push 4
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push offset byte_8A9E4A
            b"\x6A\x04"                  # 7  push 4
            b"\x6A\x00"                  # 9  push 0
            b"\x8B\x4C\x24\xAB"          # 11 mov ecx, [esp+334h+phkResult]
            b"\x68\xAB\xAB\xAB\xAB"      # 15 push offset aM_bcansetchatm
            b"\x51"                      # 20 push ecx
            b"\xFF\xD3"                  # 21 call ebx
        ),
        {
            "fixedOffset": 16,
            "retOffset": 0,
            "memberOffset": (3, 4),
        },
        {
            "strOffset": (16, False),
        }
    ],
    # 2013
    [
        (
            b"\x6A\x04"                  # 0  push 4
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push (offset g_windowMgr+2Eh)
            b"\x6A\x04"                  # 7  push 4
            b"\x6A\x00"                  # 9  push 0
            b"\x8B\x8D\xAB\xAB\xAB\xAB"  # 11 mov ecx, [ebp+phkResult]
            b"\x68\xAB\xAB\xAB\xAB"      # 17 push offset aM_bcansetchatm
            b"\x51"                      # 22 push ecx
            b"\xFF\xD3"                  # 23 call ebx
        ),
        {
            "fixedOffset": 18,
            "retOffset": 0,
            "memberOffset": (3, 4),
        },
        {
            "strOffset": (18, False),
        }
    ],
]
