#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


blocks = [
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0BE5h
            b"\xC7\x40\xAB\xAB\xAB\xAB\xAB"  # 5  mov dword ptr [eax+2Ch], 0B0h
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 89Ch
            b"\x89\x86\xAB\xAB\xAB\xAB"  # 5  mov [esi+8Ch], eax
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0A60h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_10], 0
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0A62h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_10], 0
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 12 mov [ebp+var_14], 1
            b"\xE8"                      # 19 call MsgStr
        ),
        {
            "fixedOffset": 19,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x74\xAB\xAB"          # 0  push [ebp+ebx*4+a1]
            b"\x8B\xF3"                  # 4  mov esi, ebx
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 6  mov [ebp+var_DC], eax
            b"\x83\xE6\xAB"              # 12 and esi, 1
            b"\xE8"                      # 15 call MsgStr
        ),
        {
            "fixedOffset": 15,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 87Eh
            b"\xC7\x47\xAB\xAB\xAB\xAB\xAB"  # 5  mov dword ptr [edi+2Ch], 0CAh
            b"\xC7\x87\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 12 mov dword ptr [ed
            b"\xE8"                      # 22 call MsgStr
        ),
        {
            "fixedOffset": 22,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 1
            b"\x50"                      # 2  push eax
            b"\x6A\xAB"                  # 3  push 14h
            b"\xE8"                      # 5  call MsgStr
        ),
        {
            "fixedOffset": 5,
            "msgId": (4, 1),
            "retOffset": 3,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0B55h
            b"\x8B\x30"                  # 5  mov esi, [eax]
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0C7Bh
            b"\x89\x87\xAB\xAB\xAB\xAB"  # 5  mov [edi+108h], eax
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 5D9h
            b"\x8B\x70\xAB"              # 5  mov esi, [eax+20h]
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0C12h
            b"\x89\x5D\xAB"              # 5  mov [ebp+var_4C], ebx
            b"\xC7\x43\xAB\xAB\xAB\xAB\xAB"  # 8  mov dword ptr [ebx+2Ch], 0F3h
            b"\xE8"                      # 15 call MsgStr
        ),
        {
            "fixedOffset": 15,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0B46h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+a1.m_allocated_len],
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 12 mov [ebp+a1.m_len], 0
            b"\xC6\x45\xAB\xAB"          # 19 mov byte ptr [ebp+a1.m_cstr], 0
            b"\xE8"                      # 23 call MsgStr
        ),
        {
            "fixedOffset": 23,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0B4Bh
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_74], 0Fh
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 12 mov [ebp+var_78], 0
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 19 mov byte ptr [ebp+var_88], 0
            b"\xE8"                      # 26 call MsgStr
        ),
        {
            "fixedOffset": 26,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0CAAh
            b"\x88\x87\xAB\xAB\xAB\xAB"  # 5  mov [edi+0C0h], al
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0CADh
            b"\xC6\x87\xAB\xAB\xAB\xAB\xAB"  # 5  mov byte ptr [edi+0C0h], 0
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0B9Ah
            b"\xC7\x87\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov dword ptr [ed
            b"\xE8"                      # 15 call MsgStr
        ),
        {
            "fixedOffset": 15,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0B9Eh
            b"\x03\xFB"                  # 5  add edi, ebx
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0B9Ch
            b"\x8B\xF0"                  # 5  mov esi, eax
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0CA9h
            b"\x75\xAB"                  # 5  jnz short loc_509F06
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call UIWindow_TextOutWithRightAli
            b"\xE9\xAB\xAB\xAB\xAB"      # 5  jmp loc_50A10A
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0B5Bh
            b"\xC7\x80\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov dword ptr [ea
            b"\xE8"                      # 15 call MsgStr
        ),
        {
            "fixedOffset": 15,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0A51h
            b"\x89\x5D\xAB"              # 5  mov [ebp+var_30], ebx
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0B15h
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_12C]
            b"\xE8"                      # 15 call MsgStr
        ),
        {
            "fixedOffset": 15,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC6\x45\xAB\xAB"          # 0  mov byte ptr [ebp+var_4], 2
            b"\x56"                      # 4  push esi
            b"\xE8"                      # 5  call MsgStr
        ),
        {
            "fixedOffset": 5,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x50"                      # 0  push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 1  call atoi
            b"\x50"                      # 7  push eax
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x74\xAB"                  # 0  jz short loc_540713
            b"\x52"                      # 2  push edx
            b"\x56"                      # 3  push esi
            b"\xE8"                      # 4  call MsgStr
        ),
        {
            "fixedOffset": 4,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xEB\xAB"                  # 0  jmp short loc_540746
            b"\x56"                      # 2  push esi
            b"\xE8"                      # 3  call MsgStr
        ),
        {
            "fixedOffset": 3,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x75\xAB"                  # 0  jnz short loc_5407B4
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push offset aEsc
            b"\x56"                      # 7  push esi
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 220h
            b"\x8B\xB3\xAB\xAB\xAB\xAB"  # 5  mov esi, [ebx+98h]
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 21Eh
            b"\x8B\xF9"                  # 5  mov edi, ecx
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 23Eh
            b"\x8B\xF1"                  # 5  mov esi, ecx
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 13Ah
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_198]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 15 mov [ebp+src], of
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 25 mov [ebp+var_190]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 35 mov [ebp+var_18C]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 45 mov [ebp+var_188]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 55 mov [ebp+var_184]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 65 mov [ebp+var_180]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 75 mov [ebp+var_17C]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 85 mov [ebp+var_178]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 95 mov [ebp+var_174]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 105 mov [ebp+var_170
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 115 mov [ebp+var_16C
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 125 mov [ebp+var_168
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 135 mov [ebp+var_164
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 145 mov [ebp+var_160
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 155 mov [ebp+var_24], 0A1h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 162 mov [ebp+var_20], 0C4h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 169 mov [ebp+var_1C], 0A0h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 176 mov [ebp+var_18], 0F2h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 183 mov [ebp+var_14], 104h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 190 mov [ebp+var_4C], 0BDh
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 197 mov [ebp+var_48], 60h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 204 mov [ebp+var_44], 0EAh
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 211 mov [ebp+var_40], 60h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 218 mov [ebp+var_3C], 1B2h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 225 mov [ebp+var_38], 60h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 232 mov [ebp+var_34], 4
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 239 mov [ebp+var_30], 60h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 246 mov [ebp+var_2C], 89h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 253 mov [ebp+var_28], 60h
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 260 mov [ebp+toolTip
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 270 mov [ebp+var_158
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 280 mov [ebp+var_154
            b"\xE8"                      # 290 call MsgStr
        ),
        {
            "fixedOffset": 290,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 7B9h
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+src], of
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_108]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 25 mov [ebp+var_104]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 35 mov [ebp+var_100]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 45 mov [ebp+var_FC],
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 55 mov [ebp+var_F8],
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 65 mov [ebp+var_F4],
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 75 mov [ebp+var_F0],
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 85 mov [ebp+var_EC],
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 95 mov [ebp+var_E8],
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 105 mov [ebp+var_E4]
            b"\xE8"                      # 115 call MsgStr
        ),
        {
            "fixedOffset": 115,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x8E\xAB\xAB\xAB\xAB"  # 0  jle loc_452C8F
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 995h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 0  jz loc_4594CA
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 8D7h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x89\xAB\xAB\xAB\xAB"  # 0  jns loc_45A088
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 8EAh
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x64\xA3\x00\x00\x00\x00"  # 0  mov large fs:0, eax
            b"\x8B\x75\xAB"              # 6  mov esi, [ebp+rswName]
            b"\x68\xAB\xAB\xAB\xAB"      # 9  push 0BBh
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": 10,
            "retOffset": 9,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xF1"                  # 0  mov esi, ecx
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 0A77h
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 3,
            "retOffset": 2,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 0  jnz loc_4D59EE
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 0A77h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 9C41h
            b"\x57"                      # 5  push edi
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 974h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 1
            b"\x57"                      # 2  push edi
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 213h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 4,
            "retOffset": 3,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 1
            b"\x56"                      # 2  push esi
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 0BEh
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 4,
            "retOffset": 3,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\xB1\xAB\xAB\xAB\xAB"  # 0  push dword ptr [ecx+0ACh]
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call UIWindow_Create
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 0B8Ah
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 12,
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8D\x4D\xAB"              # 0  lea ecx, [ebp+std_str_object]
            b"\xE8\xAB\xAB\xAB\xAB"      # 3  call std_string_append
            b"\x68\xAB\xAB\xAB\xAB"      # 8  push 3038
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": 9,
            "retOffset": 8,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x89\x87\xAB\xAB\xAB\xAB"  # 0  mov [edi+110h], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call UIWindow_Create
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 7CFh
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 12,
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x7E\xAB"                  # 0  jle short loc_4F623D
            b"\x52"                      # 2  push edx
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 0B52h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 4,
            "retOffset": 3,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x7E\xAB"                  # 0  jle short loc_4F7B18
            b"\x51"                      # 2  push ecx
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 0B52h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 4,
            "retOffset": 3,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\xB6\xCB"              # 0  movzx ecx, bl
            b"\x51"                      # 3  push ecx
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 0B3Bh
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 5,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\xB7\xAB\xAB\xAB\xAB"  # 0  push dword ptr [edi+0B0h]
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call UIWindow_AddChild
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 0B47h
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 12,
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 0  mov [ebp+var_4], 3
            b"\x68\xAB\xAB\xAB\x00"      # 7  push 0B48h
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 8,
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x8D\xAB\xAB\xAB\xAB"  # 0  jge loc_508843
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 0CACh
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push offset asc_C882C0
            b"\xE8\xAB\xAB\xAB\xAB"      # 5  call UIBitmapButton_SetBitmapName
            b"\x68\xAB\xAB\xAB\x00"      # 10 push 0B21h
            b"\xE8"                      # 15 call MsgStr
        ),
        {
            "fixedOffset": 15,
            "msgId": 11,
            "retOffset": 10,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call sub_517CB0
            b"\x8B\x45\xAB"              # 5  mov eax, [ebp+var_10]
            b"\xFF\x30"                  # 8  push dword ptr [eax]
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xCB"                  # 0  mov ecx, ebx
            b"\xE8\xAB\xAB\xAB\xAB"      # 2  call UIWindow_DrawBitmap
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 0C76h
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 8,
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0B04h
            b"\xEB\xAB"                  # 5  jmp short loc_52E634
            b"\x68\xAB\xAB\xAB\x00"      # 7  push 0ADCh
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 8,
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xCE"                  # 0  mov ecx, esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 2  call UIWindow_DrawBitmap
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 0C26h
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 8,
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x70\xAB"              # 0  push dword ptr [eax+34h]
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 934h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 4,
            "retOffset": 3,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\xEC\xAB"              # 0  sub esp, 8
            b"\xF2\x0F\x11\xAB\xAB"      # 3  movsd [esp+13Ch+var_13C], xmm0
            b"\x68\xAB\xAB\xAB\xAB"      # 8  push 7B7h
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": 9,
            "retOffset": 8,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xB3\xAB\xAB\xAB\xAB"  # 0  mov esi, [ebx+98h]
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 221h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x90\xAB\xAB\xAB\xAB"  # 0  call dword ptr [eax+0C0h]
            b"\x68\xAB\xAB\xAB\x00"      # 6  push 0ADAh
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 7B3h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_20], 32h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 12 mov [ebp+cy], 0Ch
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 19 mov [ebp+var_18], 50h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 26 mov [ebp+var_14], 0Ch
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 33 mov [ebp+var_E0],
            b"\xE8"                      # 43 call MsgStr
        ),
        {
            "fixedOffset": 43,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\x00"      # 0  push 196h
            b"\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_244], ecx
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\x78\xAB\xAB"          # 0  cmp dword ptr [eax+4], 1Eh
            b"\x75\xAB"                  # 4  jnz short loc_5A2D12
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 823h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 9D6h
            b"\x8B\x86\xAB\xAB\xAB\xAB"  # 5  mov eax, [esi+90h]
            b"\x8B\x38"                  # 11 mov edi, [eax]
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 0  jz loc_5A52DD
            b"\x6A\xAB"                  # 6  push 7Fh
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": (7, 1),
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 0  lea ecx, [ebp+var_250]
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call std_string_destructor
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 80h
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 12,
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8D\x8D\xAB\xAB\xAB\xAB"  # 0  lea ecx, [ebp+var_2F8]
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call std_string_destructor
            b"\x6A\xAB"                  # 11 push 7Eh
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": (12, 1),
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 0
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push offset m_resMgr.m_realResNam
            b"\x6A\xAB"                  # 7  push 40h
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": (8, 1),
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 259h
            b"\x89\x83\xAB\xAB\xAB\xAB"  # 5  mov [ebx+0A8h], eax
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 7ADh
            b"\xF3\xA5"                  # 5  rep movsd
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\xB5\xAB\xAB\xAB\xAB"  # 0  push [ebp+var_AC]
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 7A3h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 7Eh
            b"\xC7\x06\xAB\xAB\xAB\xAB"  # 2  mov dword ptr [esi], offset UICha
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": (1, 1),
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC6\x45\xAB\xAB"          # 0  mov byte ptr [ebp+var_4], 5
            b"\x6A\xAB"                  # 4  push 7Dh
            b"\xE8"                      # 6  call MsgStr
        ),
        {
            "fixedOffset": 6,
            "msgId": (5, 1),
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x76\xAB"              # 0  push dword ptr [esi+10h]
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 2B8h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 4,
            "retOffset": 3,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\x7E\xAB\xAB"          # 0  cmp dword ptr [esi+18h], 0
            b"\x7D\xAB"                  # 4  jge short loc_5BA7A0
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 73Fh
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 9DAh
            b"\x2B\xD8"                  # 5  sub ebx, eax
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 0  mov [ebp+var_4], 2
            b"\x6A\xAB"                  # 7  push 5Dh
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": (8, 1),
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC3"                      # 0  ret retn
            b"\x68\xAB\xAB\xAB\xAB"      # 1  push 830h
            b"\xE8"                      # 6  call MsgStr
        ),
        {
            "fixedOffset": 6,
            "msgId": 2,
            "retOffset": 1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 510h
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+src], of
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_DC],
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 25 mov [ebp+var_D8],
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 35 mov [ebp+var_D4],
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 45 mov [ebp+var_40], 0Ah
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 52 mov [ebp+var_3C], 19h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 59 mov [ebp+var_34], 19h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 66 mov [ebp+var_2C], 19h
            b"\x89\x45\xAB"              # 73 mov [ebp+var_24], eax
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 76 mov [ebp+var_20], 14Bh
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 83 mov [ebp+var_1C], 149h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 90 mov [ebp+var_18], 14Ah
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 97 mov [ebp+var_14], 0B0h
            b"\xE8"                      # 104 call MsgStr
        ),
        {
            "fixedOffset": 104,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 20Fh
            b"\xC6\x86\xAB\xAB\xAB\xAB\xAB"  # 5  mov byte ptr [esi+0BCh], 0
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 82h
            b"\xC7\x87\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov dword ptr [ed
            b"\xC7\x87\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 15 mov dword ptr [ed
            b"\xE8"                      # 25 call MsgStr
        ),
        {
            "fixedOffset": 25,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 0  mov [ebp+var_E4],
            b"\xE8\xAB\xAB\xAB\xAB"      # 10 call std_vector_int_insert
            b"\x68\xAB\xAB\xAB\xAB"      # 15 push 83h
            b"\xE8"                      # 20 call MsgStr
        ),
        {
            "fixedOffset": 20,
            "msgId": 16,
            "retOffset": 15,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xCE"                  # 0  mov ecx, esi
            b"\xE8\xAB\xAB\xAB\xAB"      # 2  call UIWindow_AddChild
            b"\x6A\xAB"                  # 7  push 5Ch
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": (8, 1),
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\xBF\xAB\xAB\xAB\xAB\xAB"  # 0  cmp dword ptr [edi+8Ch], 0
            b"\x75\xAB"                  # 7  jnz short loc_5C54F6
            b"\x68\xAB\xAB\xAB\xAB"      # 9  push 0A7Dh
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": 10,
            "retOffset": 9,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 50Eh
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+src], of
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_B4],
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 25 mov [ebp+var_C8],
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 35 mov [ebp+var_C4],
            b"\xE8"                      # 45 call MsgStr
        ),
        {
            "fixedOffset": 45,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x75\xAB"              # 0  push [ebp+arg_0]
            b"\xE8\xAB\xAB\xAB\xAB"      # 3  call UIItemIdentifyWnd_virt56
            b"\x68\xAB\xAB\xAB\xAB"      # 8  push 20Ah
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": 9,
            "retOffset": 8,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xF9"                  # 0  mov edi, ecx
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 209h
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 3,
            "retOffset": 2,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0B27h
            b"\x8D\x42\xAB"              # 5  lea eax, [edx-37h]
            b"\x89\x45\xAB"              # 8  mov [ebp+var_34], eax
            b"\x8D\x42\xAB"              # 11 lea eax, [edx-29h]
            b"\x89\x45\xAB"              # 14 mov [ebp+var_30], eax
            b"\x8D\x42\xAB"              # 17 lea eax, [edx-1Bh]
            b"\x89\x45\xAB"              # 20 mov [ebp+var_2C], eax
            b"\x8D\x42\xAB"              # 23 lea eax, [edx-0Dh]
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 26 mov [ebp+var_38], 1
            b"\x89\x45\xAB"              # 33 mov [ebp+var_28], eax
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 36 mov [ebp+var_24], 80h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 43 mov [ebp+var_20], 80h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 50 mov [ebp+var_1C], 80h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 57 mov [ebp+var_18], 80h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 64 mov [ebp+var_14], 80h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 71 mov [ebp+var_4C], 1ABh
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 78 mov [ebp+var_48], 0BFh
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 85 mov [ebp+var_44], 0C0h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 92 mov [ebp+var_40], 0A4h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 99 mov [ebp+var_3C], 1ADh
            b"\xE8"                      # 106 call MsgStr
        ),
        {
            "fixedOffset": 106,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\xFB\xAB"              # 0  cmp ebx, 28h
            b"\x0F\x8C\xAB\xAB\xAB\xAB"  # 3  jl loc_5C9210
            b"\x68\xAB\xAB\xAB\xAB"      # 9  push 2E3h
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": 10,
            "retOffset": 9,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0AFh
            b"\x89\xBD\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_E4], edi
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0CB5h
            b"\xC7\x80\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov dword ptr [ea
            b"\x8B\x83\xAB\xAB\xAB\xAB"  # 15 mov eax, [ebx+8Ch]
            b"\x8B\x30"                  # 21 mov esi, [eax]
            b"\xE8"                      # 23 call MsgStr
        ),
        {
            "fixedOffset": 23,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xCF"                  # 0  mov ecx, edi
            b"\xE8\xAB\xAB\xAB\xAB"      # 2  call UIWindow_AddChild
            b"\x6A\xAB"                  # 7  push 79h
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": (8, 1),
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x24\xB5\xAB\xAB\xAB\xAB"  # 0  jmp off_5CE5BC[esi*4]
            b"\x8D\x86\xAB\xAB\xAB\xAB"  # 7  lea eax, [esi+736h]
            b"\x50"                      # 13 push eax
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\x8F\xAB\xAB\xAB\xAB"  # 0  mov ecx, [edi+110h]
            b"\xEB\xAB"                  # 6  jmp short loc_5CE3C6
            b"\x68\xAB\xAB\xAB\xAB"      # 8  push 732h
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": 9,
            "retOffset": 8,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x75\xAB"                  # 0  jnz short loc_5D0250
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push offset g_session.m_otherUser
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 551h
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 8,
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xF1"                  # 0  mov esi, ecx
            b"\xE8\xAB\xAB\xAB\xAB"      # 2  call UIEmailAddressWnd_sub_5976A0
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 10Dh
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 8,
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\xF8\xAB"              # 0  cmp eax, 0Fh
            b"\x75\xAB"                  # 3  jnz short loc_5D2176
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 3A0h
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": 6,
            "retOffset": 5,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push offset word_E6E6E6
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push offset dword_F0F0F0
            b"\x68\xAB\xAB\xAB\xAB"      # 10 push 3F4h
            b"\xE8"                      # 15 call MsgStr
        ),
        {
            "fixedOffset": 15,
            "msgId": 11,
            "retOffset": 10,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0C28h
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_84],
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_80], 16h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 22 mov [ebp+var_7C], 19h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 29 mov [ebp+var_78], 26h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 36 mov [ebp+var_74], 19h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 43 mov [ebp+var_70], 36h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 50 mov [ebp+var_6C], 19h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 57 mov [ebp+var_68], 46h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 64 mov [ebp+var_64], 19h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 71 mov [ebp+var_60], 56h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 78 mov [ebp+var_5C], 19h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 85 mov [ebp+var_58], 66h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 92 mov [ebp+var_54], 80h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 99 mov [ebp+var_50], 16h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 106 mov [ebp+var_4C], 80h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 113 mov [ebp+var_48], 26h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 120 mov [ebp+var_44], 80h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 127 mov [ebp+var_40], 36h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 134 mov [ebp+var_3C], 80h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 141 mov [ebp+var_38], 46h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 148 mov [ebp+var_34], 80h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 155 mov [ebp+var_30], 56h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 162 mov [ebp+var_2C], 80h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 169 mov [ebp+var_28], 66h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 176 mov [ebp+var_24], 0D2h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 183 mov [ebp+var_20], 16h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 190 mov [ebp+var_1C], 0D2h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 197 mov [ebp+var_18], 26h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 204 mov [ebp+var_14], 0D2h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 211 mov [ebp+var_10], 36h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 218 mov [ebp+var_C], 0D2h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 225 mov [ebp+var_8], 46h
            b"\xE8"                      # 232 call MsgStr
        ),
        {
            "fixedOffset": 232,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\x30"                  # 0  mov esi, [eax]
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 0A7Eh
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 3,
            "retOffset": 2,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0B95h
            b"\x89\xB7\xAB\xAB\xAB\xAB"  # 5  mov [edi+8Ch], esi
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8D\xBB\xAB\xAB\xAB\xAB"  # 0  lea edi, [ebx+8Ch]
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 29Ah
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 0  jmp off_5DEC94[eax*4]
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 2A0h
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 8,
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 332h
            b"\x89\x9E\xAB\xAB\xAB\xAB"  # 5  mov [esi+98h], ebx
            b"\x89\x86\xAB\xAB\xAB\xAB"  # 11 mov [esi+9Ch], eax
            b"\xE8"                      # 17 call MsgStr
        ),
        {
            "fixedOffset": 17,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 186h
            b"\x8B\x37"                  # 5  mov esi, [edi]
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x80\xBE\xAB\xAB\xAB\xAB\xAB"  # 0  cmp byte ptr [esi+98h], 0Ah
            b"\x75\xAB"                  # 7  jnz short loc_5E74DF
            b"\x68\xAB\xAB\xAB\xAB"      # 9  push 4EFh
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": 10,
            "retOffset": 9,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x2B\xC8"                  # 0  sub ecx, eax
            b"\x51"                      # 2  push ecx
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 520h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 4,
            "retOffset": 3,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\xB3\xAB\xAB\xAB\xAB"  # 0  push dword ptr [ebx+148h]
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 4E5h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x52"                      # 0  push edx
            b"\xFF\x75\xAB"              # 1  push [ebp+arg_0]
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 969h
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x51"                      # 0  push ecx
            b"\xFF\x75\xAB"              # 1  push [ebp+arg_0]
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 968h
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 4FDh
            b"\xC7\x87\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov dword ptr [ed
            b"\xC6\x87\xAB\xAB\xAB\xAB\xAB"  # 15 mov byte ptr [edi+0E4h], 1
            b"\xC7\x87\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 22 mov dword ptr [ed
            b"\xE8"                      # 32 call MsgStr
        ),
        {
            "fixedOffset": 32,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 5CEh
            b"\x8D\x5A\xAB"              # 5  lea ebx, [edx-1]
            b"\x0F\xAF\x9F\xAB\xAB\xAB\xAB"  # 8  imul ebx, [edi+0A8h]
            b"\x03\x9F\xAB\xAB\xAB\xAB"  # 15 add ebx, [edi+0A4h]
            b"\x03\x9F\xAB\xAB\xAB\xAB"  # 21 add ebx, [edi+9Ch]
            b"\x03\x9F\xAB\xAB\xAB\xAB"  # 27 add ebx, [edi+94h]
            b"\xE8"                      # 33 call MsgStr
        ),
        {
            "fixedOffset": 33,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\x7E\xAB\xAB"          # 0  cmp dword ptr [esi+10h], 0
            b"\x75\xAB"                  # 4  jnz short loc_609647
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 11Eh
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 562h
            b"\x8B\x87\xAB\xAB\xAB\xAB"  # 5  mov eax, [edi+8Ch]
            b"\x8B\x90\xAB\xAB\xAB\xAB"  # 11 mov edx, [eax+0B0h]
            b"\x2B\x90\xAB\xAB\xAB\xAB"  # 17 sub edx, [eax+0ACh]
            b"\xB8\xAB\xAB\xAB\xAB"      # 23 mov eax, 2AAAAAABh
            b"\xF7\xEA"                  # 28 imul edx
            b"\xC1\xFA\xAB"              # 30 sar edx, 2
            b"\x8B\xC2"                  # 33 mov eax, edx
            b"\xC1\xE8\xAB"              # 35 shr eax, 1Fh
            b"\x03\xC2"                  # 38 add eax, edx
            b"\x0F\x94\xC3"              # 40 setz bl
            b"\xE8"                      # 43 call MsgStr
        ),
        {
            "fixedOffset": 43,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x89\x83\xAB\xAB\xAB\xAB"  # 0  mov [ebx+0D8h], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call UIWindow_Create
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 6EFh
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 12,
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\x3F"                  # 0  mov edi, [edi]
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 8A3h
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 3,
            "retOffset": 2,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0C73h
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_148]
            b"\x8D\x41\xAB"              # 15 lea eax, [ecx+3]
            b"\x89\x45\xAB"              # 18 mov [ebp+var_28.field_4], eax
            b"\x8D\x41\xAB"              # 21 lea eax, [ecx+7]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 24 mov [ebp+var_144]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 34 mov [ebp+var_140]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 44 mov [ebp+var_13C]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 54 mov [ebp+var_138]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 64 mov [ebp+var_134]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 74 mov [ebp+var_130]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 84 mov [ebp+var_12C]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 94 mov [ebp+var_128]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 104 mov [ebp+var_124
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 114 mov [ebp+var_120
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 124 mov [ebp+var_11C
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 134 mov [ebp+std_string_ptr.fiel
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 141 mov [ebp+std_string_ptr.m_le
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 148 mov [ebp+std_string_ptr.m_al
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 155 mov [ebp+var_28.m_cstr], 61h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 162 mov [ebp+var_28.field_8], 24
            b"\x89\x45\xAB"              # 169 mov [ebp+var_28.field_C], eax
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 172 mov [ebp+var_28.m_len], 23Ch
            b"\x89\x45\xAB"              # 179 mov [ebp+var_28.m_allocated_len]
            b"\xE8"                      # 182 call MsgStr
        ),
        {
            "fixedOffset": 182,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x43\x75\xAB"          # 0  cmovnb esi, [ebp+std_string_ptr]
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 988h
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\xB6\xAB\xAB\xAB\xAB"  # 0  push dword ptr [esi+98h]
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call UIWindow_AddChild
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 72Ah
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 12,
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC6\x45\xAB\xAB"          # 0  mov [ebp+MultiByteStr], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 4  call j_memset
            b"\x68\xAB\xAB\xAB\xAB"      # 9  push 698h
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": 10,
            "retOffset": 9,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\xFB\xAB"              # 0  cmp ebx, 5Ah
            b"\x7C\xAB"                  # 3  jl short loc_619230
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 6F2h
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": 6,
            "retOffset": 5,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x50"                      # 0  push eax
            b"\xFF\x56\xAB"              # 1  call dword ptr [esi+10h]
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 41Dh
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\x7D\xAB\xAB"          # 0  cmp [ebp+var_78], 0
            b"\x75\xAB"                  # 4  jnz short loc_61BA9B
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 5EEh
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xA1\xAB\xAB\xAB\xAB"      # 0  mov eax, g_session.m_dunState
            b"\x05\xAB\xAB\xAB\x00"      # 5  add eax, 539h
            b"\x50"                      # 10 push eax
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x75\xAB"                  # 0  jnz short loc_61C22D
            b"\xFF\x35\xAB\xAB\xAB\xAB"  # 2  push g_session.m_priority
            b"\x68\xAB\xAB\xAB\xAB"      # 8  push 53Eh
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": 9,
            "retOffset": 8,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call UIWindow_TextOutWithRightAli
            b"\xFF\xB6\xAB\xAB\xAB\xAB"  # 5  push dword ptr [esi+248h]
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 11Ch
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 12,
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call UIWindow_TextOutWithRightAli
            b"\xFF\x35\xAB\xAB\xAB\xAB"  # 5  push g_session.search_store_remai
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 70Ah
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 12,
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 6F5h
            b"\x83\xC6\xAB"              # 5  add esi, 32h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 424h
            b"\x81\xC6\xAB\xAB\xAB\xAB"  # 5  add esi, 0A0h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 0  jz loc_624DA9
            b"\x56"                      # 6  push esi
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 4BDh
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 8,
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 0
            b"\x8B\x33"                  # 2  mov esi, [ebx]
            b"\x6A\xAB"                  # 4  push 55h
            b"\xE8"                      # 6  call MsgStr
        ),
        {
            "fixedOffset": 6,
            "msgId": (5, 1),
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 6E5h
            b"\x66\x89\x87\xAB\xAB\xAB\xAB"  # 5  mov [edi+19Eh], ax
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\xC4\xAB"              # 0  add esp, 0Ch
            b"\x53"                      # 3  push ebx
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 0A29h
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\xC4\xAB"              # 0  add esp, 0Ch
            b"\x56"                      # 3  push esi
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 6E9h
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 70Bh
            b"\xC6\x05\xAB\xAB\xAB\xAB\xAB"  # 5  mov g_session.field_5541, 0
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8D\x8E\xAB\xAB\xAB\xAB"  # 0  lea ecx, [esi+94h]
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call std_string_assign
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 8ACh
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 12,
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x51"                      # 0  push ecx
            b"\xFF\x36"                  # 1  push dword ptr [esi]
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 8B9h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 4,
            "retOffset": 3,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x50"                      # 0  push eax
            b"\xFF\x52\xAB"              # 1  call dword ptr [edx+10h]
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 89Bh
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 6E6h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_34], 0
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 12 mov [ebp+var_30], 0
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 19 mov [ebp+var_2C], 6
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 26 mov [ebp+var_28], 0Dh
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 33 mov [ebp+var_24], 6
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 40 mov [ebp+var_20], 7
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 47 mov [ebp+var_1C], 6
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 54 mov [ebp+var_18], 0Dh
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 61 mov [ebp+var_14], 8
            b"\x89\x7D\xAB"              # 68 mov [ebp+var_50], edi
            b"\xE8"                      # 71 call MsgStr
        ),
        {
            "fixedOffset": 71,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xF8"                  # 0  mov edi, eax
            b"\x8D\x86\xAB\xAB\xAB\xAB"  # 2  lea eax, [esi+6B0h]
            b"\x50"                      # 8  push eax
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8D\x49\xAB"              # 0  lea ecx, [ecx+0]
            b"\x8D\x87\xAB\xAB\xAB\xAB"  # 3  lea eax, [edi+65Dh]
            b"\x50"                      # 9  push eax
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call UIWindow_Create
            b"\x43"                      # 5  inc ebx
            b"\x8D\x83\xAB\xAB\xAB\xAB"  # 6  lea eax, [ebx+6AFh]
            b"\x50"                      # 12 push eax
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\x45\xAB"              # 0  mov eax, [ebp+var_5C]
            b"\x05\xAB\xAB\xAB\xAB"      # 3  add eax, 65Dh
            b"\x50"                      # 8  push eax
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\x46\xAB"              # 0  mov eax, [esi+10h]
            b"\x05\xAB\xAB\xAB\xAB"      # 3  add eax, 6AFh
            b"\x50"                      # 8  push eax
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xF1"                  # 0  mov esi, ecx
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 12Ch
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 0A3Bh
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 8,
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x43\xBD\xAB\xAB\xAB\xAB"  # 0  cmovnb edi, [ebp+a1]
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 7EEh
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 8,
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x85\xC0"                  # 0  test eax, eax
            b"\x75\xAB"                  # 2  jnz short loc_651C99
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 35Dh
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x89\x86\xAB\xAB\xAB\xAB"  # 0  mov [esi+16Ch], eax
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call UIWindow_Create
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 0B30h
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 12,
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 0
            b"\x75\xAB"                  # 2  jnz short loc_657711
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 81Ch
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\xFA\xAB"              # 0  cmp edx, 0Ch
            b"\x7C\xAB"                  # 3  jl short loc_657880
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 197h
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": 6,
            "retOffset": 5,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 14Bh
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 5  mov byte ptr [ebp+var_428], 0
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\xF8\xAB"              # 0  cmp eax, 0Ch
            b"\x7C\xAB"                  # 3  jl short loc_658C30
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 1FEh
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": 6,
            "retOffset": 5,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x56"                      # 0  push esi
            b"\x56"                      # 1  push esi
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 0A04h
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 3,
            "retOffset": 2,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x56"                      # 0  push esi
            b"\x51"                      # 1  push ecx
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 0B6Ch
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 3,
            "retOffset": 2,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 1
            b"\x57"                      # 2  push edi
            b"\x6A\xAB"                  # 3  push 0Eh
            b"\xE8"                      # 5  call MsgStr
        ),
        {
            "fixedOffset": 5,
            "msgId": (4, 1),
            "retOffset": 3,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0C4Bh
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_C4],
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_C8],
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 25 mov byte ptr [ebp+var_D8], 0
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 32 mov dword ptr [eb
            b"\x66\x0F\xD6\x40\xAB"      # 42 movq qword ptr [eax+8], xmm0
            b"\xE8"                      # 47 call MsgStr
        ),
        {
            "fixedOffset": 47,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call sub_662E90
            b"\x8B\xD8"                  # 5  mov ebx, eax
            b"\xFF\x33"                  # 7  push dword ptr [ebx]
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x90\xAB\xAB\xAB\xAB"  # 0  call dword ptr [eax+8Ch]
            b"\xBE\xAB\xAB\xAB\xAB"      # 6  mov esi, 3B9ACA00h
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 9ACh
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 12,
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 0  mov [ebp+val1], 0
            b"\xE8\xAB\xAB\xAB\xAB"      # 7  call j_memset
            b"\x68\xAB\xAB\xAB\xAB"      # 12 push 9C2h
            b"\xE8"                      # 17 call MsgStr
        ),
        {
            "fixedOffset": 17,
            "msgId": 13,
            "retOffset": 12,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xEB\xAB"                  # 0  jmp short loc_675918
            b"\x75\xAB"                  # 2  jnz short loc_675912
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 0C4Fh
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xEB\xAB"                  # 0  jmp short loc_675925
            b"\x8D\x87\xAB\xAB\xAB\xAB"  # 2  lea eax, [edi+5B8h]
            b"\x50"                      # 8  push eax
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 7Ch
            b"\xFF\x52\xAB"              # 2  call dword ptr [edx+10h]
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 0B88h
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": 6,
            "retOffset": 5,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 9Dh
            b"\xFF\x52\xAB"              # 5  call dword ptr [edx+10h]
            b"\x68\xAB\xAB\xAB\xAB"      # 8  push 0BBFh
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": 9,
            "retOffset": 8,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x74\xAB"                  # 0  jz short loc_677A49
            b"\x8B\x3D\xAB\xAB\xAB\xAB"  # 2  mov edi, g_session.buying_store_s
            b"\x68\xAB\xAB\xAB\xAB"      # 8  push 6B8h
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": 9,
            "retOffset": 8,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xEB\xAB"                  # 0  jmp short loc_677A77
            b"\x8B\x3D\xAB\xAB\xAB\xAB"  # 2  mov edi, g_session.field_DD8
            b"\x68\xAB\xAB\xAB\xAB"      # 8  push 0E0h
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": 9,
            "retOffset": 8,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0B6Fh
            b"\x75\xAB"                  # 5  jnz short loc_67C101
            b"\x81\xF9\xAB\xAB\xAB\xAB"  # 7  cmp ecx, 0EA60h
            b"\x7F\xAB"                  # 13 jg short loc_67C0D7
            b"\xE8"                      # 15 call MsgStr
        ),
        {
            "fixedOffset": 15,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 0
            b"\xEB\xAB"                  # 2  jmp short loc_67C0C2
            b"\x81\xF9\xAB\xAB\xAB\xAB"  # 4  cmp ecx, 0EA60h
            b"\x7F\xAB"                  # 10 jg short loc_67C136
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 1
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 0FFh
            b"\xEB\xAB"                  # 7  jmp short loc_67C15E
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0B6Fh
            b"\x81\xF9\xAB\xAB\xAB\xAB"  # 5  cmp ecx, 0EA60h
            b"\x7F\xAB"                  # 11 jg short loc_67CDCD
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 6B9h
            b"\x8D\x70\xAB"              # 5  lea esi, [eax-0Fh]
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\xB5\xAB\xAB\xAB\xAB"  # 0  push [ebp+var_178.item_options_co
            b"\x56"                      # 6  push esi
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 0AC6h
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 8,
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\xC4\xAB"              # 0  add esp, 8
            b"\x50"                      # 3  push eax
            b"\x56"                      # 4  push esi
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 6C8h
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": 6,
            "retOffset": 5,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\x36"                  # 0  mov esi, [esi]
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 24Ch
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 3,
            "retOffset": 2,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\xC4\xAB"              # 0  add esp, 0Ch
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 7530h
            b"\x68\xAB\xAB\xAB\xAB"      # 8  push 6A8h
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": 9,
            "retOffset": 8,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 620h
            b"\xC7\x86\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov dword ptr [es
            b"\xC7\x86\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 15 mov dword ptr [es
            b"\xE8"                      # 25 call MsgStr
        ),
        {
            "fixedOffset": 25,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xCE"                  # 0  mov ecx, esi
            b"\xFF\x52\xAB"              # 2  call dword ptr [edx+10h]
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 0C95h
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": 6,
            "retOffset": 5,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x3B\xF3"                  # 0  cmp esi, ebx
            b"\x0F\x8C\xAB\xAB\xAB\xAB"  # 2  jl loc_691F30
            b"\x68\xAB\xAB\xAB\xAB"      # 8  push 918h
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": 9,
            "retOffset": 8,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call UIWindow_AddChild
            b"\x6A\xAB"                  # 5  push 66h
            b"\xC7\x87\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  mov dword ptr [ed
            b"\xE8"                      # 17 call MsgStr
        ),
        {
            "fixedOffset": 17,
            "msgId": (6, 1),
            "retOffset": 5,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 0
            b"\x6A\xAB"                  # 2  push 67h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 4  mov dword ptr [ebp+DstBuf], 3
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": (3, 1),
            "retOffset": 2,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\xF8\xAB"              # 0  cmp eax, 7Ch
            b"\x0F\x8C\xAB\xAB\xAB\xAB"  # 3  jl loc_695420
            b"\x6A\xAB"                  # 9  push 66h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": (10, 1),
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 62h
            b"\xC7\x85\x74\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 2  mov [ebp+var_8C],
            b"\xC7\x85\x78\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 12 mov [ebp+var_88],
            b"\xC7\x85\x7C\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 22 mov [ebp+var_84],
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 32 mov [ebp+var_80], 0D1h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 39 mov [ebp+var_7C], 0D2h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 46 mov [ebp+var_78], 160h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 53 mov [ebp+var_74], 160h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 60 mov [ebp+var_70], 1A4h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 67 mov [ebp+var_6C], 1A5h
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 74 mov [ebp+var_68], 1A6h
            b"\xE8"                      # 81 call MsgStr
        ),
        {
            "fixedOffset": 81,
            "msgId": (1, 1),
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 0  mov [ebp+toolTip], eax
            b"\x6A\xAB"                  # 6  push 63h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": (7, 1),
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x89\x85\xAB\xAB\xAB\xAB"  # 0  mov [ebp+var_218], eax
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 65Bh
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 0
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 0C9Fh
            b"\x8B\xF8"                  # 7  mov edi, eax
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 3,
            "retOffset": 2,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x90\xAB\xAB\xAB\xAB"  # 0  call dword ptr [eax+8Ch]
            b"\x6A\xAB"                  # 6  push 66h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": (7, 1),
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x84\xC0"                  # 0  test al, al
            b"\x74\xAB"                  # 2  jz short loc_6992CF
            b"\x6A\xAB"                  # 4  push 60h
            b"\xE8"                      # 6  call MsgStr
        ),
        {
            "fixedOffset": 6,
            "msgId": (5, 1),
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x85\xC0"                  # 0  test eax, eax
            b"\x74\xAB"                  # 2  jz short loc_699AA4
            b"\x6A\xAB"                  # 4  push 66h
            b"\xE8"                      # 6  call MsgStr
        ),
        {
            "fixedOffset": 6,
            "msgId": (5, 1),
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 0  mov byte ptr [ebp+fInfo.chara
            b"\xE8\xAB\xAB\xAB\xAB"      # 7  call j_memset
            b"\x6A\xAB"                  # 12 push 66h
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": (13, 1),
            "retOffset": 12,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x84\xDB"                  # 0  test bl, bl
            b"\x74\xAB"                  # 2  jz short loc_69AD13
            b"\x6A\xAB"                  # 4  push 60h
            b"\xE8"                      # 6  call MsgStr
        ),
        {
            "fixedOffset": 6,
            "msgId": (5, 1),
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x85\xF6"                  # 0  test esi, esi
            b"\x74\xAB"                  # 2  jz short loc_69B5C0
            b"\x6A\xAB"                  # 4  push 66h
            b"\xE8"                      # 6  call MsgStr
        ),
        {
            "fixedOffset": 6,
            "msgId": (5, 1),
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x3B\xD1"                  # 0  cmp edx, ecx
            b"\x76\xAB"                  # 2  jbe short loc_69D43A
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 0AE5h
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0AB7h
            b"\x8D\x77\xAB"              # 5  lea esi, [edi-13h]
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 1
            b"\x56"                      # 2  push esi
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 0DCh
            b"\x68\xAB\xAB\xAB\xAB"      # 8  push 0ABAh
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": 9,
            "retOffset": 8,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0ABEh
            b"\xC7\x80\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov dword ptr [ea
            b"\x8B\x83\xAB\xAB\xAB\xAB"  # 15 mov eax, [ebx+100h]
            b"\xC7\x80\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 21 mov dword ptr [ea
            b"\x8B\x83\xAB\xAB\xAB\xAB"  # 31 mov eax, [ebx+100h]
            b"\xC7\x80\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 37 mov dword ptr [ea
            b"\xE8"                      # 47 call MsgStr
        ),
        {
            "fixedOffset": 47,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 0  mov eax, [ebp+var_98]
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 0AC0h
            b"\x89\x46\xAB"              # 11 mov [esi+2Ch], eax
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0B9h
            b"\x89\xBE\xAB\xAB\xAB\xAB"  # 5  mov [esi+0D0h], edi
            b"\x89\xBE\xAB\xAB\xAB\xAB"  # 11 mov [esi+8Ch], edi
            b"\xE8"                      # 17 call MsgStr
        ),
        {
            "fixedOffset": 17,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 9E2h
            b"\xC7\x86\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov dword ptr [es
            b"\xE8"                      # 15 call MsgStr
        ),
        {
            "fixedOffset": 15,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0A09h
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_118]
            b"\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_11C]
            b"\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 25 mov [ebp+var_12C], 0
            b"\xE8"                      # 32 call MsgStr
        ),
        {
            "fixedOffset": 32,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x89\x45\xAB"              # 0  mov [ebp+id], eax
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 91h
            b"\x68\xAB\xAB\xAB\xAB"      # 8  push 8Ch
            b"\x8B\xCF"                  # 13 mov ecx, edi
            b"\xE8\xAB\xAB\xAB\xAB"      # 15 call UIWindow_Create
            b"\xFF\x75\xAB"              # 20 push [ebp+id]
            b"\xE8"                      # 23 call MsgStr
        ),
        {
            "fixedOffset": 23,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x89\xBB\xAB\xAB\xAB\xAB"  # 0  mov [ebx+UIWindowMgr.field_294],
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call UIWindow_Create
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 447h
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 12,
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8D\x4E\xAB"              # 0  lea ecx, [esi+74h]
            b"\xE8\xAB\xAB\xAB\xAB"      # 3  call std_string_assign
            b"\x68\xAB\xAB\xAB\xAB"      # 8  push 9B2h
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": 9,
            "retOffset": 8,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xF7\xFF"                  # 0  idiv edi
            b"\x52"                      # 2  push edx
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 892h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 4,
            "retOffset": 3,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\x7F\xAB\xAB"          # 0  cmp dword ptr [edi+28h], 1
            b"\x75\xAB"                  # 4  jnz short loc_7F75DA
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 2F4h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x55"                      # 0  push ebp
            b"\x8B\xEC"                  # 1  mov ebp, esp
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 2F4h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 4,
            "retOffset": 3,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x85\xAB"                  # 0  test esi, esi
            b"\x78\xAB"                  # 2  js short loc_814D06
            b"\x56"                      # 4  push esi
            b"\xE8"                      # 5  call MsgStr
        ),
        {
            "fixedOffset": 5,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x98"                      # 0  cwd cwde
            b"\x05\xAB\xAB\xAB\xAB"      # 1  add eax, 6AFh
            b"\x50"                      # 6  push eax
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x64\xA3\x00\x00\x00\x00"  # 0  mov large fs:0, eax
            b"\x8B\x5D\xAB"              # 6  mov ebx, [ebp+arg_0]
            b"\x68\xAB\xAB\xAB\xAB"      # 9  push 912h
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": 10,
            "retOffset": 9,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\x36"                  # 0  mov esi, [esi]
            b"\x57"                      # 2  push edi
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 913h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 4,
            "retOffset": 3,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\xC6"                  # 0  mov eax, esi
            b"\xEB\xAB"                  # 2  jmp short loc_878281
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 915h
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x30"                  # 0  push dword ptr [eax]
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 8D6h
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 3,
            "retOffset": 2,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\x40\xAB"              # 0  mov eax, [eax+4]
            b"\x50"                      # 3  push eax
            b"\x51"                      # 4  push ecx
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 8F4h
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": 6,
            "retOffset": 5,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x90\xAB\xAB\xAB\xAB"  # 0  call dword ptr [eax+88h]
            b"\x57"                      # 6  push edi
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 856h
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 8,
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 858h
            b"\x81\xC7\xAB\xAB\xAB\xAB"  # 5  add edi, 0B8h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 85Bh
            b"\x89\xB5\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_D0], esi
            b"\x89\x48\xAB"              # 11 mov [eax+34h], ecx
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\xC4\xAB"              # 0  add esp, 0Ch
            b"\x56"                      # 3  push esi
            b"\x57"                      # 4  push edi
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 92Dh
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": 6,
            "retOffset": 5,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\xBD\xAB\xAB\xAB\xAB\xAB"  # 0  cmp [ebp+var_400], 0
            b"\x75\xAB"                  # 7  jnz short loc_99813C
            b"\x68\xAB\xAB\xAB\xAB"      # 9  push 644h
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": 10,
            "retOffset": 9,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 0
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 64FFFFh
            b"\x74\xAB"                  # 7  jz short loc_9CA75A
            b"\x6A\xAB"                  # 9  push 17h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": (10, 1),
            "retOffset": 9,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call sub_4663F0
            b"\x56"                      # 5  push esi
            b"\x6A\xAB"                  # 6  push 19h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": (7, 1),
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xEB\xAB"                  # 0  jmp short loc_9D98B4
            b"\x8B\xFE"                  # 2  mov edi, esi
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 7EEh
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 2F7h
            b"\x66\x89\x41\xAB"          # 5  mov [ecx+4], ax
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x50"                      # 0  push eax
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 1  call sprintf
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 318h
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 8,
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\x3D\xAB\xAB\xAB\xAB\xAB"  # 0  cmp g_session.sp_, 28h
            b"\x7D\xAB"                  # 7  jge short loc_9DF5B2
            b"\x68\xAB\xAB\xAB\xAB"      # 9  push 0CAh
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": 10,
            "retOffset": 9,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\x3D\xAB\xAB\xAB\xAB\xAB"  # 0  cmp g_session.field_559C, 0
            b"\x0F\x8F\xAB\xAB\xAB\xAB"  # 7  jg loc_9DF605
            b"\x68\xAB\xAB\xAB\xAB"      # 13 push 294h
            b"\xE8"                      # 18 call MsgStr
        ),
        {
            "fixedOffset": 18,
            "msgId": 14,
            "retOffset": 13,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x80\xB8\xAB\xAB\xAB\xAB\xAB"  # 0  cmp byte ptr [eax+2F1h], 1
            b"\x75\xAB"                  # 7  jnz short loc_9E044F
            b"\x68\xAB\xAB\xAB\xAB"      # 9  push 0A2Dh
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": 10,
            "retOffset": 9,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call CRagConnection_SendPacket
            b"\x53"                      # 5  push ebx
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 17Eh
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xEB\xAB"                  # 0  jmp short loc_9E0AFF
            b"\x53"                      # 2  push ebx
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 1E6h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 4,
            "retOffset": 3,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 10h
            b"\x6A\xAB"                  # 2  push 7Ah
            b"\x8B\xF0"                  # 4  mov esi, eax
            b"\xE8"                      # 6  call MsgStr
        ),
        {
            "fixedOffset": 6,
            "msgId": (3, 1),
            "retOffset": 2,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 9B9h
            b"\xEB\xAB"                  # 5  jmp short loc_9F9BC1
            b"\xE8\xAB\xAB\xAB\xAB"      # 7  call j_memset
            b"\x68\xAB\xAB\xAB\xAB"      # 12 push 996h
            b"\xE8"                      # 17 call MsgStr
        ),
        {
            "fixedOffset": 17,
            "msgId": 13,
            "retOffset": 12,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x88\x95\xAB\xAB\xAB\xAB"  # 0  mov [ebp+buf], dl
            b"\xE8\xAB\xAB\xAB\xAB"      # 6  call j_memset
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 996h
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 12,
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 47h
            b"\xEB\xAB"                  # 2  jmp short loc_9FDCEF
            b"\x56"                      # 4  push esi
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 56Ch
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": 6,
            "retOffset": 5,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0FFh
            b"\xFF\x76\xAB"              # 5  push dword ptr [esi+4]
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xEB\xAB"                  # 0  jmp short loc_A04ED2
            b"\x51"                      # 2  push ecx
            b"\x53"                      # 3  push ebx
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 4F6h
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x75\xAB"                  # 0  jnz short loc_A04F90
            b"\x53"                      # 2  push ebx
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 4E9h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 4,
            "retOffset": 3,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 610h
            b"\x89\x0D\xAB\xAB\xAB\xAB"  # 5  mov g_session.m_amIPartyMaster, e
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x43\xB5\xAB\xAB\xAB\xAB"  # 0  cmovnb esi, [ebp+fInfo.charac
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 517h
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 8,
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\xB7\x40\xAB"          # 0  movzx eax, word ptr [eax+4]
            b"\x56"                      # 4  push esi
            b"\x57"                      # 5  push edi
            b"\x50"                      # 6  push eax
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\xAF\xC1"              # 0  imul eax, ecx
            b"\x50"                      # 3  push eax
            b"\x51"                      # 4  push ecx
            b"\x52"                      # 5  push edx
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 6D2h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\xB7\x46\xAB"          # 0  movzx eax, word ptr [esi+4]
            b"\x50"                      # 4  push eax
            b"\x51"                      # 5  push ecx
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 99h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\xBF\x46\xAB"          # 0  movsx eax, word ptr [esi+4]
            b"\x50"                      # 4  push eax
            b"\x51"                      # 5  push ecx
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 649h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\xB7\x47\xAB"          # 0  movzx eax, word ptr [edi+2]
            b"\x6A\xAB"                  # 4  push 0
            b"\x6A\xAB"                  # 6  push 0
            b"\x56"                      # 8  push esi
            b"\x50"                      # 9  push eax
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\xB7\x46\xAB"          # 0  movzx eax, word ptr [esi+2]
            b"\x6A\xAB"                  # 4  push 0
            b"\x6A\xAB"                  # 6  push 0
            b"\x57"                      # 8  push edi
            b"\x50"                      # 9  push eax
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x57"                      # 0  push edi
            b"\x8B\x7D\xAB"              # 1  mov edi, [ebp+buf]
            b"\xFF\x77\xAB"              # 4  push dword ptr [edi+4]
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xEB\xAB"                  # 0  jmp short loc_A10B93
            b"\xFF\x76\xAB"              # 2  push dword ptr [esi+4]
            b"\x50"                      # 5  push eax
            b"\xE8"                      # 6  call MsgStr
        ),
        {
            "fixedOffset": 6,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\x7E\xAB\xAB"          # 0  cmp dword ptr [esi+6], 0
            b"\x7C\xAB"                  # 4  jl short loc_A12829
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 64Fh
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\x3D\xAB\xAB\xAB\xAB\xAB"  # 0  cmp g_serviceType, 0Ch
            b"\x75\xAB"                  # 7  jnz short loc_A1A36F
            b"\x68\xAB\xAB\xAB\xAB"      # 9  push 465h
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": 10,
            "retOffset": 9,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x03\xC2"                  # 0  add eax, edx
            b"\x50"                      # 2  push eax
            b"\x57"                      # 3  push edi
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 465h
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x66\x3B\x46\xAB"          # 0  cmp ax, [esi+6]
            b"\x75\xAB"                  # 4  jnz short loc_A1C515
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 716h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC6\x45\xAB\xAB"          # 0  mov byte ptr [ebp+var_70.m_cstr],
            b"\xEB\xAB"                  # 4  jmp short loc_A1CF84
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 0C1h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x50"                      # 0  push eax
            b"\x68\xAB\xAB\xAB\xAB"      # 1  push 16Fh
            b"\x66\x0F\xD6\x45\xAB"      # 6  movq [ebp+var_10], xmm0
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 2,
            "retOffset": 1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x85\xAB\xAB\xAB\xAB"  # 0  jnz loc_A26B83
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push offset stru_DE80C8
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 245h
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 12,
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 537h
            b"\xC7\x81\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov dword ptr [ec
            b"\xE8"                      # 15 call MsgStr
        ),
        {
            "fixedOffset": 15,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x72\xAB"                  # 0  jb short loc_A29336
            b"\x8B\x36"                  # 2  mov esi, [esi]
            b"\x6A\xAB"                  # 4  push 37h
            b"\xE8"                      # 6  call MsgStr
        ),
        {
            "fixedOffset": 6,
            "msgId": (5, 1),
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\xB5\xAB\xAB\xAB\xAB"  # 0  push [ebp+var_484]
            b"\x52"                      # 6  push edx
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 7EFh
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 8,
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x7F\xAB"                  # 0  jg short loc_A2E9D1
            b"\x51"                      # 2  push ecx
            b"\x68\xAB\xAB\xAB\xAB"      # 3  push 9A7h
            b"\xE8"                      # 8  call MsgStr
        ),
        {
            "fixedOffset": 8,
            "msgId": 4,
            "retOffset": 3,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x52"                      # 0  push edx
            b"\x56"                      # 1  push esi
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 9A8h
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 3,
            "retOffset": 2,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\x85\xAB\xAB\xAB\xAB"  # 0  mov eax, [ebp+var_480]
            b"\xC6\x80\xAB\xAB\xAB\xAB\xAB"  # 6  mov byte ptr [eax+5E0h], 1
            b"\x68\xAB\xAB\xAB\xAB"      # 13 push 4E4h
            b"\xE8"                      # 18 call MsgStr
        ),
        {
            "fixedOffset": 18,
            "msgId": 14,
            "retOffset": 13,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x24\x8D\xAB\xAB\xAB\xAB"  # 0  jmp off_A2FB94[ecx*4]
            b"\x68\xAB\xAB\xAB\xAB"      # 7  push 1D0h
            b"\xE8"                      # 12 call MsgStr
        ),
        {
            "fixedOffset": 12,
            "msgId": 8,
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x3B\xC2"                  # 0  cmp eax, edx
            b"\x75\xAB"                  # 2  jnz short loc_A2F040
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 0D2h
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0A22h
            b"\x66\xA3\xAB\xAB\xAB\xAB"  # 5  mov g_session.rodex_mail_weight,
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x43\x4D\xAB"          # 0  cmovnb ecx, [ebp+a1.m_cstr]
            b"\x50"                      # 4  push eax
            b"\x51"                      # 5  push ecx
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 0CC7h
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x75\xAB"                  # 0  jnz short loc_A3A110
            b"\xC6\x83\xAB\xAB\xAB\xAB\xAB"  # 2  mov byte ptr [ebx+5E0h], 1
            b"\x68\xAB\xAB\xAB\xAB"      # 9  push 4E4h
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": 10,
            "retOffset": 9,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x3B\xC3"                  # 0  cmp eax, ebx
            b"\x75\xAB"                  # 2  jnz short loc_A3A2C0
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 1D7h
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\xB7\x75\xAB"          # 0  movzx esi, [ebp+msg_id_buf_4]
            b"\x50"                      # 4  push eax
            b"\x56"                      # 5  push esi
            b"\xE8"                      # 6  call MsgStr
        ),
        {
            "fixedOffset": 6,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\x7E\xAB\xAB"          # 0  cmp dword ptr [esi+6], 0
            b"\x72\xAB"                  # 4  jb short loc_A3BE69
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 64Fh
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x0F\x5A\xC0"              # 0  cvtps2pd xmm0, xmm0
            b"\xF2\x0F\x11\x04\x24"      # 3  movsd [esp+0D8h+var_D8], xmm0
            b"\x68\xAB\xAB\xAB\xAB"      # 8  push 0BD8h
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": 9,
            "retOffset": 8,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x15\xAB\xAB\xAB\xAB"  # 0  call timeGetTime
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 3ACh
            b"\xA3\xAB\xAB\xAB\xAB"      # 11 mov g_session.login_time, eax
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 170h
            b"\xE9\xAB\xAB\xAB\xAB"      # 5  jmp loc_A4EBEB
            b"\x56"                      # 10 push esi
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 1C1h
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 12,
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xE8\xAB\xAB\xAB\xAB"      # 0  call std_string_destructor
            b"\xE9\xAB\xAB\xAB\xAB"      # 5  jmp loc_A4F0D3
            b"\x56"                      # 10 push esi
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push 32Ah
            b"\xE8"                      # 16 call MsgStr
        ),
        {
            "fixedOffset": 16,
            "msgId": 12,
            "retOffset": 11,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 0  jmp off_A4F2CC[eax*4]
            b"\x56"                      # 7  push esi
            b"\x68\xAB\xAB\xAB\xAB"      # 8  push 337h
            b"\xE8"                      # 13 call MsgStr
        ),
        {
            "fixedOffset": 13,
            "msgId": 9,
            "retOffset": 8,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 5
            b"\xE9\xAB\xAB\xAB\xAB"      # 2  jmp loc_A4EBEB
            b"\x6A\xAB"                  # 7  push 9
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": (8, 1),
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x66\x83\x78\xAB\xAB"      # 0  cmp word ptr [eax+2], 0
            b"\x0F\x84\xAB\xAB\xAB\xAB"  # 5  jz loc_A5083C
            b"\x68\xAB\xAB\xAB\xAB"      # 11 push offset g_session.new_name
            b"\x68\xAB\xAB\xAB\xAB"      # 16 push 4BDh
            b"\xE8"                      # 21 call MsgStr
        ),
        {
            "fixedOffset": 21,
            "msgId": 17,
            "retOffset": 16,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC7\x46\xAB\xAB\xAB\xAB\xAB"  # 0  mov dword ptr [esi+0Ch], 7
            b"\xEB\xAB"                  # 7  jmp short loc_A50E83
            b"\x68\xAB\xAB\xAB\xAB"      # 9  push 718h
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": 10,
            "retOffset": 9,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 0
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 768h
            b"\xC7\x46\xAB\xAB\xAB\xAB\xAB"  # 7  mov dword ptr [esi+0Ch], 7
            b"\xE8"                      # 14 call MsgStr
        ),
        {
            "fixedOffset": 14,
            "msgId": 3,
            "retOffset": 2,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 1
            b"\x6A\xAB"                  # 2  push 0
            b"\x50"                      # 4  push eax
            b"\x56"                      # 5  push esi
            b"\xE8"                      # 6  call MsgStr
        ),
        {
            "fixedOffset": 6,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 10h
            b"\x6A\xAB"                  # 2  push 7Ah
            b"\x89\x83\xAB\xAB\xAB\xAB"  # 4  mov [ebx+69B0h], eax
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": -1,
            "retOffset": -1,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x68\xAB\xAB\xAB\xAB"      # 0  push 0CEEh
            b"\x89\x86\xAB\xAB\xAB\xAB"  # 5  mov [esi+423Ch], eax
            b"\x8B\x35\xAB\xAB\xAB\xAB"  # 11 mov esi, g_session.m_jobNameTable
            b"\xE8"                      # 17 call MsgStr
        ),
        {
            "fixedOffset": 17,
            "msgId": 1,
            "retOffset": 0,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x39\x7B\xAB"              # 0  cmp [ebx+6], edi
            b"\x75\xAB"                  # 3  jnz short loc_A560D3
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 2D5h
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": 6,
            "retOffset": 5,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC1\xEF\xAB"              # 0  shr edi, 0Ah
            b"\x57"                      # 3  push edi
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 2D6h
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC1\xE9\xAB"              # 0  shr ecx, 0Ah
            b"\x51"                      # 3  push ecx
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 2A5h
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x83\x7B\xAB\xAB"          # 0  cmp dword ptr [ebx+6], 0
            b"\x76\xAB"                  # 4  jbe short loc_A5621B
            b"\x68\xAB\xAB\xAB\xAB"      # 6  push 21Dh
            b"\xE8"                      # 11 call MsgStr
        ),
        {
            "fixedOffset": 11,
            "msgId": 7,
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xFF\x24\x85\xAB\xAB\xAB\xAB"  # 0  jmp off_A5686C[eax*4]
            b"\x6A\xAB"                  # 7  push 3
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": (8, 1),
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x75\xAB"                  # 0  jnz short loc_A562E0
            b"\xE9\xAB\xAB\xAB\xAB"      # 2  jmp loc_A567E2
            b"\x6A\xAB"                  # 7  push 4
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": (8, 1),
            "retOffset": 7,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC1\xEA\xAB"              # 0  shr edx, 0Bh
            b"\x52"                      # 3  push edx
            b"\x68\xAB\xAB\xAB\xAB"      # 4  push 216h
            b"\xE8"                      # 9  call MsgStr
        ),
        {
            "fixedOffset": 9,
            "msgId": 5,
            "retOffset": 4,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x6A\xAB"                  # 0  push 0
            b"\x6A\xAB"                  # 2  push 1
            b"\x6A\xAB"                  # 4  push 0
            b"\x6A\xAB"                  # 6  push 9
            b"\x8B\xF1"                  # 8  mov esi, ecx
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": (7, 1),
            "retOffset": 6,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\x70\xAB"              # 0  mov esi, [eax+4]
            b"\xEB\xAB"                  # 3  jmp short loc_A8DC2D
            b"\x68\xAB\xAB\xAB\xAB"      # 5  push 0BBh
            b"\xE8"                      # 10 call MsgStr
        ),
        {
            "fixedOffset": 10,
            "msgId": 6,
            "retOffset": 5,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 0  mov [ebp+value.second], 2
            b"\xE8\xAB\xAB\xAB\xAB"      # 7  call std_vector_TALKTYPE_insert
            b"\x68\xAB\xAB\xAB\xAB"      # 12 push 220h
            b"\xE8"                      # 17 call MsgStr
        ),
        {
            "fixedOffset": 17,
            "msgId": 13,
            "retOffset": 12,
        },
        {
        }
    ],
    # 2017-11-01
    [
        (
            b"\x8B\x3B"                  # 0  mov edi, [ebx]
            b"\x68\xAB\xAB\xAB\xAB"      # 2  push 220h
            b"\xE8"                      # 7  call MsgStr
        ),
        {
            "fixedOffset": 7,
            "msgId": 3,
            "retOffset": 2,
        },
        {
        }
    ],
]
