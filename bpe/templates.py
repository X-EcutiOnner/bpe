#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os


class Templates:
    def __init__(self, fileTemplate, lineTemplate, dirName="templates"):
        self.templateDir = dirName
        self.fileTemplate = Templates.loadTemplateFile(fileTemplate,
                                                       self.templateDir)
        if lineTemplate != "":
            self.lineTemplate = Templates.loadTemplateFile(lineTemplate,
                                                           self.templateDir)
        else:
            self.lineTemplate = ""
        self.data = ""


    @staticmethod
    def loadTemplateFile(fileName, dirName="templates"):
        with open(dirName + "/" + fileName, "rb") as f:
            return str(f.read().decode())


    def addLine(self, addr, name):
        self.data = self.data + \
            self.lineTemplate.format(addr, name.replace("::", "_"))


    def add(self, data):
        self.data = self.data + data


    def getData(self):
        return self.fileTemplate.format(self.data)


    def save(self, outDir, fileName, exeName):
        subName = os.path.basename(os.path.abspath("."))
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        outName = "{0}/{1}".format(outDir, fileName.format(subName, exeName))
        with open(outName, "wt") as w:
            w.write(self.getData())
