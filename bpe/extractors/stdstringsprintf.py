#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


# search in recv_packet_10A

def searchStdStringSprintf(self):
    offset, section = self.exe.string(b"%s%s")
    if offset is False:
        self.log("failed in search '%s%s'")
        exit(1)

    formatStr = section.rawToVa(offset)
    formatStrHex = self.exe.toHex(formatStr, 4)

    # 0  push 90h
    # 5  call MsgStr
    # 10 add esp, 4
    # 13 push eax
    # 14 push esi
    # 15 push 8Fh
    # 20 call MsgStr
    # 25 add esp, 4
    # 28 push eax
    # 29 lea eax, [ebp+a1]
    # 32 push offset aSS_0
    # 37 push eax
    # 38 call std_string_sprintf
    code = (
        b"\x68\x90\x00\x00\x00"            # 0
        b"\xE8\xAB\xAB\xAB\xAB"            # 5
        b"\x83\xC4\x04"                    # 10
        b"\x50"                            # 13
        b"\x56"                            # 14
        b"\x68\x8F\x00\x00\x00"            # 15
        b"\xE8\xAB\xAB\xAB\xAB"            # 20
        b"\x83\xC4\x04"                    # 25
        b"\x50"                            # 28
        b"\x8D\x45\xAB"                    # 29
        b"\x68" + formatStrHex +           # 32
        b"\x50"                            # 37
        b"\xE8"                            # 38
    )
    msgStrOffset1 = 6
    msgStrOffset2 = 21
    sprintfOffset = 39
    offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2015
        # 0  push 90h
        # 5  call MsgStr
        # 10 add esp, 4
        # 13 push eax
        # 14 push edi
        # 15 push 8Fh
        # 20 call MsgStr
        # 25 add esp, 4
        # 28 push eax
        # 29 lea edx, [ebp+var_48]
        # 32 push offset aSS_5
        # 37 push edx
        # 38 call std_string_sprintf
        code = (
            b"\x68\x90\x00\x00\x00"            # 0
            b"\xE8\xAB\xAB\xAB\xAB"            # 5
            b"\x83\xC4\x04"                    # 10
            b"\x50"                            # 13
            b"\x57"                            # 14
            b"\x68\x8F\x00\x00\x00"            # 15
            b"\xE8\xAB\xAB\xAB\xAB"            # 20
            b"\x83\xC4\x04"                    # 25
            b"\x50"                            # 28
            b"\x8D\x55\xAB"                    # 29
            b"\x68" + formatStrHex +           # 32
            b"\x52"                            # 37
            b"\xE8"                            # 38
        )
        msgStrOffset1 = 6
        msgStrOffset2 = 21
        sprintfOffset = 39
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        # 2013-01-03
        # 0  push 90h
        # 5  call MsgStr
        # 10 add esp, 4
        # 13 push eax
        # 14 push esi
        # 15 push 8Fh
        # 20 call MsgStr
        # 25 add esp, 4
        # 28 push eax
        # 29 lea edx, [esp+0F8h+var_E0]
        # 33 push offset aSS
        # 38 push edx
        # 39 call std_string_sprintf
        code = (
            b"\x68\x90\x00\x00\x00"            # 0
            b"\xE8\xAB\xAB\xAB\xAB"            # 5
            b"\x83\xC4\x04"                    # 10
            b"\x50"                            # 13
            b"\x56"                            # 14
            b"\x68\x8F\x00\x00\x00"            # 15
            b"\xE8\xAB\xAB\xAB\xAB"            # 20
            b"\x83\xC4\x04"                    # 25
            b"\x50"                            # 28
            b"\x8D\x54\x24\xAB"                # 29
            b"\x68" + formatStrHex +           # 33
            b"\x52"                            # 38
            b"\xE8"                            # 39
        )
        msgStrOffset1 = 6
        msgStrOffset2 = 21
        sprintfOffset = 40
        offset = self.exe.codeWildcard(code, b"\xAB")

    if offset is False:
        self.log("failed in search std::string::sprintf")
        if self.packetVersion < "20130000":
            return
        exit(1)

    msgStr1 = self.getAddr(offset,
                           msgStrOffset1,
                           msgStrOffset1 + 4)
    msgStr2 = self.getAddr(offset,
                           msgStrOffset2,
                           msgStrOffset2 + 4)
    if msgStr1 != self.MsgStr:
        self.log("Error: found wrong MsgStr")
        exit(1)
    if msgStr1 != msgStr2:
        self.log("Error: found different MsgStr")
        exit(1)
    self.std_string_sprintf = self.getAddr(offset,
                                           sprintfOffset,
                                           sprintfOffset + 4)
    self.addRawFuncType("std::string::sprintf",
                        self.std_string_sprintf,
                        "struct_std_string *std_string_sprintf("
                        "struct_std_string *dstStr, const char *format, ...);")
