#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def setMemberTypes(self):
    self.addStruct("CMode")
    self.addStruct("CModeMgr")
    # CModeMgr::m_curMode
    self.setStructMemberType(self.CModeMgr_m_curMode, "CMode*")
    # CLoginMode::m_charInfo
    self.addStruct("CLoginMode")
    if self.maxChars == 0:
        maxChars = 1
    else:
        maxChars = self.maxChars
    self.setStructMemberType(self.CLoginMode_m_charInfo,
                             "CHARACTER_INFO[{0}]".format(maxChars))
