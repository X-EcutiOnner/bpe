#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bpe.params import searchParams2


def searchSoundMgr(self, errorExit):
    vals = (
        (b"isBgmOn", "m_isBgmOn", 4),
        (b"isSoundOn", "m_isSoundOn", 4),
    )
    from bpe.blocks.soundmgr import blocks
    arr, soundMgr = searchParams2(self,
                                  vals,
                                  blocks,
                                  True,
                                  "g_soundMgr",
                                  False,
                                  2)
    if soundMgr == 0:
        self.log("Error: g_soundMgr not found")
        if self.packetVersion >= "20131218" and self.clientType != "iro":
            exit(1)
        return
    self.soundMgr = soundMgr
    self.addVaVar("g_soundMgr", self.soundMgr)
    if b"isSoundOn" not in arr:
        self.exe.log("Error: isSoundOn not found")
        exit(1)
    self.addStruct("CSoundMgr")
    for val in vals:
        val0 = val[0]
        if val0 in arr:
            self.addStructMember(val[1], arr[val0], val[2], True)
    self.setVarType(self.soundMgr, "CSoundMgr*")
