#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchWindowMgrDeleteWindow1(self, errorExit):
    # search in UIWindowMgr_MakeWindow, case 0x22

    # 0  jz loc_547968
    # 6  push 45h
    # 8  mov ecx, esi
    # 10 call UIWindowMgr_DeleteWindow
    # 15 mov edi, [esi+2A4h]
    code = (
        b"\x0F\x84\xAB\xAB\xAB\xAB"        # 0
        b"\x6A\x45"                        # 6
        b"\x8B\xCE"                        # 8
        b"\xE8\xAB\xAB\xAB\xAB"            # 10
        b"\x8B\xBE\xAB\xAB\x00\x00"        # 15
    )
    deleteWindowOffset = 11
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   self.UIWindowMgrMakeWindow,
                                   self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jz loc_6E2BE5
        # 6  push 45h
        # 8  mov ecx, ebx
        # 10 call UIWindowMgr_DeleteWindow
        # 15 mov edi, [ebx+UIWindowMgr.m_messengerGroupWnd]
        code = (
            b"\x0F\x84\xAB\xAB\xAB\xAB"        # 0
            b"\x6A\x45"                        # 6
            b"\x8B\xCB"                        # 8
            b"\xE8\xAB\xAB\xAB\xAB"            # 10
            b"\x8B\xBB\xAB\xAB\x00\x00"        # 15
        )
        deleteWindowOffset = 11
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jz loc_5E4B88
        # 6  push 45h
        # 8  mov ecx, esi
        # 10 call UIWindowMgr_DeleteWindow
        # 15 cmp [esi+2D0h], edi
        code = (
            b"\x0F\x84\xAB\xAB\xAB\xAB"        # 0
            b"\x6A\x45"                        # 6
            b"\x8B\xCE"                        # 8
            b"\xE8\xAB\xAB\xAB\xAB"            # 10
            b"\x39\xBE\xAB\xAB\x00\x00"        # 15
        )
        deleteWindowOffset = 11
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jmp short loc_5256F6
        # 2  push 45h
        # 4  mov ecx, esi
        # 6  call UIWindowMgr_DeleteWindow
        # 11 mov edi, [esi+2A4h]
        code = (
            b"\xEB\xAB"                        # 0
            b"\x6A\x45"                        # 2
            b"\x8B\xCE"                        # 4
            b"\xE8\xAB\xAB\xAB\xAB"            # 6
            b"\x8B\xBE\xAB\xAB\x00\x00"        # 11
        )
        deleteWindowOffset = 7
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jmp loc_4FCF00
        # 5  push 45h
        # 7  mov ecx, esi
        # 9  call UIWindowMgr_DeleteWindow
        # 14 mov ebx, [esi+294h]
        code = (
            b"\xE9\xAB\xAB\xAB\xAB"            # 0
            b"\x6A\x45"                        # 5
            b"\x8B\xCE"                        # 7
            b"\xE8\xAB\xAB\xAB\xAB"            # 9
            b"\x8B\x9E\xAB\xAB\x00\x00"        # 14
        )
        deleteWindowOffset = 10
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jmp loc_4F6CB7
        # 5  push 45h
        # 7  call UIWindowMgr_DeleteWindow
        # 12 mov edi, [esi+124h]
        code = (
            b"\xE9\xAB\xAB\x00\x00"            # 0
            b"\x6A\x45"                        # 5
            b"\xE8\xAB\xAB\xAB\xAB"            # 7
            b"\x8B\xBE\xAB\xAB\x00\x00"        # 12
        )
        deleteWindowOffset = 8
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jmp short loc_4F88CB
        # 2  push 45h
        # 4  mov ecx, ebx
        # 6  call UIWindowMgr_DeleteWindow
        # 11 mov esi, [ebx+148h]
        code = (
            b"\xEB\xAB"                        # 0
            b"\x6A\x45"                        # 2
            b"\x8B\xCB"                        # 4
            b"\xE8\xAB\xAB\xAB\xAB"            # 6
            b"\x8B\xB3\xAB\xAB\x00\x00"        # 11
        )
        deleteWindowOffset = 7
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jmp loc_4F7B87
        # 5  push 45h
        # 7  mov ecx, ebx
        # 9  call UIWindowMgr_DeleteWindow
        # 14 mov esi, [ebx+14Ch]
        code = (
            b"\xE9\xAB\xAB\xAB\xAB"            # 0
            b"\x6A\x45"                        # 5
            b"\x8B\xCB"                        # 7
            b"\xE8\xAB\xAB\xAB\xAB"            # 9
            b"\x8B\xB3\xAB\xAB\x00\x00"        # 14
        )
        deleteWindowOffset = 10
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jmp loc_4F1306
        # 5  push 45h
        # 7  mov ecx, esi
        # 9  call UIWindowMgr_DeleteWindow
        # 14 mov edi, [esi+130h]
        code = (
            b"\xE9\xAB\xAB\xAB\xAB"            # 0
            b"\x6A\x45"                        # 5
            b"\x8B\xCE"                        # 7
            b"\xE8\xAB\xAB\xAB\xAB"            # 9
            b"\x8B\xBE\xAB\xAB\x00\x00"        # 14
        )
        deleteWindowOffset = 10
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jmp short loc_5EF567
        # 2  push 45h
        # 4  mov ecx, ebx
        # 6  call UIWindowMgr_DeleteWindow
        # 11 mov edi, [ebx+288h]
        code = (
            b"\xEB\xAB"                        # 0
            b"\x6A\x45"                        # 2
            b"\x8B\xCB"                        # 4
            b"\xE8\xAB\xAB\xAB\xAB"            # 6
            b"\x8B\xBB\xAB\xAB\x00\x00"        # 11
        )
        deleteWindowOffset = 7
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jz loc_6F0A55
        # 6  push 45h
        # 8  mov ecx, edi
        # 10 call UIWindowMgr_DeleteWindow
        # 15 mov ebx, [edi+2A8h]
        code = (
            b"\x0F\x84\xAB\xAB\xAB\xAB"        # 0 jz loc_6F0A55
            b"\x6A\x45"                        # 6 push 45h
            b"\x8B\xCF"                        # 8 mov ecx, edi
            b"\xE8\xAB\xAB\xAB\xAB"            # 10 call UIWindowMgr_DeleteWind
            b"\x8B\x9F\xAB\xAB\x00\x00"        # 15 mov ebx, [edi+2A8h]
        )
        deleteWindowOffset = 11
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jmp short loc_4FA33D
        # 2  push 45h
        # 4  call UIWindowMgr_DeleteWindow
        # 9  mov edi, [esi+23Ch]
        code = (
            b"\xEB\xAB"                        # 0 jmp short loc_4FA33D
            b"\x6A\x45"                        # 2 push 45h
            b"\xE8\xAB\xAB\xAB\xAB"            # 4 call UIWindowMgr_DeleteWindo
            b"\x8B\xBE\xAB\xAB\x00\x00"        # 9 mov edi, [esi+23Ch]
        )
        deleteWindowOffset = 5
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        self.log("failed in search UIWindowMgr::DeleteWindow 1.")
        if errorExit is True:
            exit(1)
        return

    self.UIWindowMgrDeleteWindow = self.getAddr(offset,
                                                deleteWindowOffset,
                                                deleteWindowOffset + 4)
    self.UIWindowMgrDeleteWindowVa = self.exe.rawToVa(
        self.UIWindowMgrDeleteWindow)
    self.addRawFunc("UIWindowMgr::DeleteWindow",
                    self.UIWindowMgrDeleteWindow)
