#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def run4Step(self, code, num, addrOffset, instructionOffset):
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   self.initPacketMapFunction,
                                   self.initPacketMapFunction + 0x140)
    if offset is not False:
        self.showRawAddr("peek4.{0}".format(num), offset)
        self.initPacketLenWithClientFunction = self.getAddr(
            offset, addrOffset, instructionOffset)
    return offset


def run4StepAlone(self, code, num, addrOffset, instructionOffset):
    offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is not False:
        self.showRawAddr("peek4.{0}".format(num), offset)
        self.initPacketLenWithClientFunction = self.getAddr(
            offset, addrOffset, instructionOffset)
    return offset


def getBasicInfo(self):
    # Step 1 - Find the GetPacketSize function call
    # 0  call CRagConnection_instanceR
    # 5  mov ecx, eax
    # 7  call CRagConnection_GetPacketSize
    # 12 push eax
    # 13 call CRagConnection_instanceR
    # 18 mov ecx, eax
    # 20 call CRagConnection_SendPacket
    # 25 push 1
    # 27 call CRagConnection_instanceR
    # 32 mov ecx, eax
    # 34 call CConnection_SetBlock
    # 39 push 6
    code = (
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x8B\xC8" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x50" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x8B\xC8" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x6A\x01" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x8B\xC8" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x6A\x06")
    offsetInstanceR1 = 1
    offsetInstanceR2 = 14
    offsetInstanceR3 = 28
    offsetSetBlock = 35
    offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 0  call CRagConnection_instanceR
        # 5  mov ecx, eax
        # 7  call CRagConnection_GetPacketSize
        # 12 push eax
        # 13 call CRagConnection_instanceR
        # 18 mov ecx, eax
        # 20 call CRagConnection_SendPacket
        # 25 push 1
        # 27 call CRagConnection_instanceR
        # 32 mov ecx, eax
        # 34 call CConnection_SetBlock
        # 39 jmp addr
        code = (
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x8B\xC8" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x50" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x8B\xC8" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x6A\x01" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x8B\xC8" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\xe9")
        offsetInstanceR1 = 1
        offsetInstanceR2 = 14
        offsetInstanceR3 = 28
        offsetSetBlock = 35
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        # 2018-02-08_j
        # 0  call CRagConnection_instanceR
        # 5  mov ecx, eax
        # 7  call CRagConnection_GetPacketSize
        # 12 push eax
        # 13 call CRagConnection_instanceR
        # 18 mov ecx, eax
        # 20 call CRagConnection_SendPacket
        # 25 push 1
        # 27 mov byte ptr addr, 1
        # 34 call CRagConnection_instanceR
        # 39 mov ecx, eax
        # 41 call CConnection_SetBlock
        # 46 push 6
        code = (
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x8B\xC8" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x50" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x8B\xC8" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x6A\x01" +
            b"\xC6\x05\xAB\xAB\xAB\x00\x01" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x8B\xC8" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x6A\x06")
        offsetInstanceR1 = 1
        offsetInstanceR2 = 14
        offsetInstanceR3 = 35
        offsetSetBlock = 42
        offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is not False:
        self.offset1 = offset
        self.showRawAddr("peek1", offset)
        instanceR1 = self.getAddr(offset,
                                  offsetInstanceR1,
                                  offsetInstanceR1 + 4)
        instanceR2 = self.getAddr(offset,
                                  offsetInstanceR2,
                                  offsetInstanceR2 + 4)
        instanceR3 = self.getAddr(offset,
                                  offsetInstanceR3,
                                  offsetInstanceR3 + 4)
        if instanceR1 != instanceR2 or instanceR1 != instanceR3:
            self.log("Error: wrong basic block found")
            exit(1)
            return False
        if self.instanceR != 0 and self.instanceR != instanceR1:
            self.log("Error: found different CRagConnection::instanceR")
            exit(1)
            return False
        self.instanceR = instanceR1
        self.addRawFunc("CRagConnection::instanceR", self.instanceR)
        self.sendPacket = self.getAddr(offset, 21, 25)
        self.sendPacketVa = self.exe.rawToVa(self.sendPacket)
        self.addRawFunc("CRagConnection::SendPacket",
                        self.sendPacket)
        self.setBlock = self.getAddr(offset,
                                     offsetSetBlock,
                                     offsetSetBlock + 4)
        self.addRawFunc("CConnection::SetBlock", self.setBlock)

        # Step 2a - Go Inside the GetPacketSize function
        offset = offset + 12 - 4
        # addr + offset + 4 in 32 bit math
        offset = self.getAddr(offset, 0, 4)
        self.getPacketSizeFunction = offset
        self.addRawFunc("CRagConnection::GetPacketSize",
                        self.getPacketSizeFunction)
        code = (
            b"\xB9\xAB\xAB\xAB\x00" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x8B\xAB\x04")
        offset0 = offset
        pktLenFunctionOffset = 6
        offset = self.exe.codeWildcard(code, b"\xAB", offset, offset + 0x80)
        if offset is False:
            offset = offset0
            code = (
                b"\xB9\xAB\xAB\xAB\x01" +
                b"\xE8\xAB\xAB\xAB\xAB" +
                b"\x8B\xAB\x04")
            pktLenFunctionOffset = 6
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           offset,
                                           offset + 0x80)
        if offset is False:
            offset = offset0
            # 0  mov ecx, offset g_PacketLenMap
            # 5  push eax
            # 6  call pktLenFunction
            # 11 pop esi
            # 12 mov eax, [eax+4]
            code = (
                b"\xB9\xAB\xAB\xAB\x00"            # 0 mov ecx, offset g_Packet
                b"\x50"                            # 5 push eax
                b"\xE8\xAB\xAB\xAB\xAB"            # 6 call pktLenFunction
                b"\x5E"                            # 11 pop esi
                b"\x8B\x40\x04"                    # 12 mov eax, [eax+4]
            )
            pktLenFunctionOffset = 7
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           offset,
                                           offset + 0x80)
        if offset is False:
            offset = offset0
            # 0  mov ecx, offset g_PacketLenMap
            # 5  push eax
            # 6  call pktLenFunction
            # 11 pop esi
            # 12 mov eax, [eax+4]
            code = (
                b"\xB9\xAB\xAB\xAB\x01"            # 0 mov ecx, offset g_Packet
                b"\x50"                            # 5 push eax
                b"\xE8\xAB\xAB\xAB\xAB"            # 6 call pktLenFunction
                b"\x5E"                            # 11 pop esi
                b"\x8B\x40\x04"                    # 12 mov eax, [eax+4]
            )
            pktLenFunctionOffset = 7
            offset = self.exe.codeWildcard(code,
                                           b"\xAB",
                                           offset,
                                           offset + 0x80)
    else:
        # 1.2
        # push 1
        # call CRagConnection::instanceR
        # mov ecx, eax
        # call CConnection::SetBlock (probably)
        # push 6
        code = (
            b"\x6a\x01" +
            b"\xe8\xAB\xAB\xAB\xAB" +
            b"\x8B\xC8" +
            b"\xe8\xAB\xAB\xAB\xAB" +
            b"\x6A\x06")
        offset = self.exe.codeWildcard(code, b"\xAB")
        if offset is not False:
            self.offset1 = offset
            self.showRawAddr("peek1.2", offset)
            instanceR = self.getAddr(offset, 3, 7)
            if self.instanceR != 0 and self.instanceR != instanceR:
                self.log("Error: found different "
                         "CRagConnection::instanceR")
                return False
            self.instanceR = instanceR
            self.addRawFunc("CRagConnection::instanceR",
                            self.instanceR)
            # skip step 2 and 3 becase no signatures for it. Go to 4.
            offset = False
        else:
            # in very old client no way to find any functions
            # except initPacketLenWithClient
            # but functions can be wrong :(
            if self.exe.client_date <= 20031124:
                # mov [esi + N], ebx
                # mov [esi + N], ebx
                # mov dword ptr [esi], N
                # call initPacketLenWithClient
                # mov ecx, [ebp + N]
                offset = run4StepAlone(self,
                                       b"\x89\x9E\xAB\xAB\xAB\xAB" +
                                       b"\x89\x9E\xAB\xAB\xAB\xAB" +
                                       b"\xC7\x06\xAB\xAB\xAB\xAB" +
                                       b"\xE8\xAB\xAB\xAB\xAB" +
                                       b"\x8B\x4D\xAB",
                                       8,
                                       19, 23)
            elif self.exe.client_date <= 20041207:
                # mov [esi + N], M
                # mov [esi + N], M
                # mov dword ptr [esi], N
                # call initPacketLenWithClient
                # mov ecx, [ebp + N]
                offset = run4StepAlone(self,
                                       b"\x89\x9e\xAB\xAB\xAB\xAB" +
                                       b"\x89\x9e\xAB\xAB\xAB\xAB" +
                                       b"\xC7\x06\xAB\xAB\xAB\xAB" +
                                       b"\xE8\xAB\xAB\xAB\xAB" +
                                       b"\x8B\x4D\xAB",
                                       7,
                                       19, 23)
                # mov ecx, esi
                # mov byte ptr [ebp + N], M
                # mov dword ptr [esi], N
                # call initPacketLenWithClient
                # mov ecx, [ebp + N]
                if offset is False:
                    offset = run4StepAlone(self,
                                           b"\x8B\xCE" +
                                           b"\xC6\x45\xAB\xAB" +
                                           b"\xC7\x06\xAB\xAB\xAB\xAB" +
                                           b"\xE8\xAB\xAB\xAB\xAB" +
                                           b"\x8B\x4D\xAB",
                                           9,
                                           13, 17)
            if offset is False:
                self.log("failed in 1")
                return False
            self.addRawFunc("CRagConnection::InitPacketLenWithClient",
                            self.initPacketLenWithClientFunction)
            return True
    if offset is not False:
        self.showRawAddr("peek2 (mov ecx, offset g_PacketLenMap)",
                         offset)
        # Step 2c - Extract the g_PacketLenMap assignment
        self.gPacketLenMap = self.exe.read(offset + 1, 4)
        self.pktLenFunction = self.getAddr(offset,
                                           pktLenFunctionOffset,
                                           pktLenFunctionOffset + 4)
        self.addRawFunc("pktLenFunction", self.pktLenFunction)

        # 3
        code = (
            self.gPacketLenMap +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x68\xAB\xAB\xAB\x00" +
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x59" +
            b"\xC3")
        offset = self.exe.codeWildcard(code, b"\xAB")
        if offset is not False:
            self.showRawAddr("peek3 (mov ecx, offset g_PacketLenMap)",
                             offset)
            self.gPacketLenMapAddr = self.exe.readUInt(offset)
            self.addVaVar("g_PacketLenMap", self.gPacketLenMapAddr)
            offset = offset - 1
            self.initPacketMapFunction = self.getAddr(offset, 6, 10)
        else:
            # 3.2
            # mov RR, g_packetLenMap
            # call CRagConnection::InitPacketMap
            # push addr1
            # call addr2
            # add esp, N
            # mov esp, ebp
            # pop ebp
            # retn
            code = (
                self.gPacketLenMap +
                b"\xE8\xAB\xAB\xAB\xAB" +
                b"\x68\xAB\xAB\xAB\x00" +
                b"\xE8\xAB\xAB\xAB\xAB" +
                b"\x83\xC4\xAB" +
                b"\x8B\xE5" +
                b"\x5d" +
                b"\xC3")
            offset = self.exe.codeWildcard(code, b"\xAB")
            if offset is False:
                self.log("failed in 3")
                return False
            self.showRawAddr("peek3.2", offset)
            offset = offset - 1
            self.initPacketMapFunction = self.getAddr(offset, 6, 10)

    else:
        self.gPacketLenMap = None
        # search in CRagConnection::instanceR for call to InitPacketMap
        # ...
        # 2a.2
        # call CRagConnection::InitPacketMap
        # push addr1
        # call addr2
        code = (
            b"\xE8\xAB\xAB\xAB\xAB" +
            b"\x68\xAB\xAB\xAB\x00" +
            b"\xE8\xAB\xAB\xAB\xAB")
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.instanceR,
                                       self.instanceR + 0x50)
        if offset is False:
            self.log("failed in 2a")
            return False
        self.showRawAddr("peek2a.2", offset)
        self.initPacketMapFunction = self.getAddr(offset, 1, 5)

    # 4
    self.addRawFunc("CRagConnection::InitPacketMap",
                    self.initPacketMapFunction)
    # peek step 4
    res = run4Step(self,
                   b"\x8B\xCE" +
                   b"\xE8\xAB\xAB\xAB\xAB" +
                   b"\xC7",
                   1,
                   3, 7)
    if res is False:
        # 4c.2
        # mov ecx, esi
        # mov [ebp + N], ebx
        # call initPacketLenWithClient
        # mov ecx, [ebp + N]
        res = run4Step(self,
                       b"\x8B\xCE" +
                       b"\x89\x5D\xAB" +
                       b"\xE8\xAB\xAB\xAB\xAB" +
                       b"\x8B\x4D\xAB",
                       2,
                       6, 10)
    if res is False:
        # 4c.3
        # mov ecx, esi
        # mov [ebp + N], M
        # call initPacketLenWithClient
        # mov ecx, [ebp + N]
        res = run4Step(self,
                       b"\x8B\xCE" +
                       b"\xC7\x45\xAB\xAB\xAB\xAB\xAB" +
                       b"\xE8\xAB\xAB\xAB\xAB" +
                       b"\x8B\x4D\xAB",
                       3,
                       10, 14)
    if res is False:
        # 4c.4
        # mov [ebp + N], M
        # mov dword ptr [esi], N
        # call initPacketLenWithClient
        # mov ecx, [ebp + N]
        res = run4Step(self,
                       b"\xC7\x45\xAB\xAB\xAB\xAB\xAB" +
                       b"\xC7\x06\xAB\xAB\xAB\xAB" +
                       b"\xE8\xAB\xAB\xAB\xAB" +
                       b"\x8B\x4D\xAB",
                       4,
                       14, 18)
    if res is False:
        # 4c.5
        # mov [esp + N], M
        # mov dword ptr [esi], N
        # call initPacketLenWithClient
        # mov [esp + N], M
        res = run4Step(self,
                       b"\xC7\x44\x24\xAB\xAB\xAB\xAB\xAB" +
                       b"\xC7\x06\xAB\xAB\xAB\xAB" +
                       b"\xE8\xAB\xAB\xAB\xAB" +
                       b"\xC7\x44\x24\xAB\xAB\xAB\xAB\xAB",
                       5,
                       15, 19)
    if res is False:
        # 4c.6
        # mov [esp + N], M
        # mov dword ptr [esi], N
        # call initPacketLenWithClient
        # mov eax, esi
        # mov ecx, [esp + N]
        res = run4Step(self,
                       b"\xC7\x44\x24\xAB\xAB\xAB\xAB\xAB" +
                       b"\xC7\x06\xAB\xAB\xAB\xAB" +
                       b"\xE8\xAB\xAB\xAB\xAB" +
                       b"\x8B\xC6" +
                       b"\x8B\x4C\x24\xAB",
                       6,
                       15, 19)
    if res is False:
        self.log("failed in 4")
        return False
    self.addRawFunc("CRagConnection::InitPacketLenWithClient",
                    self.initPacketLenWithClientFunction)
    return True
