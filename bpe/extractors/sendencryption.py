#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchSendEncryption(self, errorExit):
    if self.gCheatDefenderMgr == 0:
        return
    if self.sendPacket == 0:
        self.log("Error: sendPacket not found")
        if errorExit is True:
            exit(1)

    # 0  mov ecx, g_CCheatDefenderMgr
    # 6  push ebx
    # 7  push edi
    # 8  call CDClient_encryption
    # 13 cmp al, 1
    # 15 jz addr1
    # 17 cmp dword ptr [esi+m_socket], 0FFFFFFFFh
    # 21 jz addr1
    hookOffset1 = 7
    hookOffset2 = 0
    hookExitOffset = 8
    callEncryptionOffset = 9
    m_socketOffset = 19
    code = (
        b"\x8B\x0D\xAB\xAB\xAB\xAB" +
        b"\xAB" +
        b"\x57" +
        b"\xE8\xAB\xAB\xAB\xAB" +
        b"\x3C\x01" +
        b"\x74\xAB" +
        b"\x83\xAB\xAB\xFF" +
        b"\x74")
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   self.sendPacket,
                                   self.sendPacket + 0x50)
    if offset is False:
        # 0  push ebx
        # 1  push edi
        # 2  xor [edi], ax
        # 5  mov ecx, g_CCheatDefenderMgr
        # 11 call CDClient_encryption
        # 16 cmp al, 1
        # 18 jz short loc_867B86
        # 20 cmp [esi+CRagConnection.m_socket], 0FFFFFFFFh
        # 24 jz short loc_867B86
        code = (
            b"\x53"                            # 0 push ebx
            b"\x57"                            # 1 push edi
            b"\x66\x31\x07"                    # 2 xor [edi], ax
            b"\x8B\x0D\xAB\xAB\xAB\xAB"        # 5 mov ecx, g_CCheatDefenderMgr
            b"\xE8\xAB\xAB\xAB\xAB"            # 11 call CDClient_encryption
            b"\x3C\x01"                        # 16 cmp al, 1
            b"\x74\xAB"                        # 18 jz short loc_867B86
            b"\x83\x7E\xAB\xFF"                # 20 cmp [esi+CRagConnection.m_s
            b"\x74"                            # 24 jz short loc_867B86
        )
        hookOffset1 = 0
        hookOffset2 = 5
        hookExitOffset = 11
        callEncryptionOffset = 12
        m_socketOffset = 22
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.sendPacket,
                                       self.sendPacket + 0x50)

    if offset is False:
        self.log("Error: CDClient_encryption not found")
        if errorExit is True:
            exit(1)
    self.sendEncryption = self.getAddr(offset,
                                       callEncryptionOffset,
                                       callEncryptionOffset + 4)
    self.addRawFunc("CDClient_encryption",
                    self.sendEncryption)
    self.hookSendEncryptionExit = self.exe.rawToVa(offset) + \
        hookExitOffset
    if hookOffset1 != 0:
        self.hookSendEncryptionStart1 = self.exe.rawToVa(offset + hookOffset1)
        self.showVaAddr("SendEncryptionHook",
                        self.hookSendEncryptionStart1)
    else:
        self.hookSendEncryptionStart1 = 0
    if hookOffset2 != 0:
        self.hookSendEncryptionStart2 = self.exe.rawToVa(offset + hookOffset2)
        self.showVaAddr("SendEncryptionHook",
                        self.hookSendEncryptionStart2)
    else:
        self.hookSendEncryptionStart2 = 0
    m_socket = self.exe.readUByte(offset + m_socketOffset)
    self.addStruct("CRagConnection")
    self.addStructMember("m_socket", m_socket, 4, True)
