#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

# search in CResMgr_ReadResNameTable


def searchResMgr(self, errorExit):
    offset, section = self.exe.string(b"resNameTable.txt")
    if offset is False:
        self.log("failed in search resNameTable.txt")
        exit(1)
        return
    strAddr = section.rawToVa(offset)
    # search in WinMain
    # 0  push offset resNameTable
    # 5  call g_resMgr
    # 10 mov ecx, eax
    # 12 call CResMgr_ReadResNameTable
    code = (
        b"\x68" + self.exe.toHex(strAddr, 4) +  # 0
        b"\xE8\xAB\xAB\xAB\xAB"            # 5
        b"\x8B\xC8"                        # 10
        b"\xE8"                            # 12
    )
    resMgrOffset = 6
    resNameTableOffset = 13
    offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        self.log("failed in search ResMgr.")
        if errorExit is True:
            exit(1)
        return
    self.gResMgr = self.getAddr(offset,
                                resMgrOffset,
                                resMgrOffset + 4)
    self.CResMgrReadResNameTable = self.getAddr(offset,
                                                resNameTableOffset,
                                                resNameTableOffset + 4)
    self.addRawFunc("g_resMgr", self.gResMgr)
    self.addRawFunc("CResMgr::ReadResNameTable",
                    self.CResMgrReadResNameTable)

    # 0  mov ecx, offset m_resMgr
    # 5  call CResMgr_CResMgr
    # 10 push offset sub_C39610
    # 15 call atexit
    # 20 add esp, 4
    code = (
        b"\xB9\xAB\xAB\xAB\xAB"            # 0
        b"\xE8\xAB\xAB\xAB\xAB"            # 5
        b"\x68\xAB\xAB\xAB\xAB"            # 10
        b"\xE8\xAB\xAB\xAB\xAB"            # 15
        b"\x83\xC4\xAB"                    # 20
    )
    resMgrOffset = 1
    CResMgrOffset = (6, False)
    atExitOffset = 16
    offset = self.exe.codeWildcard(code,
                                   b"\xAB",
                                   self.gResMgr,
                                   self.gResMgr + 0x50)
    if offset is False:
        # 0  mov ecx, offset m_resMgr
        # 5  or dl, al
        # 7  mov byte_80F354, dl
        # 13 call CResMgr_CResMgr
        # 18 push offset unknown_libname_44
        # 23 call _atexit
        # 28 add esp, 4
        code = (
            b"\xB9\xAB\xAB\xAB\xAB"            # 0
            b"\x0A\xD0"                        # 5
            b"\x88\x15\xAB\xAB\xAB\xAB"        # 7
            b"\xE8\xAB\xAB\xAB\xAB"            # 13
            b"\x68\xAB\xAB\xAB\xAB"            # 18
            b"\xE8\xAB\xAB\xAB\xAB"            # 23
            b"\x83\xC4\xAB"                    # 28
        )
        resMgrOffset = 1
        CResMgrOffset = (14, False)
        atExitOffset = 24
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.gResMgr,
                                       self.gResMgr + 0x50)
    if offset is False:
        # 0  mov ecx, offset m_resMgr
        # 5  mov [esp+10h+var_4], 0
        # 13 call CResMgr_CResMgr
        # 18 push offset sub_6D61D0
        # 23 call _atexit
        # 28 add esp, 4
        code = (
            b"\xB9\xAB\xAB\xAB\xAB"            # 0
            b"\xC7\x44\x24\xAB\x00\x00\x00\x00"  # 5
            b"\xE8\xAB\xAB\xAB\xAB"            # 13
            b"\x68\xAB\xAB\xAB\xAB"            # 18
            b"\xE8\xAB\xAB\xAB\xAB"            # 23
            b"\x83\xC4\xAB"                    # 28
        )
        resMgrOffset = 1
        CResMgrOffset = (14, False)
        atExitOffset = 24
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.gResMgr,
                                       self.gResMgr + 0x50)

    if offset is False:
        # 0  ret retn
        # 1  push offset m_resMgr_isInit
        # 6  call sub_ABAD2B
        # 11 add esp, 4
        # 14 cmp m_resMgr_isInit, 0FFFFFFFFh
        # 21 jnz short loc_8270CF
        # 23 mov [ebp+var_4], 0
        # 30 mov ecx, offset m_resMgr
        # 35 mov [ebp+var_10], offset m_resMgr
        code = (
            b"\xC3"                            # 0 ret retn
            b"\x68\xAB\xAB\xAB\xAB"            # 1 push offset m_resMgr_isInit
            b"\xE8\xAB\xAB\xAB\xAB"            # 6 call sub_ABAD2B
            b"\x83\xC4\x04"                    # 11 add esp, 4
            b"\x83\x3D\xAB\xAB\xAB\xAB\xFF"    # 14 cmp m_resMgr_isInit, 0FFFFF
            b"\x75\xAB"                        # 21 jnz short loc_8270CF
            b"\xC7\x45\xAB\x00\x00\x00\x00"    # 23 mov [ebp+var_4], 0
            b"\xB9\xAB\xAB\xAB\xAB"            # 30 mov ecx, offset m_resMgr
            b"\xC7\x45"                        # 35 mov [ebp+var_10], offset m_
        )
        resMgrOffset = 31
        CResMgrOffset = (35, True)
        atExitOffset = 0
        offset = self.exe.codeWildcard(code,
                                       b"\xAB",
                                       self.gResMgr,
                                       self.gResMgr + 0x80)


    if offset is False:
        self.log("failed in search m_resMgr.")
        if errorExit is True:
            exit(1)
        return
    self.mResMgr = self.exe.read(offset + resMgrOffset, 4, "V")
    if CResMgrOffset[1] is True:
        self.CResMgr_CResMgr = offset + CResMgrOffset[0]
    else:
        self.CResMgr_CResMgr = self.getAddr(offset,
                                            CResMgrOffset[0],
                                            CResMgrOffset[0] + 4)
    if atExitOffset != 0:
        self.atexit = self.getAddr(offset,
                                   atExitOffset,
                                   atExitOffset + 4)
        self.addRawFunc("atexit", self.atexit)
    self.addVaVar("m_resMgr", self.mResMgr)
    self.addRawFunc("CResMgr::CResMgr", self.CResMgr_CResMgr)
