#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchOEP(self):
    # call main
    # jmp $+5
    # push 14h
    # push offset addr1
    code = (
        b"\xE8\xAB\xAB\x00\x00" +
        b"\xE9\x00\x00\x00\x00" +
        b"\x6A\x14" +
        b"\x68")
    offset = self.exe.codeWildcard(code, b"\xAB")
    if offset is False:
        self.log("failed in search OEP")
        return
    self.OEPAddr = offset
    self.addRawFunc("OEP", self.OEPAddr)
