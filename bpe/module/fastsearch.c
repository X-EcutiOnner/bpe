#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <Python.h>

PyObject *python_matchWildcard(PyObject *self, PyObject *args);
PyObject *python_matchWildcardOffset(PyObject *self, PyObject *args);
PyObject *python_match(PyObject *self, PyObject *args);
PyObject *python_version(PyObject *self __attribute__ ((unused)), PyObject *args __attribute__ ((unused)));
PyObject *python_setRange(PyObject *self, PyObject *args);
PyObject *python_getNextPossibleVtbl(PyObject *self, PyObject *args);
PyObject *python_setExe(PyObject *self, PyObject *args);
PyObject *python_readUInt(PyObject *self, PyObject *args);
PyObject *python_getAddr(PyObject *self __attribute__ ((unused)), PyObject *args);

static PyMethodDef funcs[] =
{
    {
        "version",
        (PyCFunction)python_version,
        METH_NOARGS,
        "return version"
    },
    {
        "matchWildcard",
        (PyCFunction)python_matchWildcard,
        METH_VARARGS,
        "match wildcard pattern"
    },
    {
        "matchWildcardOffset",
        (PyCFunction)python_matchWildcardOffset,
        METH_VARARGS,
        "match wildcard pattern on offset"
    },
    {
        "match",
        (PyCFunction)python_match,
        METH_VARARGS,
        "match non wildcard pattern"
    },
    {
        "setRange",
        (PyCFunction)python_setRange,
        METH_VARARGS,
        "set section range for future search"
    },
    {
        "setExe",
        (PyCFunction)python_setExe,
        METH_VARARGS,
        "set exe data"
    },
    {
        "readUInt",
        (PyCFunction)python_readUInt,
        METH_VARARGS,
        "read uint32_t from exe by given offset"
    },
    {
        "getAddr",
        (PyCFunction)python_getAddr,
        METH_VARARGS,
        "read relative address from memory and add offset"
    },
    {
        "getNextPossibleVtbl",
        (PyCFunction)python_getNextPossibleVtbl,
        METH_VARARGS,
        "search for next possible vtbl start"
    }
};

static unsigned int rdataStartV = 0;
static unsigned int rdataFinishV = 0;
static unsigned int rdataStartR = 0;
static unsigned int rdataFinishR = 0;
static unsigned int rdataRawOffset = 0;
static unsigned int textStart = 0;
static unsigned int textFinish = 0;
static char *exe = NULL;
static uint32_t exeLen = 0;

void initfastsearch(void)
{
    Py_InitModule3("fastsearch", funcs, "");
}

PyObject *python_version(PyObject *self __attribute__ ((unused)), PyObject *args __attribute__ ((unused)))
{
    return PyUnicode_FromFormat("00000008");
}

PyObject *python_matchWildcard(PyObject *self __attribute__ ((unused)), PyObject *args)
{
    char *pattern = NULL;
    int patternLen = 0;
    char *wildcard = NULL;
    int wildcardLen = 0;
    int start = 0;
    int finish = 0;

    if (!PyArg_ParseTuple(args, "s#s#ii", &pattern, &patternLen, &wildcard, &wildcardLen, &start, &finish))
        return NULL;

    const int diff = exeLen - patternLen + 1;
    if (finish > diff)
        finish = diff;
    if (start >= finish)
        Py_RETURN_FALSE;

    int fpos = start;
    const char *ptr = NULL;
    const char *const startPtr = &exe[start];

    ptr = memchr(exe + fpos, pattern[0], finish - fpos);
    if (ptr == NULL)
        Py_RETURN_FALSE;
    fpos = ptr - startPtr + start;
//    printf("before fpos=%d\n", fpos);
    while (ptr != NULL)
    {
        int ppos = 0;
        int failed = 0;
        for (ppos = 1; ppos < patternLen; ppos ++)
        {
            if (exe[fpos + ppos] != pattern[ppos] && pattern[ppos] != '\xAB')
            {
                failed = 1;
                break;
            }
        }
        if (failed == 0)
        {
            return Py_BuildValue("i", fpos);
        }
        ptr = memchr(exe + fpos + 1, pattern[0], finish - fpos - 1);
        if (ptr == NULL)
            Py_RETURN_FALSE;
        fpos = ptr - startPtr + start;
//        printf("in fpos=%d\n", fpos);
    }
    Py_RETURN_FALSE;
}

PyObject *python_matchWildcardOffset(PyObject *self __attribute__ ((unused)), PyObject *args)
{
    char *pattern = NULL;
    unsigned int patternLen = 0;
    char *wildcard = NULL;
    int wildcardLen = 0;
    int start = 0;

    if (!PyArg_ParseTuple(args, "s#s#i", &pattern, &patternLen, &wildcard, &wildcardLen, &start))
        return NULL;

    if (start + patternLen > exeLen)
    {
        Py_RETURN_FALSE;
    }

    unsigned int ppos = 0;
    for (ppos = 0; ppos < patternLen; ppos ++)
    {
        if (exe[start + ppos] != pattern[ppos] && pattern[ppos] != '\xAB')
        {
            Py_RETURN_FALSE;
        }
    }
    Py_RETURN_TRUE;
}

PyObject *python_match(PyObject *self __attribute__ ((unused)), PyObject *args)
{
    char *pattern = NULL;
    int patternLen = 0;
    int start = 0;
    int finish = 0;

    if (!PyArg_ParseTuple(args, "s#ii", &pattern, &patternLen, &start, &finish))
        return NULL;

    finish = finish + patternLen - 1;
    if (finish > (int)exeLen)
        finish = exeLen;
    if (start >= finish)
        Py_RETURN_FALSE;

    const char *const ptr = memmem(exe + start, finish - start, pattern, patternLen);
    if (ptr == NULL)
        Py_RETURN_FALSE;

    return Py_BuildValue("i", ptr - exe);
}

PyObject *python_setRange(PyObject *self __attribute__ ((unused)), PyObject *args)
{
    if (!PyArg_ParseTuple(args, "iiiii", &rdataStartV, &rdataFinishV, &textStart, &textFinish, &rdataRawOffset))
        return NULL;
    rdataFinishV -= 4;
    rdataStartR = rdataStartV + rdataRawOffset;
    rdataFinishR = rdataFinishV + rdataRawOffset;
    return Py_BuildValue("i", 0);
}

PyObject *python_setExe(PyObject *self __attribute__ ((unused)), PyObject *args)
{
    if (!PyArg_ParseTuple(args, "s#", &exe, &exeLen))
    {
        exe = NULL;
        exeLen = 0;
        return NULL;
    }
    return Py_BuildValue("i", 0);
}

PyObject *python_getNextPossibleVtbl(PyObject *self __attribute__ ((unused)), PyObject *args)
{
    uint32_t offset = 0;
    uint32_t f;

    if (!PyArg_ParseTuple(args, "i", &offset))
        return NULL;

    if (offset >= exeLen || offset < rdataStartR || offset >= rdataFinishR)
        return Py_BuildValue("i", -1);
    for (f = offset; f < rdataFinishR; f += 4)
    {
        const uint32_t vtblRttiV = *(uint32_t*)(&exe[f]);
        if (vtblRttiV < rdataStartV || vtblRttiV > rdataFinishV)
            continue;
        const uint32_t vtblRttiR = vtblRttiV + rdataRawOffset;
        const uint32_t locator_signature = *(uint32_t*)(&exe[vtblRttiR]);
        if (locator_signature != 0)
            continue;
        const uint32_t vtbl0 = *(uint32_t*)(&exe[f + 4]);
        if (vtbl0 < textStart || vtbl0 > textFinish)
            continue;
        return Py_BuildValue("i", f);
    }
    return Py_BuildValue("i", -1);
}

PyObject *python_readUInt(PyObject *self __attribute__ ((unused)), PyObject *args)
{
    uint32_t offset = 0;

    if (!PyArg_ParseTuple(args, "I", &offset))
        return NULL;
    if (offset >= exeLen || offset + 4 >= exeLen)
        Py_RETURN_FALSE;
    return Py_BuildValue("I", *(uint32_t*)(&exe[offset]));
}

PyObject *python_getAddr(PyObject *self __attribute__ ((unused)), PyObject *args)
{
    uint32_t offset = 0;
    uint32_t addrOffset = 0;
    uint32_t instructionOffset = 0;
    if (!PyArg_ParseTuple(args, "III", &offset, &addrOffset, &instructionOffset))
        return NULL;
    uint32_t offset1 = offset + addrOffset;
    if (offset1 >= exeLen || offset1 + 4 >= exeLen)
        Py_RETURN_FALSE;
    return Py_BuildValue("i", offset + instructionOffset + *(uint32_t*)(&exe[offset1]));
}
