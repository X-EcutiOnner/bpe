#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bpe.blocks.blocks import matchRelativeBlock

try:
    xrange
except NameError:
    xrange = range

debug = False
defaultId = 0x100000000000


def readSwitchVar(self, offset, bvar):
    if type(bvar) is tuple:
        sz = bvar[1]
        var = bvar[0]
        if sz is False:
            val = self.getAddr(offset,
                               var,
                               var + 4) - self.exe.codeSection.rawVaDiff
        elif sz == 1:
            val = self.exe.readByte(offset + var)
            if len(bvar) > 2:
                if bvar[2] is False:
                    val = val + offset + var + 1
                    val, _ = self.exe.rawToVaUnknown(val)
        elif sz == 2:
            val = self.exe.readWord(offset + var)
        elif sz == 4:
            val = self.exe.readInt(offset + var)
        else:
            self.log("Error: unknown main var type size: {0}".
                     format(sz))
            exit(1)
    else:
        val = self.exe.readInt(offset + bvar)
    return val


def searchRelativeBlock(self, blocks, offset, offset2):
    data = matchRelativeBlock(self, blocks, offset, offset2)
    if len(data) == 0:
        return False
    block = data[0]
    arr = dict()
    offset = block[0]
    blockVars = block[1][1]
    for name in blockVars:
        val = readSwitchVar(self, offset, blockVars[name])
        arr[name] = val
    blockVars = block[1][2]
    arr2 = dict()
    for name in blockVars:
        arr2[name] = blockVars[name]
    return (offset, arr, arr2)


def updateSwitchRange(self, ranges, limits):
    start = ranges[0]
    end = ranges[1]
    if start < limits[0]:
        start = limits[0]
    if end > limits[1]:
        end = limits[1]
    if start > end:
        self.log("Error: found wrong switch range: "
                 "({0}, {1})".format(start, end))
        return ranges
    if debug is True:
        print("updateSwitchRange {0} by {1} = {2}".format(ranges,
                                                          limits,
                                                          (start, end)))
    return (start, end)


def printSwitchValue(self, text, addr):
    print("{0}: {1} (hex: 0x{2:X})".format(text, addr, addr))


def printSwitchRange(self, text, addr):
    if addr[0] != float("inf") and addr[0] != -float("inf"):
        addr0Str = "{0} (hex: 0x{1:X})".format(addr[0], addr[0])
    else:
        addr0Str = "{0}".format(addr[0])
    if addr[1] != float("inf") and addr[1] != -float("inf"):
        addr1Str = "{0} (hex: 0x{1:X})".format(addr[1], addr[1])
    else:
        addr1Str = "{0}".format(addr[1])
    print("{0}: ({1}, {2})".format(text, addr0Str, addr1Str))


def checkSwitchChains(self, offset, chains, method):
    arr = set()
    offset, _ = self.exe.rawToVaUnknown(offset)
    for chain in chains:
        val = chain[0]
        if val in arr:
            val, _ = self.exe.rawToVaUnknown(val)
            self.log("Warning: found same switch chains with "
                     "0x{0:X}:{1}: 0x{2:X}".format(offset, method, val))
        arr.add(val)


def parseSwitch(self, data, limits, inChain):
    if data is False:
        return False
    offset = data[0]
    vars1 = data[1]
    vars2 = data[2]
    method = vars2["method"]
    ret = dict()
    chains = []
    if "default" in vars1:
        ret[defaultId] = vars1["default"]
    if "switch" in vars1:
        switch, _ = self.exe.vaToRawUnknown(vars1["switch"])
        switchStep = vars2["switchStep"]
    if debug is True:
        val, _ = self.exe.rawToVaUnknown(offset)
        print("parseSwitch")
        print("method: {0}".format(method))
        printSwitchValue(self, "offset", val)
        printSwitchRange(self, "limits", limits)
        if defaultId in vars1:
            printSwitchValue(self, "default", vars1["default"])

    if inChain is True and limits[0] == limits[1]:
        valueRange = (0, 0)
        switchRange = (0, 0)
        # probably need use retOffset?
        val, _ = self.exe.rawToVaUnknown(offset)
        ret[limits[0]] = val
    elif method == 1:
        # check limit and use one table (pointer)
        # value range: -add to cmp - add
        # switch range: 0 to cmp
        add = vars1["add"]
        compare = vars1["cmp"]
        if debug is True:
            printSwitchValue(self, "compare", compare)
        if add > 0:
            self.log("Error: unknown add value in switch method: "
                     "{0}".format(method))
            exit(1)
        switchRange = updateSwitchRange(self,
                                        (0, compare),
                                        (limits[0] + add, limits[1] + add))
        valueRange = (switchRange[0] - add, switchRange[1] - add)
        for addr in xrange(0, compare + 1):
            num = addr - add
            ret[num] = self.exe.readSizeU(switch + addr * switchStep,
                                          switchStep)
    elif method == 2:
        # check limit and use two tables (byte and pointer)
        # value1 range: -add to cmp - add
        # switch1 range: 0 to cmp
        switch1, _ = self.exe.vaToRawUnknown(vars1["switch1"])
        switch1Step = vars2["switch1Step"]
        add = vars1["add"]
        compare = vars1["cmp"]
        if debug is True:
            printSwitchValue(self, "compare", compare)
        if add > 0:
            self.log("Error: unknown add value in switch method: "
                     "{0}".format(method))
            exit(1)
        switchRange = updateSwitchRange(self,
                                        (0, compare),
                                        (limits[0] + add, limits[1] + add))
        valueRange = (switchRange[0] - add, switchRange[1] - add)
        for addr in xrange(switchRange[0], switchRange[1] + 1):
            num = addr - add
            addr1 = self.exe.readSizeU(switch1 + addr * switch1Step,
                                       switch1Step)
            ret[num] = self.exe.readSizeU(switch + addr1 * switchStep,
                                          switchStep)
    elif method == 3:
        # chain
        # check limit twice, use sub and use two tables (byte and pointer)
        # value1 range: (-sub, compare2 - sub)
        # swutch1 range: (0, cmp2)
        switch1, _ = self.exe.vaToRawUnknown(vars1["switch1"])
        switch1Step = vars2["switch1Step"]
        sub = vars1["sub"]
        compare2 = vars1["cmp2"]
        if debug is True:
            printSwitchValue(self, "compare2", compare2)
        if sub < 0:
            self.log("Error: unknown sub value in switch method: "
                     "{0}".format(method))
            exit(1)
        switchRange = updateSwitchRange(self,
                                        (0, compare2),
                                        (limits[0] - sub, limits[1] - sub))
        if debug is True:
            printSwitchRange(self, "switchRange", switchRange)
        valueRange = (switchRange[0] + sub, switchRange[1] + sub)
        for addr in xrange(switchRange[0], switchRange[1] + 1):
            num = addr + sub
            addr1 = self.exe.readSizeU(switch1 + addr * switch1Step,
                                       switch1Step)
            ret[num] = self.exe.readSizeU(switch + addr1 * switchStep,
                                          switchStep)
#            print "num={0}, addr1={1:X} ret={2:X}".format(num,
#                                                          addr1,
#                                                          ret[num])
        compare = vars1["cmp1"]
        if debug is True:
            printSwitchValue(self, "compare", compare)
        switchRange = updateSwitchRange(self,
                                        (compare + 1, float("inf")),
                                        limits)
        jg, _ = self.exe.vaToRawUnknown(vars1["jg"])
        if debug is True:
            printSwitchValue(self, "jg", vars1["jg"])
            printSwitchRange(self, "jg range", switchRange)
        chains.append((jg, switchRange))
        switchRange = updateSwitchRange(self, (compare, compare), limits)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz"])
        if debug is True:
            printSwitchValue(self, "jz", vars1["jz"])
            printSwitchRange(self, "jz range", switchRange)
        chains.append((jz, switchRange))
    elif method == 4:
        # chain
        # check limit and use one table (pointer)
        # value range: -add to cmp - add
        # switch range: 0 to cmp
        add = vars1["add"]
        compare2 = vars1["cmp2"]
        if debug is True:
            printSwitchValue(self, "compare2", compare2)
        if add > 0:
            self.log("Error: unknown add value in switch method: "
                     "{0}".format(method))
            exit(1)
        switchRange = updateSwitchRange(self,
                                        (0, compare2),
                                        (limits[0] + add, limits[1] + add))
        valueRange = (switchRange[0] - add, switchRange[1] - add)
        for addr in xrange(switchRange[0], switchRange[1] + 1):
            num = addr - add
            ret[num] = self.exe.readSizeU(switch + addr * switchStep,
                                          switchStep)
#            print "num={0}, addr1={1:X} ret={2:X}".format(num,
#                                                          addr,
#                                                          ret[num])
        compare = vars1["cmp1"]
        if debug is True:
            printSwitchValue(self, "compare", compare)
        switchRange = updateSwitchRange(self,
                                        (compare + 1, float("inf")),
                                        limits)
        jg, _ = self.exe.vaToRawUnknown(vars1["jg"])
        if debug is True:
            printSwitchValue(self, "jg", vars1["jg"])
            printSwitchRange(self, "jg range", switchRange)
        chains.append((jg, switchRange))
        switchRange = updateSwitchRange(self, (compare, compare), limits)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz"])
        if debug is True:
            printSwitchValue(self, "jz", vars1["jz"])
            printSwitchRange(self, "jz range", switchRange)
        chains.append((jz, switchRange))
    elif method == 5:
        # chain
        # check limit once, use sub and use two tables (byte and pointer)
        # value1 range: (-sub, compare - sub)
        # swutch1 range: (0, cmp)
        switch1, _ = self.exe.vaToRawUnknown(vars1["switch1"])
        switch1Step = vars2["switch1Step"]
        sub = vars1["sub"]
        if sub < 0:
            self.log("Error: unknown sub value in switch method: "
                     "{0}".format(method))
            exit(1)
        compare = vars1["cmp"]
        if debug is True:
            printSwitchValue(self, "compare", compare)
        switchRange = updateSwitchRange(self,
                                        (0, compare),
                                        (limits[0] - sub, limits[1] - sub))
        if debug is True:
            printSwitchRange(self, "switchRange", switchRange)
        valueRange = (switchRange[0] + sub, switchRange[1] + sub)
        for addr in xrange(switchRange[0], switchRange[1] + 1):
            num = addr + sub
            addr1 = self.exe.readSizeU(switch1 + addr * switch1Step,
                                       switch1Step)
            ret[num] = self.exe.readSizeU(switch + addr1 * switchStep,
                                          switchStep)
#            print "num={0}, addr1={1:X} ret={2:X}".format(num,
#                                                          addr1,
#                                                          ret[num])
    elif method == 6:
        # chain
        # check limit twice, use sub and use two tables (byte and pointer)
        # value1 range: (-sub, compare2 - sub)
        # swutch1 range: (0, cmp2)
        sub = vars1["sub"]
        if sub < 0:
            self.log("Error: unknown sub value in switch method: "
                     "{0}".format(method))
            exit(1)
        compare = vars1["cmp"]
        if debug is True:
            printSwitchValue(self, "compare", compare)
        switchRange = updateSwitchRange(self,
                                        (0, compare),
                                        (limits[0] - sub, limits[1] - sub))
        if debug is True:
            printSwitchRange(self, "switchRange", switchRange)
        valueRange = (switchRange[0] + sub, switchRange[1] + sub)
        for addr in xrange(switchRange[0], switchRange[1] + 1):
            num = addr + sub
            ret[num] = self.exe.readSizeU(switch + addr * switchStep,
                                          switchStep)
#            print "num={0}, ret={1:X}".format(num, ret[num])
    elif method == 7:
        valueRange = (0, 0)
        switchRange = (0, 0)
        compare = vars1["cmp"]
        if debug is True:
            printSwitchValue(self, "compare", compare)
        switchRange = updateSwitchRange(self,
                                        (compare + 1, float("inf")),
                                        limits)
        jg, _ = self.exe.vaToRawUnknown(vars1["jg"])
        if debug is True:
            printSwitchValue(self, "jg", vars1["jg"])
            printSwitchRange(self, "jg range", switchRange)
        chains.append((jg, switchRange))
        switchRange = updateSwitchRange(self, (compare, compare), limits)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz"])
        if debug is True:
            printSwitchValue(self, "jz", vars1["jz"])
            printSwitchRange(self, "jz range", switchRange)
        chains.append((jz, switchRange))
        val = vars2["next"]
        if val is not False:
            # add next block
            limits = updateSwitchRange(self, (0, compare - 1), limits)
            nextOffset = offset + val
            chains.append((nextOffset, limits))
    elif method == 8:
        valueRange = (0, 0)
        switchRange = (0, 0)
        sub1 = vars1["sub1"]
        if sub1 < 0:
            self.log("Error: unknown sub1 value in switch method: "
                     "{0}".format(method))
            exit(1)
        sub2 = vars1["sub2"]
        if sub2 < 0:
            self.log("Error: unknown sub2 value in switch method: "
                     "{0}".format(method))
            exit(1)
        if debug is True:
            printSwitchValue(self, "sub1", sub1)
            printSwitchValue(self, "sub2", sub2)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz1"])
        switchRange = updateSwitchRange(self, (sub1, sub1), limits)
        if debug is True:
            printSwitchValue(self, "jz1", vars1["jz1"])
            printSwitchRange(self, "jz1 range", switchRange)
        chains.append((jz, switchRange))
        jz, _ = self.exe.vaToRawUnknown(vars1["jz2"])
        switchRange = updateSwitchRange(self,
                                        (sub1 + sub2, sub1 + sub2),
                                        limits)
        if debug is True:
            printSwitchValue(self, "jz2", vars1["jz2"])
            printSwitchRange(self, "jz2 range", switchRange)
        chains.append((jz, switchRange))
        val = vars2["next"]
        if val is not False:
            # add next block
            val, _ = self.exe.rawToVaUnknown(offset + val)
            ret[defaultId] = val
    elif method == 9:
        valueRange = (0, 0)
        switchRange = (0, 0)
        sub1 = vars1["sub1"]
        if sub1 < 0:
            self.log("Error: unknown sub1 value in switch method: "
                     "{0}".format(method))
            exit(1)
        sub2 = vars1["sub2"]
        if sub2 < 0:
            self.log("Error: unknown sub2 value in switch method: "
                     "{0}".format(method))
            exit(1)
        sub3 = vars1["sub3"]
        if sub3 < 0:
            self.log("Error: unknown sub3 value in switch method: "
                     "{0}".format(method))
            exit(1)
        if debug is True:
            printSwitchValue(self, "sub1", sub1)
            printSwitchValue(self, "sub2", sub2)
            printSwitchValue(self, "sub3", sub3)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz1"])
        switchRange = updateSwitchRange(self, (sub1, sub1), limits)
        if debug is True:
            printSwitchValue(self, "jz1", vars1["jz1"])
            printSwitchRange(self, "jz1 range", switchRange)
        chains.append((jz, switchRange))
        jz, _ = self.exe.vaToRawUnknown(vars1["jz2"])
        switchRange = updateSwitchRange(self,
                                        (sub1 + sub2, sub1 + sub2),
                                        limits)
        if debug is True:
            printSwitchValue(self, "jz2", vars1["jz2"])
            printSwitchRange(self, "jz2 range", switchRange)
        chains.append((jz, switchRange))

        jz, _ = self.exe.vaToRawUnknown(vars1["jz3"])
        switchRange = updateSwitchRange(self,
                                        (sub1 + sub2 + sub3,
                                         sub1 + sub2 + sub3),
                                        limits)
        if debug is True:
            printSwitchValue(self, "jz3", vars1["jz3"])
            printSwitchRange(self, "jz3 range", switchRange)
        chains.append((jz, switchRange))
        val = vars2["next"]
        if val is not False:
            # add next block
            val, _ = self.exe.rawToVaUnknown(offset + val)
            ret[defaultId] = val
    elif method == 10:
        valueRange = (0, 0)
        switchRange = (0, 0)
        compare1 = vars1["cmp1"]
        compare2 = vars1["cmp2"]
        compare3 = vars1["cmp3"]
        if debug is True:
            printSwitchValue(self, "compare1", compare1)
            printSwitchValue(self, "compare2", compare2)
            printSwitchValue(self, "compare3", compare3)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz1"])
        switchRange = updateSwitchRange(self, (compare1, compare1), limits)
        if debug is True:
            printSwitchValue(self, "jz1", vars1["jz1"])
            printSwitchRange(self, "jz1 range", switchRange)
        chains.append((jz, switchRange))
        jz, _ = self.exe.vaToRawUnknown(vars1["jz2"])
        switchRange = updateSwitchRange(self, (compare2, compare2), limits)
        if debug is True:
            printSwitchValue(self, "jz2", vars1["jz2"])
            printSwitchRange(self, "jz2 range", switchRange)
        chains.append((jz, switchRange))
        jz, _ = self.exe.vaToRawUnknown(vars1["jz3"])
        switchRange = updateSwitchRange(self, (compare3, compare3), limits)
        if debug is True:
            printSwitchValue(self, "jz3", vars1["jz3"])
            printSwitchRange(self, "jz3 range", switchRange)
        chains.append((jz, switchRange))
        val = vars2["next"]
        if val is not False:
            # add next block
            val, _ = self.exe.rawToVaUnknown(offset + val)
            ret[defaultId] = val
    elif method == 11:
        valueRange = (0, 0)
        switchRange = (0, 0)
        sub1 = vars1["sub1"]
        if sub1 < 0:
            self.log("Error: unknown sub1 value in switch method: "
                     "{0}".format(method))
            exit(1)
        # sub3 is reallty sub2
        sub2 = 1
        sub3 = vars1["sub2"]
        if sub2 < 0:
            self.log("Error: unknown sub2 value in switch method: "
                     "{0}".format(method))
            exit(1)
        if debug is True:
            printSwitchValue(self, "sub1", sub1)
            printSwitchValue(self, "sub2", sub3)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz1"])
        switchRange = updateSwitchRange(self, (sub1, sub1), limits)
        if debug is True:
            printSwitchValue(self, "jz1", vars1["jz1"])
            printSwitchRange(self, "jz1 range", switchRange)
        chains.append((jz, switchRange))
        jz, _ = self.exe.vaToRawUnknown(vars1["jz2"])
        switchRange = updateSwitchRange(self,
                                        (sub1 + sub2, sub1 + sub2),
                                        limits)
        if debug is True:
            printSwitchValue(self, "jz2", vars1["jz2"])
            printSwitchRange(self, "jz2 range", switchRange)
        chains.append((jz, switchRange))

        jz, _ = self.exe.vaToRawUnknown(vars1["jz3"])
        switchRange = updateSwitchRange(self,
                                        (sub1 + sub2 + sub3,
                                         sub1 + sub2 + sub3),
                                        limits)
        if debug is True:
            printSwitchValue(self, "jz3", vars1["jz3"])
            printSwitchRange(self, "jz3 range", switchRange)
        chains.append((jz, switchRange))
        val = vars2["next"]
        if val is not False:
            # add next block
            val, _ = self.exe.rawToVaUnknown(offset + val)
            ret[defaultId] = val
    elif method == 12:
        valueRange = (0, 0)
        switchRange = (0, 0)
        compare = vars1["cmp"]
        if debug is True:
            printSwitchValue(self, "compare", compare)
        jnz, _ = self.exe.vaToRawUnknown(vars1["jnz"])
        if debug is True:
            printSwitchValue(self, "jnz", vars1["jnz"])
        # cant exclude one value
        chains.append((jnz, limits))
        val, _ = self.exe.rawToVaUnknown(offset + vars2["next"])
        ret[compare] = val
    elif method == 13:
        valueRange = (0, 0)
        switchRange = (0, 0)
        compare1 = vars1["cmp1"]
        compare2 = vars1["cmp2"]
        if debug is True:
            printSwitchValue(self, "compare1", compare1)
            printSwitchValue(self, "compare2", compare2)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz"])
        switchRange = updateSwitchRange(self, (compare1, compare1), limits)
        if debug is True:
            printSwitchValue(self, "jz", vars1["jz"])
            printSwitchRange(self, "jz range", switchRange)
        chains.append((jz, switchRange))
        jnz, _ = self.exe.vaToRawUnknown(vars1["jnz"])
        if debug is True:
            printSwitchValue(self, "jnz", vars1["jnz"])
        # cant exclude one value
        chains.append((jnz, limits))
        val, _ = self.exe.rawToVaUnknown(offset + vars2["next"])
        ret[compare2] = val
    elif method == 14:
        valueRange = (0, 0)
        switchRange = (0, 0)
        sub1 = vars1["sub1"]
        if sub1 < 0:
            self.log("Error: unknown sub1 value in switch method: "
                     "{0}".format(method))
            exit(1)
        # sub3 is reallty sub2
        sub2 = 1
        sub3 = vars1["sub2"]
        if sub2 < 0:
            self.log("Error: unknown sub2 value in switch method: "
                     "{0}".format(method))
            exit(1)
        if debug is True:
            printSwitchValue(self, "sub1", sub1)
            printSwitchValue(self, "sub2", sub3)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz"])
        switchRange = updateSwitchRange(self, (sub1, sub1), limits)
        if debug is True:
            printSwitchValue(self, "jz", vars1["jz"])
            printSwitchRange(self, "jz range", switchRange)
        chains.append((jz, switchRange))
        jnz, _ = self.exe.vaToRawUnknown(vars1["jnz"])
        if debug is True:
            printSwitchValue(self, "jnz", vars1["jnz"])
        # cant exclude one value
        chains.append((jnz, limits))
        val, _ = self.exe.rawToVaUnknown(offset + vars2["next"])
        ret[sub1 + sub2] = val
    elif method == 15:
        valueRange = (0, 0)
        switchRange = (0, 0)
        sub1 = vars1["sub1"]
        if sub1 < 0:
            self.log("Error: unknown sub1 value in switch method: "
                     "{0}".format(method))
            exit(1)
        sub2 = vars1["sub2"]
        if sub2 < 0:
            self.log("Error: unknown sub2 value in switch method: "
                     "{0}".format(method))
            exit(1)
        sub3 = vars1["sub2"]
        if sub2 < 0:
            self.log("Error: unknown sub2 value in switch method: "
                     "{0}".format(method))
            exit(1)
        if debug is True:
            printSwitchValue(self, "sub1", sub1)
            printSwitchValue(self, "sub2", sub2)
            printSwitchValue(self, "sub3", sub3)

        jz, _ = self.exe.vaToRawUnknown(vars1["jz1"])
        switchRange = updateSwitchRange(self, (sub1, sub1), limits)
        if debug is True:
            printSwitchValue(self, "jz1", vars1["jz1"])
            printSwitchRange(self, "jz1 range", switchRange)
        chains.append((jz, switchRange))
        jz, _ = self.exe.vaToRawUnknown(vars1["jz2"])
        switchRange = updateSwitchRange(self,
                                        (sub1 + sub2, sub1 + sub2),
                                        limits)
        if debug is True:
            printSwitchValue(self, "jz2", vars1["jz2"])
            printSwitchRange(self, "jz2 range", switchRange)
        chains.append((jz, switchRange))

        jnz, _ = self.exe.vaToRawUnknown(vars1["jnz"])
        if debug is True:
            printSwitchValue(self, "jnz", vars1["jnz"])
        # cant exclude two value
        chains.append((jnz, limits))
        val, _ = self.exe.rawToVaUnknown(offset + vars2["next"])
        ret[sub1 + sub2] = val
    elif method == 16:
        valueRange = (0, 0)
        switchRange = (0, 0)
        compare = vars1["cmp"]
        if debug is True:
            printSwitchValue(self, "compare", compare)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz"])
        switchRange = updateSwitchRange(self, (compare, compare), limits)
        if debug is True:
            printSwitchValue(self, "jz", vars1["jz"])
            printSwitchRange(self, "jz range", switchRange)
        chains.append((jz, switchRange))
        val = vars2["next"]
        if val is not False:
            chains.append((offset + val, limits))
    elif method == 17:
        valueRange = (0, 0)
        switchRange = (0, 0)
        compare1 = vars1["cmp1"]
        compare2 = vars1["cmp2"]
        if debug is True:
            printSwitchValue(self, "compare1", compare1)
            printSwitchValue(self, "compare2", compare2)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz1"])
        switchRange = updateSwitchRange(self, (compare1, compare1), limits)
        if debug is True:
            printSwitchValue(self, "jz1", vars1["jz1"])
            printSwitchRange(self, "jz1 range", switchRange)
        chains.append((jz, switchRange))
        jz, _ = self.exe.vaToRawUnknown(vars1["jz2"])
        switchRange = updateSwitchRange(self, (compare2, compare2), limits)
        if debug is True:
            printSwitchValue(self, "jz2", vars1["jz2"])
            printSwitchRange(self, "jz2 range", switchRange)
        chains.append((jz, switchRange))
        val = vars2["next"]
        if val is not False:
            chains.append((offset + val, limits))
    elif method == 18:
        valueRange = (0, 0)
        switchRange = (0, 0)
        sub1 = vars1["sub1"]
        if sub1 < 0:
            self.log("Error: unknown sub1 value in switch method: "
                     "{0}".format(method))
            exit(1)
        sub2 = vars1["sub2"]
        if sub2 < 0:
            self.log("Error: unknown sub2 value in switch method: "
                     "{0}".format(method))
            exit(1)
        sub3 = 1
        if debug is True:
            printSwitchValue(self, "sub1", sub1)
            printSwitchValue(self, "sub2", sub2)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz1"])
        switchRange = updateSwitchRange(self, (sub1, sub1), limits)
        if debug is True:
            printSwitchValue(self, "jz1", vars1["jz1"])
            printSwitchRange(self, "jz1 range", switchRange)
        chains.append((jz, switchRange))
        jz, _ = self.exe.vaToRawUnknown(vars1["jz2"])
        switchRange = updateSwitchRange(self,
                                        (sub1 + sub2, sub1 + sub2),
                                        limits)
        if debug is True:
            printSwitchValue(self, "jz2", vars1["jz2"])
            printSwitchRange(self, "jz2 range", switchRange)
        chains.append((jz, switchRange))

        jz, _ = self.exe.vaToRawUnknown(vars1["jz3"])
        switchRange = updateSwitchRange(self,
                                        (sub1 + sub2 + sub3,
                                         sub1 + sub2 + sub3),
                                        limits)
        if debug is True:
            printSwitchValue(self, "jz3", vars1["jz3"])
            printSwitchRange(self, "jz3 range", switchRange)
        chains.append((jz, switchRange))
        val = vars2["next"]
        if val is not False:
            # add next block
            val, _ = self.exe.rawToVaUnknown(offset + val)
            ret[defaultId] = val
    elif method == 19:
        valueRange = (0, 0)
        switchRange = (0, 0)
        sub1 = vars1["sub1"]
        if sub1 < 0:
            self.log("Error: unknown sub1 value in switch method: "
                     "{0}".format(method))
            exit(1)
        sub2 = 1
        sub3 = 1
        if debug is True:
            printSwitchValue(self, "sub1", sub1)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz1"])
        switchRange = updateSwitchRange(self, (sub1, sub1), limits)
        if debug is True:
            printSwitchValue(self, "jz1", vars1["jz1"])
            printSwitchRange(self, "jz1 range", switchRange)
        chains.append((jz, switchRange))
        jz, _ = self.exe.vaToRawUnknown(vars1["jz2"])
        switchRange = updateSwitchRange(self,
                                        (sub1 + sub2, sub1 + sub2),
                                        limits)
        if debug is True:
            printSwitchValue(self, "jz2", vars1["jz2"])
            printSwitchRange(self, "jz2 range", switchRange)
        chains.append((jz, switchRange))
        jnz, _ = self.exe.vaToRawUnknown(vars1["jnz"])
        if debug is True:
            printSwitchValue(self, "jnz", vars1["jnz"])
        # cant exclude two value
        chains.append((jnz, limits))
        val = vars2["next"]
        val, _ = self.exe.rawToVaUnknown(offset + vars2["next"])
        ret[sub1 + sub2 + sub3] = val
    elif method == 20:
        valueRange = (0, 0)
        switchRange = (0, 0)
        sub1 = vars1["sub1"]
        if sub1 < 0:
            self.log("Error: unknown sub1 value in switch method: "
                     "{0}".format(method))
            exit(1)
        if debug is True:
            printSwitchValue(self, "sub1", sub1)
        sub2 = 1
        jz, _ = self.exe.vaToRawUnknown(vars1["jz1"])
        switchRange = updateSwitchRange(self, (sub1, sub1), limits)
        if debug is True:
            printSwitchValue(self, "jz1", vars1["jz1"])
            printSwitchRange(self, "jz1 range", switchRange)
        chains.append((jz, switchRange))
        jz, _ = self.exe.vaToRawUnknown(vars1["jz2"])
        switchRange = updateSwitchRange(self,
                                        (sub1 + sub2, sub1 + sub2),
                                        limits)
        if debug is True:
            printSwitchValue(self, "jz2", vars1["jz2"])
            printSwitchRange(self, "jz2 range", switchRange)
        chains.append((jz, switchRange))
        val = vars2["next"]
        if val is not False:
            # add next block
            val, _ = self.exe.rawToVaUnknown(offset + val)
            ret[defaultId] = val
    elif method == 21:
        valueRange = (0, 0)
        switchRange = (0, 0)
        compare1 = vars1["cmp1"]
        compare2 = vars1["cmp2"]
        compare3 = vars1["cmp3"]
        if debug is True:
            printSwitchValue(self, "compare1", compare1)
            printSwitchValue(self, "compare2", compare2)
            printSwitchValue(self, "compare3", compare3)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz1"])
        switchRange = updateSwitchRange(self, (compare1, compare1), limits)
        if debug is True:
            printSwitchValue(self, "jz1", vars1["jz1"])
            printSwitchRange(self, "jz1 range", switchRange)
        chains.append((jz, switchRange))
        jz, _ = self.exe.vaToRawUnknown(vars1["jz2"])
        switchRange = updateSwitchRange(self, (compare2, compare2), limits)
        if debug is True:
            printSwitchValue(self, "jz2", vars1["jz2"])
            printSwitchRange(self, "jz2 range", switchRange)
        chains.append((jz, switchRange))

        jnz, _ = self.exe.vaToRawUnknown(vars1["jnz"])
        if debug is True:
            printSwitchValue(self, "jnz", vars1["jnz"])
        # cant exclude one value
        chains.append((jnz, limits))
        val, _ = self.exe.rawToVaUnknown(offset + vars2["next"])
        ret[compare3] = val
    elif method == 22:
        valueRange = (0, 0)
        switchRange = (0, 0)
        compare1 = vars1["cmp1"]
        compare2 = vars1["cmp2"]
        compare3 = vars1["cmp3"]
        if debug is True:
            printSwitchValue(self, "compare1", compare1)
            printSwitchValue(self, "compare2", compare3)
            printSwitchValue(self, "compare3", compare3)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz1"])
        switchRange = updateSwitchRange(self, (compare1, compare1), limits)
        if debug is True:
            printSwitchValue(self, "jz1", vars1["jz1"])
            printSwitchRange(self, "jz1 range", switchRange)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz2"])
        chains.append((jz, switchRange))
        switchRange = updateSwitchRange(self, (compare2, compare2), limits)
        if debug is True:
            printSwitchValue(self, "jz2", vars1["jz2"])
            printSwitchRange(self, "jz2 range", switchRange)
        chains.append((jz, switchRange))
        jnz, _ = self.exe.vaToRawUnknown(vars1["jnz"])
        if debug is True:
            printSwitchValue(self, "jnz", vars1["jnz"])
        # cant exclude one value
        chains.append((jnz, limits))
        val, _ = self.exe.rawToVaUnknown(offset + vars2["next"])
        ret[compare3] = val
    elif method == 23:
        valueRange = (0, 0)
        switchRange = (0, 0)
        sub1 = vars1["sub1"]
        if sub1 < 0:
            self.log("Error: unknown sub1 value in switch method: "
                     "{0}".format(method))
            exit(1)
        sub2 = vars1["sub2"]
        if sub1 < 0:
            self.log("Error: unknown sub1 value in switch method: "
                     "{0}".format(method))
            exit(1)
        sub3 = 1
        if debug is True:
            printSwitchValue(self, "sub1", sub1)
        jz, _ = self.exe.vaToRawUnknown(vars1["jz1"])
        switchRange = updateSwitchRange(self, (sub1, sub1), limits)
        if debug is True:
            printSwitchValue(self, "jz1", vars1["jz1"])
            printSwitchRange(self, "jz1 range", switchRange)
        chains.append((jz, switchRange))
        jz, _ = self.exe.vaToRawUnknown(vars1["jz2"])
        switchRange = updateSwitchRange(self,
                                        (sub1 + sub2, sub1 + sub2),
                                        limits)
        if debug is True:
            printSwitchValue(self, "jz2", vars1["jz2"])
            printSwitchRange(self, "jz2 range", switchRange)
        chains.append((jz, switchRange))
        jnz, _ = self.exe.vaToRawUnknown(vars1["jnz"])
        if debug is True:
            printSwitchValue(self, "jnz", vars1["jnz"])
        # cant exclude two value
        chains.append((jnz, limits))
        val = vars2["next"]
        val, _ = self.exe.rawToVaUnknown(offset + vars2["next"])
        ret[sub1 + sub2 + sub3] = val
    elif method == "ignore":
        valueRange = (0, 0)
        switchRange = (0, 0)
        if debug is True:
            self.log("Found switch with type ignore")
    else:
        self.log("Error: unknown switch method: {0}".format(method))
        exit(1)
    data = (offset, (valueRange, switchRange), ret, (vars1, vars2), chains)
    if debug is True:
        print("ret switch:")
        for addr in sorted(ret):
            if addr == defaultId:
                print("{0}: {1}".format("default", hex(ret[addr])))
            else:
                print("{0}: {1}".format(hex(addr), hex(ret[addr])))
        print("ret chain")
        for chain in sorted(chains):
            val, _ = self.exe.rawToVaUnknown(chain[0])
            print("{0}".format(hex(val)))
    checkSwitchChains(self, offset, chains, method)
    # start offset, (ranges,), switch values, vars1, vars2, continue data)
    return data


def matchSwitchBlock(self, blocks, offset):
    ret = False
    for block in blocks:
        code = block[0]
        vars2 = block[2]
        fixedOffset = vars2["fixedOffset"]
        offset1 = offset - fixedOffset
        if self.exe.matchWildcardOffset(code,
                                        b"\xAB",
                                        offset1) is True:
            if ret is not False:
                self.log("Error: found more than one switch matching blocks")
                self.log("{0}:\n{1}\n{2}".format(ret[1],
                                                 ret[2],
                                                 ret[0].encode("hex")))
                self.log("{0}:\n{1}\n{2}".format(block[1],
                                                 block[2],
                                                 block[0].encode("hex")))
                exit(1)
            ret = block
    if ret is not False:
        return (offset1, ret)
    return ret


def decodeSwitchBlock(self, blocks, offset):
    if debug is True:
        addr, _ = self.exe.rawToVaUnknown(offset)
        print("---------------------------------")
        print("decodeSwitchBlock: 0x{0:X}".format(addr))
    isCorrect = True
    data = matchSwitchBlock(self, blocks, offset)
    if data is False:
        addr, _ = self.exe.rawToVaUnknown(offset)
        self.log("Error: found wrong switch chain block at {0:X}".format(addr))
        exit(1)
    block = data
    arr = dict()
    offset = block[0]
    vars2 = block[1][2]
    if "ignore" in vars2 and vars2["ignore"] is True:
        if debug is True:
            print("ignored block")
        isCorrect = False
    blockVars = block[1][1]
    for name in blockVars:
        val = readSwitchVar(self, offset, blockVars[name])
        arr[name] = val
    blockVars = block[1][2]
    arr2 = dict()
    for name in blockVars:
        arr2[name] = blockVars[name]
    return (offset, arr, arr2, isCorrect)


def searchSwitches(self, blocks, blocks2, offset, offset2):
    arr = set()
    data = searchRelativeBlock(self, blocks, offset, offset2)
    limits = (-float("inf"), float("inf"))
    parsed = parseSwitch(self, data, limits, False)
    if parsed is False:
        return False
    startOffset = parsed[0]
    arr.add(startOffset)
    allSwitches = []
    allSwitches.append(parsed)
    chains = parsed[4]
    if len(chains) == 0:
        return (parsed[0], parsed[2], allSwitches, arr)
#    ranges = parsed[1]
    values = parsed[2]
#    vars = parsed[3]
    while len(chains) > 0:
        oldchains = chains
        chains = []
        for chain in oldchains:
            addr, _ = self.exe.rawToVaUnknown(chain[0])
            data = decodeSwitchBlock(self, blocks2, chain[0])
            if data is False:
                continue
            parsed = parseSwitch(self, data, chain[1], True)
            arr.add(parsed[0])
            chains = chains + parsed[4]
            values.update(parsed[2])
            allSwitches.append(parsed)
    # startOffset, switch values, all switches, internal addresses
    return (startOffset, values, allSwitches, arr)


def labelSwitches(self, data, labelName):
    # addr = set(id,)
    addrToId = dict()
    # addr = label
    labels = dict()
    switches = data[1]
    for f in switches:
        addr = switches[f]
        packetId = f
        packets = addrToId.get(addr, set())
        packets.add(packetId)
        addrToId[addr] = packets
    for addr in sorted(addrToId.keys()):
        packets = sorted(addrToId[addr])
        name = ""
        if defaultId in packets:
            name = "_default"
        else:
            for packetId in packets:
                name = "{0}_{1:X}".format(name, packetId)
        self.addVaCommentAddr(labelName.format(name), addr, 0)
        labels[addr] = labelName.format(name)
    for addr in sorted(labels.keys()):
        label = labels[addr]
        self.addVaLabel(label, addr, False)
#        print "addLabel: {0}, {1}".format(label, hex(addr))
    return labels


def printSwitchAddr(self, data):
    data = data[1]
    for addr in sorted(data):
        if addr == defaultId:
            print("{0}: {1}".format("default", hex(data[addr])))
        else:
            idStr = hex(addr)[2:]
            idStr = "0" * (4 - len(idStr)) + idStr
#            print("{0}: {1}".format(hex(addr), hex(data[addr])))
            print("0x{0}: {1}".format(idStr, hex(data[addr])))
