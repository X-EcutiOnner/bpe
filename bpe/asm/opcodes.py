#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bpe.asm.opfunctions import op_unset_reg, op_cmp_reg_int, op_inc_reg, \
    op_push_reg, op_pop_reg, op_push_big, op_push_small, op_add_reg_val, \
    op_or_reg_val, op_and_reg_val, op_sub_val, op_mov_ptr_reg_zero_reg, \
    op_mov_ptr_reg_byte, op_mov_ptr_reg_int, op_mov_reg_ptr_zero, \
    op_mov_reg_ptr_val, op_mov_reg_reg, op_lea_reg_zero, op_lea_reg_val, \
    op_retn, op_mov_reg_val, op_mov_ptr_reg_zero_int, op_jmp_le_byte, \
    op_mov_ptr_reg_byte_int, op_mov_ptr_reg_int_int, op_call, \
    op_mov_reg_ptr_smallval, \
    unimplimentedOpcode


# cmdSize, [bytes],         text,     instructionSize,  function,                [regs],          [params]
codes = [
    [2, b"\x33\xd2",         "xor edx, edx",         2,  op_unset_reg,            ["edx"],         []],
    [2, b"\x33\xf6",         "xor esi, esi",         2,  op_unset_reg,            ["esi"],         []],
    [2, b"\x33\xff",         "xor edi, edi",         2,  op_unset_reg,            ["edi"],         []],
    [1, b"\x3d",             "cmp eax, N",           5,  op_cmp_reg_int,          ["eax"],         [[1, 4]]],
    [1, b"\x40",             "inc eax",              1,  op_inc_reg,              ["eax"],         []],
    [1, b"\x50",             "push eax",             1,  op_push_reg,             ["eax"],         []],
    [1, b"\x51",             "push ecx",             1,  op_push_reg,             ["ecx"],         []],
    [1, b"\x52",             "push edx",             1,  op_push_reg,             ["edx"],         []],
    [1, b"\x53",             "push ebx",             1,  op_push_reg,             ["ebx"],         []],
    [1, b"\x55",             "push ebp",             1,  op_push_reg,             ["ebp"],         []],
    [1, b"\x56",             "push esp",             1,  op_push_reg,             ["esp"],         []],
    [1, b"\x57",             "push edi",             1,  op_push_reg,             ["edi"],         []],
    [1, b"\x59",             "pop ecx",              1,  op_pop_reg,              ["ecx"],         []],
    [1, b"\x5b",             "pop ebx",              1,  op_pop_reg,              ["ebx"],         []],
    [1, b"\x5d",             "pop ebp",              1,  op_pop_reg,              ["ebp"],         []],
    [1, b"\x5e",             "pop esi",              1,  op_pop_reg,              ["esi"],         []],
    [1, b"\x5f",             "pop edi",              1,  op_pop_reg,              ["edi"],         []],
    [4, b"\x66\x0f\xd6\x45", "movq [ebp + N], xmm0", 5,  op_mov_ptr_reg_byte,     ["ebp", "xmm0"], [[4, 1]]],
    [1, b"\x68",             "push NNN",             5,  op_push_big,             [],              [[1, 4]]],
    [1, b"\x6a",             "push N",               2,  op_push_small,           [],              [[1, 1]]],
    [1, b"\x7e",             "jle A",                2,  op_jmp_le_byte,          [],              [[1, 1]]],
    [2, b"\x83\xc0",         "add eax, N",           3,  op_add_reg_val,          ["eax"],         [[2, 1]]],
    [2, b"\x83\xc4",         "add esp, N",           3,  op_add_reg_val,          ["esp"],         [[2, 1]]],
    [2, b"\x83\xe4",         "and esp, NNNNNN",      3,  op_and_reg_val,          ["esp"],         [[2, 1]]],
    [2, b"\x83\xc8",         "or eax, NNNNNN",       3,  op_or_reg_val,           ["eax"],         [[2, 1]]],
    [2, b"\x83\xcb",         "or ebx, NNNNNN",       3,  op_or_reg_val,           ["ebx"],         [[2, 1]]],
    [2, b"\x83\xcf",         "or edi, NNNNNN",       3,  op_or_reg_val,           ["edi"],         [[2, 1]]],
    [2, b"\x81\xec",         "sub esp, NNN",         6,  op_sub_val,              ["esp"],         [[2, 4]]],
    [2, b"\x83\xec",         "sub esp, N",           3,  op_sub_val,              ["esp"],         [[2, 1]]],
    [2, b"\x89\x18",         "mov [eax], ebx",       2,  op_mov_ptr_reg_zero_reg, ["eax", "ebx"],  []],
    [2, b"\x89\x38",         "mov [eax], edi",       2,  op_mov_ptr_reg_zero_reg, ["eax", "edi"],  []],
    [3, b"\x89\x44\x24",     "mov [esp + N], eax",   4,  op_mov_ptr_reg_byte,     ["esp", "eax"],  [[3, 1]]],
    [2, b"\x89\x45",         "mov [ebp + N], eax",   3,  op_mov_ptr_reg_byte,     ["ebp", "eax"],  [[2, 1]]],
    [2, b"\x89\x48",         "mov [eax + N], ecx",   3,  op_mov_ptr_reg_byte,     ["eax", "ecx"],  [[2, 1]]],
    [2, b"\x89\x4d",         "mov [ebp + N], ecx",   3,  op_mov_ptr_reg_byte,     ["ebp", "ecx"],  [[2, 1]]],
    [3, b"\x89\x4c\x24",     "mov [esp + N], ecx",   4,  op_mov_ptr_reg_byte,     ["esp", "ecx"],  [[3, 1]]],
    [3, b"\x89\x54\x24",     "mov [esp + N], edx",   4,  op_mov_ptr_reg_byte,     ["esp", "edx"],  [[3, 1]]],
    [2, b"\x89\x55",         "mov [ebp + N], edx",   3,  op_mov_ptr_reg_byte,     ["ebp", "edx"],  [[2, 1]]],
    [2, b"\x89\x58",         "mov [eax + N], ebx",   3,  op_mov_ptr_reg_byte,     ["eax", "ebx"],  [[2, 1]]],
    [2, b"\x89\x5d",         "mov [ebp + N], ebx",   3,  op_mov_ptr_reg_byte,     ["ebp", "ebx"],  [[2, 1]]],
    [2, b"\x89\x75",         "mov [ebp + N], esi",   3,  op_mov_ptr_reg_byte,     ["ebp", "esi"],  [[2, 1]]],
    [2, b"\x89\x78",         "mov [eax + N], edi",   3,  op_mov_ptr_reg_byte,     ["eax", "edi"],  [[2, 1]]],
    [3, b"\x89\x7c\x24",     "mov [esp + N], edi",   4,  op_mov_ptr_reg_byte,     ["esp", "edi"],  [[3, 1]]],
    [2, b"\x89\x7d",         "mov [ebp + N], edi",   3,  op_mov_ptr_reg_byte,     ["ebp", "edi"],  [[2, 1]]],
    [2, b"\x89\x85",         "mov [ebp + N], eax",   6,  op_mov_ptr_reg_int,      ["ebp", "eax"],  [[2, 4]]],
    [2, b"\x89\x8D",         "mov [ebp + N], ecx",   6,  op_mov_ptr_reg_int,      ["ebp", "ecx"],  [[2, 4]]],
    [2, b"\x89\x95",         "mov [ebp + N], edx",   6,  op_mov_ptr_reg_int,      ["ebp", "edx"],  [[2, 4]]],
    [2, b"\x89\x9d",         "mov [ebp + N], ebx",   6,  op_mov_ptr_reg_int,      ["ebp", "ebx"],  [[2, 4]]],
    [2, b"\x89\xbd",         "mov [ebp + N], edi",   6,  op_mov_ptr_reg_int,      ["ebp", "edi"],  [[2, 4]]],
    [2, b"\x8b\x00",         "mov eax, [eax]",       2,  op_mov_reg_ptr_zero,     ["eax", "eax"],  []],
    [2, b"\x8b\x02",         "mov eax, [edx]",       2,  op_mov_reg_ptr_zero,     ["eax", "edx"],  []],
    [2, b"\x8b\x10",         "mov edx, [eax]",       2,  op_mov_reg_ptr_zero,     ["edx", "eax"],  []],
    [2, b"\x8b\x40",         "mov eax, [eax + N]",   3,  op_mov_reg_ptr_val,      ["eax", "eax"],  [[2, 1]]],
    [2, b"\x8b\x45",         "mov eax, [ebp + N]",   3,  op_mov_reg_ptr_val,      ["eax", "ebp"],  [[2, 1]]],
    [2, b"\x8b\x48",         "mov ecx, [eax + N]",   3,  op_mov_reg_ptr_val,      ["ecx", "eax"],  [[2, 1]]],
    [2, b"\x8b\x4a",         "mov ecx, [edx + N]",   3,  op_mov_reg_ptr_val,      ["ecx", "edx"],  [[2, 1]]],
    [2, b"\x8b\x50",         "mov edx, [eax + N]",   3,  op_mov_reg_ptr_val,      ["edx", "eax"],  [[2, 1]]],
    [2, b"\x8b\x52",         "mov edx, [edx + N]",   3,  op_mov_reg_ptr_val,      ["edx", "edx"],  [[2, 1]]],
    [2, b"\x8b\x85",         "mov eax, [ebp + N]",   6,  op_mov_reg_ptr_val,      ["eax", "ebp"],  [[2, 4]]],
    [2, b"\x8b\x8D",         "mov ecx, [ebp + N]",   6,  op_mov_reg_ptr_val,      ["ecx", "ebp"],  [[2, 4]]],
    [2, b"\x8b\x95",         "mov edx, [ebp + N]",   6,  op_mov_reg_ptr_val,      ["edx", "ebp"],  [[2, 4]]],
    [2, b"\x8b\xc8",         "mov ecx, eax",         2,  op_mov_reg_reg,          ["ecx", "eax"],  []],
    [2, b"\x8b\xce",         "mov ecx, esi",         2,  op_mov_reg_reg,          ["ecx", "esi"],  []],
    [2, b"\x8b\xd0",         "mov edx, eax",         2,  op_mov_reg_reg,          ["edx", "eax"],  []],
    [2, b"\x8b\xe5",         "mov esp, ebp",         2,  op_mov_reg_reg,          ["esp", "ebp"],  []],
    [2, b"\x8b\xec",         "mov ebp, esp",         2,  op_mov_reg_reg,          ["ebp", "esp"],  []],
    [2, b"\x8b\xf1",         "mov esi, ecx",         2,  op_mov_reg_reg,          ["esi", "ecx"],  []],
    [3, b"\x8d\x04\x24",     "lea eax, [esp]",       3,  op_lea_reg_zero,         ["eax", "esp"],  []],
    [3, b"\x8d\x0c\x24",     "lea ecx, [esp]",       3,  op_lea_reg_zero,         ["ecx", "esp"],  []],
    [3, b"\x8d\x14\x24",     "lea edx, [esp]",       3,  op_lea_reg_zero,         ["edx", "esp"],  []],
    [3, b"\x8d\x44\x24",     "lea eax, [esp + N]",   4,  op_lea_reg_val,          ["eax", "esp"],  [[3, 1]]],
    [2, b"\x8d\x45",         "lea eax, [ebp + N]",   3,  op_lea_reg_val,          ["eax", "ebp"],  [[2, 1]]],
    [3, b"\x8d\x4c\x24",     "lea ecx, [esp + N]",   4,  op_lea_reg_val,          ["ecx", "esp"],  [[3, 1]]],
    [2, b"\x8d\x4d",         "lea ecx, [ebp + N]",   3,  op_lea_reg_val,          ["ecx", "ebp"],  [[2, 1]]],
    [3, b"\x8d\x54\x24",     "lea edx, [esp + N]",   4,  op_lea_reg_val,          ["edx", "esp"],  [[3, 1]]],
    [2, b"\x8d\x55",         "lea edx, [ebp + N]",   3,  op_lea_reg_val,          ["edx", "ebp"],  [[2, 1]]],
    [2, b"\x8d\x71",         "lea esi, [ecx + N]",   3,  op_lea_reg_val,          ["esi", "ecx"],  [[2, 1]]],
    [2, b"\x8d\x85",         "lea eax, [ebp + N]",   6,  op_lea_reg_val,          ["eax", "ebp"],  [[2, 4]]],
    [2, b"\x8d\x8d",         "lea ecx, [ebp + N]",   6,  op_lea_reg_val,          ["ecx", "ebp"],  [[2, 4]]],
    [2, b"\x8d\x95",         "lea edx, [ebp + N]",   6,  op_lea_reg_val,          ["edx", "ebp"],  [[2, 4]]],
    [1, b"\xb8",             "mov eax, N",           5,  op_mov_reg_val,          ["eax"],         [[1, 4]]],
    [1, b"\xb9",             "mov ecx, N",           5,  op_mov_reg_val,          ["ecx"],         [[1, 4]]],
    [1, b"\xba",             "mov edx, N",           5,  op_mov_reg_val,          ["edx"],         [[1, 4]]],
    [1, b"\xbb",             "mov ebx, N",           5,  op_mov_reg_val,          ["ebx"],         [[1, 4]]],
    [1, b"\xbf",             "mov edi, N",           5,  op_mov_reg_val,          ["edi"],         [[1, 4]]],
    [1, b"\xc3",             "retn",                 1,  op_retn,                 [],              []],
    [1, b"\xc2",             "retn N",               3,  unimplimentedOpcode,     [],              []],
    [1, b"\xbe",             "mov esi, N",           5,  op_mov_reg_val,          ["esi"],         [[1, 4]]],
    [2, b"\xc7\x00",         "mov [eax], M",         6,  op_mov_ptr_reg_zero_int, ["eax"],         [[2, 4]]],
    [3, b"\xc7\x04\x24",     "mov [esp], M",         7,  op_mov_ptr_reg_zero_int, ["esp"],         [[3, 4]]],
    [2, b"\xc7\x40",         "mov [eax + N], M",     7,  op_mov_ptr_reg_byte_int, ["eax"],         [[2, 1], [3, 4]]],
    [3, b"\xc7\x44\x24",     "mov [esp + N], M",     8,  op_mov_ptr_reg_byte_int, ["esp"],         [[3, 1], [4, 4]]],
    [2, b"\xc7\x45",         "mov [ebp + N], M",     7,  op_mov_ptr_reg_byte_int, ["ebp"],         [[2, 1], [3, 4]]],
    [2, b"\xc7\x85",         "mov [ebp + NN], M",    10, op_mov_ptr_reg_int_int,  ["ebp"],         [[2, 4], [6, 4]]],
    [1, b"\xe8",             "call offset",          5,  op_call,                 [],              [[1, 4]]],
    [4, b"\xf3\x0f\x7e\x45", "movq xmm0, [ebp + N]", 5,  op_mov_reg_ptr_smallval, ["xmm0", "ebp"], [[4, 1]]],
]
