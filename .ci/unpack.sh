#!/usr/bin/env bash

cd ./.ci/sample || exit 1
rm output.7z || true
openssl enc -aes-256-cbc -d -in output.enc -out output.7z -k $APSW || exit 1
7z x output.7z || exit 1
cd ../.. || exit 1

./.ci/prepare.sh
