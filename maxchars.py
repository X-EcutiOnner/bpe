#! /usr/bin/env -S python2 -OO
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

from bpe.extractor import Extractor
from bpe.maxcharsextractor import MaxCharsExtractor


def processClientsList():
    files = sorted(os.listdir("clients"))
    scripts = []
    for file1 in files:
        if file1[0] == "." or os.path.isdir("clients/" + file1):
            continue
        extractor = Extractor()
        extractor.init(file1, "bpe_maxchars.txt")
        extractor.getMaxChars()
        extractor.close()
        scripts.append(extractor)
    maxChars = MaxCharsExtractor()
    maxChars.getScript(scripts)


if len(sys.argv) > 1:
    extractor = Extractor()
    extractor.init(sys.argv[1], "bpe_maxchars.txt")
    extractor.getMaxChars()
    maxChars = MaxCharsExtractor()
    maxChars.getScript([extractor])
else:
    processClientsList()
